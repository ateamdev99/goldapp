﻿namespace Presentation
{
    partial class ExpenseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txtExpenseAmount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtExpenseDetail = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.BtnAddExpense = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.22152F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.44937F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.329114F));
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelX3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtExpenseAmount, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtExpenseDetail, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.BtnAddExpense, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.80783F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.338078F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.48755F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.68532F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.63636F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(633, 304);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.labelX1, 3);
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(3, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(627, 91);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "ADD EXPENSES متفرق ";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX2.Location = new System.Drawing.Point(3, 115);
            this.labelX2.Name = "labelX2";
            this.tableLayoutPanel1.SetRowSpan(this.labelX2, 2);
            this.labelX2.Size = new System.Drawing.Size(185, 87);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "Expense Details";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX3.Location = new System.Drawing.Point(3, 208);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(185, 36);
            this.labelX3.TabIndex = 126;
            this.labelX3.Text = "Expense Amount";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtExpenseAmount
            // 
            this.txtExpenseAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExpenseAmount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtExpenseAmount.Border.Class = "TextBoxBorder";
            this.txtExpenseAmount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpenseAmount.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpenseAmount.ForeColor = System.Drawing.Color.Blue;
            this.txtExpenseAmount.Location = new System.Drawing.Point(195, 207);
            this.txtExpenseAmount.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtExpenseAmount.Name = "txtExpenseAmount";
            this.txtExpenseAmount.Size = new System.Drawing.Size(393, 37);
            this.txtExpenseAmount.TabIndex = 17;
            this.txtExpenseAmount.WatermarkText = "0";
            this.txtExpenseAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExpenseAmount_KeyPress);
            // 
            // txtExpenseDetail
            // 
            this.txtExpenseDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtExpenseDetail.BackgroundStyle.Class = "RichTextBoxBorder";
            this.txtExpenseDetail.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtExpenseDetail.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Itme_Name", true));
            this.txtExpenseDetail.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpenseDetail.Location = new System.Drawing.Point(194, 115);
            this.txtExpenseDetail.Name = "txtExpenseDetail";
            this.tableLayoutPanel1.SetRowSpan(this.txtExpenseDetail, 2);
            this.txtExpenseDetail.Size = new System.Drawing.Size(395, 87);
            this.txtExpenseDetail.TabIndex = 128;
            // 
            // BtnAddExpense
            // 
            this.BtnAddExpense.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnAddExpense.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnAddExpense.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.tableLayoutPanel1.SetColumnSpan(this.BtnAddExpense, 2);
            this.BtnAddExpense.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddExpense.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnAddExpense.Location = new System.Drawing.Point(349, 255);
            this.BtnAddExpense.Name = "BtnAddExpense";
            this.tableLayoutPanel1.SetRowSpan(this.BtnAddExpense, 2);
            this.BtnAddExpense.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.BtnAddExpense.Size = new System.Drawing.Size(126, 41);
            this.BtnAddExpense.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.BtnAddExpense.TabIndex = 127;
            this.BtnAddExpense.Text = "Add Expense";
            this.BtnAddExpense.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.BtnAddExpense.Click += new System.EventHandler(this.BtnAddExpense_Click);
            // 
            // ExpenseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 304);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "ExpenseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ExpenseForm";
            this.Load += new System.EventHandler(this.ExpenseForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtExpenseAmount;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx txtExpenseDetail;
        private DevComponents.DotNetBar.ButtonX BtnAddExpense;
    }
}