﻿namespace Presentation
{
    partial class AccountDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.BtnGo = new DevComponents.DotNetBar.ButtonX();
            this.DtpFrom = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpTo = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.CmbClients = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tblClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TxtClientsId = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dataGridViewXClientsAccountDetils = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.cIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAddressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCityIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDistrictDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMobileNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOfficeNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOpBlncGoldDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOpBlncCashDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOpeningDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tMRDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activateAccountDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.temporaryAccountDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tranactionSMSDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.balanceSMSDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customSMSDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.balncSMSGoldWightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblCityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonX();
            this.BtnPrivew = new DevComponents.DotNetBar.ButtonX();
            this.BtnClose = new DevComponents.DotNetBar.ButtonX();
            this.BtnSendBlncSms = new DevComponents.DotNetBar.ButtonX();
            this.progressBarX1 = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.tblCityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewXClientsAccountDetils)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCityBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.06178F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.4819F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.120035F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.325684F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.207414F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.05654F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.303887F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.labelX4, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX5, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.BtnGo, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.DtpFrom, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.DtpTo, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelX3, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.CmbClients, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.TxtClientsId, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1232, 121);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.White;
            this.labelX4.Location = new System.Drawing.Point(696, 15);
            this.labelX4.Name = "labelX4";
            this.tableLayoutPanel1.SetRowSpan(this.labelX4, 2);
            this.labelX4.Size = new System.Drawing.Size(83, 90);
            this.labelX4.TabIndex = 3;
            this.labelX4.Text = "Detailes :";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.White;
            this.labelX5.Location = new System.Drawing.Point(785, 63);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(57, 42);
            this.labelX5.TabIndex = 4;
            this.labelX5.Text = "To :";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // BtnGo
            // 
            this.BtnGo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnGo.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnGo.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGo.Location = new System.Drawing.Point(1130, 14);
            this.BtnGo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnGo.Name = "BtnGo";
            this.tableLayoutPanel1.SetRowSpan(this.BtnGo, 2);
            this.BtnGo.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(3);
            this.BtnGo.Size = new System.Drawing.Size(99, 92);
            this.BtnGo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnGo.TabIndex = 1;
            this.BtnGo.Text = "GO";
            // 
            // DtpFrom
            // 
            this.DtpFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.DtpFrom.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFrom.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFrom.ButtonDropDown.Visible = true;
            this.DtpFrom.CustomFormat = "dd/MM/yyyy";
            this.DtpFrom.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpFrom.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFrom.IsPopupCalendarOpen = false;
            this.DtpFrom.Location = new System.Drawing.Point(848, 15);
            // 
            // 
            // 
            this.DtpFrom.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFrom.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFrom.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DtpFrom.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFrom.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFrom.MonthCalendar.DisplayMonth = new System.DateTime(2021, 1, 1, 0, 0, 0, 0);
            this.DtpFrom.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFrom.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFrom.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFrom.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFrom.MonthCalendar.TodayButtonVisible = true;
            this.DtpFrom.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFrom.Name = "DtpFrom";
            this.DtpFrom.Size = new System.Drawing.Size(276, 32);
            this.DtpFrom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFrom.TabIndex = 4;
            // 
            // DtpTo
            // 
            this.DtpTo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.DtpTo.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpTo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpTo.ButtonDropDown.Visible = true;
            this.DtpTo.CustomFormat = "dd/MM/yyyy";
            this.DtpTo.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpTo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpTo.IsPopupCalendarOpen = false;
            this.DtpTo.Location = new System.Drawing.Point(848, 63);
            // 
            // 
            // 
            this.DtpTo.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpTo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpTo.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DtpTo.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpTo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpTo.MonthCalendar.DisplayMonth = new System.DateTime(2021, 1, 1, 0, 0, 0, 0);
            this.DtpTo.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpTo.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpTo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpTo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpTo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpTo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpTo.MonthCalendar.TodayButtonVisible = true;
            this.DtpTo.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpTo.Name = "DtpTo";
            this.DtpTo.Size = new System.Drawing.Size(276, 32);
            this.DtpTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpTo.TabIndex = 5;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.White;
            this.labelX3.Location = new System.Drawing.Point(785, 15);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(57, 42);
            this.labelX3.TabIndex = 6;
            this.labelX3.Text = "From :";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(3, 15);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(117, 42);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Client No :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(3, 63);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(117, 42);
            this.labelX2.TabIndex = 3;
            this.labelX2.Text = "ClientName :";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // CmbClients
            // 
            this.CmbClients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CmbClients.DataSource = this.tblClientBindingSource;
            this.CmbClients.DisplayMember = "C_Name";
            this.CmbClients.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CmbClients.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbClients.FormattingEnabled = true;
            this.CmbClients.ItemHeight = 24;
            this.CmbClients.Location = new System.Drawing.Point(126, 63);
            this.CmbClients.Name = "CmbClients";
            this.CmbClients.Size = new System.Drawing.Size(465, 30);
            this.CmbClients.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CmbClients.TabIndex = 7;
            this.CmbClients.ValueMember = "C_Id";
            this.CmbClients.WatermarkText = "Select Client Name";
            this.CmbClients.SelectedIndexChanged += new System.EventHandler(this.comboBoxEx1_SelectedIndexChanged);
            // 
            // tblClientBindingSource
            // 
            this.tblClientBindingSource.DataSource = typeof(Data.tblClient);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Presentation.Properties.Resources.actorinfo;
            this.pictureBox1.Location = new System.Drawing.Point(597, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox1, 4);
            this.pictureBox1.Size = new System.Drawing.Size(93, 115);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // TxtClientsId
            // 
            this.TxtClientsId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.TxtClientsId.Border.Class = "TextBoxBorder";
            this.TxtClientsId.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtClientsId.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblClientBindingSource, "C_Id", true));
            this.TxtClientsId.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClientsId.Location = new System.Drawing.Point(126, 15);
            this.TxtClientsId.Name = "TxtClientsId";
            this.TxtClientsId.Size = new System.Drawing.Size(169, 32);
            this.TxtClientsId.TabIndex = 106;
            this.TxtClientsId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtClientsId.WatermarkText = "00";
            // 
            // dataGridViewXClientsAccountDetils
            // 
            this.dataGridViewXClientsAccountDetils.AllowUserToAddRows = false;
            this.dataGridViewXClientsAccountDetils.AllowUserToDeleteRows = false;
            this.dataGridViewXClientsAccountDetils.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewXClientsAccountDetils.AutoGenerateColumns = false;
            this.dataGridViewXClientsAccountDetils.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewXClientsAccountDetils.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cIdDataGridViewTextBoxColumn,
            this.cNameDataGridViewTextBoxColumn,
            this.cAddressDataGridViewTextBoxColumn,
            this.cCityIdDataGridViewTextBoxColumn,
            this.cDistrictDataGridViewTextBoxColumn,
            this.cMobileNoDataGridViewTextBoxColumn,
            this.cOfficeNoDataGridViewTextBoxColumn,
            this.cOpBlncGoldDataGridViewTextBoxColumn,
            this.cOpBlncCashDataGridViewTextBoxColumn,
            this.cOpeningDateDataGridViewTextBoxColumn,
            this.tMRDataGridViewTextBoxColumn,
            this.activateAccountDataGridViewCheckBoxColumn,
            this.temporaryAccountDataGridViewCheckBoxColumn,
            this.tranactionSMSDataGridViewCheckBoxColumn,
            this.balanceSMSDataGridViewCheckBoxColumn,
            this.customSMSDataGridViewCheckBoxColumn,
            this.balncSMSGoldWightDataGridViewTextBoxColumn,
            this.tblCityDataGridViewTextBoxColumn});
            this.dataGridViewXClientsAccountDetils.DataSource = this.tblClientBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewXClientsAccountDetils.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewXClientsAccountDetils.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewXClientsAccountDetils.Location = new System.Drawing.Point(0, 121);
            this.dataGridViewXClientsAccountDetils.Name = "dataGridViewXClientsAccountDetils";
            this.dataGridViewXClientsAccountDetils.ReadOnly = true;
            this.dataGridViewXClientsAccountDetils.RowTemplate.Height = 24;
            this.dataGridViewXClientsAccountDetils.Size = new System.Drawing.Size(1232, 500);
            this.dataGridViewXClientsAccountDetils.TabIndex = 2;
            // 
            // cIdDataGridViewTextBoxColumn
            // 
            this.cIdDataGridViewTextBoxColumn.DataPropertyName = "C_Id";
            this.cIdDataGridViewTextBoxColumn.HeaderText = "C_Id";
            this.cIdDataGridViewTextBoxColumn.Name = "cIdDataGridViewTextBoxColumn";
            this.cIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cNameDataGridViewTextBoxColumn
            // 
            this.cNameDataGridViewTextBoxColumn.DataPropertyName = "C_Name";
            this.cNameDataGridViewTextBoxColumn.HeaderText = "C_Name";
            this.cNameDataGridViewTextBoxColumn.Name = "cNameDataGridViewTextBoxColumn";
            this.cNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cAddressDataGridViewTextBoxColumn
            // 
            this.cAddressDataGridViewTextBoxColumn.DataPropertyName = "C_Address";
            this.cAddressDataGridViewTextBoxColumn.HeaderText = "C_Address";
            this.cAddressDataGridViewTextBoxColumn.Name = "cAddressDataGridViewTextBoxColumn";
            this.cAddressDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cCityIdDataGridViewTextBoxColumn
            // 
            this.cCityIdDataGridViewTextBoxColumn.DataPropertyName = "C_CityId";
            this.cCityIdDataGridViewTextBoxColumn.HeaderText = "C_CityId";
            this.cCityIdDataGridViewTextBoxColumn.Name = "cCityIdDataGridViewTextBoxColumn";
            this.cCityIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cDistrictDataGridViewTextBoxColumn
            // 
            this.cDistrictDataGridViewTextBoxColumn.DataPropertyName = "C_District";
            this.cDistrictDataGridViewTextBoxColumn.HeaderText = "C_District";
            this.cDistrictDataGridViewTextBoxColumn.Name = "cDistrictDataGridViewTextBoxColumn";
            this.cDistrictDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cMobileNoDataGridViewTextBoxColumn
            // 
            this.cMobileNoDataGridViewTextBoxColumn.DataPropertyName = "C_MobileNo";
            this.cMobileNoDataGridViewTextBoxColumn.HeaderText = "C_MobileNo";
            this.cMobileNoDataGridViewTextBoxColumn.Name = "cMobileNoDataGridViewTextBoxColumn";
            this.cMobileNoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cOfficeNoDataGridViewTextBoxColumn
            // 
            this.cOfficeNoDataGridViewTextBoxColumn.DataPropertyName = "C_OfficeNo";
            this.cOfficeNoDataGridViewTextBoxColumn.HeaderText = "C_OfficeNo";
            this.cOfficeNoDataGridViewTextBoxColumn.Name = "cOfficeNoDataGridViewTextBoxColumn";
            this.cOfficeNoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cOpBlncGoldDataGridViewTextBoxColumn
            // 
            this.cOpBlncGoldDataGridViewTextBoxColumn.DataPropertyName = "C_OpBlncGold";
            this.cOpBlncGoldDataGridViewTextBoxColumn.HeaderText = "C_OpBlncGold";
            this.cOpBlncGoldDataGridViewTextBoxColumn.Name = "cOpBlncGoldDataGridViewTextBoxColumn";
            this.cOpBlncGoldDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cOpBlncCashDataGridViewTextBoxColumn
            // 
            this.cOpBlncCashDataGridViewTextBoxColumn.DataPropertyName = "C_OpBlncCash";
            this.cOpBlncCashDataGridViewTextBoxColumn.HeaderText = "C_OpBlncCash";
            this.cOpBlncCashDataGridViewTextBoxColumn.Name = "cOpBlncCashDataGridViewTextBoxColumn";
            this.cOpBlncCashDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cOpeningDateDataGridViewTextBoxColumn
            // 
            this.cOpeningDateDataGridViewTextBoxColumn.DataPropertyName = "C_OpeningDate";
            this.cOpeningDateDataGridViewTextBoxColumn.HeaderText = "C_OpeningDate";
            this.cOpeningDateDataGridViewTextBoxColumn.Name = "cOpeningDateDataGridViewTextBoxColumn";
            this.cOpeningDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tMRDataGridViewTextBoxColumn
            // 
            this.tMRDataGridViewTextBoxColumn.DataPropertyName = "TMR";
            this.tMRDataGridViewTextBoxColumn.HeaderText = "TMR";
            this.tMRDataGridViewTextBoxColumn.Name = "tMRDataGridViewTextBoxColumn";
            this.tMRDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // activateAccountDataGridViewCheckBoxColumn
            // 
            this.activateAccountDataGridViewCheckBoxColumn.DataPropertyName = "ActivateAccount";
            this.activateAccountDataGridViewCheckBoxColumn.HeaderText = "ActivateAccount";
            this.activateAccountDataGridViewCheckBoxColumn.Name = "activateAccountDataGridViewCheckBoxColumn";
            this.activateAccountDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // temporaryAccountDataGridViewCheckBoxColumn
            // 
            this.temporaryAccountDataGridViewCheckBoxColumn.DataPropertyName = "TemporaryAccount";
            this.temporaryAccountDataGridViewCheckBoxColumn.HeaderText = "TemporaryAccount";
            this.temporaryAccountDataGridViewCheckBoxColumn.Name = "temporaryAccountDataGridViewCheckBoxColumn";
            this.temporaryAccountDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // tranactionSMSDataGridViewCheckBoxColumn
            // 
            this.tranactionSMSDataGridViewCheckBoxColumn.DataPropertyName = "TranactionSMS";
            this.tranactionSMSDataGridViewCheckBoxColumn.HeaderText = "TranactionSMS";
            this.tranactionSMSDataGridViewCheckBoxColumn.Name = "tranactionSMSDataGridViewCheckBoxColumn";
            this.tranactionSMSDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // balanceSMSDataGridViewCheckBoxColumn
            // 
            this.balanceSMSDataGridViewCheckBoxColumn.DataPropertyName = "BalanceSMS";
            this.balanceSMSDataGridViewCheckBoxColumn.HeaderText = "BalanceSMS";
            this.balanceSMSDataGridViewCheckBoxColumn.Name = "balanceSMSDataGridViewCheckBoxColumn";
            this.balanceSMSDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // customSMSDataGridViewCheckBoxColumn
            // 
            this.customSMSDataGridViewCheckBoxColumn.DataPropertyName = "CustomSMS";
            this.customSMSDataGridViewCheckBoxColumn.HeaderText = "CustomSMS";
            this.customSMSDataGridViewCheckBoxColumn.Name = "customSMSDataGridViewCheckBoxColumn";
            this.customSMSDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // balncSMSGoldWightDataGridViewTextBoxColumn
            // 
            this.balncSMSGoldWightDataGridViewTextBoxColumn.DataPropertyName = "BalncSMSGoldWight";
            this.balncSMSGoldWightDataGridViewTextBoxColumn.HeaderText = "BalncSMSGoldWight";
            this.balncSMSGoldWightDataGridViewTextBoxColumn.Name = "balncSMSGoldWightDataGridViewTextBoxColumn";
            this.balncSMSGoldWightDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tblCityDataGridViewTextBoxColumn
            // 
            this.tblCityDataGridViewTextBoxColumn.DataPropertyName = "C_CityId";
            this.tblCityDataGridViewTextBoxColumn.HeaderText = "tblCity";
            this.tblCityDataGridViewTextBoxColumn.Name = "tblCityDataGridViewTextBoxColumn";
            this.tblCityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.35065F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.928572F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.00649F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.17532F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.36364F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.01299F));
            this.tableLayoutPanel2.Controls.Add(this.BtnPrint, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.BtnPrivew, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.BtnClose, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.BtnSendBlncSms, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.progressBarX1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX6, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 627);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.68421F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.31579F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1232, 76);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // BtnPrint
            // 
            this.BtnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnPrint.BackColor = System.Drawing.Color.Transparent;
            this.BtnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnPrint.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnPrint.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnPrint.Location = new System.Drawing.Point(804, 3);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BtnPrint.Name = "BtnPrint";
            this.tableLayoutPanel2.SetRowSpan(this.BtnPrint, 2);
            this.BtnPrint.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnPrint.Size = new System.Drawing.Size(125, 70);
            this.BtnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPrint.TabIndex = 6;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.TextColor = System.Drawing.Color.White;
            // 
            // BtnPrivew
            // 
            this.BtnPrivew.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPrivew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnPrivew.BackColor = System.Drawing.Color.Transparent;
            this.BtnPrivew.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnPrivew.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrivew.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnPrivew.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnPrivew.Location = new System.Drawing.Point(949, 3);
            this.BtnPrivew.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BtnPrivew.Name = "BtnPrivew";
            this.tableLayoutPanel2.SetRowSpan(this.BtnPrivew, 2);
            this.BtnPrivew.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnPrivew.Size = new System.Drawing.Size(125, 70);
            this.BtnPrivew.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPrivew.TabIndex = 7;
            this.BtnPrivew.Text = "Preview";
            this.BtnPrivew.TextColor = System.Drawing.Color.White;
            // 
            // BtnClose
            // 
            this.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnClose.BackColor = System.Drawing.Color.Transparent;
            this.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnClose.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClose.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnClose.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnClose.Location = new System.Drawing.Point(1094, 3);
            this.BtnClose.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BtnClose.Name = "BtnClose";
            this.tableLayoutPanel2.SetRowSpan(this.BtnClose, 2);
            this.BtnClose.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnClose.Size = new System.Drawing.Size(125, 70);
            this.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnClose.TabIndex = 8;
            this.BtnClose.Text = "Close";
            this.BtnClose.TextColor = System.Drawing.Color.White;
            // 
            // BtnSendBlncSms
            // 
            this.BtnSendBlncSms.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSendBlncSms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnSendBlncSms.BackColor = System.Drawing.Color.Transparent;
            this.BtnSendBlncSms.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnSendBlncSms.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSendBlncSms.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnSendBlncSms.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnSendBlncSms.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnSendBlncSms.Location = new System.Drawing.Point(53, 3);
            this.BtnSendBlncSms.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BtnSendBlncSms.Name = "BtnSendBlncSms";
            this.tableLayoutPanel2.SetRowSpan(this.BtnSendBlncSms, 2);
            this.BtnSendBlncSms.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnSendBlncSms.Size = new System.Drawing.Size(193, 70);
            this.BtnSendBlncSms.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSendBlncSms.TabIndex = 5;
            this.BtnSendBlncSms.Text = "Send Balance SMS";
            this.BtnSendBlncSms.TextColor = System.Drawing.Color.White;
            // 
            // progressBarX1
            // 
            this.progressBarX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.progressBarX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel2.SetColumnSpan(this.progressBarX1, 2);
            this.progressBarX1.Location = new System.Drawing.Point(303, 3);
            this.progressBarX1.Name = "progressBarX1";
            this.progressBarX1.Size = new System.Drawing.Size(486, 31);
            this.progressBarX1.TabIndex = 9;
            this.progressBarX1.Text = "progressBarX1";
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel2.SetColumnSpan(this.labelX6, 2);
            this.labelX6.Location = new System.Drawing.Point(303, 40);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(486, 33);
            this.labelX6.TabIndex = 10;
            this.labelX6.Text = "Records";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tblCityBindingSource
            // 
            this.tblCityBindingSource.DataSource = typeof(Data.tblCity);
            // 
            // AccountDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(1232, 703);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.dataGridViewXClientsAccountDetils);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AccountDetailsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountDetailsForm";
            this.Load += new System.EventHandler(this.AccountDetailsForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewXClientsAccountDetils)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblCityBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.ButtonX BtnGo;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpTo;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CmbClients;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtClientsId;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewXClientsAccountDetils;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevComponents.DotNetBar.ButtonX BtnPrint;
        private DevComponents.DotNetBar.ButtonX BtnPrivew;
        private DevComponents.DotNetBar.ButtonX BtnClose;
        private DevComponents.DotNetBar.ButtonX BtnSendBlncSms;
        private DevComponents.DotNetBar.Controls.ProgressBarX progressBarX1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.BindingSource tblClientBindingSource;
        private System.Windows.Forms.BindingSource tblCityBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn cIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAddressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCityIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDistrictDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMobileNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOfficeNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOpBlncGoldDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOpBlncCashDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOpeningDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tMRDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn activateAccountDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn temporaryAccountDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn tranactionSMSDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn balanceSMSDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customSMSDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn balncSMSGoldWightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblCityDataGridViewTextBoxColumn;
    }
}