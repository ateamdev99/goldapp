﻿namespace Presentation
{
    partial class ClintInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevComponents.DotNetBar.ButtonX btnFind;
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.tblClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.BtnSearchClient = new DevComponents.DotNetBar.ButtonX();
            this.txtClintName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtClintAddres = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtClintOpBlncGold = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtClintOpBlncCash = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDestrict = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtClintTMR = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtClintNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmbCity = new System.Windows.Forms.ComboBox();
            this.tblCityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.itemPanel1 = new DevComponents.DotNetBar.ItemPanel();
            this.chkOrderByName = new System.Windows.Forms.CheckBox();
            this.btnPrintClntList = new System.Windows.Forms.Button();
            this.btnAddDistrict = new System.Windows.Forms.Button();
            this.btnAddCity = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnClose = new DevComponents.DotNetBar.ButtonX();
            this.btnRecycleBin = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.txtClintmobile = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.txtClintOffice = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            btnFind = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCityBindingSource)).BeginInit();
            this.itemPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnFind
            // 
            btnFind.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            btnFind.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnFind.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btnFind.Image = global::Presentation.Properties.Resources.search;
            btnFind.ImageFixedSize = new System.Drawing.Size(25, 25);
            btnFind.Location = new System.Drawing.Point(435, 4);
            btnFind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            btnFind.Name = "btnFind";
            btnFind.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            btnFind.ShowSubItems = false;
            btnFind.Size = new System.Drawing.Size(121, 51);
            btnFind.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            btnFind.TabIndex = 126;
            btnFind.Text = "FIND";
            btnFind.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX1.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderRightWidth = 2;
            this.labelX1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderTopWidth = 2;
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX1.Location = new System.Drawing.Point(2, 114);
            this.labelX1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX1.Size = new System.Drawing.Size(192, 31);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX1.TabIndex = 69;
            this.labelX1.Text = "Client No ";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX4.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderRightWidth = 2;
            this.labelX4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderTopWidth = 2;
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX4.Location = new System.Drawing.Point(2, 151);
            this.labelX4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX4.Size = new System.Drawing.Size(192, 31);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX4.TabIndex = 73;
            this.labelX4.Text = "Client Name ";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX5.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX5.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX5.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderRightWidth = 2;
            this.labelX5.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderTopWidth = 2;
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX5.Location = new System.Drawing.Point(2, 188);
            this.labelX5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX5.Size = new System.Drawing.Size(192, 31);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX5.TabIndex = 74;
            this.labelX5.Text = "Client Address ";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX6.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX6.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX6.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderRightWidth = 2;
            this.labelX6.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderTopWidth = 2;
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX6.Location = new System.Drawing.Point(394, 225);
            this.labelX6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX6.Size = new System.Drawing.Size(192, 31);
            this.labelX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX6.TabIndex = 75;
            this.labelX6.Text = "Office #";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX7.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX7.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX7.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderRightWidth = 2;
            this.labelX7.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderTopWidth = 2;
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX7.Location = new System.Drawing.Point(2, 262);
            this.labelX7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX7.Size = new System.Drawing.Size(192, 31);
            this.labelX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX7.TabIndex = 76;
            this.labelX7.Text = "City";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX3.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderRightWidth = 2;
            this.labelX3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderTopWidth = 2;
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX3.Location = new System.Drawing.Point(2, 299);
            this.labelX3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX3.Size = new System.Drawing.Size(192, 31);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX3.TabIndex = 72;
            this.labelX3.Text = "Opening Balance Gold";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX8.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX8.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX8.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderRightWidth = 2;
            this.labelX8.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderTopWidth = 2;
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX8.Location = new System.Drawing.Point(2, 336);
            this.labelX8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX8.Size = new System.Drawing.Size(192, 31);
            this.labelX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX8.TabIndex = 77;
            this.labelX8.Text = "Opening Balance Cash ";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX10.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX10.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX10.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderRightWidth = 2;
            this.labelX10.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderTopWidth = 2;
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX10.Location = new System.Drawing.Point(394, 262);
            this.labelX10.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX10.Size = new System.Drawing.Size(192, 31);
            this.labelX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX10.TabIndex = 79;
            this.labelX10.Text = "District";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX9.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX9.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX9.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderRightWidth = 2;
            this.labelX9.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderTopWidth = 2;
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX9.Location = new System.Drawing.Point(2, 225);
            this.labelX9.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX9.Size = new System.Drawing.Size(192, 31);
            this.labelX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX9.TabIndex = 78;
            this.labelX9.Text = "Mobile #";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX11
            // 
            this.labelX11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX11.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX11.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX11.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderRightWidth = 2;
            this.labelX11.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderTopWidth = 2;
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX11.Location = new System.Drawing.Point(394, 299);
            this.labelX11.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX11.Size = new System.Drawing.Size(192, 31);
            this.labelX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX11.TabIndex = 80;
            this.labelX11.Text = "T-M-R ";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tblClientBindingSource
            // 
            this.tblClientBindingSource.DataSource = typeof(Data.tblClient);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.labelX2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelX4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelX5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelX7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.labelX3, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.labelX8, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.labelX11, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.labelX10, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.BtnSearchClient, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtClintName, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtClintAddres, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtClintOpBlncGold, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtClintOpBlncCash, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtDestrict, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtClintTMR, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtClintNo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbCity, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.itemPanel1, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtClintmobile, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelX9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelX6, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtClintOffice, 3, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237468F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.237469F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.912897F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 524);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderBottomWidth = 7;
            this.labelX2.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX2.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderLeftWidth = 7;
            this.labelX2.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderRightWidth = 7;
            this.labelX2.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderTopWidth = 7;
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.labelX2, 5);
            this.labelX2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(2, 3);
            this.labelX2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.labelX2.Name = "labelX2";
            this.tableLayoutPanel1.SetRowSpan(this.labelX2, 3);
            this.labelX2.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX2.Size = new System.Drawing.Size(980, 77);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX2.TabIndex = 71;
            this.labelX2.Text = "CLIENT INFORMATION FORM";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // BtnSearchClient
            // 
            this.BtnSearchClient.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSearchClient.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSearchClient.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSearchClient.Image = global::Presentation.Properties.Resources.search;
            this.BtnSearchClient.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.BtnSearchClient.Location = new System.Drawing.Point(394, 114);
            this.BtnSearchClient.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.BtnSearchClient.Name = "BtnSearchClient";
            this.BtnSearchClient.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.BtnSearchClient.Size = new System.Drawing.Size(192, 31);
            this.BtnSearchClient.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.BtnSearchClient.TabIndex = 1;
            this.BtnSearchClient.Text = "Search Client";
            this.BtnSearchClient.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.BtnSearchClient.Click += new System.EventHandler(this.BtnSearchClient_Click);
            // 
            // txtClintName
            // 
            this.txtClintName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintName.Border.Class = "TextBoxBorder";
            this.txtClintName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.txtClintName, 4);
            this.txtClintName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblClientBindingSource, "Name", true));
            this.txtClintName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintName.Location = new System.Drawing.Point(198, 151);
            this.txtClintName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintName.Name = "txtClintName";
            this.txtClintName.Size = new System.Drawing.Size(784, 29);
            this.txtClintName.TabIndex = 2;
            this.txtClintName.WatermarkText = "Enter Client Name Here";
            // 
            // txtClintAddres
            // 
            this.txtClintAddres.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintAddres.Border.Class = "TextBoxBorder";
            this.txtClintAddres.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.txtClintAddres, 4);
            this.txtClintAddres.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblClientBindingSource, "Address", true));
            this.txtClintAddres.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintAddres.Location = new System.Drawing.Point(198, 188);
            this.txtClintAddres.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintAddres.Name = "txtClintAddres";
            this.txtClintAddres.Size = new System.Drawing.Size(784, 29);
            this.txtClintAddres.TabIndex = 3;
            this.txtClintAddres.WatermarkText = "Enter Client Address Here";
            // 
            // txtClintOpBlncGold
            // 
            this.txtClintOpBlncGold.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintOpBlncGold.Border.Class = "TextBoxBorder";
            this.txtClintOpBlncGold.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClintOpBlncGold.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintOpBlncGold.Location = new System.Drawing.Point(198, 299);
            this.txtClintOpBlncGold.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintOpBlncGold.Name = "txtClintOpBlncGold";
            this.txtClintOpBlncGold.Size = new System.Drawing.Size(192, 29);
            this.txtClintOpBlncGold.TabIndex = 8;
            this.txtClintOpBlncGold.WatermarkText = "0000";
            this.txtClintOpBlncGold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClintOpBlncCash_KeyPress);
            // 
            // txtClintOpBlncCash
            // 
            this.txtClintOpBlncCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintOpBlncCash.Border.Class = "TextBoxBorder";
            this.txtClintOpBlncCash.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClintOpBlncCash.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintOpBlncCash.Location = new System.Drawing.Point(198, 336);
            this.txtClintOpBlncCash.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintOpBlncCash.Name = "txtClintOpBlncCash";
            this.txtClintOpBlncCash.Size = new System.Drawing.Size(192, 29);
            this.txtClintOpBlncCash.TabIndex = 10;
            this.txtClintOpBlncCash.WatermarkText = "0000";
            this.txtClintOpBlncCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClintOpBlncCash_KeyPress);
            // 
            // txtDestrict
            // 
            this.txtDestrict.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtDestrict.Border.Class = "TextBoxBorder";
            this.txtDestrict.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDestrict.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblClientBindingSource, "District", true));
            this.txtDestrict.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestrict.Location = new System.Drawing.Point(590, 262);
            this.txtDestrict.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDestrict.Name = "txtDestrict";
            this.txtDestrict.Size = new System.Drawing.Size(192, 29);
            this.txtDestrict.TabIndex = 7;
            this.txtDestrict.WatermarkText = "Enter District Name";
            // 
            // txtClintTMR
            // 
            this.txtClintTMR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintTMR.Border.Class = "TextBoxBorder";
            this.txtClintTMR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClintTMR.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblClientBindingSource, "TMR", true));
            this.txtClintTMR.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintTMR.Location = new System.Drawing.Point(590, 299);
            this.txtClintTMR.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintTMR.Name = "txtClintTMR";
            this.txtClintTMR.Size = new System.Drawing.Size(192, 29);
            this.txtClintTMR.TabIndex = 9;
            this.txtClintTMR.WatermarkText = "0000";
            // 
            // txtClintNo
            // 
            this.txtClintNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintNo.Border.Class = "TextBoxBorder";
            this.txtClintNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClintNo.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintNo.Location = new System.Drawing.Point(198, 114);
            this.txtClintNo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintNo.Name = "txtClintNo";
            this.txtClintNo.Size = new System.Drawing.Size(192, 29);
            this.txtClintNo.TabIndex = 0;
            this.txtClintNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtClintNo.WatermarkText = "0000";
            this.txtClintNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClintNo_KeyPress);
            // 
            // cmbCity
            // 
            this.cmbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCity.BackColor = System.Drawing.Color.White;
            this.cmbCity.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.tblClientBindingSource, "CityId", true));
            this.cmbCity.DataSource = this.tblCityBindingSource;
            this.cmbCity.DisplayMember = "City_Name";
            this.cmbCity.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCity.ForeColor = System.Drawing.Color.Black;
            this.cmbCity.FormattingEnabled = true;
            this.cmbCity.Location = new System.Drawing.Point(199, 263);
            this.cmbCity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbCity.Name = "cmbCity";
            this.cmbCity.Size = new System.Drawing.Size(190, 28);
            this.cmbCity.TabIndex = 6;
            this.cmbCity.ValueMember = "City_Id";
            // 
            // tblCityBindingSource
            // 
            this.tblCityBindingSource.DataSource = typeof(Data.tblCity);
            // 
            // itemPanel1
            // 
            this.itemPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPanel1.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.itemPanel1.BackgroundStyle.Class = "ItemPanel";
            this.itemPanel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemPanel1.ContainerControlProcessDialogKey = true;
            this.itemPanel1.Controls.Add(this.chkOrderByName);
            this.itemPanel1.Controls.Add(this.btnPrintClntList);
            this.itemPanel1.Controls.Add(this.btnAddDistrict);
            this.itemPanel1.Controls.Add(this.btnAddCity);
            this.itemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemPanel1.Location = new System.Drawing.Point(787, 226);
            this.itemPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.itemPanel1.Name = "itemPanel1";
            this.tableLayoutPanel1.SetRowSpan(this.itemPanel1, 4);
            this.itemPanel1.Size = new System.Drawing.Size(194, 140);
            this.itemPanel1.TabIndex = 123;
            this.itemPanel1.Text = "itemPanel1";
            // 
            // chkOrderByName
            // 
            this.chkOrderByName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkOrderByName.AutoSize = true;
            this.chkOrderByName.BackColor = System.Drawing.Color.Transparent;
            this.chkOrderByName.Enabled = false;
            this.chkOrderByName.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOrderByName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.chkOrderByName.Location = new System.Drawing.Point(52, 117);
            this.chkOrderByName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkOrderByName.Name = "chkOrderByName";
            this.chkOrderByName.Size = new System.Drawing.Size(105, 17);
            this.chkOrderByName.TabIndex = 3;
            this.chkOrderByName.Text = "Order By Name";
            this.chkOrderByName.UseVisualStyleBackColor = false;
            // 
            // btnPrintClntList
            // 
            this.btnPrintClntList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintClntList.Enabled = false;
            this.btnPrintClntList.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintClntList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnPrintClntList.Location = new System.Drawing.Point(7, 80);
            this.btnPrintClntList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrintClntList.Name = "btnPrintClntList";
            this.btnPrintClntList.Size = new System.Drawing.Size(183, 33);
            this.btnPrintClntList.TabIndex = 2;
            this.btnPrintClntList.Text = "Print Client List";
            this.btnPrintClntList.UseVisualStyleBackColor = true;
            // 
            // btnAddDistrict
            // 
            this.btnAddDistrict.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDistrict.Enabled = false;
            this.btnAddDistrict.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDistrict.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnAddDistrict.Location = new System.Drawing.Point(7, 44);
            this.btnAddDistrict.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddDistrict.Name = "btnAddDistrict";
            this.btnAddDistrict.Size = new System.Drawing.Size(183, 34);
            this.btnAddDistrict.TabIndex = 1;
            this.btnAddDistrict.Text = "Add New District";
            this.btnAddDistrict.UseVisualStyleBackColor = true;
            // 
            // btnAddCity
            // 
            this.btnAddCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddCity.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnAddCity.Location = new System.Drawing.Point(7, 9);
            this.btnAddCity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddCity.Name = "btnAddCity";
            this.btnAddCity.Size = new System.Drawing.Size(183, 33);
            this.btnAddCity.TabIndex = 0;
            this.btnAddCity.Text = "Add New City";
            this.btnAddCity.UseVisualStyleBackColor = true;
            this.btnAddCity.Click += new System.EventHandler(this.btnAddCity_Click);
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 5);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnRecycleBin);
            this.panel1.Controls.Add(btnFind);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Location = new System.Drawing.Point(3, 410);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(978, 68);
            this.panel1.TabIndex = 129;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = global::Presentation.Properties.Resources.cancel;
            this.btnCancel.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnCancel.Location = new System.Drawing.Point(308, 4);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnCancel.ShowSubItems = false;
            this.btnCancel.Size = new System.Drawing.Size(121, 51);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnCancel.TabIndex = 125;
            this.btnCancel.Text = "Refresh";
            this.btnCancel.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // btnClose
            // 
            this.btnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = global::Presentation.Properties.Resources.close;
            this.btnClose.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnClose.Location = new System.Drawing.Point(689, 4);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            this.btnClose.ShowSubItems = false;
            this.btnClose.Size = new System.Drawing.Size(125, 51);
            this.btnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnClose.TabIndex = 128;
            this.btnClose.Text = "CLOSE";
            this.btnClose.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRecycleBin
            // 
            this.btnRecycleBin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRecycleBin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRecycleBin.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecycleBin.Image = global::Presentation.Properties.Resources.recycling_bin;
            this.btnRecycleBin.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnRecycleBin.Location = new System.Drawing.Point(562, 4);
            this.btnRecycleBin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRecycleBin.Name = "btnRecycleBin";
            this.btnRecycleBin.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            this.btnRecycleBin.ShowSubItems = false;
            this.btnRecycleBin.Size = new System.Drawing.Size(121, 51);
            this.btnRecycleBin.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnRecycleBin.TabIndex = 127;
            this.btnRecycleBin.Text = "Recyle Bin";
            this.btnRecycleBin.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = global::Presentation.Properties.Resources.add;
            this.btnSave.ImageFixedSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(181, 4);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(5);
            this.btnSave.ShowSubItems = false;
            this.btnSave.Size = new System.Drawing.Size(121, 51);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnSave.TabIndex = 124;
            this.btnSave.Text = "Add Client";
            this.btnSave.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtClintmobile
            // 
            this.txtClintmobile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintmobile.BackgroundStyle.Class = "TextBoxBorder";
            this.txtClintmobile.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClintmobile.ButtonClear.Visible = true;
            this.txtClintmobile.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblClientBindingSource, "MobileNo", true));
            this.txtClintmobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintmobile.Location = new System.Drawing.Point(198, 225);
            this.txtClintmobile.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintmobile.Mask = "0000-0000000";
            this.txtClintmobile.Name = "txtClintmobile";
            this.txtClintmobile.Size = new System.Drawing.Size(192, 31);
            this.txtClintmobile.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtClintmobile.TabIndex = 5;
            this.txtClintmobile.Text = "";
            this.txtClintmobile.WatermarkText = "0000-0000000";
            // 
            // txtClintOffice
            // 
            this.txtClintOffice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtClintOffice.BackgroundStyle.Class = "TextBoxBorder";
            this.txtClintOffice.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClintOffice.ButtonClear.Visible = true;
            this.txtClintOffice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClintOffice.Location = new System.Drawing.Point(590, 225);
            this.txtClintOffice.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtClintOffice.Mask = "000-0000000";
            this.txtClintOffice.Name = "txtClintOffice";
            this.txtClintOffice.Size = new System.Drawing.Size(192, 31);
            this.txtClintOffice.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.txtClintOffice.TabIndex = 4;
            this.txtClintOffice.Text = "";
            this.txtClintOffice.WatermarkText = "0000-0000000";
            // 
            // ClintInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(984, 524);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "ClintInfoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Client Information Form";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ClintInfoForm_FormClosed);
            this.Load += new System.EventHandler(this.ClintInfoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblCityBindingSource)).EndInit();
            this.itemPanel1.ResumeLayout(false);
            this.itemPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.BindingSource tblClientBindingSource;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtClintOffice;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv txtClintmobile;
        private DevComponents.DotNetBar.ButtonX BtnSearchClient;
        private System.Windows.Forms.BindingSource tblCityBindingSource;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClintName;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClintAddres;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClintOpBlncGold;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClintOpBlncCash;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDestrict;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClintTMR;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClintNo;
        private System.Windows.Forms.ComboBox cmbCity;
        private DevComponents.DotNetBar.ItemPanel itemPanel1;
        private System.Windows.Forms.CheckBox chkOrderByName;
        private System.Windows.Forms.Button btnPrintClntList;
        private System.Windows.Forms.Button btnAddDistrict;
        private System.Windows.Forms.Button btnAddCity;
        private DevComponents.DotNetBar.ButtonX btnClose;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnRecycleBin;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.Panel panel1;
    }
}