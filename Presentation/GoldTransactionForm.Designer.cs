﻿namespace Presentation
{
    partial class GoldTransactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelTop = new System.Windows.Forms.Panel();
            this.btnManual = new DevComponents.DotNetBar.ButtonX();
            this.btnScale = new DevComponents.DotNetBar.ButtonX();
            this.txtCurrentDate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtCurrentTime = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TxtTotlAmuntRs = new DevComponents.DotNetBar.LabelX();
            this.TxtNetAmuntRs = new DevComponents.DotNetBar.LabelX();
            this.TxtCutPrTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.Rdb4CashDebit = new System.Windows.Forms.RadioButton();
            this.Rdb3CashCredit = new System.Windows.Forms.RadioButton();
            this.Rdb2GoldDebit = new System.Windows.Forms.RadioButton();
            this.Rdb1GoldCredit = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnRefresh = new DevComponents.DotNetBar.ButtonX();
            this.BtnTransaction = new DevComponents.DotNetBar.ButtonX();
            this.TxtPriceofGoldRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtItemName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.TxtClintNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.TxtWeightGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.TxtFinlWghtGrams = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkSumCut = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.CmbClints = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.tblClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelTop.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.panelTop.Controls.Add(this.btnManual);
            this.panelTop.Controls.Add(this.btnScale);
            this.panelTop.Controls.Add(this.txtCurrentDate);
            this.panelTop.Controls.Add(this.txtCurrentTime);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(984, 64);
            this.panelTop.TabIndex = 115;
            // 
            // btnManual
            // 
            this.btnManual.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnManual.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnManual.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnManual.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManual.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.btnManual.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnManual.Location = new System.Drawing.Point(744, 12);
            this.btnManual.Name = "btnManual";
            this.btnManual.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnManual.Size = new System.Drawing.Size(102, 34);
            this.btnManual.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnManual.TabIndex = 112;
            this.btnManual.Text = "Manual";
            this.btnManual.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // btnScale
            // 
            this.btnScale.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnScale.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnScale.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnScale.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScale.Image = global::Presentation.Properties.Resources.WeightSacle_png;
            this.btnScale.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnScale.Location = new System.Drawing.Point(852, 12);
            this.btnScale.Name = "btnScale";
            this.btnScale.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnScale.Size = new System.Drawing.Size(100, 34);
            this.btnScale.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnScale.TabIndex = 111;
            this.btnScale.Text = "Scale";
            this.btnScale.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // txtCurrentDate
            // 
            this.txtCurrentDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCurrentDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // 
            // 
            this.txtCurrentDate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCurrentDate.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentDate.ForeColor = System.Drawing.Color.White;
            this.txtCurrentDate.Location = new System.Drawing.Point(38, 14);
            this.txtCurrentDate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtCurrentDate.Name = "txtCurrentDate";
            this.txtCurrentDate.ReadOnly = true;
            this.txtCurrentDate.Size = new System.Drawing.Size(175, 34);
            this.txtCurrentDate.TabIndex = 110;
            this.txtCurrentDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCurrentTime
            // 
            this.txtCurrentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCurrentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // 
            // 
            this.txtCurrentTime.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCurrentTime.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentTime.ForeColor = System.Drawing.Color.White;
            this.txtCurrentTime.Location = new System.Drawing.Point(240, 14);
            this.txtCurrentTime.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtCurrentTime.Name = "txtCurrentTime";
            this.txtCurrentTime.ReadOnly = true;
            this.txtCurrentTime.Size = new System.Drawing.Size(184, 34);
            this.txtCurrentTime.TabIndex = 7;
            this.txtCurrentTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TxtTotlAmuntRs
            // 
            this.TxtTotlAmuntRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtTotlAmuntRs.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderBottomWidth = 2;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderLeftWidth = 2;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderRightWidth = 2;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtTotlAmuntRs.BackgroundStyle.BorderTopWidth = 2;
            this.TxtTotlAmuntRs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.TxtTotlAmuntRs, 2);
            this.TxtTotlAmuntRs.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlAmuntRs.ForeColor = System.Drawing.Color.Black;
            this.TxtTotlAmuntRs.Location = new System.Drawing.Point(169, 175);
            this.TxtTotlAmuntRs.Name = "TxtTotlAmuntRs";
            this.TxtTotlAmuntRs.SingleLineColor = System.Drawing.Color.Transparent;
            this.TxtTotlAmuntRs.Size = new System.Drawing.Size(400, 37);
            this.TxtTotlAmuntRs.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TxtTotlAmuntRs.TabIndex = 129;
            this.TxtTotlAmuntRs.Text = "00";
            this.TxtTotlAmuntRs.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtNetAmuntRs
            // 
            this.TxtNetAmuntRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtNetAmuntRs.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtNetAmuntRs.BackgroundStyle.BorderBottomWidth = 2;
            this.TxtNetAmuntRs.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.TxtNetAmuntRs.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtNetAmuntRs.BackgroundStyle.BorderLeftWidth = 2;
            this.TxtNetAmuntRs.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtNetAmuntRs.BackgroundStyle.BorderRightWidth = 2;
            this.TxtNetAmuntRs.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtNetAmuntRs.BackgroundStyle.BorderTopWidth = 2;
            this.TxtNetAmuntRs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.TxtNetAmuntRs, 2);
            this.TxtNetAmuntRs.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetAmuntRs.ForeColor = System.Drawing.Color.Black;
            this.TxtNetAmuntRs.Location = new System.Drawing.Point(169, 218);
            this.TxtNetAmuntRs.Name = "TxtNetAmuntRs";
            this.TxtNetAmuntRs.SingleLineColor = System.Drawing.Color.Transparent;
            this.TxtNetAmuntRs.Size = new System.Drawing.Size(400, 37);
            this.TxtNetAmuntRs.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TxtNetAmuntRs.TabIndex = 130;
            this.TxtNetAmuntRs.Text = "00";
            this.TxtNetAmuntRs.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtCutPrTola
            // 
            this.TxtCutPrTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutPrTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutPrTola.Border.Class = "TextBoxBorder";
            this.TxtCutPrTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.TxtCutPrTola, 2);
            this.TxtCutPrTola.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrTola.ForeColor = System.Drawing.Color.Black;
            this.TxtCutPrTola.Location = new System.Drawing.Point(170, 88);
            this.TxtCutPrTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutPrTola.Name = "TxtCutPrTola";
            this.TxtCutPrTola.Size = new System.Drawing.Size(398, 36);
            this.TxtCutPrTola.TabIndex = 14;
            this.TxtCutPrTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtCutPrTola.WatermarkImageAlignment = System.Drawing.ContentAlignment.BottomLeft;
            this.TxtCutPrTola.WatermarkText = "0";
            this.TxtCutPrTola.TextChanged += new System.EventHandler(this.TxtCutPrTola_TextChanged);
            this.TxtCutPrTola.Enter += new System.EventHandler(this.TxtFieldsCutEnter);
            this.TxtCutPrTola.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCutPrGrams_KeyPress);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(778, 261);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(190, 32);
            this.dateTimePicker1.TabIndex = 126;
            // 
            // groupPanel3
            // 
            this.groupPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.tableLayoutPanelMain.SetColumnSpan(this.groupPanel3, 2);
            this.groupPanel3.Controls.Add(this.Rdb4CashDebit);
            this.groupPanel3.Controls.Add(this.Rdb3CashCredit);
            this.groupPanel3.Controls.Add(this.Rdb2GoldDebit);
            this.groupPanel3.Controls.Add(this.Rdb1GoldCredit);
            this.groupPanel3.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel3.Location = new System.Drawing.Point(576, 88);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupPanel3.Name = "groupPanel3";
            this.tableLayoutPanelMain.SetRowSpan(this.groupPanel3, 4);
            this.groupPanel3.Size = new System.Drawing.Size(400, 168);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuUnusedBackground;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuUnusedSide;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColor = System.Drawing.SystemColors.Window;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.groupPanel3.Style.TextShadowColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionBackground2;
            this.groupPanel3.Style.TextShadowOffset = new System.Drawing.Point(5, 5);
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 117;
            this.groupPanel3.Text = "Transaction Type";
            // 
            // Rdb4CashDebit
            // 
            this.Rdb4CashDebit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb4CashDebit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb4CashDebit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(171)))));
            this.Rdb4CashDebit.Location = new System.Drawing.Point(72, 106);
            this.Rdb4CashDebit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb4CashDebit.Name = "Rdb4CashDebit";
            this.Rdb4CashDebit.Size = new System.Drawing.Size(250, 25);
            this.Rdb4CashDebit.TabIndex = 41;
            this.Rdb4CashDebit.Text = "4 Cash Debit  کیش ادائیگی (-)";
            this.Rdb4CashDebit.UseVisualStyleBackColor = false;
            // 
            // Rdb3CashCredit
            // 
            this.Rdb3CashCredit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb3CashCredit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb3CashCredit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(160)))), ((int)(((byte)(111)))));
            this.Rdb3CashCredit.Location = new System.Drawing.Point(72, 71);
            this.Rdb3CashCredit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb3CashCredit.Name = "Rdb3CashCredit";
            this.Rdb3CashCredit.Size = new System.Drawing.Size(250, 25);
            this.Rdb3CashCredit.TabIndex = 40;
            this.Rdb3CashCredit.Text = "3 Cash Credit  کیش وصولی (+)";
            this.Rdb3CashCredit.UseVisualStyleBackColor = false;
            // 
            // Rdb2GoldDebit
            // 
            this.Rdb2GoldDebit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb2GoldDebit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb2GoldDebit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(181)))), ((int)(((byte)(25)))));
            this.Rdb2GoldDebit.Location = new System.Drawing.Point(72, 38);
            this.Rdb2GoldDebit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb2GoldDebit.Name = "Rdb2GoldDebit";
            this.Rdb2GoldDebit.Size = new System.Drawing.Size(250, 25);
            this.Rdb2GoldDebit.TabIndex = 39;
            this.Rdb2GoldDebit.Text = "2 Gold Debit سونا ادائیگی  (-)";
            this.Rdb2GoldDebit.UseVisualStyleBackColor = false;
            // 
            // Rdb1GoldCredit
            // 
            this.Rdb1GoldCredit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb1GoldCredit.Checked = true;
            this.Rdb1GoldCredit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb1GoldCredit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(145)))), ((int)(((byte)(42)))));
            this.Rdb1GoldCredit.Location = new System.Drawing.Point(72, 10);
            this.Rdb1GoldCredit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb1GoldCredit.Name = "Rdb1GoldCredit";
            this.Rdb1GoldCredit.Size = new System.Drawing.Size(250, 25);
            this.Rdb1GoldCredit.TabIndex = 38;
            this.Rdb1GoldCredit.TabStop = true;
            this.Rdb1GoldCredit.Text = "1 Gold Credit سونا وصولی  (+)";
            this.Rdb1GoldCredit.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.panel1, 3);
            this.panel1.Controls.Add(this.BtnRefresh);
            this.panel1.Controls.Add(this.BtnTransaction);
            this.panel1.Location = new System.Drawing.Point(169, 390);
            this.panel1.Name = "panel1";
            this.tableLayoutPanelMain.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(582, 80);
            this.panel1.TabIndex = 127;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnRefresh.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnRefresh.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Image = global::Presentation.Properties.Resources.cancel;
            this.BtnRefresh.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnRefresh.Location = new System.Drawing.Point(279, 26);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.BtnRefresh.Size = new System.Drawing.Size(178, 54);
            this.BtnRefresh.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.BtnRefresh.TabIndex = 123;
            this.BtnRefresh.Text = "Refresh";
            this.BtnRefresh.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.BtnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // BtnTransaction
            // 
            this.BtnTransaction.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnTransaction.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnTransaction.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnTransaction.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTransaction.Image = global::Presentation.Properties.Resources.add;
            this.BtnTransaction.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnTransaction.Location = new System.Drawing.Point(96, 26);
            this.BtnTransaction.Name = "BtnTransaction";
            this.BtnTransaction.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.BtnTransaction.Size = new System.Drawing.Size(177, 54);
            this.BtnTransaction.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.BtnTransaction.TabIndex = 124;
            this.BtnTransaction.Text = "Add Transaction";
            this.BtnTransaction.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.BtnTransaction.Click += new System.EventHandler(this.BtnTransaction_Click);
            // 
            // TxtPriceofGoldRs
            // 
            this.TxtPriceofGoldRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPriceofGoldRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPriceofGoldRs.Border.Class = "TextBoxBorder";
            this.TxtPriceofGoldRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.TxtPriceofGoldRs, 2);
            this.TxtPriceofGoldRs.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceofGoldRs.ForeColor = System.Drawing.Color.Black;
            this.TxtPriceofGoldRs.Location = new System.Drawing.Point(373, 260);
            this.TxtPriceofGoldRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPriceofGoldRs.Name = "TxtPriceofGoldRs";
            this.TxtPriceofGoldRs.Size = new System.Drawing.Size(398, 37);
            this.TxtPriceofGoldRs.TabIndex = 110;
            this.TxtPriceofGoldRs.WatermarkText = "0";
            this.TxtPriceofGoldRs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCutPrGrams_KeyPress);
            // 
            // TxtItemName
            // 
            this.TxtItemName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtItemName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtItemName.Border.Class = "TextBoxBorder";
            this.TxtItemName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.TxtItemName, 3);
            this.TxtItemName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Itme_Name", true));
            this.TxtItemName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItemName.ForeColor = System.Drawing.Color.Black;
            this.TxtItemName.Location = new System.Drawing.Point(170, 351);
            this.TxtItemName.Margin = new System.Windows.Forms.Padding(4);
            this.TxtItemName.Name = "TxtItemName";
            this.TxtItemName.Size = new System.Drawing.Size(601, 29);
            this.TxtItemName.TabIndex = 118;
            this.TxtItemName.WatermarkText = "Item Details";
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX10.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX10.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX10.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderRightWidth = 2;
            this.labelX10.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderTopWidth = 2;
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX10.Location = new System.Drawing.Point(3, 347);
            this.labelX10.Name = "labelX10";
            this.labelX10.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX10.Size = new System.Drawing.Size(160, 37);
            this.labelX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX10.TabIndex = 119;
            this.labelX10.Text = "Item Details";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX8.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX8.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX8.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderRightWidth = 2;
            this.labelX8.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderTopWidth = 2;
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.labelX8, 2);
            this.labelX8.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX8.Location = new System.Drawing.Point(3, 261);
            this.labelX8.Name = "labelX8";
            this.labelX8.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX8.Size = new System.Drawing.Size(363, 37);
            this.labelX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX8.TabIndex = 112;
            this.labelX8.Text = "Current Price Of Gold  (Per Tola)";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX13
            // 
            this.labelX13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX13.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX13.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX13.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderRightWidth = 2;
            this.labelX13.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderTopWidth = 2;
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX13.Location = new System.Drawing.Point(3, 218);
            this.labelX13.Name = "labelX13";
            this.labelX13.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX13.Size = new System.Drawing.Size(160, 37);
            this.labelX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX13.TabIndex = 115;
            this.labelX13.Text = " Net Amount ";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtClintNo
            // 
            this.TxtClintNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtClintNo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtClintNo.Border.Class = "TextBoxBorder";
            this.TxtClintNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtClintNo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClintNo.ForeColor = System.Drawing.Color.Black;
            this.TxtClintNo.Location = new System.Drawing.Point(170, 305);
            this.TxtClintNo.Margin = new System.Windows.Forms.Padding(4);
            this.TxtClintNo.Name = "TxtClintNo";
            this.TxtClintNo.ReadOnly = true;
            this.TxtClintNo.Size = new System.Drawing.Size(195, 29);
            this.TxtClintNo.TabIndex = 121;
            this.TxtClintNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtClintNo.WatermarkText = "Client No";
            // 
            // labelX11
            // 
            this.labelX11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX11.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX11.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX11.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderRightWidth = 2;
            this.labelX11.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderTopWidth = 2;
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX11.Location = new System.Drawing.Point(3, 304);
            this.labelX11.Name = "labelX11";
            this.labelX11.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX11.Size = new System.Drawing.Size(160, 37);
            this.labelX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX11.TabIndex = 120;
            this.labelX11.Text = "Client Information";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtWeightGrams
            // 
            this.TxtWeightGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightGrams.Border.Class = "TextBoxBorder";
            this.TxtWeightGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightGrams.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightGrams.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightGrams.Location = new System.Drawing.Point(779, 45);
            this.TxtWeightGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightGrams.Name = "TxtWeightGrams";
            this.TxtWeightGrams.Size = new System.Drawing.Size(197, 37);
            this.TxtWeightGrams.TabIndex = 16;
            this.TxtWeightGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightGrams.WatermarkText = "0";
            this.TxtWeightGrams.TextChanged += new System.EventHandler(this.TxtWeightGrams_TextChanged);
            this.TxtWeightGrams.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightGrams.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtWeightGrams_KeyPress_1);
            this.TxtWeightGrams.Leave += new System.EventHandler(this.TxtWeightGrams_Leave);
            // 
            // TxtWeightRatti
            // 
            this.TxtWeightRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightRatti.Border.Class = "TextBoxBorder";
            this.TxtWeightRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightRatti.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightRatti.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightRatti.Location = new System.Drawing.Point(576, 45);
            this.TxtWeightRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightRatti.Name = "TxtWeightRatti";
            this.TxtWeightRatti.Size = new System.Drawing.Size(195, 37);
            this.TxtWeightRatti.TabIndex = 11;
            this.TxtWeightRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightRatti.WatermarkText = "0";
            this.TxtWeightRatti.TextChanged += new System.EventHandler(this.TxtWeightRatti_TextChanged);
            this.TxtWeightRatti.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightRatti.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtWeightGrams_KeyPress_1);
            // 
            // TxtWeightMasha
            // 
            this.TxtWeightMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightMasha.Border.Class = "TextBoxBorder";
            this.TxtWeightMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightMasha.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightMasha.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightMasha.Location = new System.Drawing.Point(373, 45);
            this.TxtWeightMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightMasha.Name = "TxtWeightMasha";
            this.TxtWeightMasha.Size = new System.Drawing.Size(195, 37);
            this.TxtWeightMasha.TabIndex = 14;
            this.TxtWeightMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightMasha.WatermarkText = "0";
            this.TxtWeightMasha.TextChanged += new System.EventHandler(this.TxtWeightMasha_TextChanged);
            this.TxtWeightMasha.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightMasha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Enter_KeyPress);
            // 
            // TxtWeightTola
            // 
            this.TxtWeightTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightTola.Border.Class = "TextBoxBorder";
            this.TxtWeightTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightTola.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightTola.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightTola.Location = new System.Drawing.Point(170, 45);
            this.TxtWeightTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightTola.Name = "TxtWeightTola";
            this.TxtWeightTola.Size = new System.Drawing.Size(195, 37);
            this.TxtWeightTola.TabIndex = 5;
            this.TxtWeightTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightTola.WatermarkText = "0";
            this.TxtWeightTola.TextChanged += new System.EventHandler(this.TxtWeightTola_TextChanged);
            this.TxtWeightTola.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightTola.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtWeightGrams_KeyPress_1);
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX2.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX2.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX2.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderRightWidth = 2;
            this.labelX2.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderTopWidth = 2;
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI Black", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX2.Location = new System.Drawing.Point(3, 3);
            this.labelX2.Name = "labelX2";
            this.tableLayoutPanelMain.SetRowSpan(this.labelX2, 2);
            this.labelX2.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX2.Size = new System.Drawing.Size(160, 80);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX2.TabIndex = 105;
            this.labelX2.Text = "Weight";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX3.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderRightWidth = 2;
            this.labelX3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderTopWidth = 2;
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX3.Location = new System.Drawing.Point(169, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX3.Size = new System.Drawing.Size(197, 37);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX3.TabIndex = 107;
            this.labelX3.Text = "Tola";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX4.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderRightWidth = 2;
            this.labelX4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderTopWidth = 2;
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX4.Location = new System.Drawing.Point(372, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX4.Size = new System.Drawing.Size(197, 37);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX4.TabIndex = 108;
            this.labelX4.Text = "Masha";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX5.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX5.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX5.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderRightWidth = 2;
            this.labelX5.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderTopWidth = 2;
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX5.Location = new System.Drawing.Point(575, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX5.Size = new System.Drawing.Size(197, 37);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX5.TabIndex = 109;
            this.labelX5.Text = "Ratti";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX6.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX6.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX6.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderRightWidth = 2;
            this.labelX6.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderTopWidth = 2;
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX6.Location = new System.Drawing.Point(778, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX6.Size = new System.Drawing.Size(199, 37);
            this.labelX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX6.TabIndex = 110;
            this.labelX6.Text = "Grams";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX7.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX7.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX7.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderRightWidth = 2;
            this.labelX7.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderTopWidth = 2;
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX7.Location = new System.Drawing.Point(3, 132);
            this.labelX7.Name = "labelX7";
            this.labelX7.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX7.Size = new System.Drawing.Size(160, 37);
            this.labelX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX7.TabIndex = 111;
            this.labelX7.Text = " Final Weight";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX12
            // 
            this.labelX12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX12.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX12.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX12.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderRightWidth = 2;
            this.labelX12.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderTopWidth = 2;
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX12.Location = new System.Drawing.Point(3, 175);
            this.labelX12.Name = "labelX12";
            this.labelX12.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX12.Size = new System.Drawing.Size(160, 37);
            this.labelX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX12.TabIndex = 114;
            this.labelX12.Text = " Total Amount ";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtFinlWghtGrams
            // 
            this.TxtFinlWghtGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TxtFinlWghtGrams.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderBottomWidth = 2;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderLeftWidth = 2;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderRightWidth = 2;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TxtFinlWghtGrams.BackgroundStyle.BorderTopWidth = 2;
            this.TxtFinlWghtGrams.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelMain.SetColumnSpan(this.TxtFinlWghtGrams, 2);
            this.TxtFinlWghtGrams.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtGrams.ForeColor = System.Drawing.Color.Black;
            this.TxtFinlWghtGrams.Location = new System.Drawing.Point(169, 132);
            this.TxtFinlWghtGrams.Name = "TxtFinlWghtGrams";
            this.TxtFinlWghtGrams.SingleLineColor = System.Drawing.Color.Transparent;
            this.TxtFinlWghtGrams.Size = new System.Drawing.Size(400, 37);
            this.TxtFinlWghtGrams.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TxtFinlWghtGrams.TabIndex = 128;
            this.TxtFinlWghtGrams.Text = "00";
            this.TxtFinlWghtGrams.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 5;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.75F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.75F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.75F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.75F));
            this.tableLayoutPanelMain.Controls.Add(this.TxtFinlWghtGrams, 1, 3);
            this.tableLayoutPanelMain.Controls.Add(this.labelX12, 0, 4);
            this.tableLayoutPanelMain.Controls.Add(this.labelX7, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.labelX6, 4, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelX5, 3, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelX4, 2, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelX3, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelX2, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightTola, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightMasha, 2, 1);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightRatti, 3, 1);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightGrams, 4, 1);
            this.tableLayoutPanelMain.Controls.Add(this.labelX11, 0, 7);
            this.tableLayoutPanelMain.Controls.Add(this.TxtClintNo, 1, 7);
            this.tableLayoutPanelMain.Controls.Add(this.labelX13, 0, 5);
            this.tableLayoutPanelMain.Controls.Add(this.labelX8, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.labelX10, 0, 8);
            this.tableLayoutPanelMain.Controls.Add(this.TxtItemName, 1, 8);
            this.tableLayoutPanelMain.Controls.Add(this.TxtPriceofGoldRs, 2, 6);
            this.tableLayoutPanelMain.Controls.Add(this.panel1, 1, 9);
            this.tableLayoutPanelMain.Controls.Add(this.groupPanel3, 3, 2);
            this.tableLayoutPanelMain.Controls.Add(this.dateTimePicker1, 4, 6);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutPrTola, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.TxtNetAmuntRs, 1, 5);
            this.tableLayoutPanelMain.Controls.Add(this.TxtTotlAmuntRs, 1, 4);
            this.tableLayoutPanelMain.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.CmbClints, 2, 7);
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 70);
            this.tableLayoutPanelMain.MaximumSize = new System.Drawing.Size(980, 535);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 13;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.164871F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.02154F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(980, 535);
            this.tableLayoutPanelMain.TabIndex = 78;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkSumCut);
            this.panel2.Controls.Add(this.labelX1);
            this.panel2.Location = new System.Drawing.Point(3, 89);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(160, 37);
            this.panel2.TabIndex = 133;
            // 
            // chkSumCut
            // 
            // 
            // 
            // 
            this.chkSumCut.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkSumCut.CheckBoxPosition = DevComponents.DotNetBar.eCheckBoxPosition.Right;
            this.chkSumCut.CheckSignSize = new System.Drawing.Size(15, 15);
            this.chkSumCut.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSumCut.Location = new System.Drawing.Point(102, 10);
            this.chkSumCut.Name = "chkSumCut";
            this.chkSumCut.Size = new System.Drawing.Size(51, 21);
            this.chkSumCut.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkSumCut.TabIndex = 131;
            this.chkSumCut.Text = "Sum ";
            this.chkSumCut.TextColor = System.Drawing.Color.Black;
            this.chkSumCut.CheckedChanged += new System.EventHandler(this.chkSumCut_CheckedChanged);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX1.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderRightWidth = 2;
            this.labelX1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderTopWidth = 2;
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX1.Location = new System.Drawing.Point(0, 0);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX1.Size = new System.Drawing.Size(157, 37);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX1.TabIndex = 106;
            this.labelX1.Text = "Cut";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // CmbClints
            // 
            this.CmbClints.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelMain.SetColumnSpan(this.CmbClints, 2);
            this.CmbClints.DisplayMember = "Name";
            this.CmbClints.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CmbClints.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbClints.FormattingEnabled = true;
            this.CmbClints.ItemHeight = 23;
            this.CmbClints.Location = new System.Drawing.Point(372, 304);
            this.CmbClints.Name = "CmbClints";
            this.CmbClints.Size = new System.Drawing.Size(400, 29);
            this.CmbClints.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CmbClints.TabIndex = 134;
            this.CmbClints.Text = "Select Customer";
            this.CmbClints.ValueMember = "C_Id";
            // 
            // tblClientBindingSource
            // 
            this.tblClientBindingSource.DataSource = typeof(Data.tblClient);
            // 
            // GoldTransactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(226)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(984, 611);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(1000, 650);
            this.Name = "GoldTransactionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GoldTransaction";
            this.Load += new System.EventHandler(this.GoldTransactionForm_Load);
            this.panelTop.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelTop;
        private DevComponents.DotNetBar.ButtonX btnManual;
        private DevComponents.DotNetBar.ButtonX btnScale;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private System.Windows.Forms.BindingSource tblClientBindingSource;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCurrentDate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCurrentTime;
        private DevComponents.DotNetBar.LabelX TxtTotlAmuntRs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private DevComponents.DotNetBar.LabelX TxtFinlWghtGrams;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightGrams;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtClintNo;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtItemName;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPriceofGoldRs;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.ButtonX BtnRefresh;
        private DevComponents.DotNetBar.ButtonX BtnTransaction;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.RadioButton Rdb4CashDebit;
        private System.Windows.Forms.RadioButton Rdb3CashCredit;
        private System.Windows.Forms.RadioButton Rdb2GoldDebit;
        private System.Windows.Forms.RadioButton Rdb1GoldCredit;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrTola;
        private DevComponents.DotNetBar.LabelX TxtNetAmuntRs;
        private System.Windows.Forms.Panel panel2;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkSumCut;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CmbClints;
    }
}