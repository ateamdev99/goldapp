﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class JewellersForm : Form
    {
        
        public JewellersForm(tblTransaction obj)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            timer1.Start();
            if (obj == null || obj.T_Id == 0)
            {
                tblTransactionBindingSource.DataSource = new tblTransaction();
            }
            else
            {

                int WM = obj.Weightin_Mode;
                int CP = obj.Cut_Priority;
                int LPC = obj.Labour_And_Polish_Calculation;
                if (WM == 0)
                {
                    RdbPakaTola.Checked = true;
                }
                else { RdbKachTola.Checked = true; }
                if (WM == 0)
                {
                    RdbCutPrTolaFirst.Checked = true;
                }
                else { RdbTotlCutFirst.Checked = true; }
                if (LPC == 0)
                {
                    RdbTopWeight.Checked = true;
                }
                else { RdbFinalWeight.Checked = true; }

                tblTransactionBindingSource.DataSource = obj;

            }
        }

        private void JewellersForm_Load(object sender, EventArgs e)
        {
        }
        private void BtnTransaction_Click(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(TxtWeightGrams.Text))
                {
                    MessageBox.Show("Enter Weight in Grams", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtWeightGrams.Focus();
                }
            else if (string.IsNullOrEmpty(TxtCurRatePrGramsRs.Text))
            {
                MessageBox.Show("Enter CurRatePrGrams", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TxtCurRatePrGramsRs.Focus();
            }
            else
            {
                try
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                    tblObj.Cut_Priority = CutPeriortyGet();
                    tblObj.Weightin_Mode = WeghtingModeGet();
                    tblObj.Labour_And_Polish_Calculation = LburPolisCalcuGet();
                    if (tblObj.T_Id == 0)
                    { tblObj.T_Date = dateTimePicker1.Value; }
                    if (tblObj != null)
                    {
                        Presentation.TransactionEnteryAndSearchForm frEmp = new TransactionEnteryAndSearchForm(tblObj);
                        frEmp.ShowDialog();
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
               
            }
        }

        public enum WeghtingMode
        {
            PakaTola = 0,
            KatchTola = 1,
            
        }
        public int WeghtingModeGet()
        {
            if (RdbPakaTola.Checked)
            {
                return (int)WeghtingMode.PakaTola;
            }

            return (int)WeghtingMode.KatchTola;
        }
        public enum CutPeriorty
        {
            CutPerTolaFirst = 0,
            TotalCutFirst = 1,

        }
        public int CutPeriortyGet()
        {
            if (RdbCutPrTolaFirst.Checked)
            {
                return (int)CutPeriorty.CutPerTolaFirst;
            }

            return (int)CutPeriorty.TotalCutFirst;
        }
        public enum LburPolisCalcu
        {
            TopWeight = 0,
            FinalWeight = 1,

        }
        public int LburPolisCalcuGet()
        {
            if (RdbTopWeight.Checked)
            {
                return (int)LburPolisCalcu.TopWeight;
            }

            return (int)LburPolisCalcu.FinalWeight;
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void RestForm()
        {
            TxtWeightGrams.Clear();
            TxtCutPrTola.Clear();
            TxtCutFrmTotlWghtTola.Clear();
            TxtPolishPrTola.Clear();
            TxtTotlPolishofTola.Clear();
            TxtCurRatePrTolaRs.Clear();
            TxtFinlWghtTola.Clear();
            TxtLabourPerTolaRs.Clear();
            TxtPriceofGoldRs.Clear();
            TxtTotlLaburRs.Clear();
            TxtTotlAmuntRs.Clear();
            TxtPymtPercntCut.Clear();
            TxtNetAmuntRs.Clear();
            RdbKachTola.Checked = true;
            RdbTotlCutFirst.Checked = true;
            RdbFinalWeight.Checked = true;
            tblTransactionBindingSource.DataSource = new tblTransaction();
            dateTimePicker1.Value = DateTime.Now.Date;

        }

        private void BtnClearData_Click(object sender, EventArgs e)
        {
            RestForm();
        }

        private void panelSecondTopParent_Paint(object sender, PaintEventArgs e)
        {

        }
        private void pictureBox17_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            txtCurrentTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            txtCurrentDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
        }

        private void TxtCutPrMasha_TextChanged(object sender, EventArgs e)
        {

        }
       
    }
}
