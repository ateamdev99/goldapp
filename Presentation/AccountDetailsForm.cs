﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;

namespace Presentation
{
    public partial class AccountDetailsForm : Form
    {
        public AccountDetailsForm()
        {
            InitializeComponent();
        }

        private void AccountDetailsForm_Load(object sender, EventArgs e)
        {
            tblClientBindingSource.DataSource = ClientServicesClass.GetAllClients();
            //dataGridViewX1.DataSource = ClientServicesClass.GetAllClientsWithCityname();
        }

        private void comboBoxEx1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CmbClients.SelectedIndex > -1) { TxtClientsId.Text = CmbClients.SelectedValue.ToString(); }
        }
    }
}
