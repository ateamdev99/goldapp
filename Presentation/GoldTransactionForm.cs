﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    
    public partial class GoldTransactionForm : Form
    {
       
       //bool isTyping = false;
        bool isGramTyping = false;
        bool isRattiTyping = false;
        bool isMashayping = false;
        bool isTolaTyping = false;
        //is Cut
        //bool isCutGramTyping = false;
        //bool isCutRattiTyping = false;
        //bool isCutMashayping = false;
        //bool isCutTolaTyping = false;
        public double ratti = 0.1215;
        public double masha = 0.972;
        public double tola = 11.664;
        int trId = 0;
        public GoldTransactionForm(tblTransaction obj )
        {
            InitializeComponent();
            timer1.Start();
            if (obj == null || obj.T_Id == 0)
            {
                tblTransactionBindingSource.DataSource = new tblTransaction();
                
            }
            else
            {
               tblTransaction objTr= TransactionServicesClass.GetByIdTransaction(obj.T_Id);
                TxtWeightGrams.Text = Conversion.toString(objTr.ImpureGoldinGram);
                TxtItemName.Text = Conversion.toString(objTr.Itme_Name);
                TxtCutPrTola.Text = Conversion.toString(objTr.Cut_Per_Tola);
                TxtPriceofGoldRs.Text = Conversion.toString(objTr.Price_of_Gold);
                TxtNetAmuntRs.Text = Conversion.toString(objTr.Net_Amount);
                CmbPopulate();
                if (objTr.ClientId == 1) { objTr.ClientId = 1; }
                else
                {
                    CmbClints.SelectedValue = objTr.ClientId;
                    TxtClintNo.Text = Conversion.toString(objTr.ClientId);
                }
                int CD = objTr.Tran_Type;
                if (CD == 1) { Rdb1GoldCredit.Checked = true; }
                else if (CD == 2) { Rdb2GoldDebit.Checked = true; }
                else if (CD == 3) { Rdb3CashCredit.Checked = true; }
                else if (CD == 4) { Rdb4CashDebit.Checked = true; }
                
                trId = objTr.T_Id;
                tblTransactionBindingSource.DataSource = objTr;
                BtnTransaction.Text = "Update";
            }
        }
        void GETGoldCurrentRate()
        {
            string rate = DBUtils.executeSqlGetID("SELECT TOP 1 TodayRate FROM tblGoldRate where UpdatedDate BETWEEN '" + DateTime.Now.Date + "' AND '" + DateTime.Now + "' ORDER BY Id desc  ");
            if (rate != null && rate != "")
            {
                TxtPriceofGoldRs.Text = rate;
                //linkAddGoldRate.Text = "Edit Rate";
            }
            else
            {
                TxtPriceofGoldRs.Text = "";
            }

        }
        private void GoldTransactionForm_Load(object sender, EventArgs e)
        {
            CmbPopulate();
            GETGoldCurrentRate  ();
            CmbClints.SelectedIndex = -1;
            TxtClintNo.Text = "";
        }

        private void BtnTransaction_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(TxtWeightGrams.Text))
                {
                    MessageBox.Show("Enter Weight in Grams", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtWeightGrams.Focus();
                }

                else if (string.IsNullOrEmpty(TxtItemName.Text))
                {
                    MessageBox.Show("Enter Item Name", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtItemName.Focus();
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                   if (tblObj != null && tblObj.T_Id == 0  && BtnTransaction.Text== "Add Transaction")
                    {
                        try
                        {
                            tblObj.ImpureGoldinGram = Conversion.toDouble(TxtWeightGrams.Text);
                            tblObj.Cut_Per_Tola = Conversion.toDouble(TxtCutPrTola.Text);
                            tblObj.Net_Amount = Conversion.toDouble(TxtNetAmuntRs.Text);
                            tblObj.Price_of_Gold = Conversion.toDouble(TxtPriceofGoldRs.Text);
                            if (tblObj.ClientId == 0)
                            { tblObj.ClientId = 1;   }
                            else { tblObj.ClientId = Conversion.toInt(CmbClints.SelectedValue); }
                            if (tblObj.T_Id == 0)
                            {tblObj.T_Date = DateTime.Now;tblObj.T_Date = dateTimePicker1.Value; }
                            tblObj.Tran_Type = GetCreditDebit();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.Tran_Status = "Jewellers";
                            tblObj.IsTeleConfirmation = false;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.Purity = 1000;
                            tblObj.IsDeleted = false;
                            tblObj.UpatedDate = DateTime.Now;

                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Insert(tblObj);
                                RestForm();
                                Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.ImpureGoldinGram = Conversion.toDouble(TxtWeightGrams.Text);
                            tblObj.Cut_Per_Tola = Conversion.toDouble(TxtCutPrTola.Text);
                            tblObj.Net_Amount = Conversion.toDouble(TxtNetAmuntRs.Text);
                            tblObj.Price_of_Gold = Conversion.toDouble(TxtPriceofGoldRs.Text);
                            if (tblObj.ClientId == 0)
                            { tblObj.ClientId = 1; }
                            else { tblObj.ClientId = Conversion.toInt(CmbClints.SelectedValue); }
                            if (tblObj.IsTeleConfirmation == true) { tblObj.IsTeleConfirmation = true; } else { tblObj.IsTeleConfirmation = false; tblObj.TeleDate = DateTime.Now; }
                            
                            tblObj.Tran_Type = GetCreditDebit();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Purity = 1000;
                            tblObj.Added_By = "Admin";
                            //tblObj.T_Date = DateTime.Now.Date;
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                                RestForm();
                                this.Close();
                            }

                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {RestForm();}

        private void CmbClints_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CmbClints.SelectedIndex > -1) { TxtClintNo.Text = CmbClints.SelectedValue.ToString(); }
        }
       
        private void TxtWeightGrams_TextChanged(object sender, EventArgs e)
        {
            if (isGramTyping)
            {
                double zerovalue = 0.0;
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);
                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);
                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                TxtWeightTola.Text = ratiMashaTola["tola"].ToString();
            }
        }

        private void TxtWeightRatti_TextChanged(object sender, EventArgs e)
        {
            double valueInMasha = Conversion.toDouble(TxtWeightMasha.Text);
            double valueInTola = Conversion.toDouble(TxtWeightTola.Text);
            double valueInRatti = Conversion.toDouble(TxtWeightRatti.Text);
            double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;

            if (isRattiTyping)
            {
                TxtWeightGrams.Text = totalInGrams.ToString();
            }
        }

        private void TxtWeightMasha_TextChanged(object sender, EventArgs e)
        {
            double valueInRatti = Conversion.toDouble(TxtWeightRatti.Text);
            double valueInTola = Conversion.toDouble(TxtWeightTola.Text);
            double valueInMasha = Conversion.toDouble(TxtWeightMasha.Text);
            double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;


            if (isMashayping)
            {
                double zerovalue = 0.0;
                TxtWeightGrams.Text = totalInGrams.ToString();
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);

                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);

                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                //TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                TxtWeightTola.Text = ratiMashaTola["tola"].ToString();
            }
        }

        private void TxtWeightTola_TextChanged(object sender, EventArgs e)
        {
            double valueInRatti = Conversion.toDouble(TxtWeightRatti.Text);
            double valueInMasha = Conversion.toDouble(TxtWeightMasha.Text);
            double valueInTola = Conversion.toDouble(TxtWeightTola.Text);
            double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;


            if (isTolaTyping)
            {
                double zerovalue = 0.0;
                TxtWeightGrams.Text = totalInGrams.ToString();
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);

                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);

                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                //TxtWeightTola.Text = ratiMashaTola["tola"].ToString();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            txtCurrentTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            txtCurrentDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
        }
        void CmbPopulate()
        {
            try
            {
                using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
                {
                    var QueryReturn = (from t in db.tblClients where t.C_Id != 1 && t.C_Id != 2 && t.ActivateAccount != true select new { t.C_Id, t.Name }).ToList();
                    CmbClints.DataSource = QueryReturn;
                    CmbClints.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CmbClints.AutoCompleteSource = AutoCompleteSource.ListItems;
                    if (QueryReturn.Count > 0)
                    {
                       
                        CmbClints.Text= "Select Customer";

                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error In Populating Clients.", exp.Message);
            }
            //var dt = Core.DBUtils.GetDataTable("select  C_Id,Name from tblClient where C_Id != 1 and C_Id != 2");
            //CmbClints.DataSource = dt;
        }
        private void TxtFieldsEnter(object sender, EventArgs e)
        {
            if (object.ReferenceEquals(TxtWeightGrams, sender))
            {
                isGramTyping = true;
                isRattiTyping = false;
                isMashayping = false;
                isTolaTyping = false;
            }
            else if (object.ReferenceEquals(TxtWeightRatti, sender))
            {
                isRattiTyping = true;
                isGramTyping = false;
                isMashayping = false;
                isTolaTyping = false;
            }
            else if (object.ReferenceEquals(TxtWeightMasha, sender))
            {
                isMashayping = true;
                isGramTyping = false;
                isRattiTyping = false;
                isTolaTyping = false;
            }
            else if (object.ReferenceEquals(TxtWeightTola, sender))
            {
                isTolaTyping = true;
                isGramTyping = false;
                isRattiTyping = false;
                isMashayping = false;
            }
        }
        //typing cut TxtFeildEnter
        private void TxtFieldsCutEnter(object sender, EventArgs e)
        {
            //if (object.ReferenceEquals(TxtCutPrGrams, sender))
            //{
            //    isCutGramTyping = true;
            //    isCutRattiTyping = false;
            //    isCutMashayping = false;
            //    isCutTolaTyping = false;
            //}
            //else if (object.ReferenceEquals(TxtCutPrRatti, sender))
            //{
            //    isCutRattiTyping = true;
            //    isCutGramTyping = false;
            //    isCutMashayping = false;
            //    isCutTolaTyping = false;
            //}
            //else if (object.ReferenceEquals(TxtCutPrMasha, sender))
            //{
            //    isCutMashayping = true;
            //    isCutGramTyping = false;
            //    isCutRattiTyping = false;
            //    isCutTolaTyping = false;
            //}
            //else if (object.ReferenceEquals(TxtCutPrTola, sender))
            //{
            //    isCutTolaTyping = true;
            //    isCutGramTyping = false;
            //    isCutRattiTyping = false;
            //    isCutMashayping = false;
            //}
        }
        private void Enter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);

                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);
                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                TxtWeightTola.Text = ratiMashaTola["tola"].ToString();

            }

        }
        /// <summary>
        //Key press per Cut 
        private void EnterCut_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)13)
            //{
            //    double txtDblGrm = Conversion.toDouble(TxtCutPrGrams.Text);

            //    Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);
            //    TxtCutPrRatti.Text = ratiMashaTola["ratti"].ToString();
            //    TxtCutPrMasha.Text = ratiMashaTola["masha"].ToString();
            //    TxtCutPrTola.Text = ratiMashaTola["tola"].ToString();
            //}

        }
        /// </summary>
        void RestForm()
        {
            CmbPopulate();
            CmbClints.SelectedIndex = -1;
            TxtWeightGrams.Clear();
            TxtWeightMasha.Clear();
            TxtWeightTola.Clear();
            TxtWeightRatti.Clear();
            TxtWeightGrams.Focus();
            TxtCutPrTola.Clear();
            //TxtCutPrMasha.Clear();
            //TxtCutPrRatti.Clear();
            //TxtCutPrGrams.Clear();
            TxtFinlWghtGrams.Text="0";
            TxtPriceofGoldRs.Clear();
            TxtTotlAmuntRs.Text = "0";
            TxtNetAmuntRs.Text = "0";
            Rdb1GoldCredit.Checked = true;
            TxtClintNo.Clear();
            TxtItemName.Clear();
            BtnTransaction.Text = "Add Transaction";
            tblTransactionBindingSource.DataSource = new tblTransaction();

        }
        public enum CreditDebit
        {
            GoldCredit = 1,
            GoldDebit = 2,
            CashCredit = 3,
            CashDebit = 4,
            Expense = 5
        }
        private int GetCreditDebit()
        {
            if (Rdb1GoldCredit.Checked)
            {
                return (int)CreditDebit.GoldCredit;
            }
            if (Rdb2GoldDebit.Checked)
            {
                return (int)CreditDebit.GoldDebit;
            }
            if (Rdb3CashCredit.Checked)
            {
                return (int)CreditDebit.CashCredit;
            }
            if (Rdb4CashDebit.Checked)
            {
                return (int)CreditDebit.CashDebit;
            }
            return (int)CreditDebit.Expense;
        }
        
        private void TxtWeightGrams_Leave(object sender, EventArgs e)
        {
            bool isGramTyping = false;
            double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);
            if (txtDblGrm <= ratti)
            {
                TxtWeightRatti.Text = Conversion.ConvertToRatti(txtDblGrm).ToString();

            }
            else if (txtDblGrm > ratti && txtDblGrm < masha)
            {
                TxtWeightRatti.Text = Conversion.ConvertToRatti(txtDblGrm).ToString();

            }
            else if (txtDblGrm > masha && txtDblGrm < tola)
            {
                TxtWeightMasha.Text = Conversion.ConvertToMash(txtDblGrm).ToString();

            }
            else if (txtDblGrm > tola)
            {
                TxtWeightTola.Text = Conversion.ConvertToTola(txtDblGrm).ToString();

            }
        }
        
        private void TxtCutPrGrams_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void TxtWeightGrams_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 5)
                {
                    e.Handled = true;
                }

            }
        }
        //cut per grame form Final Weight
        private void TxtCutPrGrams_TextChanged(object sender, EventArgs e)
        {
            //double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);
            //double dbCutPrGrams = Conversion.toDouble(TxtCutPrGrams.Text);
            

            //if (isCutGramTyping)
            //{
            //    double zerovalue = 0.0;
            //    double txtDblCutGrm = Conversion.toDouble(TxtCutPrGrams.Text);
            //    Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);
            //    TxtCutPrRatti.Text = ratiMashaTola["ratti"].ToString();
            //    TxtCutPrMasha.Text = ratiMashaTola["masha"].ToString();
            //    TxtCutPrTola.Text = ratiMashaTola["tola"].ToString();
            //    //TxtFinlWghtGrams.Text = (txtDblGrm - dbCutPrGrams).ToString();
            //    calculateFinalWeight();
            //}
        }

        private void TxtFinlWghtGrams_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtCutPrRatti_TextChanged(object sender, EventArgs e)
        {
            //double valueInMasha = Conversion.toDouble(TxtCutPrMasha.Text);
            //double valueInTola = Conversion.toDouble(TxtCutPrTola.Text);
            //double valueInRatti = Conversion.toDouble(TxtCutPrRatti.Text);
            //double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;

            //if (isCutRattiTyping)
            //{
            //    TxtCutPrRatti.Text = totalInGrams.ToString();
            //}
            //calculateFinalWeight();
            //

        }
       void calculateFinalWeight()
        {
            //double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);
            //double dbCutPrGrams = Conversion.toDouble(TxtCutPrGrams.Text);
            //TxtFinlWghtGrams.Text = (txtDblGrm - dbCutPrGrams).ToString();
        }

        private void TxtCutPrTola_TextChanged(object sender, EventArgs e)
        {
            //double valueInRatti = Conversion.toDouble(TxtCutPrRatti.Text);
            //double valueInMasha = Conversion.toDouble(TxtCutPrMasha.Text);
            //double valueInTola = Conversion.toDouble(TxtCutPrTola.Text);
            //double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;

            //if (isCutTolaTyping)
            //{
            //    double zerovalue = 0.0;
            //    TxtCutPrGrams.Text = totalInGrams.ToString();
            //    double txtDblGrm = Conversion.toDouble(TxtCutPrGrams.Text);

            //    Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);

            //    TxtCutPrRatti.Text = ratiMashaTola["ratti"].ToString();
            //    TxtCutPrMasha.Text = ratiMashaTola["masha"].ToString();
            //    TxtCutPrTola.Text = ratiMashaTola["tola"].ToString();
            //    calculateFinalWeight();
            //}
        }

        private void TxtCutPrMasha_TextChanged(object sender, EventArgs e)
        {
            //double valueInRatti = Conversion.toDouble(TxtCutPrRatti.Text);
            //double valueInTola = Conversion.toDouble(TxtCutPrTola.Text);
            //double valueInMasha = Conversion.toDouble(TxtCutPrMasha.Text);
            //double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;


            //if (isCutMashayping)
            //{
            //    double zerovalue = 0.0;
            //    TxtCutPrGrams.Text = totalInGrams.ToString();
            //    double txtDblGrm = Conversion.toDouble(TxtCutPrGrams.Text);

            //    Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);

            //    TxtCutPrRatti.Text = ratiMashaTola["ratti"].ToString();
            //    TxtCutPrMasha.Text = ratiMashaTola["masha"].ToString();
            //    TxtCutPrTola.Text = ratiMashaTola["tola"].ToString();
            //    calculateFinalWeight();
            //}
        }

        private void chkSumCut_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSumCut.Checked == true)
            {
                TxtCutPrTola.ReadOnly = false;
            }
            else
            {
                TxtCutPrTola.ReadOnly = true;
            }
        }
    }
}
