﻿namespace Presentation
{
    partial class TransactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtCurrentTime = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX17 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCurRatePrTolaRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TxtCurRatePrMashaRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCurRatePrRattiRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX10 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX7 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX4 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX16 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX15 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX14 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.TxtCurRatePrGramsRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX9 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtCurrentDate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.BtnTransaction = new DevComponents.DotNetBar.ButtonX();
            this.BtnClearData = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.TxtPriceofGoldRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Rdb4CashDebit = new System.Windows.Forms.RadioButton();
            this.Rdb3CashCredit = new System.Windows.Forms.RadioButton();
            this.Rdb2GoldDebit = new System.Windows.Forms.RadioButton();
            this.Rdb1GoldCredit = new System.Windows.Forms.RadioButton();
            this.TxtItemName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CmbClints = new System.Windows.Forms.ComboBox();
            this.tblClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TxtTotlAmuntRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtClintNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtNetAmuntRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPymtPercntCut = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.groupPanelLabourPolisCalcu = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.RdbFinalWeight = new System.Windows.Forms.RadioButton();
            this.RdbTopWeight = new System.Windows.Forms.RadioButton();
            this.groupPanelWeightinMode = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.RdbKachTola = new System.Windows.Forms.RadioButton();
            this.RdbPakaTola = new System.Windows.Forms.RadioButton();
            this.groupPanelcutPriority = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.RdbTotlCutFirst = new System.Windows.Forms.RadioButton();
            this.RdbCutPrTolaFirst = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanelMain.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).BeginInit();
            this.groupPanelLabourPolisCalcu.SuspendLayout();
            this.groupPanelWeightinMode.SuspendLayout();
            this.groupPanelcutPriority.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 5;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tableLayoutPanelMain.Controls.Add(this.panel3, 3, 0);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX17, 0, 9);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCurRatePrTolaRs, 1, 9);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCurRatePrMashaRs, 2, 9);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCurRatePrRattiRs, 3, 9);
            this.tableLayoutPanelMain.Controls.Add(this.TxtFinlWghtRatti, 3, 8);
            this.tableLayoutPanelMain.Controls.Add(this.TxtPolishPrRatti, 3, 7);
            this.tableLayoutPanelMain.Controls.Add(this.TxtPolishPrGrams, 3, 6);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutFrmTotlWghtRatti, 3, 5);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutPrRatti, 3, 4);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightRatti, 3, 3);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX10, 3, 2);
            this.tableLayoutPanelMain.Controls.Add(this.TxtFinlWghtMasha, 2, 8);
            this.tableLayoutPanelMain.Controls.Add(this.TxtTotlPolishofMasha, 2, 7);
            this.tableLayoutPanelMain.Controls.Add(this.TxtPolishPrMasha, 2, 6);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutFrmTotlWghtMasha, 2, 5);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutPrMasha, 2, 4);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightMasha, 2, 3);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX7, 2, 2);
            this.tableLayoutPanelMain.Controls.Add(this.TxtFinlWghtTola, 1, 8);
            this.tableLayoutPanelMain.Controls.Add(this.TxtTotlPolishofTola, 1, 7);
            this.tableLayoutPanelMain.Controls.Add(this.TxtPolishPrTola, 1, 6);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutFrmTotlWghtTola, 1, 5);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutPrTola, 1, 4);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightTola, 1, 3);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX4, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX16, 0, 8);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX15, 0, 7);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX14, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX3, 0, 5);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX1, 0, 4);
            this.tableLayoutPanelMain.Controls.Add(this.labelX2, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCurRatePrGramsRs, 4, 9);
            this.tableLayoutPanelMain.Controls.Add(this.TxtFinlWghtGrams, 4, 8);
            this.tableLayoutPanelMain.Controls.Add(this.TxtTotlPolishofGrams, 4, 7);
            this.tableLayoutPanelMain.Controls.Add(this.TxtTotlPolishofRatti, 4, 6);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutFrmTotlWghtGrams, 4, 5);
            this.tableLayoutPanelMain.Controls.Add(this.TxtCutPrGrams, 4, 4);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxX9, 4, 2);
            this.tableLayoutPanelMain.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.TxtWeightGrams, 4, 3);
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(5, 6);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 10;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1070, 388);
            this.tableLayoutPanelMain.TabIndex = 77;
            // 
            // panel3
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.txtCurrentTime);
            this.panel3.Location = new System.Drawing.Point(665, 3);
            this.panel3.Name = "panel3";
            this.tableLayoutPanelMain.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(402, 70);
            this.panel3.TabIndex = 108;
            // 
            // txtCurrentTime
            // 
            this.txtCurrentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCurrentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.txtCurrentTime.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCurrentTime.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentTime.ForeColor = System.Drawing.Color.White;
            this.txtCurrentTime.Location = new System.Drawing.Point(89, 15);
            this.txtCurrentTime.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtCurrentTime.Name = "txtCurrentTime";
            this.txtCurrentTime.ReadOnly = true;
            this.txtCurrentTime.Size = new System.Drawing.Size(225, 34);
            this.txtCurrentTime.TabIndex = 7;
            this.txtCurrentTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxX17
            // 
            this.textBoxX17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.textBoxX17.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX17.Border.Class = "TextBoxBorder";
            this.textBoxX17.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX17.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX17.ForeColor = System.Drawing.Color.White;
            this.textBoxX17.Location = new System.Drawing.Point(4, 345);
            this.textBoxX17.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxX17.Name = "textBoxX17";
            this.textBoxX17.ReadOnly = true;
            this.textBoxX17.Size = new System.Drawing.Size(248, 29);
            this.textBoxX17.TabIndex = 73;
            this.textBoxX17.Text = " Current Rate Per Tola Rs.";
            // 
            // TxtCurRatePrTolaRs
            // 
            this.TxtCurRatePrTolaRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCurRatePrTolaRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCurRatePrTolaRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrTolaRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrTolaRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Current_Rate_Per_Tola", true));
            this.TxtCurRatePrTolaRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrTolaRs.ForeColor = System.Drawing.Color.Black;
            this.TxtCurRatePrTolaRs.Location = new System.Drawing.Point(260, 344);
            this.TxtCurRatePrTolaRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCurRatePrTolaRs.Name = "TxtCurRatePrTolaRs";
            this.TxtCurRatePrTolaRs.Size = new System.Drawing.Size(195, 33);
            this.TxtCurRatePrTolaRs.TabIndex = 54;
            this.TxtCurRatePrTolaRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCurRatePrTolaRs.WatermarkText = "0";
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // TxtCurRatePrMashaRs
            // 
            this.TxtCurRatePrMashaRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCurRatePrMashaRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCurRatePrMashaRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrMashaRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrMashaRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrMashaRs.ForeColor = System.Drawing.Color.Black;
            this.TxtCurRatePrMashaRs.Location = new System.Drawing.Point(463, 344);
            this.TxtCurRatePrMashaRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCurRatePrMashaRs.Name = "TxtCurRatePrMashaRs";
            this.TxtCurRatePrMashaRs.Size = new System.Drawing.Size(195, 33);
            this.TxtCurRatePrMashaRs.TabIndex = 64;
            this.TxtCurRatePrMashaRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCurRatePrMashaRs.WatermarkText = "0";
            // 
            // TxtCurRatePrRattiRs
            // 
            this.TxtCurRatePrRattiRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCurRatePrRattiRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCurRatePrRattiRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrRattiRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrRattiRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrRattiRs.ForeColor = System.Drawing.Color.Black;
            this.TxtCurRatePrRattiRs.Location = new System.Drawing.Point(666, 344);
            this.TxtCurRatePrRattiRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCurRatePrRattiRs.Name = "TxtCurRatePrRattiRs";
            this.TxtCurRatePrRattiRs.Size = new System.Drawing.Size(195, 33);
            this.TxtCurRatePrRattiRs.TabIndex = 65;
            this.TxtCurRatePrRattiRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCurRatePrRattiRs.WatermarkText = "0";
            // 
            // TxtFinlWghtRatti
            // 
            this.TxtFinlWghtRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFinlWghtRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtFinlWghtRatti.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtRatti.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtRatti.ForeColor = System.Drawing.Color.Black;
            this.TxtFinlWghtRatti.Location = new System.Drawing.Point(666, 306);
            this.TxtFinlWghtRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtFinlWghtRatti.Name = "TxtFinlWghtRatti";
            this.TxtFinlWghtRatti.Size = new System.Drawing.Size(195, 36);
            this.TxtFinlWghtRatti.TabIndex = 62;
            this.TxtFinlWghtRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtFinlWghtRatti.WatermarkText = "0";
            // 
            // TxtPolishPrRatti
            // 
            this.TxtPolishPrRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPolishPrRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPolishPrRatti.Border.Class = "TextBoxBorder";
            this.TxtPolishPrRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrRatti.ForeColor = System.Drawing.Color.Black;
            this.TxtPolishPrRatti.Location = new System.Drawing.Point(666, 268);
            this.TxtPolishPrRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPolishPrRatti.Name = "TxtPolishPrRatti";
            this.TxtPolishPrRatti.Size = new System.Drawing.Size(195, 33);
            this.TxtPolishPrRatti.TabIndex = 57;
            this.TxtPolishPrRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtPolishPrRatti.WatermarkText = "0";
            // 
            // TxtPolishPrGrams
            // 
            this.TxtPolishPrGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPolishPrGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPolishPrGrams.Border.Class = "TextBoxBorder";
            this.TxtPolishPrGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrGrams.ForeColor = System.Drawing.Color.Black;
            this.TxtPolishPrGrams.Location = new System.Drawing.Point(666, 230);
            this.TxtPolishPrGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPolishPrGrams.Name = "TxtPolishPrGrams";
            this.TxtPolishPrGrams.Size = new System.Drawing.Size(195, 33);
            this.TxtPolishPrGrams.TabIndex = 58;
            this.TxtPolishPrGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtPolishPrGrams.WatermarkText = "0";
            // 
            // TxtCutFrmTotlWghtRatti
            // 
            this.TxtCutFrmTotlWghtRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutFrmTotlWghtRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtRatti.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtRatti.ForeColor = System.Drawing.Color.Black;
            this.TxtCutFrmTotlWghtRatti.Location = new System.Drawing.Point(666, 192);
            this.TxtCutFrmTotlWghtRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutFrmTotlWghtRatti.Name = "TxtCutFrmTotlWghtRatti";
            this.TxtCutFrmTotlWghtRatti.ReadOnly = true;
            this.TxtCutFrmTotlWghtRatti.Size = new System.Drawing.Size(195, 33);
            this.TxtCutFrmTotlWghtRatti.TabIndex = 42;
            this.TxtCutFrmTotlWghtRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutFrmTotlWghtRatti.WatermarkText = "0";
            // 
            // TxtCutPrRatti
            // 
            this.TxtCutPrRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutPrRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutPrRatti.Border.Class = "TextBoxBorder";
            this.TxtCutPrRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrRatti.ForeColor = System.Drawing.Color.Black;
            this.TxtCutPrRatti.Location = new System.Drawing.Point(666, 154);
            this.TxtCutPrRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutPrRatti.Name = "TxtCutPrRatti";
            this.TxtCutPrRatti.Size = new System.Drawing.Size(195, 33);
            this.TxtCutPrRatti.TabIndex = 36;
            this.TxtCutPrRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutPrRatti.WatermarkText = "0";
            // 
            // TxtWeightRatti
            // 
            this.TxtWeightRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightRatti.Border.Class = "TextBoxBorder";
            this.TxtWeightRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightRatti.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightRatti.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightRatti.Location = new System.Drawing.Point(666, 116);
            this.TxtWeightRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightRatti.Name = "TxtWeightRatti";
            this.TxtWeightRatti.Size = new System.Drawing.Size(195, 37);
            this.TxtWeightRatti.TabIndex = 11;
            this.TxtWeightRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightRatti.WatermarkText = "0";
            this.TxtWeightRatti.TextChanged += new System.EventHandler(this.TxtWeightRatti_TextChanged);
            this.TxtWeightRatti.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightRatti.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Enter_KeyPress);
            this.TxtWeightRatti.Leave += new System.EventHandler(this.TxtFieldsLeave);
            // 
            // textBoxX10
            // 
            this.textBoxX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX10.BackColor = System.Drawing.Color.DarkTurquoise;
            // 
            // 
            // 
            this.textBoxX10.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX10.Border.Class = "TextBoxBorder";
            this.textBoxX10.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX10.Font = new System.Drawing.Font("Segoe UI Black", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX10.ForeColor = System.Drawing.Color.White;
            this.textBoxX10.Location = new System.Drawing.Point(666, 78);
            this.textBoxX10.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.textBoxX10.Name = "textBoxX10";
            this.textBoxX10.ReadOnly = true;
            this.textBoxX10.Size = new System.Drawing.Size(195, 30);
            this.textBoxX10.TabIndex = 15;
            this.textBoxX10.Text = "Ratti";
            this.textBoxX10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtFinlWghtMasha
            // 
            this.TxtFinlWghtMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFinlWghtMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtFinlWghtMasha.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtMasha.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtMasha.ForeColor = System.Drawing.Color.Black;
            this.TxtFinlWghtMasha.Location = new System.Drawing.Point(463, 306);
            this.TxtFinlWghtMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtFinlWghtMasha.Name = "TxtFinlWghtMasha";
            this.TxtFinlWghtMasha.Size = new System.Drawing.Size(195, 36);
            this.TxtFinlWghtMasha.TabIndex = 61;
            this.TxtFinlWghtMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtFinlWghtMasha.WatermarkText = "0";
            // 
            // TxtTotlPolishofMasha
            // 
            this.TxtTotlPolishofMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotlPolishofMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtTotlPolishofMasha.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofMasha.ForeColor = System.Drawing.Color.Black;
            this.TxtTotlPolishofMasha.Location = new System.Drawing.Point(463, 268);
            this.TxtTotlPolishofMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtTotlPolishofMasha.Name = "TxtTotlPolishofMasha";
            this.TxtTotlPolishofMasha.Size = new System.Drawing.Size(195, 33);
            this.TxtTotlPolishofMasha.TabIndex = 56;
            this.TxtTotlPolishofMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtTotlPolishofMasha.WatermarkText = "0";
            // 
            // TxtPolishPrMasha
            // 
            this.TxtPolishPrMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPolishPrMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPolishPrMasha.Border.Class = "TextBoxBorder";
            this.TxtPolishPrMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrMasha.ForeColor = System.Drawing.Color.Black;
            this.TxtPolishPrMasha.Location = new System.Drawing.Point(463, 230);
            this.TxtPolishPrMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPolishPrMasha.Name = "TxtPolishPrMasha";
            this.TxtPolishPrMasha.Size = new System.Drawing.Size(195, 33);
            this.TxtPolishPrMasha.TabIndex = 55;
            this.TxtPolishPrMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtPolishPrMasha.WatermarkText = "0";
            // 
            // TxtCutFrmTotlWghtMasha
            // 
            this.TxtCutFrmTotlWghtMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutFrmTotlWghtMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtMasha.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtMasha.ForeColor = System.Drawing.Color.Black;
            this.TxtCutFrmTotlWghtMasha.Location = new System.Drawing.Point(463, 192);
            this.TxtCutFrmTotlWghtMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutFrmTotlWghtMasha.Name = "TxtCutFrmTotlWghtMasha";
            this.TxtCutFrmTotlWghtMasha.ReadOnly = true;
            this.TxtCutFrmTotlWghtMasha.Size = new System.Drawing.Size(195, 33);
            this.TxtCutFrmTotlWghtMasha.TabIndex = 41;
            this.TxtCutFrmTotlWghtMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutFrmTotlWghtMasha.WatermarkText = "0";
            // 
            // TxtCutPrMasha
            // 
            this.TxtCutPrMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutPrMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutPrMasha.Border.Class = "TextBoxBorder";
            this.TxtCutPrMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrMasha.ForeColor = System.Drawing.Color.Black;
            this.TxtCutPrMasha.Location = new System.Drawing.Point(463, 154);
            this.TxtCutPrMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutPrMasha.Name = "TxtCutPrMasha";
            this.TxtCutPrMasha.ReadOnly = true;
            this.TxtCutPrMasha.Size = new System.Drawing.Size(195, 33);
            this.TxtCutPrMasha.TabIndex = 32;
            this.TxtCutPrMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutPrMasha.WatermarkText = "0";
            // 
            // TxtWeightMasha
            // 
            this.TxtWeightMasha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightMasha.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightMasha.Border.Class = "TextBoxBorder";
            this.TxtWeightMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightMasha.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightMasha.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightMasha.Location = new System.Drawing.Point(463, 116);
            this.TxtWeightMasha.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightMasha.Name = "TxtWeightMasha";
            this.TxtWeightMasha.Size = new System.Drawing.Size(195, 37);
            this.TxtWeightMasha.TabIndex = 14;
            this.TxtWeightMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightMasha.WatermarkText = "0";
            this.TxtWeightMasha.TextChanged += new System.EventHandler(this.TxtWeightMasha_TextChanged);
            this.TxtWeightMasha.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightMasha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Enter_KeyPress);
            this.TxtWeightMasha.Leave += new System.EventHandler(this.TxtFieldsLeave);
            // 
            // textBoxX7
            // 
            this.textBoxX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX7.BackColor = System.Drawing.Color.DarkTurquoise;
            // 
            // 
            // 
            this.textBoxX7.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX7.Border.Class = "TextBoxBorder";
            this.textBoxX7.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX7.Font = new System.Drawing.Font("Segoe UI Black", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX7.ForeColor = System.Drawing.Color.White;
            this.textBoxX7.Location = new System.Drawing.Point(463, 78);
            this.textBoxX7.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.textBoxX7.Name = "textBoxX7";
            this.textBoxX7.ReadOnly = true;
            this.textBoxX7.Size = new System.Drawing.Size(195, 30);
            this.textBoxX7.TabIndex = 12;
            this.textBoxX7.Text = "Masha";
            this.textBoxX7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtFinlWghtTola
            // 
            this.TxtFinlWghtTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFinlWghtTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtFinlWghtTola.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtTola.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtTola.ForeColor = System.Drawing.Color.Black;
            this.TxtFinlWghtTola.Location = new System.Drawing.Point(260, 306);
            this.TxtFinlWghtTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtFinlWghtTola.Name = "TxtFinlWghtTola";
            this.TxtFinlWghtTola.Size = new System.Drawing.Size(195, 36);
            this.TxtFinlWghtTola.TabIndex = 53;
            this.TxtFinlWghtTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtFinlWghtTola.WatermarkText = "0";
            // 
            // TxtTotlPolishofTola
            // 
            this.TxtTotlPolishofTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotlPolishofTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtTotlPolishofTola.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Total_Polish", true));
            this.TxtTotlPolishofTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofTola.ForeColor = System.Drawing.Color.Black;
            this.TxtTotlPolishofTola.Location = new System.Drawing.Point(260, 268);
            this.TxtTotlPolishofTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtTotlPolishofTola.Name = "TxtTotlPolishofTola";
            this.TxtTotlPolishofTola.Size = new System.Drawing.Size(195, 33);
            this.TxtTotlPolishofTola.TabIndex = 52;
            this.TxtTotlPolishofTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtTotlPolishofTola.WatermarkText = "0";
            // 
            // TxtPolishPrTola
            // 
            this.TxtPolishPrTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPolishPrTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPolishPrTola.Border.Class = "TextBoxBorder";
            this.TxtPolishPrTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Polish_Per_Tola", true));
            this.TxtPolishPrTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrTola.ForeColor = System.Drawing.Color.Black;
            this.TxtPolishPrTola.Location = new System.Drawing.Point(260, 230);
            this.TxtPolishPrTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPolishPrTola.Name = "TxtPolishPrTola";
            this.TxtPolishPrTola.Size = new System.Drawing.Size(195, 33);
            this.TxtPolishPrTola.TabIndex = 44;
            this.TxtPolishPrTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtPolishPrTola.WatermarkText = "0";
            // 
            // TxtCutFrmTotlWghtTola
            // 
            this.TxtCutFrmTotlWghtTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutFrmTotlWghtTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtTola.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Cut_from_total_Weight", true));
            this.TxtCutFrmTotlWghtTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtTola.ForeColor = System.Drawing.Color.Black;
            this.TxtCutFrmTotlWghtTola.Location = new System.Drawing.Point(260, 192);
            this.TxtCutFrmTotlWghtTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutFrmTotlWghtTola.Name = "TxtCutFrmTotlWghtTola";
            this.TxtCutFrmTotlWghtTola.Size = new System.Drawing.Size(195, 33);
            this.TxtCutFrmTotlWghtTola.TabIndex = 40;
            this.TxtCutFrmTotlWghtTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutFrmTotlWghtTola.WatermarkText = "0";
            // 
            // TxtCutPrTola
            // 
            this.TxtCutPrTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutPrTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutPrTola.Border.Class = "TextBoxBorder";
            this.TxtCutPrTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Cut_Per_Tola", true));
            this.TxtCutPrTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrTola.ForeColor = System.Drawing.Color.Black;
            this.TxtCutPrTola.Location = new System.Drawing.Point(260, 154);
            this.TxtCutPrTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutPrTola.Name = "TxtCutPrTola";
            this.TxtCutPrTola.Size = new System.Drawing.Size(195, 33);
            this.TxtCutPrTola.TabIndex = 14;
            this.TxtCutPrTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutPrTola.WatermarkText = "0";
            // 
            // TxtWeightTola
            // 
            this.TxtWeightTola.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightTola.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightTola.Border.Class = "TextBoxBorder";
            this.TxtWeightTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightTola.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightTola.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightTola.Location = new System.Drawing.Point(260, 116);
            this.TxtWeightTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightTola.Name = "TxtWeightTola";
            this.TxtWeightTola.Size = new System.Drawing.Size(195, 37);
            this.TxtWeightTola.TabIndex = 5;
            this.TxtWeightTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightTola.WatermarkText = "0";
            this.TxtWeightTola.TextChanged += new System.EventHandler(this.TxtWeightTola_TextChanged);
            this.TxtWeightTola.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightTola.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Enter_KeyPress);
            this.TxtWeightTola.Leave += new System.EventHandler(this.TxtFieldsLeave);
            // 
            // textBoxX4
            // 
            this.textBoxX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX4.BackColor = System.Drawing.Color.DarkTurquoise;
            // 
            // 
            // 
            this.textBoxX4.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX4.Border.Class = "TextBoxBorder";
            this.textBoxX4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX4.Font = new System.Drawing.Font("Segoe UI Black", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX4.ForeColor = System.Drawing.Color.White;
            this.textBoxX4.Location = new System.Drawing.Point(260, 78);
            this.textBoxX4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.textBoxX4.Name = "textBoxX4";
            this.textBoxX4.ReadOnly = true;
            this.textBoxX4.Size = new System.Drawing.Size(195, 30);
            this.textBoxX4.TabIndex = 4;
            this.textBoxX4.Text = "Tola";
            this.textBoxX4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxX16
            // 
            this.textBoxX16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.textBoxX16.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX16.Border.Class = "TextBoxBorder";
            this.textBoxX16.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX16.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX16.ForeColor = System.Drawing.Color.White;
            this.textBoxX16.Location = new System.Drawing.Point(4, 307);
            this.textBoxX16.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxX16.Name = "textBoxX16";
            this.textBoxX16.ReadOnly = true;
            this.textBoxX16.Size = new System.Drawing.Size(248, 33);
            this.textBoxX16.TabIndex = 72;
            this.textBoxX16.Text = " Final Weight";
            // 
            // textBoxX15
            // 
            this.textBoxX15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.textBoxX15.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX15.Border.Class = "TextBoxBorder";
            this.textBoxX15.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX15.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX15.ForeColor = System.Drawing.Color.White;
            this.textBoxX15.Location = new System.Drawing.Point(4, 269);
            this.textBoxX15.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxX15.Name = "textBoxX15";
            this.textBoxX15.ReadOnly = true;
            this.textBoxX15.Size = new System.Drawing.Size(248, 29);
            this.textBoxX15.TabIndex = 71;
            this.textBoxX15.Text = " Total Polish";
            // 
            // textBoxX14
            // 
            this.textBoxX14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.textBoxX14.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX14.Border.Class = "TextBoxBorder";
            this.textBoxX14.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX14.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX14.ForeColor = System.Drawing.Color.White;
            this.textBoxX14.Location = new System.Drawing.Point(4, 231);
            this.textBoxX14.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxX14.Name = "textBoxX14";
            this.textBoxX14.ReadOnly = true;
            this.textBoxX14.Size = new System.Drawing.Size(248, 29);
            this.textBoxX14.TabIndex = 70;
            this.textBoxX14.Text = " Polish Per Tola";
            // 
            // textBoxX3
            // 
            this.textBoxX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.textBoxX3.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX3.Border.Class = "TextBoxBorder";
            this.textBoxX3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX3.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX3.ForeColor = System.Drawing.Color.White;
            this.textBoxX3.Location = new System.Drawing.Point(4, 193);
            this.textBoxX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxX3.Name = "textBoxX3";
            this.textBoxX3.ReadOnly = true;
            this.textBoxX3.Size = new System.Drawing.Size(248, 29);
            this.textBoxX3.TabIndex = 69;
            this.textBoxX3.Text = " Cut Frm Total Weight";
            // 
            // textBoxX1
            // 
            this.textBoxX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.textBoxX1.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX1.ForeColor = System.Drawing.Color.White;
            this.textBoxX1.Location = new System.Drawing.Point(4, 155);
            this.textBoxX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.ReadOnly = true;
            this.textBoxX1.Size = new System.Drawing.Size(248, 29);
            this.textBoxX1.TabIndex = 68;
            this.textBoxX1.Text = " Cut Per Tola";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX2.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX2.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX2.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderRightWidth = 2;
            this.labelX2.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX2.BackgroundStyle.BorderTopWidth = 2;
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI Black", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(3, 79);
            this.labelX2.Name = "labelX2";
            this.tableLayoutPanelMain.SetRowSpan(this.labelX2, 2);
            this.labelX2.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX2.Size = new System.Drawing.Size(250, 70);
            this.labelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX2.TabIndex = 105;
            this.labelX2.Text = "Weight";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.buttonX2);
            this.groupBox2.Controls.Add(this.buttonX3);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(183)))), ((int)(((byte)(204)))));
            this.groupBox2.Location = new System.Drawing.Point(4, 6);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanelMain.SetRowSpan(this.groupBox2, 2);
            this.groupBox2.Size = new System.Drawing.Size(248, 63);
            this.groupBox2.TabIndex = 106;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Image = global::Presentation.Properties.Resources.WeightSacle_png;
            this.buttonX2.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.buttonX2.Location = new System.Drawing.Point(14, 19);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.buttonX2.Size = new System.Drawing.Size(107, 39);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.TabIndex = 5;
            this.buttonX2.Text = "Scale";
            this.buttonX2.TextColor = System.Drawing.Color.White;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.buttonX3.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.buttonX3.Location = new System.Drawing.Point(124, 19);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.buttonX3.Size = new System.Drawing.Size(115, 39);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.TabIndex = 4;
            this.buttonX3.Text = "Manual";
            this.buttonX3.TextColor = System.Drawing.Color.White;
            // 
            // TxtCurRatePrGramsRs
            // 
            this.TxtCurRatePrGramsRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCurRatePrGramsRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCurRatePrGramsRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrGramsRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrGramsRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrGramsRs.ForeColor = System.Drawing.Color.Black;
            this.TxtCurRatePrGramsRs.Location = new System.Drawing.Point(869, 344);
            this.TxtCurRatePrGramsRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCurRatePrGramsRs.Name = "TxtCurRatePrGramsRs";
            this.TxtCurRatePrGramsRs.Size = new System.Drawing.Size(197, 33);
            this.TxtCurRatePrGramsRs.TabIndex = 66;
            this.TxtCurRatePrGramsRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCurRatePrGramsRs.WatermarkText = "0";
            // 
            // TxtFinlWghtGrams
            // 
            this.TxtFinlWghtGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFinlWghtGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtFinlWghtGrams.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtGrams.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtGrams.ForeColor = System.Drawing.Color.Black;
            this.TxtFinlWghtGrams.Location = new System.Drawing.Point(869, 306);
            this.TxtFinlWghtGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtFinlWghtGrams.Name = "TxtFinlWghtGrams";
            this.TxtFinlWghtGrams.Size = new System.Drawing.Size(197, 36);
            this.TxtFinlWghtGrams.TabIndex = 63;
            this.TxtFinlWghtGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtFinlWghtGrams.WatermarkText = "0.000";
            // 
            // TxtTotlPolishofGrams
            // 
            this.TxtTotlPolishofGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotlPolishofGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtTotlPolishofGrams.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofGrams.ForeColor = System.Drawing.Color.Black;
            this.TxtTotlPolishofGrams.Location = new System.Drawing.Point(869, 268);
            this.TxtTotlPolishofGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtTotlPolishofGrams.Name = "TxtTotlPolishofGrams";
            this.TxtTotlPolishofGrams.Size = new System.Drawing.Size(197, 33);
            this.TxtTotlPolishofGrams.TabIndex = 60;
            this.TxtTotlPolishofGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtTotlPolishofGrams.WatermarkText = "0";
            // 
            // TxtTotlPolishofRatti
            // 
            this.TxtTotlPolishofRatti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotlPolishofRatti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtTotlPolishofRatti.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofRatti.ForeColor = System.Drawing.Color.Black;
            this.TxtTotlPolishofRatti.Location = new System.Drawing.Point(869, 230);
            this.TxtTotlPolishofRatti.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtTotlPolishofRatti.Name = "TxtTotlPolishofRatti";
            this.TxtTotlPolishofRatti.Size = new System.Drawing.Size(197, 33);
            this.TxtTotlPolishofRatti.TabIndex = 59;
            this.TxtTotlPolishofRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtTotlPolishofRatti.WatermarkText = "0";
            // 
            // TxtCutFrmTotlWghtGrams
            // 
            this.TxtCutFrmTotlWghtGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutFrmTotlWghtGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtGrams.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtGrams.ForeColor = System.Drawing.Color.Black;
            this.TxtCutFrmTotlWghtGrams.Location = new System.Drawing.Point(869, 192);
            this.TxtCutFrmTotlWghtGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutFrmTotlWghtGrams.Name = "TxtCutFrmTotlWghtGrams";
            this.TxtCutFrmTotlWghtGrams.Size = new System.Drawing.Size(197, 33);
            this.TxtCutFrmTotlWghtGrams.TabIndex = 43;
            this.TxtCutFrmTotlWghtGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutFrmTotlWghtGrams.WatermarkText = "0";
            // 
            // TxtCutPrGrams
            // 
            this.TxtCutPrGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCutPrGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCutPrGrams.Border.Class = "TextBoxBorder";
            this.TxtCutPrGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrGrams.ForeColor = System.Drawing.Color.Black;
            this.TxtCutPrGrams.Location = new System.Drawing.Point(869, 154);
            this.TxtCutPrGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtCutPrGrams.Name = "TxtCutPrGrams";
            this.TxtCutPrGrams.Size = new System.Drawing.Size(197, 33);
            this.TxtCutPrGrams.TabIndex = 39;
            this.TxtCutPrGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutPrGrams.WatermarkText = "0";
            // 
            // textBoxX9
            // 
            this.textBoxX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxX9.BackColor = System.Drawing.Color.DarkTurquoise;
            // 
            // 
            // 
            this.textBoxX9.Border.BackColor = System.Drawing.Color.White;
            this.textBoxX9.Border.Class = "TextBoxBorder";
            this.textBoxX9.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX9.Font = new System.Drawing.Font("Segoe UI Black", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX9.ForeColor = System.Drawing.Color.White;
            this.textBoxX9.Location = new System.Drawing.Point(869, 78);
            this.textBoxX9.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.textBoxX9.Name = "textBoxX9";
            this.textBoxX9.ReadOnly = true;
            this.textBoxX9.Size = new System.Drawing.Size(197, 30);
            this.textBoxX9.TabIndex = 13;
            this.textBoxX9.Text = "Grams";
            this.textBoxX9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel2
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.txtCurrentDate);
            this.panel2.Location = new System.Drawing.Point(259, 3);
            this.panel2.Name = "panel2";
            this.tableLayoutPanelMain.SetRowSpan(this.panel2, 2);
            this.panel2.Size = new System.Drawing.Size(400, 70);
            this.panel2.TabIndex = 107;
            // 
            // txtCurrentDate
            // 
            this.txtCurrentDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCurrentDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.txtCurrentDate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCurrentDate.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentDate.ForeColor = System.Drawing.Color.White;
            this.txtCurrentDate.Location = new System.Drawing.Point(88, 19);
            this.txtCurrentDate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtCurrentDate.Name = "txtCurrentDate";
            this.txtCurrentDate.ReadOnly = true;
            this.txtCurrentDate.Size = new System.Drawing.Size(225, 34);
            this.txtCurrentDate.TabIndex = 110;
            this.txtCurrentDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtWeightGrams
            // 
            this.TxtWeightGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtWeightGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtWeightGrams.Border.Class = "TextBoxBorder";
            this.TxtWeightGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightGrams.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Weight_in_Gram", true));
            this.TxtWeightGrams.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightGrams.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightGrams.Location = new System.Drawing.Point(869, 116);
            this.TxtWeightGrams.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtWeightGrams.Name = "TxtWeightGrams";
            this.TxtWeightGrams.Size = new System.Drawing.Size(197, 37);
            this.TxtWeightGrams.TabIndex = 16;
            this.TxtWeightGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtWeightGrams.WatermarkText = "0";
            this.TxtWeightGrams.TextChanged += new System.EventHandler(this.TxtWeightGrams_TextChanged);
            this.TxtWeightGrams.Enter += new System.EventHandler(this.TxtFieldsEnter);
            this.TxtWeightGrams.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtWeightGrams_KeyPress);
            this.TxtWeightGrams.Leave += new System.EventHandler(this.TxtWeightGrams_Leave);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel1.Controls.Add(this.labelX4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX5, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX10, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelX9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TxtPriceofGoldRs, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupPanel3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.TxtItemName, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.CmbClints, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.TxtTotlAmuntRs, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TxtClintNo, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.TxtNetAmuntRs, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.TxtPymtPercntCut, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupPanelLabourPolisCalcu, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.groupPanelWeightinMode, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.groupPanelcutPriority, 0, 4);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 394);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1070, 298);
            this.tableLayoutPanel1.TabIndex = 98;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX4.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderRightWidth = 2;
            this.labelX4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderTopWidth = 2;
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.White;
            this.labelX4.Location = new System.Drawing.Point(3, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX4.Size = new System.Drawing.Size(250, 41);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX4.TabIndex = 104;
            this.labelX4.Text = "Current Price Of Gold :";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX5.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX5.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX5.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderRightWidth = 2;
            this.labelX5.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderTopWidth = 2;
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.White;
            this.labelX5.Location = new System.Drawing.Point(462, 50);
            this.labelX5.Name = "labelX5";
            this.labelX5.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX5.Size = new System.Drawing.Size(122, 41);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX5.TabIndex = 101;
            this.labelX5.Text = "Item Detailes :";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX10.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX10.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX10.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderRightWidth = 2;
            this.labelX10.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderTopWidth = 2;
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.White;
            this.labelX10.Location = new System.Drawing.Point(462, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX10.Size = new System.Drawing.Size(122, 41);
            this.labelX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX10.TabIndex = 100;
            this.labelX10.Text = "Client No and Name :";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX3.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderRightWidth = 2;
            this.labelX3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderTopWidth = 2;
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.White;
            this.labelX3.Location = new System.Drawing.Point(3, 144);
            this.labelX3.Name = "labelX3";
            this.labelX3.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX3.Size = new System.Drawing.Size(250, 41);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX3.TabIndex = 99;
            this.labelX3.Text = " Net Amount in Rs :";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX9.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX9.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX9.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderRightWidth = 2;
            this.labelX9.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderTopWidth = 2;
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.White;
            this.labelX9.Location = new System.Drawing.Point(3, 97);
            this.labelX9.Name = "labelX9";
            this.labelX9.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX9.Size = new System.Drawing.Size(250, 41);
            this.labelX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX9.TabIndex = 100;
            this.labelX9.Text = " Payment/Percent Cut :";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.BtnTransaction);
            this.panel1.Controls.Add(this.BtnClearData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(729, 191);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(338, 104);
            this.panel1.TabIndex = 79;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(105, 79);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(108, 20);
            this.dateTimePicker1.TabIndex = 89;
            // 
            // BtnTransaction
            // 
            this.BtnTransaction.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnTransaction.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnTransaction.BackColor = System.Drawing.Color.Transparent;
            this.BtnTransaction.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnTransaction.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTransaction.Image = global::Presentation.Properties.Resources.HandMoneyjpg;
            this.BtnTransaction.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.BtnTransaction.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.BtnTransaction.Location = new System.Drawing.Point(-15, 21);
            this.BtnTransaction.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.BtnTransaction.Name = "BtnTransaction";
            this.BtnTransaction.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnTransaction.Size = new System.Drawing.Size(192, 52);
            this.BtnTransaction.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnTransaction.TabIndex = 81;
            this.BtnTransaction.Text = "Save Transaction";
            this.BtnTransaction.TextColor = System.Drawing.Color.White;
            this.BtnTransaction.Click += new System.EventHandler(this.BtnTransaction_Click);
            // 
            // BtnClearData
            // 
            this.BtnClearData.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnClearData.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnClearData.BackColor = System.Drawing.Color.Transparent;
            this.BtnClearData.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnClearData.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClearData.Image = global::Presentation.Properties.Resources.Refresh_jpg;
            this.BtnClearData.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.BtnClearData.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.BtnClearData.Location = new System.Drawing.Point(190, 21);
            this.BtnClearData.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.BtnClearData.Name = "BtnClearData";
            this.BtnClearData.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnClearData.Size = new System.Drawing.Size(143, 52);
            this.BtnClearData.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnClearData.TabIndex = 82;
            this.BtnClearData.Text = "Refresh";
            this.BtnClearData.TextColor = System.Drawing.Color.White;
            this.BtnClearData.Click += new System.EventHandler(this.BtnClearData_Click_1);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX1.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderRightWidth = 2;
            this.labelX1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderTopWidth = 2;
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(3, 50);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX1.Size = new System.Drawing.Size(250, 41);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX1.TabIndex = 98;
            this.labelX1.Text = " Total Amount in Rs :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtPriceofGoldRs
            // 
            this.TxtPriceofGoldRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPriceofGoldRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPriceofGoldRs.Border.Class = "TextBoxBorder";
            this.TxtPriceofGoldRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPriceofGoldRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Price_of_Gold", true));
            this.TxtPriceofGoldRs.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceofGoldRs.ForeColor = System.Drawing.Color.Black;
            this.TxtPriceofGoldRs.Location = new System.Drawing.Point(260, 2);
            this.TxtPriceofGoldRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPriceofGoldRs.Name = "TxtPriceofGoldRs";
            this.TxtPriceofGoldRs.Size = new System.Drawing.Size(195, 37);
            this.TxtPriceofGoldRs.TabIndex = 24;
            this.TxtPriceofGoldRs.WatermarkText = "0";
            // 
            // groupPanel3
            // 
            this.groupPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tableLayoutPanel1.SetColumnSpan(this.groupPanel3, 3);
            this.groupPanel3.Controls.Add(this.panel5);
            this.groupPanel3.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel3.Location = new System.Drawing.Point(463, 96);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupPanel3.Name = "groupPanel3";
            this.tableLayoutPanel1.SetRowSpan(this.groupPanel3, 2);
            this.groupPanel3.Size = new System.Drawing.Size(603, 90);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 71;
            this.groupPanel3.Text = "Transaction Type";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.Rdb4CashDebit);
            this.panel5.Controls.Add(this.Rdb3CashCredit);
            this.panel5.Controls.Add(this.Rdb2GoldDebit);
            this.panel5.Controls.Add(this.Rdb1GoldCredit);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(597, 63);
            this.panel5.TabIndex = 0;
            // 
            // Rdb4CashDebit
            // 
            this.Rdb4CashDebit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb4CashDebit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb4CashDebit.ForeColor = System.Drawing.Color.Black;
            this.Rdb4CashDebit.Location = new System.Drawing.Point(353, 27);
            this.Rdb4CashDebit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb4CashDebit.Name = "Rdb4CashDebit";
            this.Rdb4CashDebit.Size = new System.Drawing.Size(250, 25);
            this.Rdb4CashDebit.TabIndex = 37;
            this.Rdb4CashDebit.Text = "4 Cash Debit/Wasool(-)";
            this.Rdb4CashDebit.UseVisualStyleBackColor = false;
            // 
            // Rdb3CashCredit
            // 
            this.Rdb3CashCredit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb3CashCredit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb3CashCredit.ForeColor = System.Drawing.Color.Black;
            this.Rdb3CashCredit.Location = new System.Drawing.Point(353, 1);
            this.Rdb3CashCredit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb3CashCredit.Name = "Rdb3CashCredit";
            this.Rdb3CashCredit.Size = new System.Drawing.Size(250, 25);
            this.Rdb3CashCredit.TabIndex = 36;
            this.Rdb3CashCredit.Text = "3 Cash Credit/Qaraz (+)";
            this.Rdb3CashCredit.UseVisualStyleBackColor = false;
            // 
            // Rdb2GoldDebit
            // 
            this.Rdb2GoldDebit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb2GoldDebit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb2GoldDebit.ForeColor = System.Drawing.Color.Black;
            this.Rdb2GoldDebit.Location = new System.Drawing.Point(95, 27);
            this.Rdb2GoldDebit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb2GoldDebit.Name = "Rdb2GoldDebit";
            this.Rdb2GoldDebit.Size = new System.Drawing.Size(250, 25);
            this.Rdb2GoldDebit.TabIndex = 35;
            this.Rdb2GoldDebit.Text = "2 Gold Debit/Wasool(-)";
            this.Rdb2GoldDebit.UseVisualStyleBackColor = false;
            // 
            // Rdb1GoldCredit
            // 
            this.Rdb1GoldCredit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb1GoldCredit.Checked = true;
            this.Rdb1GoldCredit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb1GoldCredit.ForeColor = System.Drawing.Color.Black;
            this.Rdb1GoldCredit.Location = new System.Drawing.Point(95, 1);
            this.Rdb1GoldCredit.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb1GoldCredit.Name = "Rdb1GoldCredit";
            this.Rdb1GoldCredit.Size = new System.Drawing.Size(250, 25);
            this.Rdb1GoldCredit.TabIndex = 34;
            this.Rdb1GoldCredit.TabStop = true;
            this.Rdb1GoldCredit.Text = "1 Gold Credit/Qaraz (+)";
            this.Rdb1GoldCredit.UseVisualStyleBackColor = false;
            // 
            // TxtItemName
            // 
            this.TxtItemName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtItemName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtItemName.Border.Class = "TextBoxBorder";
            this.TxtItemName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.TxtItemName, 2);
            this.TxtItemName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Itme_Name", true));
            this.TxtItemName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItemName.ForeColor = System.Drawing.Color.Black;
            this.TxtItemName.Location = new System.Drawing.Point(591, 58);
            this.TxtItemName.Margin = new System.Windows.Forms.Padding(4);
            this.TxtItemName.Name = "TxtItemName";
            this.TxtItemName.Size = new System.Drawing.Size(475, 25);
            this.TxtItemName.TabIndex = 3;
            this.TxtItemName.WatermarkText = "Item Details";
            // 
            // CmbClints
            // 
            this.CmbClints.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CmbClints.BackColor = System.Drawing.Color.White;
            this.CmbClints.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.tblTransactionBindingSource, "ClientId", true));
            this.CmbClints.DataSource = this.tblClientBindingSource;
            this.CmbClints.DisplayMember = "C_Name";
            this.CmbClints.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbClints.ForeColor = System.Drawing.Color.Black;
            this.CmbClints.FormattingEnabled = true;
            this.CmbClints.Location = new System.Drawing.Point(730, 11);
            this.CmbClints.Margin = new System.Windows.Forms.Padding(4);
            this.CmbClints.Name = "CmbClints";
            this.CmbClints.Size = new System.Drawing.Size(336, 25);
            this.CmbClints.TabIndex = 31;
            this.CmbClints.ValueMember = "C_Id";
            this.CmbClints.SelectedIndexChanged += new System.EventHandler(this.CmbClints_SelectedIndexChanged_1);
            // 
            // tblClientBindingSource
            // 
            this.tblClientBindingSource.DataSource = typeof(Data.tblClient);
            // 
            // TxtTotlAmuntRs
            // 
            this.TxtTotlAmuntRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotlAmuntRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtTotlAmuntRs.Border.Class = "TextBoxBorder";
            this.TxtTotlAmuntRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlAmuntRs.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlAmuntRs.ForeColor = System.Drawing.Color.Black;
            this.TxtTotlAmuntRs.Location = new System.Drawing.Point(260, 49);
            this.TxtTotlAmuntRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtTotlAmuntRs.Name = "TxtTotlAmuntRs";
            this.TxtTotlAmuntRs.Size = new System.Drawing.Size(195, 37);
            this.TxtTotlAmuntRs.TabIndex = 25;
            this.TxtTotlAmuntRs.WatermarkText = "0";
            // 
            // TxtClintNo
            // 
            this.TxtClintNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtClintNo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtClintNo.Border.Class = "TextBoxBorder";
            this.TxtClintNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtClintNo.Font = new System.Drawing.Font("Segoe UI Black", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClintNo.ForeColor = System.Drawing.Color.Black;
            this.TxtClintNo.Location = new System.Drawing.Point(591, 4);
            this.TxtClintNo.Margin = new System.Windows.Forms.Padding(4);
            this.TxtClintNo.Name = "TxtClintNo";
            this.TxtClintNo.ReadOnly = true;
            this.TxtClintNo.Size = new System.Drawing.Size(131, 31);
            this.TxtClintNo.TabIndex = 22;
            this.TxtClintNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtClintNo.WatermarkText = "Client No";
            // 
            // TxtNetAmuntRs
            // 
            this.TxtNetAmuntRs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtNetAmuntRs.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtNetAmuntRs.Border.Class = "TextBoxBorder";
            this.TxtNetAmuntRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtNetAmuntRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Net_Amount", true));
            this.TxtNetAmuntRs.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetAmuntRs.ForeColor = System.Drawing.Color.Black;
            this.TxtNetAmuntRs.Location = new System.Drawing.Point(260, 143);
            this.TxtNetAmuntRs.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtNetAmuntRs.Name = "TxtNetAmuntRs";
            this.TxtNetAmuntRs.Size = new System.Drawing.Size(195, 37);
            this.TxtNetAmuntRs.TabIndex = 19;
            this.TxtNetAmuntRs.WatermarkText = "0";
            // 
            // TxtPymtPercntCut
            // 
            this.TxtPymtPercntCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPymtPercntCut.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtPymtPercntCut.Border.Class = "TextBoxBorder";
            this.TxtPymtPercntCut.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPymtPercntCut.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Payment_Percnt_Cut", true));
            this.TxtPymtPercntCut.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPymtPercntCut.ForeColor = System.Drawing.Color.Black;
            this.TxtPymtPercntCut.Location = new System.Drawing.Point(260, 96);
            this.TxtPymtPercntCut.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtPymtPercntCut.Name = "TxtPymtPercntCut";
            this.TxtPymtPercntCut.Size = new System.Drawing.Size(195, 37);
            this.TxtPymtPercntCut.TabIndex = 26;
            this.TxtPymtPercntCut.WatermarkText = "0";
            // 
            // groupPanelLabourPolisCalcu
            // 
            this.groupPanelLabourPolisCalcu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupPanelLabourPolisCalcu.BackColor = System.Drawing.Color.Transparent;
            this.groupPanelLabourPolisCalcu.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanelLabourPolisCalcu.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.tableLayoutPanel1.SetColumnSpan(this.groupPanelLabourPolisCalcu, 2);
            this.groupPanelLabourPolisCalcu.Controls.Add(this.RdbFinalWeight);
            this.groupPanelLabourPolisCalcu.Controls.Add(this.RdbTopWeight);
            this.groupPanelLabourPolisCalcu.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelLabourPolisCalcu.Location = new System.Drawing.Point(477, 207);
            this.groupPanelLabourPolisCalcu.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupPanelLabourPolisCalcu.Name = "groupPanelLabourPolisCalcu";
            this.tableLayoutPanel1.SetRowSpan(this.groupPanelLabourPolisCalcu, 3);
            this.groupPanelLabourPolisCalcu.Size = new System.Drawing.Size(230, 71);
            // 
            // 
            // 
            this.groupPanelLabourPolisCalcu.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanelLabourPolisCalcu.Style.BackColorGradientAngle = 90;
            this.groupPanelLabourPolisCalcu.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanelLabourPolisCalcu.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderBottomWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanelLabourPolisCalcu.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderLeftWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderRightWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderTopWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.CornerDiameter = 4;
            this.groupPanelLabourPolisCalcu.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanelLabourPolisCalcu.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanelLabourPolisCalcu.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanelLabourPolisCalcu.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanelLabourPolisCalcu.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanelLabourPolisCalcu.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanelLabourPolisCalcu.TabIndex = 73;
            this.groupPanelLabourPolisCalcu.Text = "Labour & Polish Calculation";
            // 
            // RdbFinalWeight
            // 
            this.RdbFinalWeight.AutoSize = true;
            this.RdbFinalWeight.BackColor = System.Drawing.Color.Transparent;
            this.RdbFinalWeight.Checked = true;
            this.RdbFinalWeight.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbFinalWeight.ForeColor = System.Drawing.Color.Black;
            this.RdbFinalWeight.Location = new System.Drawing.Point(14, 21);
            this.RdbFinalWeight.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.RdbFinalWeight.Name = "RdbFinalWeight";
            this.RdbFinalWeight.Size = new System.Drawing.Size(94, 19);
            this.RdbFinalWeight.TabIndex = 1;
            this.RdbFinalWeight.TabStop = true;
            this.RdbFinalWeight.Text = "Final Weight";
            this.RdbFinalWeight.UseVisualStyleBackColor = false;
            // 
            // RdbTopWeight
            // 
            this.RdbTopWeight.AutoSize = true;
            this.RdbTopWeight.BackColor = System.Drawing.Color.Transparent;
            this.RdbTopWeight.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbTopWeight.ForeColor = System.Drawing.Color.Black;
            this.RdbTopWeight.Location = new System.Drawing.Point(14, 2);
            this.RdbTopWeight.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.RdbTopWeight.Name = "RdbTopWeight";
            this.RdbTopWeight.Size = new System.Drawing.Size(89, 19);
            this.RdbTopWeight.TabIndex = 0;
            this.RdbTopWeight.Text = "Top Weight";
            this.RdbTopWeight.UseVisualStyleBackColor = false;
            // 
            // groupPanelWeightinMode
            // 
            this.groupPanelWeightinMode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupPanelWeightinMode.BackColor = System.Drawing.Color.Transparent;
            this.groupPanelWeightinMode.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanelWeightinMode.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanelWeightinMode.Controls.Add(this.RdbKachTola);
            this.groupPanelWeightinMode.Controls.Add(this.RdbPakaTola);
            this.groupPanelWeightinMode.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelWeightinMode.Location = new System.Drawing.Point(260, 207);
            this.groupPanelWeightinMode.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupPanelWeightinMode.Name = "groupPanelWeightinMode";
            this.tableLayoutPanel1.SetRowSpan(this.groupPanelWeightinMode, 3);
            this.groupPanelWeightinMode.Size = new System.Drawing.Size(195, 71);
            // 
            // 
            // 
            this.groupPanelWeightinMode.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanelWeightinMode.Style.BackColorGradientAngle = 90;
            this.groupPanelWeightinMode.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanelWeightinMode.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderBottomWidth = 1;
            this.groupPanelWeightinMode.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanelWeightinMode.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderLeftWidth = 1;
            this.groupPanelWeightinMode.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderRightWidth = 1;
            this.groupPanelWeightinMode.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderTopWidth = 1;
            this.groupPanelWeightinMode.Style.CornerDiameter = 4;
            this.groupPanelWeightinMode.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanelWeightinMode.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanelWeightinMode.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanelWeightinMode.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanelWeightinMode.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanelWeightinMode.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanelWeightinMode.TabIndex = 72;
            this.groupPanelWeightinMode.Text = "Weighting Mode";
            // 
            // RdbKachTola
            // 
            this.RdbKachTola.AutoSize = true;
            this.RdbKachTola.BackColor = System.Drawing.Color.Transparent;
            this.RdbKachTola.Checked = true;
            this.RdbKachTola.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbKachTola.ForeColor = System.Drawing.Color.Black;
            this.RdbKachTola.Location = new System.Drawing.Point(14, 24);
            this.RdbKachTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.RdbKachTola.Name = "RdbKachTola";
            this.RdbKachTola.Size = new System.Drawing.Size(136, 19);
            this.RdbKachTola.TabIndex = 1;
            this.RdbKachTola.TabStop = true;
            this.RdbKachTola.Text = "Kach Tola (11.664 g)";
            this.RdbKachTola.UseVisualStyleBackColor = false;
            // 
            // RdbPakaTola
            // 
            this.RdbPakaTola.AutoSize = true;
            this.RdbPakaTola.BackColor = System.Drawing.Color.Transparent;
            this.RdbPakaTola.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbPakaTola.ForeColor = System.Drawing.Color.Black;
            this.RdbPakaTola.Location = new System.Drawing.Point(14, 2);
            this.RdbPakaTola.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.RdbPakaTola.Name = "RdbPakaTola";
            this.RdbPakaTola.Size = new System.Drawing.Size(128, 19);
            this.RdbPakaTola.TabIndex = 0;
            this.RdbPakaTola.Text = "Paka Tola (12.15 g)";
            this.RdbPakaTola.UseVisualStyleBackColor = false;
            // 
            // groupPanelcutPriority
            // 
            this.groupPanelcutPriority.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupPanelcutPriority.BackColor = System.Drawing.Color.Transparent;
            this.groupPanelcutPriority.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanelcutPriority.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanelcutPriority.Controls.Add(this.RdbTotlCutFirst);
            this.groupPanelcutPriority.Controls.Add(this.RdbCutPrTolaFirst);
            this.groupPanelcutPriority.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelcutPriority.Location = new System.Drawing.Point(13, 207);
            this.groupPanelcutPriority.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupPanelcutPriority.Name = "groupPanelcutPriority";
            this.tableLayoutPanel1.SetRowSpan(this.groupPanelcutPriority, 3);
            this.groupPanelcutPriority.Size = new System.Drawing.Size(230, 71);
            // 
            // 
            // 
            this.groupPanelcutPriority.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanelcutPriority.Style.BackColorGradientAngle = 90;
            this.groupPanelcutPriority.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanelcutPriority.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderBottomWidth = 1;
            this.groupPanelcutPriority.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanelcutPriority.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderLeftWidth = 1;
            this.groupPanelcutPriority.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderRightWidth = 1;
            this.groupPanelcutPriority.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderTopWidth = 1;
            this.groupPanelcutPriority.Style.CornerDiameter = 4;
            this.groupPanelcutPriority.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanelcutPriority.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanelcutPriority.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanelcutPriority.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanelcutPriority.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanelcutPriority.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanelcutPriority.TabIndex = 74;
            this.groupPanelcutPriority.Text = "Cut Priority";
            // 
            // RdbTotlCutFirst
            // 
            this.RdbTotlCutFirst.AutoSize = true;
            this.RdbTotlCutFirst.BackColor = System.Drawing.Color.Transparent;
            this.RdbTotlCutFirst.Checked = true;
            this.RdbTotlCutFirst.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbTotlCutFirst.ForeColor = System.Drawing.Color.Black;
            this.RdbTotlCutFirst.Location = new System.Drawing.Point(25, 21);
            this.RdbTotlCutFirst.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.RdbTotlCutFirst.Name = "RdbTotlCutFirst";
            this.RdbTotlCutFirst.Size = new System.Drawing.Size(94, 19);
            this.RdbTotlCutFirst.TabIndex = 3;
            this.RdbTotlCutFirst.TabStop = true;
            this.RdbTotlCutFirst.Text = "Total Cut 1st";
            this.RdbTotlCutFirst.UseVisualStyleBackColor = false;
            // 
            // RdbCutPrTolaFirst
            // 
            this.RdbCutPrTolaFirst.AutoSize = true;
            this.RdbCutPrTolaFirst.BackColor = System.Drawing.Color.Transparent;
            this.RdbCutPrTolaFirst.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbCutPrTolaFirst.ForeColor = System.Drawing.Color.Black;
            this.RdbCutPrTolaFirst.Location = new System.Drawing.Point(27, 2);
            this.RdbCutPrTolaFirst.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.RdbCutPrTolaFirst.Name = "RdbCutPrTolaFirst";
            this.RdbCutPrTolaFirst.Size = new System.Drawing.Size(111, 19);
            this.RdbCutPrTolaFirst.TabIndex = 2;
            this.RdbCutPrTolaFirst.Text = "Cut Per Tola 1st";
            this.RdbCutPrTolaFirst.UseVisualStyleBackColor = false;
            // 
            // TransactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(1084, 691);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TransactionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TransactionForm";
            this.Load += new System.EventHandler(this.TransactionForm_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).EndInit();
            this.groupPanelLabourPolisCalcu.ResumeLayout(false);
            this.groupPanelLabourPolisCalcu.PerformLayout();
            this.groupPanelWeightinMode.ResumeLayout(false);
            this.groupPanelWeightinMode.PerformLayout();
            this.groupPanelcutPriority.ResumeLayout(false);
            this.groupPanelcutPriority.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX4;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightTola;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX7;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX10;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX9;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX17;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX16;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX15;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX14;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX3;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrTolaRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrMashaRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrRattiRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrGramsRs;
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private System.Windows.Forms.BindingSource tblClientBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private DevComponents.DotNetBar.ButtonX BtnTransaction;
        private DevComponents.DotNetBar.ButtonX BtnClearData;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPriceofGoldRs;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanelcutPriority;
        private System.Windows.Forms.RadioButton RdbTotlCutFirst;
        private System.Windows.Forms.RadioButton RdbCutPrTolaFirst;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton Rdb4CashDebit;
        private System.Windows.Forms.RadioButton Rdb3CashCredit;
        private System.Windows.Forms.RadioButton Rdb2GoldDebit;
        private System.Windows.Forms.RadioButton Rdb1GoldCredit;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtItemName;
        private System.Windows.Forms.ComboBox CmbClints;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlAmuntRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtClintNo;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtNetAmuntRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPymtPercntCut;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanelLabourPolisCalcu;
        private System.Windows.Forms.RadioButton RdbFinalWeight;
        private System.Windows.Forms.RadioButton RdbTopWeight;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanelWeightinMode;
        private System.Windows.Forms.RadioButton RdbKachTola;
        private System.Windows.Forms.RadioButton RdbPakaTola;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCurrentDate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCurrentTime;


    }
}