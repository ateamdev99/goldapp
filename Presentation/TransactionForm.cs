﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;
using System.Data.Entity.Core.Common.Utils;

namespace Presentation
{
    public partial class TransactionForm : Form
    {
        //bool isTyping = false;
        bool isGramTyping = false;
        bool isRattiTyping = false;
        bool isMashayping = false;
        bool isTolaTyping = false;
        public double ratti = 0.1215;
        public double masha = 0.972;
        public double tola = 11.664;
        public TransactionForm(tblTransaction obj)
        {
            InitializeComponent();
            timer1.Start();
            if (obj == null || obj.T_Id == 0)
            {
                tblTransactionBindingSource.DataSource = new tblTransaction();
                //tblClientBindingSource.DataSource = ClientServicesClass.GetAllClients();
            }
            else
            {
                int WM = obj.Weightin_Mode;
                int CP = obj.Cut_Priority;
                int LPC = obj.Labour_And_Polish_Calculation;
                int CD = obj.Tran_Type;
                if (WM == 0){ RdbPakaTola.Checked = true; } else { RdbKachTola.Checked = true; }
                if (WM == 0) { RdbCutPrTolaFirst.Checked = true;} else { RdbTotlCutFirst.Checked = true; }
                if (LPC == 0) {RdbTopWeight.Checked = true;} else { RdbFinalWeight.Checked = true; }
                if (CD == 1) { Rdb1GoldCredit.Checked = true; }
                else if (CD == 2) { Rdb2GoldDebit.Checked = true; }
                else if (CD == 3) { Rdb3CashCredit.Checked = true; }
                else if (CD == 4) { Rdb4CashDebit.Checked = true; }
                //else if (CD == 5) { RdbNone.Checked = true; }
                tblClientBindingSource.DataSource = ClientServicesClass.GetAllClients();
                CmbClints.SelectedValue = obj.ClientId;
                TxtClintNo.Text = obj.ClientId.ToString();
                tblTransactionBindingSource.DataSource = obj;
                BtnTransaction.Text = "Update Transaction";
            }
        }
        private void TransactionForm_Load(object sender, EventArgs e)
        {
            RestForm();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            txtCurrentTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            txtCurrentDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
        }
        void CmbPopulate()
        {
            var dt = Core.DBUtils.GetDataTable("select C_Name, C_Id ,C_MobileNo from tblClient where C_Id != 1");
            CmbClints.DataSource = dt;
        }
        //void RestCalculation()
        //{
        //    TxtWeightGrams.Clear();
        //    TxtWeightMasha.Clear();
        //    TxtWeightTola.Clear();
        //    TxtWeightRatti.Clear();
        //    TxtWeightGrams.Focus();
        //    TxtCutPrTola.Clear();
        //    TxtCutFrmTotlWghtTola.Clear();
        //    TxtPolishPrTola.Clear();
        //    TxtTotlPolishofTola.Clear();
        //    TxtCurRatePrTolaRs.Clear();
        //    TxtFinlWghtTola.Clear();
        //    //TxtLabourPerTolaRs.Clear();
        //    TxtPriceofGoldRs.Clear();
        //    //TxtTotlLaburRs.Clear();
        //    TxtTotlAmuntRs.Clear();
        //    TxtPymtPercntCut.Clear();
        //    TxtNetAmuntRs.Clear();
        //}
        void RestForm()
        {
            CmbPopulate();
            TxtWeightGrams.Clear();
            TxtWeightMasha.Clear();
            TxtWeightTola.Clear();
            TxtWeightRatti.Clear();
            TxtWeightGrams.Focus();
            TxtCutPrTola.Clear();
            TxtCutPrMasha.Clear();
            TxtCutPrRatti.Clear();
            TxtCutPrGrams.Clear();
            TxtCutFrmTotlWghtTola.Clear();
            TxtCutFrmTotlWghtMasha.Clear();
            TxtCutFrmTotlWghtRatti.Clear();
            TxtCutFrmTotlWghtGrams.Clear();
            TxtPolishPrTola.Clear();
            TxtPolishPrMasha.Clear();
            TxtPolishPrRatti.Clear();
            TxtPolishPrGrams.Clear();
            TxtTotlPolishofTola.Clear();
            TxtTotlPolishofMasha.Clear();
            TxtTotlPolishofRatti.Clear();
            TxtTotlPolishofGrams.Clear();
            TxtCurRatePrTolaRs.Clear();
            TxtCurRatePrRattiRs.Clear();
            TxtCurRatePrMashaRs.Clear();
            TxtCurRatePrGramsRs.Clear();
            TxtFinlWghtTola.Clear();
            TxtFinlWghtRatti.Clear();
            TxtFinlWghtMasha.Clear();
            TxtFinlWghtGrams.Clear();
            //TxtLabourPerTolaRs.Clear();
            TxtPriceofGoldRs.Clear();
            //TxtTotlLaburRs.Clear();
            TxtTotlAmuntRs.Clear();
            TxtPymtPercntCut.Clear();
            TxtNetAmuntRs.Clear();
            RdbKachTola.Checked = true;
            RdbTotlCutFirst.Checked = true;
            RdbFinalWeight.Checked = true;
            Rdb1GoldCredit.Checked = true;
            //TxtSerialNo.Clear();
            TxtClintNo.Clear();
            TxtItemName.Clear();
            BtnTransaction.Text = "Save Transaction";
            tblTransactionBindingSource.DataSource = new tblTransaction();
            dateTimePicker1.Value = DateTime.Now.Date;

        }
        public enum CreditDebit
        {
            GoldCredit = 1,
            GoldDebit = 2,
            CashCredit = 3,
            CashDebit = 4,
            None = 5
        }
        private int GetCreditDebit()
        {
            if (Rdb1GoldCredit.Checked)
            {
                return (int)CreditDebit.GoldCredit;
            }
            if (Rdb2GoldDebit.Checked)
            {
                return (int)CreditDebit.GoldDebit;
            }
            if (Rdb3CashCredit.Checked)
            {
                return (int)CreditDebit.CashCredit;
            } if (Rdb4CashDebit.Checked)
            {
                return (int)CreditDebit.CashDebit;
            }
            return (int)CreditDebit.None;
        }
        public enum WeghtingMode
        {
            PakaTola = 0,
            KatchTola = 1

        }
        public int WeghtingModeGet()
        {
            if (RdbPakaTola.Checked)
            {
                return (int)WeghtingMode.PakaTola;
            }

            return (int)WeghtingMode.KatchTola;
        }
        public enum CutPeriorty
        {
            CutPerTolaFirst = 0,
            TotalCutFirst = 1

        }
        public int CutPeriortyGet()
        {
            if (RdbCutPrTolaFirst.Checked)
            {
                return (int)CutPeriorty.CutPerTolaFirst;
            }

            return (int)CutPeriorty.TotalCutFirst;
        }
        public enum LburPolisCalcu
        {
            TopWeight = 0,
            FinalWeight = 1
        }
        public int LburPolisCalcuGet()
        {
            if (RdbTopWeight.Checked)
            {
                return (int)LburPolisCalcu.TopWeight;
            }

            return (int)LburPolisCalcu.FinalWeight;
        }
        private void CmbClints_TextChanged(object sender, EventArgs e)
        {
           // tblClientBindingSource.DataSource = ClientServicesClass.GetAllClients();
            //CmbClints.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //CmbClints.AutoCompleteSource = AutoCompleteSource.ListItems;

        }
        private void BtnClearData_Click_1(object sender, EventArgs e)
        {
            RestForm();
        }
        private void BtnTransaction_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(TxtWeightGrams.Text))
                {
                    MessageBox.Show("Enter Weight in Grams", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtWeightGrams.Focus();
                }
                
                else if (string.IsNullOrEmpty(TxtItemName.Text))
                {
                    MessageBox.Show("Enter Item Name", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtItemName.Focus();
                }
                //else if (string.IsNullOrEmpty(TxtClintNo.Text))
                //{
                //    MessageBox.Show("Select Client", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                //else if (CmbClints.SelectedIndex < 0)
                //{
                //    MessageBox.Show("Select Client", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                else
                {
                   
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;

                    if (tblObj != null && tblObj.T_Id == 0)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(TxtCutPrTola.Text)) { tblObj.Cut_Per_Tola = 0; }
                            if (string.IsNullOrEmpty(TxtCutFrmTotlWghtTola.Text)) { tblObj.Cut_from_total_Weight = 0; }
                            if (string.IsNullOrEmpty(TxtPolishPrTola.Text)) { tblObj.Polish_Per_Tola = 0; }
                            if (string.IsNullOrEmpty(TxtTotlPolishofTola.Text)) { tblObj.Total_Polish = 0; }
                            if (string.IsNullOrEmpty(TxtCurRatePrTolaRs.Text)) { tblObj.Current_Rate_Per_Tola = 0; }
                            if (string.IsNullOrEmpty(TxtPriceofGoldRs.Text)) { tblObj.Price_of_Gold = 0; }
                            if (string.IsNullOrEmpty(TxtNetAmuntRs.Text)) { tblObj.Net_Amount = 0; }
                            if (string.IsNullOrEmpty(TxtPymtPercntCut.Text)) { tblObj.Payment_Percnt_Cut = 0; }


                            tblObj.Cut_Priority = CutPeriortyGet();
                            tblObj.Weightin_Mode = WeghtingModeGet();
                            tblObj.Labour_And_Polish_Calculation = LburPolisCalcuGet();
                            if (tblObj.ClientId == null){ tblObj.ClientId = 1; }else{tblObj.ClientId = Convert.ToInt32(CmbClints.SelectedValue);}
                            if (tblObj.T_Id == 0)
                            { tblObj.T_Date = dateTimePicker1.Value; }
                            tblObj.Tran_Type = GetCreditDebit();
                            tblObj.Added_Date = DateTime.Now.Date;
                            tblObj.Added_By = "Admin";
                            tblObj.Tran_Status = "Nill";
                            tblObj.IsTeleConfirmation = false;
                            //tblObj.TeleDate = DateTime.MinValue;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.Purity = 1000;
                            tblObj.Labour_Per_Tola = 0;
                            tblObj.Total_Labour = 0;
                            TransactionServicesClass.Insert(tblObj);
                            MessageBox.Show("Transaction Data Inserted", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            RestForm();

                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        
                        
                    }
                    else
                    {
                        try
                        {
                            if (tblObj.ClientId == 0) { tblObj.ClientId = 1; } else { tblObj.ClientId = Convert.ToInt32(CmbClints.SelectedValue); }
                            if (tblObj.IsTeleConfirmation == true) { tblObj.IsTeleConfirmation = true; } else { tblObj.IsTeleConfirmation = false; tblObj.TeleDate = DateTime.Now; }
                            tblObj.ClientId = Convert.ToInt32(CmbClints.SelectedValue);
                            tblObj.Tran_Type = GetCreditDebit();
                            tblObj.Added_Date = DateTime.Now.Date;
                            tblObj.Added_By = "Admin";
                            tblObj.Tran_Status = "Nill";
                            tblObj.Purity = 1000;
                            tblObj.Labour_Per_Tola = 0;
                            tblObj.Total_Labour = 0;
                            TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                            MessageBox.Show("Transaction Data Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            RestForm();
                            this.Close();
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        private void CmbClints_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (CmbClints.SelectedIndex > -1) { TxtClintNo.Text = CmbClints.SelectedValue.ToString(); }
        }
        private void TxtFieldsEnter(object sender, EventArgs e)
        {
            if(object.ReferenceEquals(TxtWeightGrams,sender))
            {
                isGramTyping = true;
                isRattiTyping = false;
                isMashayping = false;
                isTolaTyping = false;
            }
            else if (object.ReferenceEquals(TxtWeightRatti, sender))
            {
                isRattiTyping = true;
                isGramTyping = false;
                isMashayping = false;
                isTolaTyping = false;
            }
            else if (object.ReferenceEquals(TxtWeightMasha, sender))
            {
                isMashayping = true;
                isGramTyping = false;
                isRattiTyping = false;
                isTolaTyping = false;
            }
            else if (object.ReferenceEquals(TxtWeightTola, sender))
            {
                isTolaTyping = true;
                isGramTyping = false;
                isRattiTyping = false;
                isMashayping = false;
            }
        }
        private void TxtFieldsLeave(object sender, EventArgs e)
        {
             
        }
        private void TxtWeightGrams_TextChanged(object sender, EventArgs e)
       {
           if (isGramTyping)
           {
               double zerovalue = 0.0;
               double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);
               Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);
               TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
               TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
               TxtWeightTola.Text = ratiMashaTola["tola"].ToString();
           }
        }
        private void TxtWeightRatti_TextChanged(object sender, EventArgs e)
        {
            double valueInMasha = Conversion.toDouble(TxtWeightMasha.Text);
            double valueInTola = Conversion.toDouble(TxtWeightTola.Text);
            double valueInRatti = Conversion.toDouble(TxtWeightRatti.Text);
            double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;
            
            if (isRattiTyping)
            {
                TxtWeightGrams.Text = totalInGrams.ToString();
            }
        }
        private void Enter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);

                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);
                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                TxtWeightTola.Text = ratiMashaTola["tola"].ToString();

            }
            
        }

        private void TxtWeightMasha_TextChanged(object sender, EventArgs e)
        {
            double valueInRatti = Conversion.toDouble(TxtWeightRatti.Text);
            double valueInTola = Conversion.toDouble(TxtWeightTola.Text);
            double valueInMasha = Conversion.toDouble(TxtWeightMasha.Text);
            double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;


            if (isMashayping)
            {
                double zerovalue = 0.0;
                TxtWeightGrams.Text = totalInGrams.ToString();
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);

                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);

                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                //TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                TxtWeightTola.Text = ratiMashaTola["tola"].ToString();
            }
        }

        private void TxtWeightTola_TextChanged(object sender, EventArgs e)
        {
            double valueInRatti = Conversion.toDouble(TxtWeightRatti.Text);
            double valueInMasha = Conversion.toDouble(TxtWeightMasha.Text);
            double valueInTola = Conversion.toDouble(TxtWeightTola.Text);
            double totalInGrams = valueInRatti * 0.1215 + valueInMasha * 0.972 + valueInTola * 11.664;


            if (isTolaTyping)
            {
                double zerovalue = 0.0;
                TxtWeightGrams.Text = totalInGrams.ToString();
                double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);

                Dictionary<string, double> ratiMashaTola = Conversion.ConvertToTolaMshaRatti(txtDblGrm);

                TxtWeightRatti.Text = ratiMashaTola["ratti"].ToString();
                TxtWeightMasha.Text = ratiMashaTola["masha"].ToString();
                //TxtWeightTola.Text = ratiMashaTola["tola"].ToString();
            }
        }

        private void TxtWeightGrams_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 5)
                {
                    e.Handled = true;
                }

            }
        }

        private void TxtWeightGrams_Leave(object sender, EventArgs e)
        {
            bool isGramTyping = false;
            double txtDblGrm = Conversion.toDouble(TxtWeightGrams.Text);
            if (txtDblGrm <= ratti)
            {
                TxtWeightRatti.Text = Conversion.ConvertToRatti(txtDblGrm).ToString();

            }
            else if (txtDblGrm > ratti && txtDblGrm < masha)
            {
                TxtWeightRatti.Text = Conversion.ConvertToRatti(txtDblGrm).ToString();

            }
            else if (txtDblGrm > masha && txtDblGrm < tola)
            {
                TxtWeightMasha.Text = Conversion.ConvertToMash(txtDblGrm).ToString();

            }
            else if (txtDblGrm > tola)
            {
                TxtWeightTola.Text = Conversion.ConvertToTola(txtDblGrm).ToString();

            }

        }
    }
}
