﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class ClientListForm : Form
    {
        public ClientListForm()
        {
            InitializeComponent();
        }

        private void ClientListForm_Load(object sender, EventArgs e)
        {
            DGVPopulate();
        }
        void DGVPopulate()
        {
            DataTable dt = Core.DBUtils.GetDataTable("SELECT  c.C_Id,c.Name,c.MobileNo , c.GoldOpeningBalance ,c.CashOpeningBalance ,c.Address, ct.City_Name, c.AddedDate FROM tblCity as ct INNER JOIN tblClient as c ON ct.City_Id = c.CityId  where ActivateAccount=0");
            
            dataGridViewX1.DataSource = dt;
            int n = dataGridViewX1.Columns.Count;
            dataGridViewX1.Columns["Edit"].DisplayIndex =9 ;
            dataGridViewX1.Columns["Edit"].Width = 50;
            dataGridViewX1.Columns["Remove"].DisplayIndex =9;
            dataGridViewX1.Columns["Remove"].Width = 60;

            dataGridViewX1.Columns["C_Id"].Visible = false;
            dataGridViewX1.Columns["Name"].HeaderText = "Client Name";
            //dataGridViewX1.Columns["Name"].Width = 198;
            dataGridViewX1.Columns["MobileNo"].HeaderText = "Mobile No";
            dataGridViewX1.Columns["GoldOpeningBalance"].HeaderText = "GoldOpeningBalance";
            //dataGridViewX1.Columns["Itme_Name"].Width = 300;
            dataGridViewX1.Columns["CashOpeningBalance"].HeaderText = "CashOpeningBalance";
            dataGridViewX1.Columns["Address"].HeaderText = "Address";
            dataGridViewX1.Columns["City_Name"].HeaderText = "City";
            dataGridViewX1.Columns["AddedDate"].HeaderText = "OpeningDate";
            //for (int i = 0; i < dataGridViewX1.RowCount; i++)
            //{
            //    if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "1")
            //    { dataGridViewX1.Rows[i].Cells["Action"].Value = "Add"; }
            //    else if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "2")
            //    { dataGridViewX1.Rows[i].Cells["Action"].Value = "Edit"; }
            //    else { dataGridViewX1.Rows[i].Cells["Action"].Value = "History"; }
            //}
        }

        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridViewX1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (e.ColumnIndex == 0 && e.RowIndex > -1)
            {
                try
                {
                    //if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Edit"].Value) == "Edit")
                    //{
                         tblClient obj = new tblClient();
                          int clientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                        obj.C_Id = clientId;
                        Presentation.ClintInfoForm jwfrm = new ClintInfoForm(obj, 0);
                        jwfrm.ShowDialog();
                        DGVPopulate();
                    //}
                    //else if(Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Remove"].Value) == "Remove"){}
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            if (e.ColumnIndex == 1 && e.RowIndex > -1)
            {
                try
                {
                    int clientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                    if (clientId > 0)
                    {
                        DialogResult DResult;
                        DResult = MessageBox.Show("Click Yes to Delete Client and Its Transactions !", "Are You Sure to Process ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (DResult == DialogResult.Yes)
                        {
                            DBUtils.ExecuteSQL("Update tblClient set ActivateAccount=1 where C_Id='" + clientId + "'");
                            DBUtils.ExecuteSQL("Update tblTransaction set IsDeleted=1 where ClientId='" + clientId + "'");
                            this.Close();
                        }
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }

                DGVPopulate();
            }
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            tblClient obj = new tblClient();
            Presentation.ClintInfoForm jwfrm = new ClintInfoForm(obj, 0);
            jwfrm.ShowDialog();
            DGVPopulate();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

            ReportViewerForm frm = new ReportViewerForm();
            frm.ShowDialog();
        }
    }
}
