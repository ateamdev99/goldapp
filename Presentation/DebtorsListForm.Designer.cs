﻿namespace Presentation
{
    partial class DebtorsListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dateTimeInput1 = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.ChkSelectCity = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.BtnClose = new DevComponents.DotNetBar.ButtonX();
            this.ChkSelectDistrict = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.RdbLstAllClients = new System.Windows.Forms.RadioButton();
            this.RdbLstDebtors = new System.Windows.Forms.RadioButton();
            this.RdbLstCreditors = new System.Windows.Forms.RadioButton();
            this.txtNoOfclients = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtGoldBlnce = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCashblnc = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonX();
            this.BtnPreview = new DevComponents.DotNetBar.ButtonX();
            this.ChkDisplyTemAcc = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.CmbDistrict = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CmbCity = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.LblHeading = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeInput1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0.9802952F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.67977F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.67977F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.22006F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.22006F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.22006F));
            this.tableLayoutPanel1.Controls.Add(this.dateTimeInput1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ChkSelectCity, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnClose, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.ChkSelectDistrict, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.RdbLstAllClients, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.RdbLstDebtors, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.RdbLstCreditors, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNoOfclients, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TxtGoldBlnce, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.TxtCashblnc, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnPrint, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.BtnPreview, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.ChkDisplyTemAcc, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.CmbDistrict, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.CmbCity, 2, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 418);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(924, 154);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // dateTimeInput1
            // 
            this.dateTimeInput1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.dateTimeInput1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dateTimeInput1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dateTimeInput1.ButtonDropDown.Visible = true;
            this.dateTimeInput1.CustomFormat = "dd/MM/yyyy";
            this.dateTimeInput1.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeInput1.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dateTimeInput1.IsPopupCalendarOpen = false;
            this.dateTimeInput1.Location = new System.Drawing.Point(312, 2);
            this.dateTimeInput1.Margin = new System.Windows.Forms.Padding(2);
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dateTimeInput1.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dateTimeInput1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.MonthCalendar.DisplayMonth = new System.DateTime(2021, 1, 1, 0, 0, 0, 0);
            this.dateTimeInput1.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dateTimeInput1.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dateTimeInput1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dateTimeInput1.MonthCalendar.TodayButtonVisible = true;
            this.dateTimeInput1.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dateTimeInput1.Name = "dateTimeInput1";
            this.dateTimeInput1.Size = new System.Drawing.Size(297, 27);
            this.dateTimeInput1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dateTimeInput1.TabIndex = 4;
            // 
            // ChkSelectCity
            // 
            this.ChkSelectCity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkSelectCity.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkSelectCity.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectCity.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.ChkSelectCity.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectCity.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectCity.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectCity.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkSelectCity.CheckSignSize = new System.Drawing.Size(15, 15);
            this.ChkSelectCity.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSelectCity.Location = new System.Drawing.Point(312, 32);
            this.ChkSelectCity.Margin = new System.Windows.Forms.Padding(2);
            this.ChkSelectCity.Name = "ChkSelectCity";
            this.ChkSelectCity.Size = new System.Drawing.Size(297, 26);
            this.ChkSelectCity.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkSelectCity.TabIndex = 41;
            this.ChkSelectCity.Text = "Select City";
            this.ChkSelectCity.TextColor = System.Drawing.Color.Black;
            this.ChkSelectCity.CheckedChanged += new System.EventHandler(this.checkBoxX2_CheckedChanged);
            // 
            // BtnClose
            // 
            this.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnClose.BackColor = System.Drawing.Color.Transparent;
            this.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnClose.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClose.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnClose.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnClose.Location = new System.Drawing.Point(823, 92);
            this.BtnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnClose.Name = "BtnClose";
            this.tableLayoutPanel1.SetRowSpan(this.BtnClose, 2);
            this.BtnClose.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnClose.Size = new System.Drawing.Size(94, 60);
            this.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnClose.TabIndex = 112;
            this.BtnClose.Text = "Close";
            this.BtnClose.TextColor = System.Drawing.Color.White;
            // 
            // ChkSelectDistrict
            // 
            this.ChkSelectDistrict.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkSelectDistrict.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkSelectDistrict.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectDistrict.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.ChkSelectDistrict.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectDistrict.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectDistrict.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkSelectDistrict.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkSelectDistrict.CheckSignSize = new System.Drawing.Size(15, 15);
            this.ChkSelectDistrict.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSelectDistrict.Location = new System.Drawing.Point(312, 92);
            this.ChkSelectDistrict.Margin = new System.Windows.Forms.Padding(2);
            this.ChkSelectDistrict.Name = "ChkSelectDistrict";
            this.ChkSelectDistrict.Size = new System.Drawing.Size(297, 26);
            this.ChkSelectDistrict.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkSelectDistrict.TabIndex = 40;
            this.ChkSelectDistrict.Text = "Select District";
            this.ChkSelectDistrict.TextColor = System.Drawing.Color.Black;
            // 
            // RdbLstAllClients
            // 
            this.RdbLstAllClients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RdbLstAllClients.BackColor = System.Drawing.Color.Transparent;
            this.RdbLstAllClients.Checked = true;
            this.RdbLstAllClients.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbLstAllClients.ForeColor = System.Drawing.Color.Black;
            this.RdbLstAllClients.Location = new System.Drawing.Point(12, 3);
            this.RdbLstAllClients.Name = "RdbLstAllClients";
            this.RdbLstAllClients.Size = new System.Drawing.Size(295, 24);
            this.RdbLstAllClients.TabIndex = 37;
            this.RdbLstAllClients.TabStop = true;
            this.RdbLstAllClients.Text = "List All Clients";
            this.RdbLstAllClients.UseVisualStyleBackColor = false;
            this.RdbLstAllClients.CheckedChanged += new System.EventHandler(this.RdbLstAllClients_CheckedChanged);
            // 
            // RdbLstDebtors
            // 
            this.RdbLstDebtors.BackColor = System.Drawing.Color.Transparent;
            this.RdbLstDebtors.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbLstDebtors.ForeColor = System.Drawing.Color.Black;
            this.RdbLstDebtors.Location = new System.Drawing.Point(12, 33);
            this.RdbLstDebtors.Name = "RdbLstDebtors";
            this.RdbLstDebtors.Size = new System.Drawing.Size(188, 20);
            this.RdbLstDebtors.TabIndex = 39;
            this.RdbLstDebtors.Text = "List Debtors";
            this.RdbLstDebtors.UseVisualStyleBackColor = false;
            this.RdbLstDebtors.CheckedChanged += new System.EventHandler(this.RdbLstDebtors_CheckedChanged);
            // 
            // RdbLstCreditors
            // 
            this.RdbLstCreditors.BackColor = System.Drawing.Color.Transparent;
            this.RdbLstCreditors.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbLstCreditors.ForeColor = System.Drawing.Color.Black;
            this.RdbLstCreditors.Location = new System.Drawing.Point(12, 63);
            this.RdbLstCreditors.Name = "RdbLstCreditors";
            this.RdbLstCreditors.Size = new System.Drawing.Size(188, 20);
            this.RdbLstCreditors.TabIndex = 38;
            this.RdbLstCreditors.Text = "List Creditors";
            this.RdbLstCreditors.UseVisualStyleBackColor = false;
            this.RdbLstCreditors.CheckedChanged += new System.EventHandler(this.RdbLstCreditors_CheckedChanged);
            // 
            // txtNoOfclients
            // 
            this.txtNoOfclients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoOfclients.BackColor = System.Drawing.Color.Linen;
            // 
            // 
            // 
            this.txtNoOfclients.Border.BackColor = System.Drawing.Color.White;
            this.txtNoOfclients.Border.Class = "TextBoxBorder";
            this.txtNoOfclients.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNoOfclients, 2);
            this.txtNoOfclients.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoOfclients.ForeColor = System.Drawing.Color.Red;
            this.txtNoOfclients.Location = new System.Drawing.Point(3, 122);
            this.txtNoOfclients.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNoOfclients.Name = "txtNoOfclients";
            this.txtNoOfclients.ReadOnly = true;
            this.txtNoOfclients.Size = new System.Drawing.Size(304, 29);
            this.txtNoOfclients.TabIndex = 107;
            this.txtNoOfclients.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNoOfclients.WatermarkText = " Number of Clients:00";
            // 
            // TxtGoldBlnce
            // 
            this.TxtGoldBlnce.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtGoldBlnce.BackColor = System.Drawing.Color.Linen;
            // 
            // 
            // 
            this.TxtGoldBlnce.Border.BackColor = System.Drawing.Color.White;
            this.TxtGoldBlnce.Border.Class = "TextBoxBorder";
            this.TxtGoldBlnce.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.TxtGoldBlnce, 3);
            this.TxtGoldBlnce.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGoldBlnce.ForeColor = System.Drawing.Color.Red;
            this.TxtGoldBlnce.Location = new System.Drawing.Point(614, 2);
            this.TxtGoldBlnce.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TxtGoldBlnce.Name = "TxtGoldBlnce";
            this.TxtGoldBlnce.ReadOnly = true;
            this.TxtGoldBlnce.Size = new System.Drawing.Size(307, 29);
            this.TxtGoldBlnce.TabIndex = 109;
            this.TxtGoldBlnce.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtGoldBlnce.WatermarkText = "Gold Balance :000.00";
            // 
            // TxtCashblnc
            // 
            this.TxtCashblnc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCashblnc.BackColor = System.Drawing.Color.Linen;
            // 
            // 
            // 
            this.TxtCashblnc.Border.BackColor = System.Drawing.Color.White;
            this.TxtCashblnc.Border.Class = "TextBoxBorder";
            this.TxtCashblnc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.TxtCashblnc, 3);
            this.TxtCashblnc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCashblnc.ForeColor = System.Drawing.Color.Red;
            this.TxtCashblnc.Location = new System.Drawing.Point(614, 32);
            this.TxtCashblnc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TxtCashblnc.Name = "TxtCashblnc";
            this.TxtCashblnc.ReadOnly = true;
            this.TxtCashblnc.Size = new System.Drawing.Size(307, 29);
            this.TxtCashblnc.TabIndex = 110;
            this.TxtCashblnc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtCashblnc.WatermarkText = "Cash Balance Rs :0,00/-";
            // 
            // BtnPrint
            // 
            this.BtnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnPrint.BackColor = System.Drawing.Color.Transparent;
            this.BtnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnPrint.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnPrint.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnPrint.Location = new System.Drawing.Point(615, 92);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnPrint.Name = "BtnPrint";
            this.tableLayoutPanel1.SetRowSpan(this.BtnPrint, 2);
            this.BtnPrint.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnPrint.Size = new System.Drawing.Size(94, 60);
            this.BtnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPrint.TabIndex = 108;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.TextColor = System.Drawing.Color.White;
            // 
            // BtnPreview
            // 
            this.BtnPreview.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.BtnPreview.BackColor = System.Drawing.Color.Transparent;
            this.BtnPreview.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnPreview.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPreview.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.BtnPreview.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnPreview.Location = new System.Drawing.Point(718, 92);
            this.BtnPreview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnPreview.Name = "BtnPreview";
            this.tableLayoutPanel1.SetRowSpan(this.BtnPreview, 2);
            this.BtnPreview.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.BtnPreview.Size = new System.Drawing.Size(94, 60);
            this.BtnPreview.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPreview.TabIndex = 111;
            this.BtnPreview.Text = "Preview";
            this.BtnPreview.TextColor = System.Drawing.Color.White;
            // 
            // ChkDisplyTemAcc
            // 
            this.ChkDisplyTemAcc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkDisplyTemAcc.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkDisplyTemAcc.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkDisplyTemAcc.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.ChkDisplyTemAcc.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkDisplyTemAcc.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkDisplyTemAcc.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ChkDisplyTemAcc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkDisplyTemAcc.Checked = true;
            this.ChkDisplyTemAcc.CheckSignSize = new System.Drawing.Size(20, 20);
            this.ChkDisplyTemAcc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkDisplyTemAcc.CheckValue = "Y";
            this.ChkDisplyTemAcc.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDisplyTemAcc.Location = new System.Drawing.Point(11, 92);
            this.ChkDisplyTemAcc.Margin = new System.Windows.Forms.Padding(2);
            this.ChkDisplyTemAcc.Name = "ChkDisplyTemAcc";
            this.ChkDisplyTemAcc.Size = new System.Drawing.Size(297, 26);
            this.ChkDisplyTemAcc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkDisplyTemAcc.TabIndex = 113;
            this.ChkDisplyTemAcc.Text = "Display Temporary Acount ";
            this.ChkDisplyTemAcc.TextColor = System.Drawing.Color.Red;
            // 
            // CmbDistrict
            // 
            this.CmbDistrict.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CmbDistrict.DisplayMember = "Text";
            this.CmbDistrict.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CmbDistrict.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbDistrict.FormattingEnabled = true;
            this.CmbDistrict.ItemHeight = 24;
            this.CmbDistrict.Location = new System.Drawing.Point(312, 122);
            this.CmbDistrict.Margin = new System.Windows.Forms.Padding(2);
            this.CmbDistrict.Name = "CmbDistrict";
            this.CmbDistrict.Size = new System.Drawing.Size(297, 30);
            this.CmbDistrict.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CmbDistrict.TabIndex = 7;
            // 
            // CmbCity
            // 
            this.CmbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CmbCity.DisplayMember = "Text";
            this.CmbCity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CmbCity.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbCity.FormattingEnabled = true;
            this.CmbCity.ItemHeight = 24;
            this.CmbCity.Location = new System.Drawing.Point(312, 62);
            this.CmbCity.Margin = new System.Windows.Forms.Padding(2);
            this.CmbCity.Name = "CmbCity";
            this.CmbCity.Size = new System.Drawing.Size(297, 30);
            this.CmbCity.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CmbCity.TabIndex = 115;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 28);
            this.dataGridViewX1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.RowTemplate.Height = 24;
            this.dataGridViewX1.Size = new System.Drawing.Size(924, 385);
            this.dataGridViewX1.TabIndex = 3;
            // 
            // LblHeading
            // 
            // 
            // 
            // 
            this.LblHeading.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblHeading.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHeading.ForeColor = System.Drawing.Color.White;
            this.LblHeading.Location = new System.Drawing.Point(0, 1);
            this.LblHeading.Margin = new System.Windows.Forms.Padding(2);
            this.LblHeading.Name = "LblHeading";
            this.LblHeading.Size = new System.Drawing.Size(924, 23);
            this.LblHeading.TabIndex = 4;
            this.LblHeading.Text = "Heading For Records";
            this.LblHeading.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // DebtorsListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(924, 571);
            this.Controls.Add(this.LblHeading);
            this.Controls.Add(this.dataGridViewX1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DebtorsListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DebtorsListForm";
            this.Load += new System.EventHandler(this.DebtorsListForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateTimeInput1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dateTimeInput1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CmbDistrict;
        private DevComponents.DotNetBar.Controls.TextBoxX txtNoOfclients;
        private DevComponents.DotNetBar.ButtonX BtnPrint;
        private System.Windows.Forms.RadioButton RdbLstAllClients;
        private System.Windows.Forms.RadioButton RdbLstDebtors;
        private System.Windows.Forms.RadioButton RdbLstCreditors;
        private DevComponents.DotNetBar.ButtonX BtnClose;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkSelectDistrict;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtGoldBlnce;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCashblnc;
        private DevComponents.DotNetBar.ButtonX BtnPreview;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkDisplyTemAcc;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkSelectCity;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CmbCity;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private DevComponents.DotNetBar.LabelX LblHeading;
    }
}