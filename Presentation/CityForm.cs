﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration; 
using Data;
using Core;
using System.Diagnostics;

namespace Presentation
{
    public partial class CityForm : Form
    {
       
        public CityForm()
        {
            InitializeComponent();
        }
        string connectionString = ConfigurationManager.ConnectionStrings["JewellersDataBaseEntitiess"].ConnectionString;
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            //MessageBox.Show(connectionString);
        }

        private void btnAddCity_Click(object sender, EventArgs e)
        {
            try
            {

                using (JewellersDataBaseEntitiess gdbe = new JewellersDataBaseEntitiess())
                {
                    tblCity city = new tblCity();

                    city.City_Name = txtCity.Text;
                    gdbe.tblCities.Add(city);
                    gdbe.SaveChanges();

                }
                    
                
                SaveMassage();
                ClearFields();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ClearFields()
        {
            txtCity.Text = "";
            txtCity.Focus();
        }
        public static void SaveMassage()
        {
            MessageBox.Show("Successfully Save Data", "Save Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void DeleteMassage()
        {
            MessageBox.Show("Successfully Delete Data", "Delete Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
