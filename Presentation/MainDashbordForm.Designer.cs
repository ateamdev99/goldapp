﻿namespace Presentation
{
    partial class MainDashbordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelMenuBarHorizontal = new System.Windows.Forms.Panel();
            this.btnCrystalReport = new System.Windows.Forms.Button();
            this.BtnSetting = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.LblTimer = new DevComponents.DotNetBar.LabelX();
            this.BtnCloseDshBrdFrm = new System.Windows.Forms.Button();
            this.BtnAddDistrict = new System.Windows.Forms.Button();
            this.BtnDBClenup = new System.Windows.Forms.Button();
            this.BtnPrntRecipt = new System.Windows.Forms.Button();
            this.BtnTrnasactJewelr = new System.Windows.Forms.Button();
            this.BtnDateWisTran = new System.Windows.Forms.Button();
            this.BtnTranEntrySearc = new System.Windows.Forms.Button();
            this.BtnAddClentInf = new System.Windows.Forms.Button();
            this.panelAdminTop = new System.Windows.Forms.Panel();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.LblTotalClients = new DevComponents.DotNetBar.LabelX();
            this.LblTotlTransac = new DevComponents.DotNetBar.LabelX();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LblAvailableBlncinGold = new DevComponents.DotNetBar.LabelX();
            this.lblGoldCreditSum = new DevComponents.DotNetBar.LabelX();
            this.lblGoldDebitSum = new DevComponents.DotNetBar.LabelX();
            this.lbl55 = new System.Windows.Forms.Label();
            this.lblTotlCreditGold = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LblAvailableBlncinCash = new DevComponents.DotNetBar.LabelX();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.LblCashDebitSum = new DevComponents.DotNetBar.LabelX();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.LblCashCreditSum = new DevComponents.DotNetBar.LabelX();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxTotlClints = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCash = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.BtnClientAccDetils = new System.Windows.Forms.Button();
            this.BtnDebtorsList = new System.Windows.Forms.Button();
            this.panelMenuBarHorizontal.SuspendLayout();
            this.panelAdminTop.SuspendLayout();
            this.panelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTotlClints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCash)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenuBarHorizontal
            // 
            this.panelMenuBarHorizontal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.panelMenuBarHorizontal.Controls.Add(this.btnCrystalReport);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnSetting);
            this.panelMenuBarHorizontal.Controls.Add(this.button1);
            this.panelMenuBarHorizontal.Controls.Add(this.LblTimer);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnCloseDshBrdFrm);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnAddDistrict);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnDBClenup);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnPrntRecipt);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnTrnasactJewelr);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnDateWisTran);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnTranEntrySearc);
            this.panelMenuBarHorizontal.Controls.Add(this.BtnAddClentInf);
            this.panelMenuBarHorizontal.Controls.Add(this.panelAdminTop);
            this.panelMenuBarHorizontal.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenuBarHorizontal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelMenuBarHorizontal.ForeColor = System.Drawing.Color.Black;
            this.panelMenuBarHorizontal.Location = new System.Drawing.Point(0, 0);
            this.panelMenuBarHorizontal.Name = "panelMenuBarHorizontal";
            this.panelMenuBarHorizontal.Size = new System.Drawing.Size(256, 750);
            this.panelMenuBarHorizontal.TabIndex = 15;
            // 
            // btnCrystalReport
            // 
            this.btnCrystalReport.BackColor = System.Drawing.Color.White;
            this.btnCrystalReport.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCrystalReport.FlatAppearance.BorderSize = 0;
            this.btnCrystalReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.btnCrystalReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCrystalReport.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrystalReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.btnCrystalReport.Location = new System.Drawing.Point(3, 394);
            this.btnCrystalReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnCrystalReport.Name = "btnCrystalReport";
            this.btnCrystalReport.Size = new System.Drawing.Size(250, 50);
            this.btnCrystalReport.TabIndex = 67;
            this.btnCrystalReport.Text = "Crysal Report ";
            this.btnCrystalReport.UseVisualStyleBackColor = false;
            this.btnCrystalReport.Click += new System.EventHandler(this.btnCrystalReport_Click);
            // 
            // BtnSetting
            // 
            this.BtnSetting.BackColor = System.Drawing.Color.White;
            this.BtnSetting.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnSetting.FlatAppearance.BorderSize = 0;
            this.BtnSetting.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSetting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSetting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnSetting.Location = new System.Drawing.Point(3, 502);
            this.BtnSetting.Margin = new System.Windows.Forms.Padding(2);
            this.BtnSetting.Name = "BtnSetting";
            this.BtnSetting.Size = new System.Drawing.Size(250, 50);
            this.BtnSetting.TabIndex = 66;
            this.BtnSetting.Text = "Setting Page";
            this.BtnSetting.UseVisualStyleBackColor = false;
            this.BtnSetting.Click += new System.EventHandler(this.BtnSetting_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.button1.Location = new System.Drawing.Point(3, 448);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 50);
            this.button1.TabIndex = 65;
            this.button1.Text = "Laboratory";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LblTimer
            // 
            this.LblTimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            // 
            // 
            // 
            this.LblTimer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblTimer.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTimer.ForeColor = System.Drawing.Color.White;
            this.LblTimer.Location = new System.Drawing.Point(1, 674);
            this.LblTimer.Margin = new System.Windows.Forms.Padding(2);
            this.LblTimer.Name = "LblTimer";
            this.LblTimer.Size = new System.Drawing.Size(252, 32);
            this.LblTimer.TabIndex = 64;
            this.LblTimer.Text = "Time";
            this.LblTimer.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // BtnCloseDshBrdFrm
            // 
            this.BtnCloseDshBrdFrm.BackColor = System.Drawing.Color.White;
            this.BtnCloseDshBrdFrm.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnCloseDshBrdFrm.FlatAppearance.BorderSize = 0;
            this.BtnCloseDshBrdFrm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnCloseDshBrdFrm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCloseDshBrdFrm.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCloseDshBrdFrm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnCloseDshBrdFrm.Location = new System.Drawing.Point(3, 610);
            this.BtnCloseDshBrdFrm.Margin = new System.Windows.Forms.Padding(2);
            this.BtnCloseDshBrdFrm.Name = "BtnCloseDshBrdFrm";
            this.BtnCloseDshBrdFrm.Size = new System.Drawing.Size(250, 50);
            this.BtnCloseDshBrdFrm.TabIndex = 16;
            this.BtnCloseDshBrdFrm.Text = "Close";
            this.BtnCloseDshBrdFrm.UseVisualStyleBackColor = false;
            this.BtnCloseDshBrdFrm.Click += new System.EventHandler(this.BtnCloseDshBrdFrm_Click);
            // 
            // BtnAddDistrict
            // 
            this.BtnAddDistrict.BackColor = System.Drawing.Color.White;
            this.BtnAddDistrict.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnAddDistrict.FlatAppearance.BorderSize = 0;
            this.BtnAddDistrict.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnAddDistrict.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddDistrict.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddDistrict.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnAddDistrict.Location = new System.Drawing.Point(3, 124);
            this.BtnAddDistrict.Margin = new System.Windows.Forms.Padding(2);
            this.BtnAddDistrict.Name = "BtnAddDistrict";
            this.BtnAddDistrict.Size = new System.Drawing.Size(250, 50);
            this.BtnAddDistrict.TabIndex = 15;
            this.BtnAddDistrict.Text = "NewTransaction Form";
            this.BtnAddDistrict.UseVisualStyleBackColor = false;
            this.BtnAddDistrict.Click += new System.EventHandler(this.BtnAddDistrict_Click_1);
            // 
            // BtnDBClenup
            // 
            this.BtnDBClenup.BackColor = System.Drawing.Color.White;
            this.BtnDBClenup.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnDBClenup.FlatAppearance.BorderSize = 0;
            this.BtnDBClenup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnDBClenup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDBClenup.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDBClenup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnDBClenup.Location = new System.Drawing.Point(3, 340);
            this.BtnDBClenup.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDBClenup.Name = "BtnDBClenup";
            this.BtnDBClenup.Size = new System.Drawing.Size(250, 50);
            this.BtnDBClenup.TabIndex = 14;
            this.BtnDBClenup.Text = "Database Cleanup";
            this.BtnDBClenup.UseVisualStyleBackColor = false;
            this.BtnDBClenup.Click += new System.EventHandler(this.BtnDBClenup_Click);
            // 
            // BtnPrntRecipt
            // 
            this.BtnPrntRecipt.BackColor = System.Drawing.Color.White;
            this.BtnPrntRecipt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnPrntRecipt.FlatAppearance.BorderSize = 0;
            this.BtnPrntRecipt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnPrntRecipt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPrntRecipt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrntRecipt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnPrntRecipt.Location = new System.Drawing.Point(3, 286);
            this.BtnPrntRecipt.Margin = new System.Windows.Forms.Padding(2);
            this.BtnPrntRecipt.Name = "BtnPrntRecipt";
            this.BtnPrntRecipt.Size = new System.Drawing.Size(250, 50);
            this.BtnPrntRecipt.TabIndex = 13;
            this.BtnPrntRecipt.Text = "Print Recipet";
            this.BtnPrntRecipt.UseVisualStyleBackColor = false;
            this.BtnPrntRecipt.Click += new System.EventHandler(this.BtnPrntRecipt_Click);
            // 
            // BtnTrnasactJewelr
            // 
            this.BtnTrnasactJewelr.BackColor = System.Drawing.Color.White;
            this.BtnTrnasactJewelr.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnTrnasactJewelr.FlatAppearance.BorderSize = 0;
            this.BtnTrnasactJewelr.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnTrnasactJewelr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnTrnasactJewelr.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTrnasactJewelr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnTrnasactJewelr.Location = new System.Drawing.Point(3, 556);
            this.BtnTrnasactJewelr.Margin = new System.Windows.Forms.Padding(2);
            this.BtnTrnasactJewelr.Name = "BtnTrnasactJewelr";
            this.BtnTrnasactJewelr.Size = new System.Drawing.Size(250, 50);
            this.BtnTrnasactJewelr.TabIndex = 12;
            this.BtnTrnasactJewelr.Text = "Transaction";
            this.BtnTrnasactJewelr.UseVisualStyleBackColor = false;
            this.BtnTrnasactJewelr.Click += new System.EventHandler(this.BtnTrnasactJewelr_Click);
            // 
            // BtnDateWisTran
            // 
            this.BtnDateWisTran.BackColor = System.Drawing.Color.White;
            this.BtnDateWisTran.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnDateWisTran.FlatAppearance.BorderSize = 0;
            this.BtnDateWisTran.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnDateWisTran.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDateWisTran.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDateWisTran.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnDateWisTran.Location = new System.Drawing.Point(3, 178);
            this.BtnDateWisTran.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDateWisTran.Name = "BtnDateWisTran";
            this.BtnDateWisTran.Size = new System.Drawing.Size(250, 50);
            this.BtnDateWisTran.TabIndex = 11;
            this.BtnDateWisTran.Text = "DateWiseTransaction";
            this.BtnDateWisTran.UseVisualStyleBackColor = false;
            this.BtnDateWisTran.Click += new System.EventHandler(this.BtnDateWisTran_Click);
            // 
            // BtnTranEntrySearc
            // 
            this.BtnTranEntrySearc.BackColor = System.Drawing.Color.White;
            this.BtnTranEntrySearc.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnTranEntrySearc.FlatAppearance.BorderSize = 0;
            this.BtnTranEntrySearc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnTranEntrySearc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnTranEntrySearc.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTranEntrySearc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnTranEntrySearc.Location = new System.Drawing.Point(3, 232);
            this.BtnTranEntrySearc.Margin = new System.Windows.Forms.Padding(2);
            this.BtnTranEntrySearc.Name = "BtnTranEntrySearc";
            this.BtnTranEntrySearc.Size = new System.Drawing.Size(250, 50);
            this.BtnTranEntrySearc.TabIndex = 10;
            this.BtnTranEntrySearc.Text = "Transaction Entery And Search";
            this.BtnTranEntrySearc.UseVisualStyleBackColor = false;
            this.BtnTranEntrySearc.Click += new System.EventHandler(this.BtnTranEntrySearc_Click);
            // 
            // BtnAddClentInf
            // 
            this.BtnAddClentInf.BackColor = System.Drawing.Color.White;
            this.BtnAddClentInf.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnAddClentInf.FlatAppearance.BorderSize = 0;
            this.BtnAddClentInf.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnAddClentInf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddClentInf.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddClentInf.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnAddClentInf.Location = new System.Drawing.Point(3, 70);
            this.BtnAddClentInf.Margin = new System.Windows.Forms.Padding(2);
            this.BtnAddClentInf.Name = "BtnAddClentInf";
            this.BtnAddClentInf.Size = new System.Drawing.Size(250, 50);
            this.BtnAddClentInf.TabIndex = 9;
            this.BtnAddClentInf.Text = "Add Client Information";
            this.BtnAddClentInf.UseVisualStyleBackColor = false;
            this.BtnAddClentInf.Click += new System.EventHandler(this.BtnAddClentInf_Click);
            // 
            // panelAdminTop
            // 
            this.panelAdminTop.Controls.Add(this.labelX12);
            this.panelAdminTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAdminTop.Location = new System.Drawing.Point(0, 0);
            this.panelAdminTop.Name = "panelAdminTop";
            this.panelAdminTop.Size = new System.Drawing.Size(256, 73);
            this.panelAdminTop.TabIndex = 0;
            // 
            // labelX12
            // 
            this.labelX12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.White;
            this.labelX12.Location = new System.Drawing.Point(2, -1);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(255, 84);
            this.labelX12.TabIndex = 15;
            this.labelX12.Text = "Admin ";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(120, 74);
            this.labelX2.Margin = new System.Windows.Forms.Padding(2);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(790, 38);
            this.labelX2.TabIndex = 8;
            this.labelX2.Text = "Cash";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // panelContainer
            // 
            this.panelContainer.BackColor = System.Drawing.Color.White;
            this.panelContainer.Controls.Add(this.labelX13);
            this.panelContainer.Controls.Add(this.pictureBox16);
            this.panelContainer.Controls.Add(this.labelX11);
            this.panelContainer.Controls.Add(this.pictureBox15);
            this.panelContainer.Controls.Add(this.labelX10);
            this.panelContainer.Controls.Add(this.pictureBox14);
            this.panelContainer.Controls.Add(this.labelX9);
            this.panelContainer.Controls.Add(this.labelX8);
            this.panelContainer.Controls.Add(this.pictureBox11);
            this.panelContainer.Controls.Add(this.labelX7);
            this.panelContainer.Controls.Add(this.pictureBox17);
            this.panelContainer.Controls.Add(this.labelX14);
            this.panelContainer.Controls.Add(this.labelX1);
            this.panelContainer.Controls.Add(this.LblTotalClients);
            this.panelContainer.Controls.Add(this.LblTotlTransac);
            this.panelContainer.Controls.Add(this.label14);
            this.panelContainer.Controls.Add(this.label13);
            this.panelContainer.Controls.Add(this.LblAvailableBlncinGold);
            this.panelContainer.Controls.Add(this.lblGoldCreditSum);
            this.panelContainer.Controls.Add(this.lblGoldDebitSum);
            this.panelContainer.Controls.Add(this.lbl55);
            this.panelContainer.Controls.Add(this.lblTotlCreditGold);
            this.panelContainer.Controls.Add(this.label10);
            this.panelContainer.Controls.Add(this.LblAvailableBlncinCash);
            this.panelContainer.Controls.Add(this.pictureBox13);
            this.panelContainer.Controls.Add(this.label5);
            this.panelContainer.Controls.Add(this.pictureBox6);
            this.panelContainer.Controls.Add(this.LblCashDebitSum);
            this.panelContainer.Controls.Add(this.label4);
            this.panelContainer.Controls.Add(this.pictureBox12);
            this.panelContainer.Controls.Add(this.LblCashCreditSum);
            this.panelContainer.Controls.Add(this.label1);
            this.panelContainer.Controls.Add(this.pictureBoxTotlClints);
            this.panelContainer.Controls.Add(this.pictureBox2);
            this.panelContainer.Controls.Add(this.labelX3);
            this.panelContainer.Controls.Add(this.pictureBox5);
            this.panelContainer.Controls.Add(this.pictureBox4);
            this.panelContainer.Controls.Add(this.pictureBox9);
            this.panelContainer.Controls.Add(this.pictureBox8);
            this.panelContainer.Controls.Add(this.pictureBox7);
            this.panelContainer.Controls.Add(this.labelX2);
            this.panelContainer.Controls.Add(this.pictureBox3);
            this.panelContainer.Controls.Add(this.pictureBoxCash);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContainer.Location = new System.Drawing.Point(256, 0);
            this.panelContainer.Margin = new System.Windows.Forms.Padding(2);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(1044, 618);
            this.panelContainer.TabIndex = 16;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Black;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Segoe UI Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.ForeColor = System.Drawing.Color.White;
            this.labelX13.Location = new System.Drawing.Point(219, 336);
            this.labelX13.Margin = new System.Windows.Forms.Padding(2);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(21, 23);
            this.labelX13.TabIndex = 70;
            this.labelX13.Text = "$";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Presentation.Properties.Resources.Ellipse__1;
            this.pictureBox16.Location = new System.Drawing.Point(210, 329);
            this.pictureBox16.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(40, 40);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 69;
            this.pictureBox16.TabStop = false;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Black;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.White;
            this.labelX11.Location = new System.Drawing.Point(509, 336);
            this.labelX11.Margin = new System.Windows.Forms.Padding(2);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(21, 23);
            this.labelX11.TabIndex = 68;
            this.labelX11.Text = "$";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Presentation.Properties.Resources.Ellipse__1;
            this.pictureBox15.Location = new System.Drawing.Point(500, 329);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(40, 40);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 67;
            this.pictureBox15.TabStop = false;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Black;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.White;
            this.labelX10.Location = new System.Drawing.Point(219, 162);
            this.labelX10.Margin = new System.Windows.Forms.Padding(2);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(21, 23);
            this.labelX10.TabIndex = 66;
            this.labelX10.Text = "$";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Presentation.Properties.Resources.Ellipse__1;
            this.pictureBox14.Location = new System.Drawing.Point(210, 155);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(40, 40);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 65;
            this.pictureBox14.TabStop = false;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Black;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI Black", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.White;
            this.labelX9.Location = new System.Drawing.Point(509, 162);
            this.labelX9.Margin = new System.Windows.Forms.Padding(2);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(21, 23);
            this.labelX9.TabIndex = 64;
            this.labelX9.Text = "$";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Black;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.White;
            this.labelX8.Location = new System.Drawing.Point(776, 342);
            this.labelX8.Margin = new System.Windows.Forms.Padding(2);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(20, 27);
            this.labelX8.TabIndex = 63;
            this.labelX8.Text = "$";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Presentation.Properties.Resources.Ellipse__1;
            this.pictureBox11.Location = new System.Drawing.Point(766, 339);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(40, 40);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 62;
            this.pictureBox11.TabStop = false;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Black;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.White;
            this.labelX7.Location = new System.Drawing.Point(776, 166);
            this.labelX7.Margin = new System.Windows.Forms.Padding(2);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(20, 27);
            this.labelX7.TabIndex = 61;
            this.labelX7.Text = "$";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Presentation.Properties.Resources.Group_86;
            this.pictureBox17.Location = new System.Drawing.Point(1005, 4);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(28, 23);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox17.TabIndex = 60;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // labelX14
            // 
            this.labelX14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(3, 38);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(1021, 26);
            this.labelX14.TabIndex = 59;
            this.labelX14.Text = "Dashbord of Gold Application";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX1.Location = new System.Drawing.Point(6, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(1021, 23);
            this.labelX1.TabIndex = 14;
            this.labelX1.Text = "Welcome To";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // LblTotalClients
            // 
            this.LblTotalClients.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.LblTotalClients.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblTotalClients.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalClients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.LblTotalClients.Location = new System.Drawing.Point(585, 513);
            this.LblTotalClients.Margin = new System.Windows.Forms.Padding(2);
            this.LblTotalClients.Name = "LblTotalClients";
            this.LblTotalClients.Size = new System.Drawing.Size(181, 31);
            this.LblTotalClients.TabIndex = 54;
            this.LblTotalClients.Text = "0";
            this.LblTotalClients.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // LblTotlTransac
            // 
            this.LblTotlTransac.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.LblTotlTransac.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblTotlTransac.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotlTransac.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.LblTotlTransac.Location = new System.Drawing.Point(285, 513);
            this.LblTotlTransac.Margin = new System.Windows.Forms.Padding(2);
            this.LblTotlTransac.Name = "LblTotlTransac";
            this.LblTotlTransac.Size = new System.Drawing.Size(172, 31);
            this.LblTotlTransac.TabIndex = 53;
            this.LblTotlTransac.Text = "0";
            this.LblTotlTransac.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(596, 461);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(170, 50);
            this.label14.TabIndex = 52;
            this.label14.Text = "Total \r\nClients";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(285, 456);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(172, 55);
            this.label13.TabIndex = 51;
            this.label13.Text = "Total \r\nTransactions";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LblAvailableBlncinGold
            // 
            // 
            // 
            // 
            this.LblAvailableBlncinGold.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblAvailableBlncinGold.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAvailableBlncinGold.ForeColor = System.Drawing.Color.LimeGreen;
            this.LblAvailableBlncinGold.Location = new System.Drawing.Point(691, 379);
            this.LblAvailableBlncinGold.Margin = new System.Windows.Forms.Padding(2);
            this.LblAvailableBlncinGold.Name = "LblAvailableBlncinGold";
            this.LblAvailableBlncinGold.Size = new System.Drawing.Size(183, 26);
            this.LblAvailableBlncinGold.TabIndex = 50;
            this.LblAvailableBlncinGold.Text = "5677.00";
            this.LblAvailableBlncinGold.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGoldCreditSum
            // 
            // 
            // 
            // 
            this.lblGoldCreditSum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGoldCreditSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldCreditSum.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblGoldCreditSum.Location = new System.Drawing.Point(430, 379);
            this.lblGoldCreditSum.Margin = new System.Windows.Forms.Padding(2);
            this.lblGoldCreditSum.Name = "lblGoldCreditSum";
            this.lblGoldCreditSum.Size = new System.Drawing.Size(180, 26);
            this.lblGoldCreditSum.TabIndex = 49;
            this.lblGoldCreditSum.Text = "0";
            this.lblGoldCreditSum.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGoldDebitSum
            // 
            // 
            // 
            // 
            this.lblGoldDebitSum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGoldDebitSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldDebitSum.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblGoldDebitSum.Location = new System.Drawing.Point(145, 379);
            this.lblGoldDebitSum.Margin = new System.Windows.Forms.Padding(2);
            this.lblGoldDebitSum.Name = "lblGoldDebitSum";
            this.lblGoldDebitSum.Size = new System.Drawing.Size(181, 26);
            this.lblGoldDebitSum.TabIndex = 48;
            this.lblGoldDebitSum.Text = "0";
            this.lblGoldDebitSum.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl55
            // 
            this.lbl55.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl55.ForeColor = System.Drawing.Color.Black;
            this.lbl55.Location = new System.Drawing.Point(696, 296);
            this.lbl55.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl55.Name = "lbl55";
            this.lbl55.Size = new System.Drawing.Size(178, 41);
            this.lbl55.TabIndex = 47;
            this.lbl55.Text = "Available Balance\r\nIn Gold";
            this.lbl55.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTotlCreditGold
            // 
            this.lblTotlCreditGold.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotlCreditGold.ForeColor = System.Drawing.Color.Black;
            this.lblTotlCreditGold.Location = new System.Drawing.Point(430, 296);
            this.lblTotlCreditGold.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotlCreditGold.Name = "lblTotlCreditGold";
            this.lblTotlCreditGold.Size = new System.Drawing.Size(184, 32);
            this.lblTotlCreditGold.TabIndex = 46;
            this.lblTotlCreditGold.Text = "Credit";
            this.lblTotlCreditGold.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(140, 296);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(186, 28);
            this.label10.TabIndex = 45;
            this.label10.Text = "Debit";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LblAvailableBlncinCash
            // 
            // 
            // 
            // 
            this.LblAvailableBlncinCash.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblAvailableBlncinCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAvailableBlncinCash.ForeColor = System.Drawing.Color.LimeGreen;
            this.LblAvailableBlncinCash.Location = new System.Drawing.Point(691, 206);
            this.LblAvailableBlncinCash.Margin = new System.Windows.Forms.Padding(2);
            this.LblAvailableBlncinCash.Name = "LblAvailableBlncinCash";
            this.LblAvailableBlncinCash.Size = new System.Drawing.Size(183, 22);
            this.LblAvailableBlncinCash.TabIndex = 44;
            this.LblAvailableBlncinCash.Text = "7989.00";
            this.LblAvailableBlncinCash.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Presentation.Properties.Resources.Ellipse__1;
            this.pictureBox13.Location = new System.Drawing.Point(766, 162);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(40, 40);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 32;
            this.pictureBox13.TabStop = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(692, 118);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 42);
            this.label5.TabIndex = 31;
            this.label5.Text = "Available Balance\r\nIn Cash";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Presentation.Properties.Resources.Rectangle_331;
            this.pictureBox6.Location = new System.Drawing.Point(674, 112);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(220, 130);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 30;
            this.pictureBox6.TabStop = false;
            // 
            // LblCashDebitSum
            // 
            // 
            // 
            // 
            this.LblCashDebitSum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblCashDebitSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashDebitSum.ForeColor = System.Drawing.Color.LimeGreen;
            this.LblCashDebitSum.Location = new System.Drawing.Point(135, 199);
            this.LblCashDebitSum.Margin = new System.Windows.Forms.Padding(2);
            this.LblCashDebitSum.Name = "LblCashDebitSum";
            this.LblCashDebitSum.Size = new System.Drawing.Size(191, 26);
            this.LblCashDebitSum.TabIndex = 29;
            this.LblCashDebitSum.Text = "0";
            this.LblCashDebitSum.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(425, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(189, 29);
            this.label4.TabIndex = 28;
            this.label4.Text = "Credit";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Presentation.Properties.Resources.Ellipse__1;
            this.pictureBox12.Location = new System.Drawing.Point(500, 155);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(40, 40);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 26;
            this.pictureBox12.TabStop = false;
            // 
            // LblCashCreditSum
            // 
            // 
            // 
            // 
            this.LblCashCreditSum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblCashCreditSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashCreditSum.ForeColor = System.Drawing.Color.LimeGreen;
            this.LblCashCreditSum.Location = new System.Drawing.Point(435, 199);
            this.LblCashCreditSum.Margin = new System.Windows.Forms.Padding(2);
            this.LblCashCreditSum.Name = "LblCashCreditSum";
            this.LblCashCreditSum.Size = new System.Drawing.Size(168, 26);
            this.LblCashCreditSum.TabIndex = 22;
            this.LblCashCreditSum.Text = "0";
            this.LblCashCreditSum.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Black", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(135, 120);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 28);
            this.label1.TabIndex = 18;
            this.label1.Text = "Debit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBoxTotlClints
            // 
            this.pictureBoxTotlClints.Image = global::Presentation.Properties.Resources.Rectangle_331;
            this.pictureBoxTotlClints.Location = new System.Drawing.Point(573, 450);
            this.pictureBoxTotlClints.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxTotlClints.Name = "pictureBoxTotlClints";
            this.pictureBoxTotlClints.Size = new System.Drawing.Size(207, 107);
            this.pictureBoxTotlClints.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxTotlClints.TabIndex = 17;
            this.pictureBoxTotlClints.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Presentation.Properties.Resources.Rectangle_331;
            this.pictureBox2.Location = new System.Drawing.Point(269, 450);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(206, 107);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX3.Location = new System.Drawing.Point(120, 256);
            this.labelX3.Margin = new System.Windows.Forms.Padding(2);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(774, 32);
            this.labelX3.TabIndex = 15;
            this.labelX3.Text = "Gold";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Presentation.Properties.Resources.Rectangle_331;
            this.pictureBox5.Location = new System.Drawing.Point(409, 112);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(220, 130);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 13;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::Presentation.Properties.Resources.Rectangle_33;
            this.pictureBox4.Location = new System.Drawing.Point(674, 291);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(220, 130);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.Image = global::Presentation.Properties.Resources.Rectangle_33;
            this.pictureBox9.Location = new System.Drawing.Point(409, 291);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(220, 130);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 11;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Image = global::Presentation.Properties.Resources.Rectangle_33;
            this.pictureBox8.Location = new System.Drawing.Point(120, 291);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(220, 130);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 10;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Presentation.Properties.Resources.Rectangle_331;
            this.pictureBox7.Location = new System.Drawing.Point(120, 112);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(220, 130);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 9;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::Presentation.Properties.Resources.Rectangle_37;
            this.pictureBox3.Location = new System.Drawing.Point(37, 433);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(956, 137);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBoxCash
            // 
            this.pictureBoxCash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCash.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxCash.Image = global::Presentation.Properties.Resources.Rectangle_37;
            this.pictureBoxCash.Location = new System.Drawing.Point(37, 70);
            this.pictureBoxCash.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxCash.Name = "pictureBoxCash";
            this.pictureBoxCash.Size = new System.Drawing.Size(967, 181);
            this.pictureBoxCash.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCash.TabIndex = 1;
            this.pictureBoxCash.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // BtnClientAccDetils
            // 
            this.BtnClientAccDetils.BackColor = System.Drawing.Color.White;
            this.BtnClientAccDetils.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnClientAccDetils.FlatAppearance.BorderSize = 0;
            this.BtnClientAccDetils.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnClientAccDetils.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnClientAccDetils.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClientAccDetils.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnClientAccDetils.Location = new System.Drawing.Point(259, 622);
            this.BtnClientAccDetils.Margin = new System.Windows.Forms.Padding(2);
            this.BtnClientAccDetils.Name = "BtnClientAccDetils";
            this.BtnClientAccDetils.Size = new System.Drawing.Size(250, 50);
            this.BtnClientAccDetils.TabIndex = 68;
            this.BtnClientAccDetils.Text = "Client Account Details";
            this.BtnClientAccDetils.UseVisualStyleBackColor = false;
            this.BtnClientAccDetils.Click += new System.EventHandler(this.BtnClientAccDetils_Click);
            // 
            // BtnDebtorsList
            // 
            this.BtnDebtorsList.BackColor = System.Drawing.Color.White;
            this.BtnDebtorsList.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnDebtorsList.FlatAppearance.BorderSize = 0;
            this.BtnDebtorsList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.BtnDebtorsList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDebtorsList.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDebtorsList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnDebtorsList.Location = new System.Drawing.Point(513, 622);
            this.BtnDebtorsList.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDebtorsList.Name = "BtnDebtorsList";
            this.BtnDebtorsList.Size = new System.Drawing.Size(250, 50);
            this.BtnDebtorsList.TabIndex = 69;
            this.BtnDebtorsList.Text = "Debtors List";
            this.BtnDebtorsList.UseVisualStyleBackColor = false;
            this.BtnDebtorsList.Click += new System.EventHandler(this.BtnDebtorsList_Click);
            // 
            // MainDashbordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(1300, 750);
            this.Controls.Add(this.BtnDebtorsList);
            this.Controls.Add(this.BtnClientAccDetils);
            this.Controls.Add(this.panelContainer);
            this.Controls.Add(this.panelMenuBarHorizontal);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainDashbordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Dashbord";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelMenuBarHorizontal.ResumeLayout(false);
            this.panelAdminTop.ResumeLayout(false);
            this.panelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTotlClints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCash)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenuBarHorizontal;
        private System.Windows.Forms.Panel panelAdminTop;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel panelContainer;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBoxTotlClints;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBoxCash;
        private DevComponents.DotNetBar.LabelX lblGoldDebitSum;
        private System.Windows.Forms.Label lbl55;
        private System.Windows.Forms.Label lblTotlCreditGold;
        private System.Windows.Forms.Label label10;
        private DevComponents.DotNetBar.LabelX LblAvailableBlncinCash;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private DevComponents.DotNetBar.LabelX LblCashDebitSum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox12;
        private DevComponents.DotNetBar.LabelX LblCashCreditSum;
        private DevComponents.DotNetBar.LabelX LblTotalClients;
        private DevComponents.DotNetBar.LabelX LblTotlTransac;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private DevComponents.DotNetBar.LabelX LblAvailableBlncinGold;
        private DevComponents.DotNetBar.LabelX lblGoldCreditSum;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.Button BtnAddClentInf;
        private System.Windows.Forms.Button BtnTrnasactJewelr;
        private System.Windows.Forms.Button BtnTranEntrySearc;
        private System.Windows.Forms.Button BtnDateWisTran;
        private System.Windows.Forms.Button BtnAddDistrict;
        private System.Windows.Forms.Button BtnDBClenup;
        private System.Windows.Forms.Button BtnPrntRecipt;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Button BtnCloseDshBrdFrm;
        private DevComponents.DotNetBar.LabelX LblTimer;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX13;
        private System.Windows.Forms.PictureBox pictureBox16;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.PictureBox pictureBox15;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.PictureBox pictureBox14;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BtnSetting;
        private System.Windows.Forms.Button btnCrystalReport;
        private System.Windows.Forms.Button BtnClientAccDetils;
        private System.Windows.Forms.Button BtnDebtorsList;


    }
}

