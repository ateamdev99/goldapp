﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;
using Data;

namespace Presentation
{
    public partial class LaboratoryForm : Form
    {
        double dbKachSonalGrm;
        double dbSumGrm;
        double dbSumCustomFrom;
        double dbSumCustomTo;
        double dbPurity;
        double dbKhalisSonaInGrm;
        double dbSumToKhalisSonaInGrm;
        double dbSumFromKhalisSonaInGrm;
        public LaboratoryForm(tblTransaction obj)
                                                                                                                 {
            InitializeComponent();
            if (obj == null || obj.T_Id == 0)
            {
                tblTransactionBindingSource.DataSource = new tblTransaction();
                LabortorytabControl.SelectedTab = kachaSonatabItem;
                GETGoldCurrentRate();
                CmbPopulate();
            }
            else
            {
                obj = TransactionServicesClass.GetByIdTransaction(obj.T_Id);
                CmbPopulate();
                if (obj.Tran_Status == "KachaSona")
                {
                    LabortorytabControl.SelectedTab = kachaSonatabItem;
                    txtKachaSonaGrams.Text=obj.ImpureGoldinGram.ToString();
                    txtSumKachaSona.Text=obj.Sum.ToString();
                    txtPurityKachaSona.Text=obj.Purity.ToString();
                    txtSumCustomToKachaSona.Text=obj.CustomSumTo.ToString();
                    txtSumCustomFromKachaSona.Text=obj.CustomSumFrom.ToString();
                    txtPureGoldKachaSona.Text=obj.PureGoldinGram.ToString();
                    txtDescriptionKachasSona.Text=obj.Itme_Name;
                    if (obj.ClientId == 1) { obj.ClientId = 1; }
                    else { cmbClientsKachaSona.SelectedValue = obj.ClientId; }
                    int TRtype = obj.Tran_Type;
                    if (TRtype == 1) { Rdb1GoldCreditKachaSona.Checked = true; } else { Rdb2GoldDebitKachaSona.Checked = true; }
                    tblTransactionBindingSource.DataSource = obj;
                    btnAddTransactionKachaSona.Text = "Update";
                }
                else if (obj.Tran_Status == "KhalisSona")
                {
                    
                    LabortorytabControl.SelectedTab = tabItemKhalisSona;
                    txtSumTo990KhalisSona.Text = obj.CustomSumTo.ToString();
                    txtSumFrom997KhalisSona.Text = obj.CustomSumFrom.ToString();
                    txtPureGoldKhalisSonaFinal.Text = obj.PureGoldinGram.ToString();
                    txtDescriptionKhalisSona.Text = obj.Itme_Name;
                    if (obj.ClientId == 1) { obj.ClientId = 1; }
                    else { cmbClientsKhilsSona.SelectedValue = obj.ClientId; }
                    int TRtype = obj.Tran_Type;
                    if (TRtype == 1) { rdbGoldCreditKhalisSona.Checked = true; } else { rdbrdbGoldDebitKhalisSona.Checked = true; }
                    tblTransactionBindingSource.DataSource = obj;
                    btnAddTransactionKhalisSona.Text = "Update";
                }
                else if (obj.Tran_Status == "Cash")
                {
                    LabortorytabControl.SelectedTab = CashtabItem;
                    GETGoldCurrentRate();
                    txtAmountCash.Text = obj.Net_Amount.ToString();
                    txtGoldRateCash.Text = obj.Price_of_Gold.ToString();
                    txtPureGoldCash.Text = obj.PureGoldinGram.ToString();
                    txtDiscriptionCash.Text = obj.Itme_Name;
                    if (obj.ClientId == 1) { obj.ClientId = 1; }
                    else {cmbCash.SelectedValue = obj.ClientId;}
                    int TRtype = obj.Tran_Type;
                    if (TRtype == 3) { rdbCashCredit.Checked = true; }else  { rdbCashDebit.Checked = true; }
                    tblTransactionBindingSource.DataSource = obj;
                    btnAddTransactionCash.Text = "Update";
                }
                else if (obj.Tran_Status == "RefineGold")
                {
                    LabortorytabControl.SelectedTab = refineGoldtabItem;
                    txtImpureGoldRefineGold.Text = obj.ImpureGoldinGram.ToString();
                    txtPurityRefineGold.Text = obj.Purity.ToString();
                    txtPureRefineGold.Text = obj.PureGoldinGram.ToString();
                    txtDisriptionRefineGold.Text = obj.Itme_Name;
                    int TRtype = obj.Tran_Type;
                    if (TRtype == 1) { rdbGoldCreditRefineGold.Checked = true; } else { rdbGoldDebitRefineGold.Checked = true; }
                    tblTransactionBindingSource.DataSource = obj;
                    btnAddTransactionRefineGold.Text = "Update";
                }
                
            }
        }

        //private void textGoldCustomPurity_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    //if (e.KeyChar == (char)13)
        //    //{
        //    //    txtSumKachaSona.MaxLength = 1000;
        //    //    dbKachSonalGrm = Conversion.toDouble(txtKachaSonaGrams.Text);
        //    //    dbSumGrm = Conversion.toDouble(txtSumKachaSona.Text);
        //    //    if (dbSumGrm <= 1000)
        //    //    {
        //    //        txtPurityKachaSona.Text = ((dbKachSonalGrm * dbSumGrm) / 1000).ToString();
        //    //    }
        //    //}
        //    
        //}
        private void LaboratoryForm_Load(object sender, EventArgs e)
        {
        }
        private void BtnTransactionKachaSona_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtKachaSonaGrams.Text))
                {
                    MessageBox.Show("Enter KachaSonaGrams", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtKachaSonaGrams.Focus();
                }

                else if (string.IsNullOrEmpty(txtDescriptionKachasSona.Text))
                {
                    MessageBox.Show("Enter DescriptionKachasSona", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDescriptionKachasSona.Focus();
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                    if (tblObj != null && tblObj.T_Id == 0 && btnAddTransactionKachaSona.Text == "Add Transaction")
                    {
                        try
                        {
                            tblObj.ImpureGoldinGram = Conversion.toDouble(txtKachaSonaGrams.Text);
                            tblObj.Sum = Conversion.toDouble(txtSumKachaSona.Text);
                            tblObj.CustomSumFrom = Conversion.toDouble(txtSumCustomFromKachaSona.Text);
                            tblObj.CustomSumTo = Conversion.toDouble(txtSumCustomToKachaSona.Text);
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureGoldKachaSona.Text);
                            tblObj.Purity = Conversion.toDouble(txtPurityKachaSona.Text);
                            tblObj.Itme_Name = txtDescriptionKachasSona.Text;
                            tblObj.Tran_Status = "KachaSona";
                            tblObj.ClientId = Conversion.toInt(cmbClientsKachaSona.SelectedValue);
                            if(tblObj.ClientId ==0) { tblObj.ClientId = 1; }
                            if (tblObj.T_Id == 0)
                            { tblObj.T_Date = DateTime.Now;/* tblObj.T_Date = dateTimePicker1.Value;*/ }
                            tblObj.Tran_Type = GetCreditDebitforKachSona();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            
                            tblObj.IsTeleConfirmation = false;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.IsDeleted = false;
                            tblObj.UpatedDate = DateTime.Now;

                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Insert(tblObj);
                                RestKachaSonaTabItem();
                                Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.ImpureGoldinGram = Conversion.toDouble(txtKachaSonaGrams.Text);
                            tblObj.Sum = Conversion.toDouble(txtSumKachaSona.Text);
                            tblObj.CustomSumFrom = Conversion.toDouble(txtSumCustomFromKachaSona.Text);
                            tblObj.CustomSumTo = Conversion.toDouble(txtSumCustomToKachaSona.Text);
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureGoldKachaSona.Text);
                            tblObj.Purity = Conversion.toInt(txtPurityKachaSona.Text);
                            tblObj.Itme_Name = txtDescriptionKachasSona.Text;
                            tblObj.ClientId = Conversion.toInt(cmbClientsKachaSona.SelectedValue);
                            if (tblObj.ClientId == 0) { tblObj.ClientId = 1; }
                            tblObj.Tran_Type = GetCreditDebitforKachSona();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                                RestKachaSonaTabItem();
                                this.Close();
                            }

                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        private void btnAddTransactionCash_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtAmountCash.Text))
                {
                    MessageBox.Show("Enter AmountCash", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtAmountCash.Focus();
                }

                else if (string.IsNullOrEmpty(txtDiscriptionCash.Text))
                {
                    MessageBox.Show("Enter DiscriptionCash", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDiscriptionCash.Focus();
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                    if (tblObj != null && tblObj.T_Id == 0 && btnAddTransactionKachaSona.Text == "Add Transaction")
                    {
                        try
                        {
                            tblObj.Net_Amount = Conversion.toDouble(txtAmountCash.Text);
                            tblObj.Price_of_Gold = Conversion.toDouble(txtGoldRateCash.Text);
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureGoldCash.Text);
                            tblObj.Itme_Name = txtDiscriptionCash.Text;

                            tblObj.Tran_Status = "Cash";
                            tblObj.ClientId = Conversion.toInt(cmbCash.SelectedValue);
                            if (tblObj.ClientId == 0) { tblObj.ClientId = 1; }
                            if (tblObj.T_Id == 0)
                                if (tblObj.T_Id == 0)
                            { tblObj.T_Date = DateTime.Now;/* tblObj.T_Date = dateTimePicker1.Value;*/ }
                            tblObj.Tran_Type = GetCreditDebitforCash();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            
                            tblObj.IsTeleConfirmation = false;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.IsDeleted = false;
                            tblObj.UpatedDate = DateTime.Now;

                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Insert(tblObj);
                                RestCashTabItem();
                                Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.Net_Amount = Conversion.toDouble(txtAmountCash.Text);
                            tblObj.Price_of_Gold = Conversion.toDouble(txtGoldRateCash.Text);
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureGoldCash.Text);
                            tblObj.Itme_Name = txtDiscriptionCash.Text;
                            tblObj.ClientId = Conversion.toInt(cmbCash.SelectedValue);
                            if (tblObj.ClientId == 0) { tblObj.ClientId = 1; }
                            tblObj.Tran_Type = GetCreditDebitforCash();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                                RestCashTabItem();
                                this.Close();
                            }

                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        }

        private void btnAddTransactionKhalisSona_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPureGoldKhalisSonaFinal.Text))
                {
                    MessageBox.Show("Enter KhalisSonaGrams", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPureGoldKhalisSonaFinal.Focus();
                }

                else if (string.IsNullOrEmpty(txtDescriptionKhalisSona.Text))
                {
                    MessageBox.Show("Enter DescriptionKachasSona", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDescriptionKhalisSona.Focus();
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                    if (tblObj != null && tblObj.T_Id == 0 && btnAddTransactionKachaSona.Text == "Add Transaction")
                    {
                        try
                        {
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureGoldKhalisSonaFinal.Text);
                            tblObj.CustomSumFrom = Conversion.toDouble(txtSumFrom997KhalisSona.Text);
                            tblObj.CustomSumTo = Conversion.toDouble(txtSumTo990KhalisSona.Text);
                            tblObj.Itme_Name = txtDescriptionKhalisSona.Text;
                            tblObj.ClientId = Conversion.toInt(cmbClientsKhilsSona.SelectedValue);
                            if (tblObj.ClientId == 0) { tblObj.ClientId = 1; }
                            if (tblObj.T_Id == 0)
                            { tblObj.T_Date = DateTime.Now;/* tblObj.T_Date = dateTimePicker1.Value;*/ }
                            tblObj.Tran_Type = GetCreditDebitforKhalisSona();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.Tran_Status = "KhalisSona";
                            tblObj.IsTeleConfirmation = false;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.IsDeleted = false;
                            tblObj.UpatedDate = DateTime.Now;

                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Insert(tblObj);
                                RestKhalisSonaTabItem();
                               Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureGoldKhalisSonaFinal.Text);
                            tblObj.CustomSumFrom = Conversion.toDouble(txtSumFrom997KhalisSona.Text);
                            tblObj.CustomSumTo = Conversion.toDouble(txtSumTo990KhalisSona.Text);
                            tblObj.Itme_Name = txtDescriptionKhalisSona.Text;
                            tblObj.ClientId = Conversion.toInt(cmbClientsKhilsSona.SelectedValue);
                            if (tblObj.ClientId == 0) { tblObj.ClientId = 1; }
                            tblObj.Tran_Type = GetCreditDebitforKhalisSona();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                                RestKhalisSonaTabItem();
                               this.Close();
                            }

                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void BtnTransactionRefineGold_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtImpureGoldRefineGold.Text))
                {
                    MessageBox.Show("Enter PureGoldRefineGold", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtImpureGoldRefineGold.Focus();
                }

                else if (string.IsNullOrEmpty(txtDisriptionRefineGold.Text))
                {
                    MessageBox.Show("Enter Discription", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDisriptionRefineGold.Focus();
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                    if (tblObj != null && tblObj.T_Id == 0 && btnAddTransactionKachaSona.Text == "Add Transaction")
                    {
                        try
                        {
                            tblObj.ImpureGoldinGram = Conversion.toDouble(txtImpureGoldRefineGold.Text);
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureRefineGold.Text);
                            tblObj.Purity = Conversion.toDouble(txtPurityRefineGold.Text);
                            tblObj.Itme_Name = txtDisriptionRefineGold.Text;
                            tblObj.Tran_Type = GetCreditDebitforRefineGold();
                            tblObj.ClientId = 1;
                            if (tblObj.T_Id == 0)
                            { tblObj.T_Date = DateTime.Now;/* tblObj.T_Date = dateTimePicker1.Value;*/ }
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.Tran_Status = "RefineGold";
                            tblObj.IsTeleConfirmation = false;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.IsDeleted = false;
                            tblObj.UpatedDate = DateTime.Now;

                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Insert(tblObj);
                                RestRefineGoldTabItem();
                                Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.ImpureGoldinGram = Conversion.toDouble(txtImpureGoldRefineGold.Text);
                            tblObj.PureGoldinGram = Conversion.toDouble(txtPureRefineGold.Text);
                            tblObj.Purity = Conversion.toDouble(txtPurityRefineGold.Text);
                            tblObj.Itme_Name = txtDisriptionRefineGold.Text;
                            tblObj.Tran_Type = GetCreditDebitforRefineGold();
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin"; 
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Process !", "Are You Sure to Continue ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                                RestRefineGoldTabItem();
                                this.Close();
                            }

                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        void GETGoldCurrentRate()
        {
            string rate = DBUtils.executeSqlGetID("SELECT TOP 1 TodayRate FROM tblGoldRate where UpdatedDate BETWEEN '" + DateTime.Now.Date + "' AND '" + DateTime.Now + "' ORDER BY Id desc  ");
            if (rate != null && rate != "")
            {
                txtGoldRateCash.Text = rate;
            }
            else
            {
                txtGoldRateCash.Text = "";
            }

        }
        private void btnRefreshKachSona_Click(object sender, EventArgs e)
        {
            RestKachaSonaTabItem();
        }

        private void btnRefreshKhalisSona_Click(object sender, EventArgs e)
        {
            RestKhalisSonaTabItem();
        }

        private void btnRefreshCash_Click(object sender, EventArgs e)
        {
            RestCashTabItem();
        }

        private void btnRefreshRefineGold_Click(object sender, EventArgs e)
        {
            RestRefineGoldTabItem();
        }

        
        

        public enum CreditDebit
        {
            GoldCredit = 1,
            GoldDebit = 2,
            CashCredit = 3,
            CashDebit = 4,
            Expense = 5
        }
        private int GetCreditDebitforKhalisSona()
        {
            if (rdbGoldCreditKhalisSona.Checked)
            {
                return (int)CreditDebit.GoldCredit;
            }
            if (rdbrdbGoldDebitKhalisSona.Checked)
            {
                return (int)CreditDebit.GoldDebit;
            }
            return (int)CreditDebit.Expense;
        }
        private int GetCreditDebitforCash()
        {
            if (rdbCashCredit.Checked)
            {
                return (int)CreditDebit.CashCredit;
            }
            if (rdbCashDebit.Checked)
            {
                return (int)CreditDebit.CashDebit;
            }
            return (int)CreditDebit.Expense;
        }
        private int GetCreditDebitforKachSona()
        {
            if (Rdb1GoldCreditKachaSona.Checked)
            {
                return (int)CreditDebit.GoldCredit;
            }
            if (Rdb2GoldDebitKachaSona.Checked)
            {
                return (int)CreditDebit.GoldDebit;
            }
            return (int)CreditDebit.Expense;
        }
        private int GetCreditDebitforRefineGold()
        {
            if (rdbGoldCreditRefineGold.Checked)
            {
                return (int)CreditDebit.GoldCredit;
            }
            if (rdbGoldDebitRefineGold.Checked)
            {
                return (int)CreditDebit.GoldDebit;
            }
            return (int)CreditDebit.Expense;
        }

        void CmbPopulate()
        {
            try
            {
                using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
                {
                    // var dt = Core.DBUtils.GetDataTable("select  C_Id,Name from tblClient where C_Id != 1 and C_Id != 2");
                    var QueryReturn = (from t in db.tblClients where t.C_Id != 1 && t.C_Id != 2 && t.ActivateAccount != true select new { t.C_Id, t.Name }).ToList();
                    cmbClientsKachaSona.DataSource = QueryReturn;
                    cmbClientsKhilsSona.DataSource = QueryReturn;
                    cmbCash.DataSource = QueryReturn;
                    cmbClientsKachaSona.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbClientsKachaSona.AutoCompleteSource = AutoCompleteSource.ListItems;
                    cmbClientsKhilsSona.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbClientsKhilsSona.AutoCompleteSource = AutoCompleteSource.ListItems;
                    cmbCash.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbCash.AutoCompleteSource = AutoCompleteSource.ListItems;
                    if (QueryReturn.Count > 0)
                    {

                        cmbClientsKachaSona.SelectedIndex = -1;
                        cmbClientsKhilsSona.SelectedIndex = -1;
                        cmbCash.SelectedIndex = -1;
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error In Populating Clients.", exp.Message);
            }
        }
        void RestKachaSonaTabItem()
        {
            txtKachaSonaGrams.Clear();
            txtSumKachaSona.Clear();
            txtPurityKachaSona.Clear();
            txtSumCustomFromKachaSona.Clear();
            txtSumCustomToKachaSona.Clear();
            txtPureGoldKachaSona.Clear();
            txtDescriptionKachasSona.Clear();
            cmbClientsKachaSona.SelectedIndex = -1;
            Rdb1GoldCreditKachaSona.Checked = true;
            btnAddTransactionKachaSona.Text = "Add Transaction";
        }
        void RestKhalisSonaTabItem()
        {
            txtPureGoldNoneKhalisSona.Clear();
            txtSumTo990KhalisSona.Clear();
            txtSumFrom997KhalisSona.Clear();
            txtPureGoldKhalisSonaFinal.Clear();
            txtDescriptionKhalisSona.Clear();
            cmbClientsKhilsSona.SelectedIndex = -1;
            rdbGoldCreditKhalisSona.Checked = true;
            btnAddTransactionKhalisSona.Text = "Add Transaction";

        }
        void RestCashTabItem()
        {
            txtAmountCash.Clear();
            txtPureGoldCash.Clear();
            txtGoldRateCash.Clear();
            txtDiscriptionCash.Clear();
            cmbCash.SelectedIndex = -1;
            rdbCashCredit.Checked = true;
            btnAddTransactionCash.Text = "Add Transaction";

        }
        void RestRefineGoldTabItem()
        {
            txtImpureGoldRefineGold.Clear();
            txtDisriptionRefineGold.Clear();
            txtPurityRefineGold.Clear();
            txtPureRefineGold.Clear();
            btnAddTransactionRefineGold.Text = "Add Transaction";

        }

      

        private void txtKachaSonaGrams_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            
            dbKachSonalGrm = Conversion.toDouble(txtKachaSonaGrams.Text);
            dbSumGrm = Conversion.toDouble(txtSumKachaSona.Text);
            if (dbPurity <= 1000)
            {
                txtPurityKachaSona.Text = ((dbKachSonalGrm * dbSumGrm) / 1000).ToString();
            }
            else
            {
                MessageBox.Show("Sum value must between 0 to 1000");
                txtSumKachaSona.Text = txtSumKachaSona.Text.Substring(0, txtSumKachaSona.Text.Length - 1);
            }
           
        }

        //private void txtSumKachaSona_KeyPress_1(object sender, KeyPressEventArgs e)
        //{
            
        //    dbKachSonalGrm = Conversion.toDouble(txtKachaSonaGrams.Text);
        //    dbSumGrm = Conversion.toDouble(txtSumKachaSona.Text);
        //    if (dbSumGrm <= 1000)
        //    {
        //        txtPurityKachaSona.Text = ((dbKachSonalGrm * dbSumGrm) / 1000).ToString();
        //    }
        //    else
        //    {
        //        MessageBox.Show("Sum value must between 0 to 1000");
        //        txtSumKachaSona.Text = txtSumKachaSona.Text.Substring(0, txtSumKachaSona.Text.Length - 1);
        //    }
            
        //}

        private void txtPurityKachaSona_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void txtSumCustomFromKachaSona_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            
            dbPurity = Conversion.toDouble(txtPurityKachaSona.Text);
            dbSumCustomFrom = Conversion.toDouble(txtSumCustomFromKachaSona.Text);
            dbSumCustomTo = Conversion.toDouble(txtSumCustomToKachaSona.Text);
            if (dbPurity > 0)
            {
                if (dbSumCustomFrom <= 1000 && dbSumCustomTo <= 1000)
                {
                    //  textGoldCustomPurity.Text = ((dbSumGrm * dbSumCustomFrom) / (dbSumCustomTo)).ToString();
                    txtPureGoldKachaSona.Text = Math.Round(((dbPurity * dbSumCustomFrom) / (dbSumCustomTo)), 2, MidpointRounding.AwayFromZero).ToString();
                }
                else
                {
                    MessageBox.Show("Custom  value must between 1 to 1000");
                    txtSumCustomFromKachaSona.Text = txtSumCustomFromKachaSona.Text.Substring(0, txtSumCustomFromKachaSona.Text.Length - 1);
                }
            }
            else
            {
                MessageBox.Show("Please Enter missing value Sum");
            }
            
        }

        //private void txtSumCustomToKachaSona_KeyPress_1(object sender, KeyEventArgs e)
        //{
            
        //    dbPurity = Conversion.toDouble(txtPurityKachaSona.Text);
        //    dbSumCustomFrom = Conversion.toDouble(txtSumCustomFromKachaSona.Text);
        //    dbSumCustomTo = Conversion.toDouble(txtSumCustomToKachaSona.Text);
        //    if (dbSumGrm > 0)
        //    {
        //        if (dbSumCustomFrom <= 1000 && dbSumCustomTo <= 1000)
        //        {
        //            txtPureGoldKachaSona.Text = Math.Round(((dbPurity * dbSumCustomFrom) / (dbSumCustomTo)), 2, MidpointRounding.AwayFromZero).ToString();
        //        }
        //        else
        //        {
        //            MessageBox.Show("Custom  value must between 1 to 1000");
        //            txtSumCustomToKachaSona.Text = txtSumCustomToKachaSona.Text.Substring(0, txtSumCustomToKachaSona.Text.Length - 1);
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please Enter missing value Sum");
        //    }
            
        //}

        private void txtPureGoldKachaSona_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void txtPureGoldNoneKhalisSona_KeyDown(object sender, KeyEventArgs e)
        {
            
            dbKhalisSonaInGrm = Conversion.toDouble(txtPureGoldNoneKhalisSona.Text);
            dbSumFromKhalisSonaInGrm = Conversion.toDouble(txtSumFrom997KhalisSona.Text);
            dbSumToKhalisSonaInGrm = Conversion.toDouble(txtSumTo990KhalisSona.Text);

            if (dbKhalisSonaInGrm > 0 && dbSumFromKhalisSonaInGrm == 0 && dbSumToKhalisSonaInGrm == 0)
            {
                txtPureGoldKhalisSonaFinal.Text = ((dbKhalisSonaInGrm * 1000) / 1000).ToString();
            }
            else
            {
               
            }
        }

        private void txtSumFrom997KhalisSona_KeyDown(object sender, KeyEventArgs e)
        {
           
            dbKhalisSonaInGrm = Conversion.toDouble(txtPureGoldNoneKhalisSona.Text);
            dbSumFromKhalisSonaInGrm = Conversion.toDouble(txtSumFrom997KhalisSona.Text);
            dbSumToKhalisSonaInGrm = Conversion.toDouble(txtSumTo990KhalisSona.Text);

            if (dbKhalisSonaInGrm > 0 && dbSumFromKhalisSonaInGrm <= 1000 && dbSumToKhalisSonaInGrm <= 1000)
            {
                txtPureGoldKhalisSonaFinal.Text = ((dbKhalisSonaInGrm * dbSumFromKhalisSonaInGrm) / dbSumToKhalisSonaInGrm).ToString();
            }
            else
            {
                MessageBox.Show("Sum To value must between 0 to 1000");
                txtSumFrom997KhalisSona.Text = txtSumFrom997KhalisSona.Text.Substring(0, txtSumFrom997KhalisSona.Text.Length - 1);
            }
        }

        private void txtSumTo990KhalisSona_KeyDown(object sender, KeyEventArgs e)
        {
            dbKhalisSonaInGrm = Conversion.toDouble(txtPureGoldNoneKhalisSona.Text);
            dbSumFromKhalisSonaInGrm = Conversion.toDouble(txtSumFrom997KhalisSona.Text);
            dbSumToKhalisSonaInGrm = Conversion.toDouble(txtSumTo990KhalisSona.Text);

            if (dbKhalisSonaInGrm > 0 && dbSumFromKhalisSonaInGrm <= 1000 && dbSumToKhalisSonaInGrm <= 1000)
            {
                txtPureGoldKhalisSonaFinal.Text = ((dbKhalisSonaInGrm * dbSumFromKhalisSonaInGrm) / dbSumToKhalisSonaInGrm).ToString();
            }
            else
            {
                MessageBox.Show("Sum To value must between 0 to 1000");
                txtSumTo990KhalisSona.Text = txtSumTo990KhalisSona.Text.Substring(0, txtSumTo990KhalisSona.Text.Length - 1);
            }
        }

        private void txtPureGoldKhalisSonaFinal_KeyDown(object sender, KeyEventArgs e)
        {

        }
       
        private void txtSumTo990KhalisSona_KeyPress(object sender, KeyPressEventArgs e)
        {

            dbKhalisSonaInGrm = Conversion.toDouble(txtPureGoldNoneKhalisSona.Text);
            dbSumFromKhalisSonaInGrm = Conversion.toDouble(txtSumFrom997KhalisSona.Text);
            dbSumToKhalisSonaInGrm = Conversion.toDouble(txtSumTo990KhalisSona.Text);

            if (dbKhalisSonaInGrm > 0 && dbSumFromKhalisSonaInGrm <= 1000 && dbSumToKhalisSonaInGrm <= 1000)
            {
                txtPureGoldKhalisSonaFinal.Text = ((dbKhalisSonaInGrm * dbSumFromKhalisSonaInGrm) / dbSumToKhalisSonaInGrm).ToString();
            }
            else
            {
                MessageBox.Show("Sum To value must between 0 to 1000");
                txtSumTo990KhalisSona.Text = txtSumTo990KhalisSona.Text.Substring(0, txtSumTo990KhalisSona.Text.Length - 1);
            }
           
        }

        private void txtPureGoldKachaSona_TextChanged(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }
    }
}
