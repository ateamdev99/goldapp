﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class AddCurrentGoldPriceForm : Form
    {
        tblGoldRate TblObj = new tblGoldRate();
        public AddCurrentGoldPriceForm(tblGoldRate obj)
        {
            InitializeComponent();
            if (obj.Id == 0)
            {
                TblObj = obj;
                
            }
            else
            {
                TblObj = obj;
                TxtGoldCurrentRate.Text= Conversion.toString(TblObj.TodayRate);
                BtnAddCurrentGoldRate.Text = "Update";
            }
        
        }

        private void TxtGoldCurrentRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void BtnAddCurrentGoldRate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(TxtGoldCurrentRate.Text))
                {
                    MessageBox.Show("Enter Today Gold Current Rate", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtGoldCurrentRate.Focus();
                }
                else
                {
                    tblGoldRate tblObj = TblObj;
                    if (tblObj != null && tblObj.Id == 0 && BtnAddCurrentGoldRate.Text == "Add Gold Rate")
                    {
                        try
                        {
                            tblObj.TodayRate= Conversion.toDouble(TxtGoldCurrentRate.Text);
                            tblObj.AddedDate = DateTime.Now;
                            tblObj.UpdatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Make Transaction", "Sure to Make Transaction ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.InsertGoldRate(tblObj);
                                //DayBookFrm.RefreshDayBookForm();
                                Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.TodayRate = Conversion.toDouble(TxtGoldCurrentRate.Text);
                            //tblObj.AddedDate = DateTime.Now;
                            tblObj.UpdatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Updated Transaction", "Sure to Updated Transaction ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.UpdateGoldRate(tblObj);
                                
                                this.Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }

        }

        private void AddCurrentGoldPriceForm_Load(object sender, EventArgs e)
        {

        }
    }
}
