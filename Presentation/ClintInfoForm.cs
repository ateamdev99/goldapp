﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;
using Data;


namespace Presentation
{
    public partial class ClintInfoForm : Form
    {
        public int TRid=0;
        
        public ClintInfoForm(tblClient obj,int id)
        {
            InitializeComponent();
            if (obj == null || obj.C_Id == 0)
            {
                tblClientBindingSource.DataSource = new tblClient();
                TRid = id;
                if (TRid > 0) { TRid = id; }
            }
            else
            {
                obj = ClientServicesClass.GetByIdClient(obj.C_Id);
                CmbPopulate();
                txtClintName.Text = obj.Name.ToString();
                if (string.IsNullOrEmpty(obj.Address))
                {
                    txtClintAddres.Text = null;
                }
                if (string.IsNullOrEmpty(obj.MobileNo))
                {
                    txtClintmobile.Text = null;
                }
                if (string.IsNullOrEmpty(obj.OfficeNo))
                {
                    txtClintOffice.Text = null;
                }
                if (string.IsNullOrEmpty(obj.District))
                {
                    txtDestrict.Text = null;
                }
                if (string.IsNullOrEmpty(obj.TMR))
                {
                    txtClintTMR.Text = null;
                }
                txtClintOpBlncCash.Text = obj.CashOpeningBalance.ToString();
                txtClintOpBlncGold.Text = obj.GoldOpeningBalance.ToString();
                cmbCity.SelectedValue = obj.CityId;
                tblClientBindingSource.DataSource = obj;
                btnSave.Text = "Update";
            }
        }
        private void ClintInfoForm_Load(object sender, EventArgs e)
        {
            tblCityBindingSource.DataSource = ClientServicesClass.GetAllCity();
        }

        //public enum Unit
        //{
        //    Grams = 0,
        //    TMR = 1,
        //    Both = 2
        //}
        //private int GetBalacSmsGoldWeight()
        //{
        //    if (rdbGrams.Checked)
        //    {
        //        return (int)Unit.Grams;
        //    }
        //    if (rdbTMR.Checked)
        //    {
        //        return (int)Unit.TMR;
        //    }
        //    //if (rdbBoth.Checked)
        //    //{
        //    //    return (int)Unit.Both;
        //    //}
        //    return (int)Unit.Both;
        //}
        void CmbPopulate()
        {
            tblCityBindingSource.DataSource = new tblCity();
            tblCityBindingSource.DataSource = ClientServicesClass.GetAllCity();
            cmbCity.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbCity.AutoCompleteSource = AutoCompleteSource.ListItems;
        }
        void RestForm()
        {
            txtClintNo.Clear();
            txtClintName.Clear();
            txtClintAddres.Clear();
            txtClintOffice.Refresh();
            txtClintmobile.Refresh();
            txtDestrict.Clear();
            txtClintOpBlncCash.Clear();
            txtClintOpBlncGold.Clear();
            txtClintTMR.Clear();
            cmbCity.SelectedIndex = -1;
            txtClintName.Focus();
            CmbPopulate();
            tblClientBindingSource.DataSource = new tblClient();
            btnSave.Text = "Add Client";
        }
        private void BtnSearchClient_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClintNo.Text != "" && txtClintNo.Text != "0" && txtClintNo.Text != "1")
                {
                    int id = Convert.ToInt32(txtClintNo.Text);
                    tblClient tblClientObj = ClientServicesClass.GetByIdClient(id);
                    if (tblClientObj != null)
                    {

                        tblClientBindingSource.DataSource = tblClientObj;
                        btnSave.Text = "Update";
                    }
                    else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); RestForm(); }
                }
                else { MessageBox.Show("Enter Correct Id ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); txtClintNo.Focus(); RestForm(); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error Catched", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtClintName.Text))
                {
                    MessageBox.Show("Enter Your Name", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtClintName.Focus();
                }
                else if(string.IsNullOrEmpty(txtClintAddres.Text))
                {
                    MessageBox.Show("Enter Your ClientAddres", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtClintAddres.Focus();
                }
                else if (txtClintmobile.MaskCompleted == false)
                {
                    MessageBox.Show("Enter Your Mobile #", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtClintmobile.Focus();
                }
                else if (cmbCity.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Your City", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                

                else
                {
                    tblClient tblClientObj = tblClientBindingSource.Current as tblClient;
                    if (tblClientObj.C_Id == 0)
                    {
                        DialogResult DR;
                        DR = MessageBox.Show("Click OK To Process !", "Are You Sure to Continue ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (DR == DialogResult.OK)
                        {
                            tblClientObj.BalncSMSGoldWight = 0;
                            tblClientObj.CashOpeningBalance = Conversion.toInt(txtClintOpBlncCash.Text);
                            tblClientObj.GoldOpeningBalance = Conversion.toInt(txtClintOpBlncGold.Text);
                            tblClientObj.AddedDate = DateTime.Now;
                            tblClientObj.UpdatedDate = DateTime.Now;
                            ClientServicesClass.Insert(tblClientObj);
                            //MessageBox.Show("Client Data Inserted ", "Succeed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (TRid > 0)
                            {
                                DialogResult DResult;
                                DResult = MessageBox.Show("Click OK To Process or Update Client to that Transaction !", "Are You Sure to Continue ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                                if (DResult == DialogResult.OK)
                                {
                                    int clientid = Convert.ToInt32(DBUtils.executeSqlGetID("select Max (C_Id) from tblClient "));
                                    DBUtils.ExecuteSQL("Update tblTransaction set ClientId='" + clientid + "' where T_Id='" + TRid + "'");
                                    //MessageBox.Show("Succesfully Client Added to Transaction #" + TRid);
                                }
                            }
                            Close();
                        }
                    }
                    else
                    {
                        DialogResult DResult;
                        DResult = MessageBox.Show("Sure to Updated !", "Click Yes to Update No to Cancel !", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (DResult == DialogResult.Yes)
                        {
                            tblClientObj.CashOpeningBalance = Conversion.toInt(txtClintOpBlncCash.Text);
                            tblClientObj.GoldOpeningBalance = Conversion.toInt(txtClintOpBlncGold.Text);
                            tblClientObj.UpdatedDate = DateTime.Now;
                            ClientServicesClass.Update(tblClientBindingSource.Current as tblClient);
                            RestForm();
                            Close();
                        }
                    }
                }
            } catch (Exception ex) { MessageBox.Show(ex.Message, "Error Catched", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            RestForm();
        }

        private void btnAddCity_Click(object sender, EventArgs e)
        {
            Presentation.CityForm frEmp = new CityForm();
            frEmp.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ClintInfoForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void txtClintOpBlncCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            //if (!char.IsControl(e.KeyChar))
            //{
            //    TextBox textBox = (TextBox)sender;
            //    if (textBox.Text.IndexOf('.') > -1 && textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 5)
            //    {
            //        e.Handled = true;
            //    }
            //}
        }

        private void txtClintNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }
    }
}
