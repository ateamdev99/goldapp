﻿namespace Presentation
{
    partial class TransactionEnteryAndSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.CmbClints = new System.Windows.Forms.ComboBox();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RdbNone = new System.Windows.Forms.RadioButton();
            this.Rdb4CashDebit = new System.Windows.Forms.RadioButton();
            this.Rdb3CashCredit = new System.Windows.Forms.RadioButton();
            this.Rdb2GoldDebit = new System.Windows.Forms.RadioButton();
            this.Rdb1GoldCredit = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnCancelOrder = new DevComponents.DotNetBar.ButtonX();
            this.BtnAddMoreItems = new DevComponents.DotNetBar.ButtonX();
            this.TxtClintNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BtnSaveExit = new DevComponents.DotNetBar.ButtonX();
            this.BtnCancel = new DevComponents.DotNetBar.ButtonX();
            this.BtnEntryPrint = new DevComponents.DotNetBar.ButtonX();
            this.ChkSendSms = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtSerialNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItemName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ChkSerhKratCashMemo = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ChkSerchGroupTr = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.DtpToSearchTr = new System.Windows.Forms.DateTimePicker();
            this.DtpFromSearchTr = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ChkFromDateSerchtr = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.BtnPreview = new DevComponents.DotNetBar.ButtonX();
            this.BtnSerchTransPrint = new DevComponents.DotNetBar.ButtonX();
            this.BtnSearchTransction = new DevComponents.DotNetBar.ButtonX();
            this.textBoxX7 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCashMemoNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label7 = new System.Windows.Forms.Label();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.DtpFromDeleteTr = new System.Windows.Forms.DateTimePicker();
            this.DtpToDeleteTr = new System.Windows.Forms.DateTimePicker();
            this.txtToSrNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ChkFromdateDeleteTr = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.BtnExit = new DevComponents.DotNetBar.ButtonX();
            this.BtnDeleteTr = new DevComponents.DotNetBar.ButtonX();
            this.TxtFrmSrNo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label8 = new System.Windows.Forms.Label();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.ColorTable = DevComponents.DotNetBar.Controls.ePanelColorTable.Yellow;
            this.groupPanel1.Controls.Add(this.CmbClints);
            this.groupPanel1.Controls.Add(this.RdbNone);
            this.groupPanel1.Controls.Add(this.Rdb4CashDebit);
            this.groupPanel1.Controls.Add(this.Rdb3CashCredit);
            this.groupPanel1.Controls.Add(this.Rdb2GoldDebit);
            this.groupPanel1.Controls.Add(this.Rdb1GoldCredit);
            this.groupPanel1.Controls.Add(this.label9);
            this.groupPanel1.Controls.Add(this.BtnCancelOrder);
            this.groupPanel1.Controls.Add(this.BtnAddMoreItems);
            this.groupPanel1.Controls.Add(this.TxtClintNo);
            this.groupPanel1.Controls.Add(this.BtnSaveExit);
            this.groupPanel1.Controls.Add(this.BtnCancel);
            this.groupPanel1.Controls.Add(this.BtnEntryPrint);
            this.groupPanel1.Controls.Add(this.ChkSendSms);
            this.groupPanel1.Controls.Add(this.numericUpDown1);
            this.groupPanel1.Controls.Add(this.label6);
            this.groupPanel1.Controls.Add(this.TxtSerialNo);
            this.groupPanel1.Controls.Add(this.label5);
            this.groupPanel1.Controls.Add(this.label4);
            this.groupPanel1.Controls.Add(this.label3);
            this.groupPanel1.Controls.Add(this.TxtItemName);
            this.groupPanel1.Controls.Add(this.label2);
            this.groupPanel1.Controls.Add(this.label1);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel1.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 31);
            this.groupPanel1.Size = new System.Drawing.Size(925, 271);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(178)))));
            this.groupPanel1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(217)))), ((int)(((byte)(69)))));
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(147)))), ((int)(((byte)(17)))));
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColor = System.Drawing.Color.Blue;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 1;
            this.groupPanel1.Text = "Transactions Entery";
            // 
            // CmbClints
            // 
            this.CmbClints.BackColor = System.Drawing.Color.White;
            this.CmbClints.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.tblTransactionBindingSource, "ClientId", true));
            this.CmbClints.DataSource = this.tblClientBindingSource;
            this.CmbClints.DisplayMember = "C_Name";
            this.CmbClints.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbClints.ForeColor = System.Drawing.Color.Gray;
            this.CmbClints.FormattingEnabled = true;
            this.CmbClints.Location = new System.Drawing.Point(393, 50);
            this.CmbClints.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CmbClints.Name = "CmbClints";
            this.CmbClints.Size = new System.Drawing.Size(344, 25);
            this.CmbClints.TabIndex = 31;
            this.CmbClints.ValueMember = "C_Id";
            this.CmbClints.SelectedIndexChanged += new System.EventHandler(this.CmbClints_SelectedIndexChanged);
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // tblClientBindingSource
            // 
            this.tblClientBindingSource.DataSource = typeof(Data.tblClient);
            // 
            // RdbNone
            // 
            this.RdbNone.AutoSize = true;
            this.RdbNone.BackColor = System.Drawing.Color.Transparent;
            this.RdbNone.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbNone.ForeColor = System.Drawing.Color.Blue;
            this.RdbNone.Location = new System.Drawing.Point(241, 206);
            this.RdbNone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RdbNone.Name = "RdbNone";
            this.RdbNone.Size = new System.Drawing.Size(66, 24);
            this.RdbNone.TabIndex = 30;
            this.RdbNone.TabStop = true;
            this.RdbNone.Text = "None";
            this.RdbNone.UseVisualStyleBackColor = false;
            // 
            // Rdb4CashDebit
            // 
            this.Rdb4CashDebit.AutoSize = true;
            this.Rdb4CashDebit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb4CashDebit.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb4CashDebit.ForeColor = System.Drawing.Color.Blue;
            this.Rdb4CashDebit.Location = new System.Drawing.Point(569, 169);
            this.Rdb4CashDebit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Rdb4CashDebit.Name = "Rdb4CashDebit";
            this.Rdb4CashDebit.Size = new System.Drawing.Size(117, 24);
            this.Rdb4CashDebit.TabIndex = 29;
            this.Rdb4CashDebit.TabStop = true;
            this.Rdb4CashDebit.Text = "4 Cash Debit";
            this.Rdb4CashDebit.UseVisualStyleBackColor = false;
            // 
            // Rdb3CashCredit
            // 
            this.Rdb3CashCredit.AutoSize = true;
            this.Rdb3CashCredit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb3CashCredit.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb3CashCredit.ForeColor = System.Drawing.Color.Blue;
            this.Rdb3CashCredit.Location = new System.Drawing.Point(567, 132);
            this.Rdb3CashCredit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Rdb3CashCredit.Name = "Rdb3CashCredit";
            this.Rdb3CashCredit.Size = new System.Drawing.Size(122, 24);
            this.Rdb3CashCredit.TabIndex = 28;
            this.Rdb3CashCredit.TabStop = true;
            this.Rdb3CashCredit.Text = "3 Cash Credit";
            this.Rdb3CashCredit.UseVisualStyleBackColor = false;
            // 
            // Rdb2GoldDebit
            // 
            this.Rdb2GoldDebit.AutoSize = true;
            this.Rdb2GoldDebit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb2GoldDebit.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb2GoldDebit.ForeColor = System.Drawing.Color.Blue;
            this.Rdb2GoldDebit.Location = new System.Drawing.Point(241, 169);
            this.Rdb2GoldDebit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Rdb2GoldDebit.Name = "Rdb2GoldDebit";
            this.Rdb2GoldDebit.Size = new System.Drawing.Size(120, 24);
            this.Rdb2GoldDebit.TabIndex = 27;
            this.Rdb2GoldDebit.TabStop = true;
            this.Rdb2GoldDebit.Text = "2 Gold Debit";
            this.Rdb2GoldDebit.UseVisualStyleBackColor = false;
            // 
            // Rdb1GoldCredit
            // 
            this.Rdb1GoldCredit.AutoSize = true;
            this.Rdb1GoldCredit.BackColor = System.Drawing.Color.Transparent;
            this.Rdb1GoldCredit.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb1GoldCredit.ForeColor = System.Drawing.Color.Blue;
            this.Rdb1GoldCredit.Location = new System.Drawing.Point(241, 132);
            this.Rdb1GoldCredit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Rdb1GoldCredit.Name = "Rdb1GoldCredit";
            this.Rdb1GoldCredit.Size = new System.Drawing.Size(123, 24);
            this.Rdb1GoldCredit.TabIndex = 26;
            this.Rdb1GoldCredit.TabStop = true;
            this.Rdb1GoldCredit.Text = "1 Gold Credit";
            this.Rdb1GoldCredit.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(59, 205);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 20);
            this.label9.TabIndex = 25;
            this.label9.Text = "Credit/Debit :";
            // 
            // BtnCancelOrder
            // 
            this.BtnCancelOrder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCancelOrder.BackColor = System.Drawing.Color.White;
            this.BtnCancelOrder.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnCancelOrder.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelOrder.Location = new System.Drawing.Point(774, 197);
            this.BtnCancelOrder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnCancelOrder.Name = "BtnCancelOrder";
            this.BtnCancelOrder.Size = new System.Drawing.Size(136, 41);
            this.BtnCancelOrder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCancelOrder.TabIndex = 24;
            this.BtnCancelOrder.Text = "Cancel Order";
            this.BtnCancelOrder.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // BtnAddMoreItems
            // 
            this.BtnAddMoreItems.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnAddMoreItems.BackColor = System.Drawing.Color.White;
            this.BtnAddMoreItems.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnAddMoreItems.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddMoreItems.Location = new System.Drawing.Point(774, 149);
            this.BtnAddMoreItems.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAddMoreItems.Name = "BtnAddMoreItems";
            this.BtnAddMoreItems.Size = new System.Drawing.Size(136, 41);
            this.BtnAddMoreItems.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnAddMoreItems.TabIndex = 23;
            this.BtnAddMoreItems.Text = "Add More Items";
            this.BtnAddMoreItems.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // TxtClintNo
            // 
            this.TxtClintNo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtClintNo.Border.Class = "TextBoxBorder";
            this.TxtClintNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtClintNo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClintNo.ForeColor = System.Drawing.Color.Gray;
            this.TxtClintNo.Location = new System.Drawing.Point(199, 52);
            this.TxtClintNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtClintNo.Name = "TxtClintNo";
            this.TxtClintNo.ReadOnly = true;
            this.TxtClintNo.Size = new System.Drawing.Size(187, 25);
            this.TxtClintNo.TabIndex = 22;
            this.TxtClintNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnSaveExit
            // 
            this.BtnSaveExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSaveExit.BackColor = System.Drawing.Color.White;
            this.BtnSaveExit.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnSaveExit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSaveExit.Location = new System.Drawing.Point(774, 54);
            this.BtnSaveExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSaveExit.Name = "BtnSaveExit";
            this.BtnSaveExit.Size = new System.Drawing.Size(136, 41);
            this.BtnSaveExit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSaveExit.TabIndex = 20;
            this.BtnSaveExit.Text = "Save Exit";
            this.BtnSaveExit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnSaveExit.Click += new System.EventHandler(this.BtnSaveExit_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCancel.BackColor = System.Drawing.Color.White;
            this.BtnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Location = new System.Drawing.Point(774, 101);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(136, 41);
            this.BtnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCancel.TabIndex = 19;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // BtnEntryPrint
            // 
            this.BtnEntryPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnEntryPrint.BackColor = System.Drawing.Color.White;
            this.BtnEntryPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnEntryPrint.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEntryPrint.Location = new System.Drawing.Point(774, 6);
            this.BtnEntryPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnEntryPrint.Name = "BtnEntryPrint";
            this.BtnEntryPrint.Size = new System.Drawing.Size(136, 41);
            this.BtnEntryPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnEntryPrint.TabIndex = 17;
            this.BtnEntryPrint.Text = "Print";
            this.BtnEntryPrint.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // ChkSendSms
            // 
            this.ChkSendSms.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkSendSms.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkSendSms.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSendSms.Location = new System.Drawing.Point(569, 197);
            this.ChkSendSms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkSendSms.Name = "ChkSendSms";
            this.ChkSendSms.Size = new System.Drawing.Size(143, 28);
            this.ChkSendSms.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkSendSms.TabIndex = 16;
            this.ChkSendSms.Text = "Send SMS";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BackColor = System.Drawing.SystemColors.Control;
            this.numericUpDown1.Location = new System.Drawing.Point(619, 10);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(118, 28);
            this.numericUpDown1.TabIndex = 15;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gray;
            this.label6.Location = new System.Drawing.Point(463, 10);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "5 No of Copies :";
            // 
            // TxtSerialNo
            // 
            this.TxtSerialNo.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtSerialNo.Border.Class = "TextBoxBorder";
            this.TxtSerialNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSerialNo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "T_Id", true));
            this.TxtSerialNo.Font = new System.Drawing.Font("Segoe UI Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSerialNo.ForeColor = System.Drawing.Color.White;
            this.TxtSerialNo.Location = new System.Drawing.Point(200, 11);
            this.TxtSerialNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSerialNo.Name = "TxtSerialNo";
            this.TxtSerialNo.ReadOnly = true;
            this.TxtSerialNo.Size = new System.Drawing.Size(194, 25);
            this.TxtSerialNo.TabIndex = 11;
            this.TxtSerialNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(30, 131);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Credit/Qaraz (+) :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(32, 168);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Debit/Wasool(-) :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(30, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Client No/Name :";
            // 
            // TxtItemName
            // 
            this.TxtItemName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtItemName.Border.Class = "TextBoxBorder";
            this.TxtItemName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtItemName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Itme_Name", true));
            this.TxtItemName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItemName.ForeColor = System.Drawing.Color.Gray;
            this.TxtItemName.Location = new System.Drawing.Point(200, 92);
            this.TxtItemName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtItemName.Name = "TxtItemName";
            this.TxtItemName.Size = new System.Drawing.Size(539, 25);
            this.TxtItemName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(65, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Item Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(81, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Serial No :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.ColorTable = DevComponents.DotNetBar.Controls.ePanelColorTable.Yellow;
            this.groupPanel2.Controls.Add(this.ChkSerhKratCashMemo);
            this.groupPanel2.Controls.Add(this.ChkSerchGroupTr);
            this.groupPanel2.Controls.Add(this.DtpToSearchTr);
            this.groupPanel2.Controls.Add(this.DtpFromSearchTr);
            this.groupPanel2.Controls.Add(this.label12);
            this.groupPanel2.Controls.Add(this.label10);
            this.groupPanel2.Controls.Add(this.ChkFromDateSerchtr);
            this.groupPanel2.Controls.Add(this.BtnPreview);
            this.groupPanel2.Controls.Add(this.BtnSerchTransPrint);
            this.groupPanel2.Controls.Add(this.BtnSearchTransction);
            this.groupPanel2.Controls.Add(this.textBoxX7);
            this.groupPanel2.Controls.Add(this.TxtCashMemoNo);
            this.groupPanel2.Controls.Add(this.label7);
            this.groupPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel2.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel2.Location = new System.Drawing.Point(0, 271);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(925, 177);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(178)))));
            this.groupPanel2.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(217)))), ((int)(((byte)(69)))));
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(147)))), ((int)(((byte)(17)))));
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColor = System.Drawing.Color.Blue;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 21;
            this.groupPanel2.Text = "Search Transaction";
            this.groupPanel2.Click += new System.EventHandler(this.groupPanel2_Click);
            // 
            // ChkSerhKratCashMemo
            // 
            this.ChkSerhKratCashMemo.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkSerhKratCashMemo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkSerhKratCashMemo.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSerhKratCashMemo.Location = new System.Drawing.Point(196, 100);
            this.ChkSerhKratCashMemo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkSerhKratCashMemo.Name = "ChkSerhKratCashMemo";
            this.ChkSerhKratCashMemo.Size = new System.Drawing.Size(198, 28);
            this.ChkSerhKratCashMemo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkSerhKratCashMemo.TabIndex = 35;
            this.ChkSerhKratCashMemo.Text = "Search Karat Cash Memo(s)";
            // 
            // ChkSerchGroupTr
            // 
            this.ChkSerchGroupTr.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkSerchGroupTr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkSerchGroupTr.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSerchGroupTr.Location = new System.Drawing.Point(440, 98);
            this.ChkSerchGroupTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkSerchGroupTr.Name = "ChkSerchGroupTr";
            this.ChkSerchGroupTr.Size = new System.Drawing.Size(204, 28);
            this.ChkSerchGroupTr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkSerchGroupTr.TabIndex = 34;
            this.ChkSerchGroupTr.Text = "Search Group Transaction(s)";
            // 
            // DtpToSearchTr
            // 
            this.DtpToSearchTr.CustomFormat = "dd/MM/yyyy";
            this.DtpToSearchTr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpToSearchTr.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToSearchTr.Location = new System.Drawing.Point(440, 53);
            this.DtpToSearchTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DtpToSearchTr.Name = "DtpToSearchTr";
            this.DtpToSearchTr.Size = new System.Drawing.Size(203, 23);
            this.DtpToSearchTr.TabIndex = 33;
            // 
            // DtpFromSearchTr
            // 
            this.DtpFromSearchTr.CustomFormat = "dd/MM/yyyy";
            this.DtpFromSearchTr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpFromSearchTr.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFromSearchTr.Location = new System.Drawing.Point(199, 53);
            this.DtpFromSearchTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DtpFromSearchTr.Name = "DtpFromSearchTr";
            this.DtpFromSearchTr.Size = new System.Drawing.Size(194, 23);
            this.DtpFromSearchTr.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Gray;
            this.label12.Location = new System.Drawing.Point(402, 57);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 20);
            this.label12.TabIndex = 29;
            this.label12.Text = "To";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Gray;
            this.label10.Location = new System.Drawing.Point(72, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "Fom Date :";
            // 
            // ChkFromDateSerchtr
            // 
            // 
            // 
            // 
            this.ChkFromDateSerchtr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkFromDateSerchtr.Location = new System.Drawing.Point(60, 48);
            this.ChkFromDateSerchtr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkFromDateSerchtr.Name = "ChkFromDateSerchtr";
            this.ChkFromDateSerchtr.Size = new System.Drawing.Size(17, 28);
            this.ChkFromDateSerchtr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkFromDateSerchtr.TabIndex = 23;
            // 
            // BtnPreview
            // 
            this.BtnPreview.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPreview.BackColor = System.Drawing.Color.White;
            this.BtnPreview.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnPreview.Location = new System.Drawing.Point(774, 104);
            this.BtnPreview.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnPreview.Name = "BtnPreview";
            this.BtnPreview.Size = new System.Drawing.Size(136, 36);
            this.BtnPreview.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPreview.TabIndex = 22;
            this.BtnPreview.Text = "Privew";
            this.BtnPreview.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // BtnSerchTransPrint
            // 
            this.BtnSerchTransPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSerchTransPrint.BackColor = System.Drawing.Color.White;
            this.BtnSerchTransPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnSerchTransPrint.Location = new System.Drawing.Point(774, 62);
            this.BtnSerchTransPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSerchTransPrint.Name = "BtnSerchTransPrint";
            this.BtnSerchTransPrint.Size = new System.Drawing.Size(136, 36);
            this.BtnSerchTransPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSerchTransPrint.TabIndex = 21;
            this.BtnSerchTransPrint.Text = "Print";
            this.BtnSerchTransPrint.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // BtnSearchTransction
            // 
            this.BtnSearchTransction.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSearchTransction.BackColor = System.Drawing.Color.White;
            this.BtnSearchTransction.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnSearchTransction.Location = new System.Drawing.Point(774, 11);
            this.BtnSearchTransction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSearchTransction.Name = "BtnSearchTransction";
            this.BtnSearchTransction.Size = new System.Drawing.Size(136, 42);
            this.BtnSearchTransction.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSearchTransction.TabIndex = 20;
            this.BtnSearchTransction.Text = "Search";
            this.BtnSearchTransction.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnSearchTransction.Click += new System.EventHandler(this.BtnSearchTransction_Click);
            // 
            // textBoxX7
            // 
            this.textBoxX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX7.Border.Class = "TextBoxBorder";
            this.textBoxX7.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX7.ForeColor = System.Drawing.Color.Gray;
            this.textBoxX7.Location = new System.Drawing.Point(440, 12);
            this.textBoxX7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxX7.Name = "textBoxX7";
            this.textBoxX7.ReadOnly = true;
            this.textBoxX7.Size = new System.Drawing.Size(204, 25);
            this.textBoxX7.TabIndex = 8;
            this.textBoxX7.Text = "<<=Enter Receipt No to Search";
            this.textBoxX7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtCashMemoNo
            // 
            this.TxtCashMemoNo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtCashMemoNo.Border.Class = "TextBoxBorder";
            this.TxtCashMemoNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCashMemoNo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCashMemoNo.ForeColor = System.Drawing.Color.Gray;
            this.TxtCashMemoNo.Location = new System.Drawing.Point(196, 12);
            this.TxtCashMemoNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtCashMemoNo.Name = "TxtCashMemoNo";
            this.TxtCashMemoNo.Size = new System.Drawing.Size(198, 25);
            this.TxtCashMemoNo.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(33, 14);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Cash Memo No :";
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.ColorTable = DevComponents.DotNetBar.Controls.ePanelColorTable.Yellow;
            this.groupPanel3.Controls.Add(this.DtpFromDeleteTr);
            this.groupPanel3.Controls.Add(this.DtpToDeleteTr);
            this.groupPanel3.Controls.Add(this.txtToSrNo);
            this.groupPanel3.Controls.Add(this.label16);
            this.groupPanel3.Controls.Add(this.label14);
            this.groupPanel3.Controls.Add(this.label15);
            this.groupPanel3.Controls.Add(this.ChkFromdateDeleteTr);
            this.groupPanel3.Controls.Add(this.BtnExit);
            this.groupPanel3.Controls.Add(this.BtnDeleteTr);
            this.groupPanel3.Controls.Add(this.TxtFrmSrNo);
            this.groupPanel3.Controls.Add(this.label8);
            this.groupPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel3.Location = new System.Drawing.Point(0, 448);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(925, 145);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(178)))));
            this.groupPanel3.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(217)))), ((int)(((byte)(69)))));
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(147)))), ((int)(((byte)(17)))));
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColor = System.Drawing.Color.Blue;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 22;
            this.groupPanel3.Text = "Delete Transaction";
            // 
            // DtpFromDeleteTr
            // 
            this.DtpFromDeleteTr.CustomFormat = "dd/MM/yyyy";
            this.DtpFromDeleteTr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpFromDeleteTr.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFromDeleteTr.Location = new System.Drawing.Point(196, 52);
            this.DtpFromDeleteTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DtpFromDeleteTr.Name = "DtpFromDeleteTr";
            this.DtpFromDeleteTr.Size = new System.Drawing.Size(194, 23);
            this.DtpFromDeleteTr.TabIndex = 38;
            // 
            // DtpToDeleteTr
            // 
            this.DtpToDeleteTr.CustomFormat = "dd/MM/yyyy";
            this.DtpToDeleteTr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpToDeleteTr.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToDeleteTr.Location = new System.Drawing.Point(449, 52);
            this.DtpToDeleteTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DtpToDeleteTr.Name = "DtpToDeleteTr";
            this.DtpToDeleteTr.Size = new System.Drawing.Size(194, 23);
            this.DtpToDeleteTr.TabIndex = 37;
            // 
            // txtToSrNo
            // 
            this.txtToSrNo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtToSrNo.Border.Class = "TextBoxBorder";
            this.txtToSrNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtToSrNo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToSrNo.ForeColor = System.Drawing.Color.Gray;
            this.txtToSrNo.Location = new System.Drawing.Point(449, 14);
            this.txtToSrNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtToSrNo.Name = "txtToSrNo";
            this.txtToSrNo.Size = new System.Drawing.Size(195, 25);
            this.txtToSrNo.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Gray;
            this.label16.Location = new System.Drawing.Point(402, 20);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 20);
            this.label16.TabIndex = 35;
            this.label16.Text = "To";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Gray;
            this.label14.Location = new System.Drawing.Point(402, 57);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 20);
            this.label14.TabIndex = 34;
            this.label14.Text = "To";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Gray;
            this.label15.Location = new System.Drawing.Point(64, 54);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 20);
            this.label15.TabIndex = 31;
            this.label15.Text = "Fom Date :";
            // 
            // ChkFromdateDeleteTr
            // 
            this.ChkFromdateDeleteTr.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChkFromdateDeleteTr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkFromdateDeleteTr.Location = new System.Drawing.Point(51, 49);
            this.ChkFromdateDeleteTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkFromdateDeleteTr.Name = "ChkFromdateDeleteTr";
            this.ChkFromdateDeleteTr.Size = new System.Drawing.Size(17, 28);
            this.ChkFromdateDeleteTr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkFromdateDeleteTr.TabIndex = 30;
            // 
            // BtnExit
            // 
            this.BtnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnExit.BackColor = System.Drawing.Color.White;
            this.BtnExit.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnExit.Location = new System.Drawing.Point(774, 53);
            this.BtnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(136, 39);
            this.BtnExit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnExit.TabIndex = 22;
            this.BtnExit.Text = "Exit";
            this.BtnExit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // BtnDeleteTr
            // 
            this.BtnDeleteTr.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnDeleteTr.BackColor = System.Drawing.Color.White;
            this.BtnDeleteTr.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnDeleteTr.Location = new System.Drawing.Point(774, 7);
            this.BtnDeleteTr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnDeleteTr.Name = "BtnDeleteTr";
            this.BtnDeleteTr.Size = new System.Drawing.Size(136, 38);
            this.BtnDeleteTr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnDeleteTr.TabIndex = 21;
            this.BtnDeleteTr.Text = "Delete";
            this.BtnDeleteTr.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // TxtFrmSrNo
            // 
            this.TxtFrmSrNo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtFrmSrNo.Border.Class = "TextBoxBorder";
            this.TxtFrmSrNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFrmSrNo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFrmSrNo.ForeColor = System.Drawing.Color.Gray;
            this.TxtFrmSrNo.Location = new System.Drawing.Point(198, 16);
            this.TxtFrmSrNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFrmSrNo.Name = "TxtFrmSrNo";
            this.TxtFrmSrNo.Size = new System.Drawing.Size(195, 25);
            this.TxtFrmSrNo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(56, 14);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 20);
            this.label8.TabIndex = 6;
            this.label8.Text = "From S.No :";
            // 
            // TransactionEnteryAndSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(925, 570);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "TransactionEnteryAndSearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TransactionEnteryAndSearchForm";
            this.Load += new System.EventHandler(this.TransactionEnteryAndSearchForm_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX BtnSaveExit;
        private DevComponents.DotNetBar.ButtonX BtnCancel;
        private DevComponents.DotNetBar.ButtonX BtnEntryPrint;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkSendSms;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label6;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSerialNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtItemName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX BtnPreview;
        private DevComponents.DotNetBar.ButtonX BtnSerchTransPrint;
        private DevComponents.DotNetBar.ButtonX BtnSearchTransction;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX7;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCashMemoNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton Rdb4CashDebit;
        private System.Windows.Forms.RadioButton Rdb3CashCredit;
        private System.Windows.Forms.RadioButton Rdb2GoldDebit;
        private System.Windows.Forms.RadioButton Rdb1GoldCredit;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.ButtonX BtnCancelOrder;
        private DevComponents.DotNetBar.ButtonX BtnAddMoreItems;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtClintNo;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX BtnExit;
        private DevComponents.DotNetBar.ButtonX BtnDeleteTr;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFrmSrNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton RdbNone;
        private System.Windows.Forms.Label label10;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkFromDateSerchtr;
        private System.Windows.Forms.ComboBox CmbClints;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkFromdateDeleteTr;
        private DevComponents.DotNetBar.Controls.TextBoxX txtToSrNo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.BindingSource tblClientBindingSource;
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private System.Windows.Forms.DateTimePicker DtpToSearchTr;
        private System.Windows.Forms.DateTimePicker DtpFromSearchTr;
        private System.Windows.Forms.DateTimePicker DtpFromDeleteTr;
        private System.Windows.Forms.DateTimePicker DtpToDeleteTr;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkSerhKratCashMemo;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkSerchGroupTr;
    }
}