﻿namespace Presentation
{
    partial class LaboratoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.HeadertableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.LabortorytabControl = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.tableLayoutPanelKACHSONA = new System.Windows.Forms.TableLayoutPanel();
            this.txtKachaSonaGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtSumKachaSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtPurityKachaSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txtDescriptionKachasSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmbClientsKachaSona = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Rdb2GoldDebitKachaSona = new System.Windows.Forms.RadioButton();
            this.Rdb1GoldCreditKachaSona = new System.Windows.Forms.RadioButton();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.line1 = new DevComponents.DotNetBar.Controls.Line();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnAddTransactionKachaSona = new DevComponents.DotNetBar.ButtonX();
            this.btnRefreshKachSona = new DevComponents.DotNetBar.ButtonX();
            this.line2 = new DevComponents.DotNetBar.Controls.Line();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txtPureGoldKachaSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSumCustomToKachaSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtSumCustomFromKachaSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.kachaSonatabItem = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.tableLayoutPanelOTHERS = new System.Windows.Forms.TableLayoutPanel();
            this.txtPureGoldNoneKhalisSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.txtDescriptionKhalisSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmbClientsKhilsSona = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rdbrdbGoldDebitKhalisSona = new System.Windows.Forms.RadioButton();
            this.rdbGoldCreditKhalisSona = new System.Windows.Forms.RadioButton();
            this.labelX31 = new DevComponents.DotNetBar.LabelX();
            this.labelX34 = new DevComponents.DotNetBar.LabelX();
            this.line5 = new DevComponents.DotNetBar.Controls.Line();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnAddTransactionKhalisSona = new DevComponents.DotNetBar.ButtonX();
            this.btnRefreshKhalisSona = new DevComponents.DotNetBar.ButtonX();
            this.line6 = new DevComponents.DotNetBar.Controls.Line();
            this.panel9 = new System.Windows.Forms.Panel();
            this.labelX33 = new DevComponents.DotNetBar.LabelX();
            this.txtSumTo990KhalisSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX32 = new DevComponents.DotNetBar.LabelX();
            this.txtSumFrom997KhalisSona = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtPureGoldKhalisSonaFinal = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.tabItemKhalisSona = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel5 = new DevComponents.DotNetBar.TabControlPanel();
            this.tableLayoutPanelCASH = new System.Windows.Forms.TableLayoutPanel();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.txtDiscriptionCash = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnAddTransactionCash = new DevComponents.DotNetBar.ButtonX();
            this.btnRefreshCash = new DevComponents.DotNetBar.ButtonX();
            this.labelX36 = new DevComponents.DotNetBar.LabelX();
            this.panel13 = new System.Windows.Forms.Panel();
            this.rdbCashDebit = new System.Windows.Forms.RadioButton();
            this.rdbCashCredit = new System.Windows.Forms.RadioButton();
            this.txtAmountCash = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.line7 = new DevComponents.DotNetBar.Controls.Line();
            this.labelX35 = new DevComponents.DotNetBar.LabelX();
            this.cmbCash = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtPureGoldCash = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtGoldRateCash = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.CashtabItem = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.tableLayoutPanelREFINEGOLD = new System.Windows.Forms.TableLayoutPanel();
            this.txtImpureGoldRefineGold = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.txtPurityRefineGold = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.txtPureRefineGold = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDisriptionRefineGold = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX40 = new DevComponents.DotNetBar.LabelX();
            this.line4 = new DevComponents.DotNetBar.Controls.Line();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnAddTransactionRefineGold = new DevComponents.DotNetBar.ButtonX();
            this.btnRefreshRefineGold = new DevComponents.DotNetBar.ButtonX();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rdbGoldDebitRefineGold = new System.Windows.Forms.RadioButton();
            this.rdbGoldCreditRefineGold = new System.Windows.Forms.RadioButton();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.refineGoldtabItem = new DevComponents.DotNetBar.TabItem(this.components);
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel6 = new DevComponents.DotNetBar.TabControlPanel();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2.SuspendLayout();
            this.HeadertableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabortorytabControl)).BeginInit();
            this.LabortorytabControl.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.tableLayoutPanelKACHSONA.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            this.tableLayoutPanelOTHERS.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabControlPanel5.SuspendLayout();
            this.tableLayoutPanelCASH.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            this.tableLayoutPanelREFINEGOLD.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.panel2.Controls.Add(this.HeadertableLayoutPanel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(795, 63);
            this.panel2.TabIndex = 127;
            // 
            // HeadertableLayoutPanel
            // 
            this.HeadertableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HeadertableLayoutPanel.ColumnCount = 5;
            this.HeadertableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.HeadertableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.HeadertableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.HeadertableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.HeadertableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.HeadertableLayoutPanel.Controls.Add(this.pictureBox1, 3, 0);
            this.HeadertableLayoutPanel.Controls.Add(this.labelX2, 0, 0);
            this.HeadertableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.HeadertableLayoutPanel.Name = "HeadertableLayoutPanel";
            this.HeadertableLayoutPanel.RowCount = 2;
            this.HeadertableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.HeadertableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.HeadertableLayoutPanel.Size = new System.Drawing.Size(792, 57);
            this.HeadertableLayoutPanel.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Presentation.Properties.Resources._2699438;
            this.pictureBox1.Location = new System.Drawing.Point(477, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.HeadertableLayoutPanel.SetRowSpan(this.pictureBox1, 2);
            this.pictureBox1.Size = new System.Drawing.Size(48, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 126;
            this.pictureBox1.TabStop = false;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HeadertableLayoutPanel.SetColumnSpan(this.labelX2, 3);
            this.labelX2.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(3, 3);
            this.labelX2.Name = "labelX2";
            this.HeadertableLayoutPanel.SetRowSpan(this.labelX2, 2);
            this.labelX2.Size = new System.Drawing.Size(468, 51);
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "Laboratory لیبارٹری";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // LabortorytabControl
            // 
            this.LabortorytabControl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabortorytabControl.AutoHideSystemBox = false;
            this.LabortorytabControl.BackColor = System.Drawing.Color.White;
            this.LabortorytabControl.CanReorderTabs = true;
            this.LabortorytabControl.Controls.Add(this.tabControlPanel1);
            this.LabortorytabControl.Controls.Add(this.tabControlPanel2);
            this.LabortorytabControl.Controls.Add(this.tabControlPanel5);
            this.LabortorytabControl.Controls.Add(this.tabControlPanel3);
            this.LabortorytabControl.Font = new System.Drawing.Font("Segoe UI Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabortorytabControl.Location = new System.Drawing.Point(3, 63);
            this.LabortorytabControl.Name = "LabortorytabControl";
            this.LabortorytabControl.SelectedTabFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabortorytabControl.SelectedTabIndex = 1;
            this.LabortorytabControl.Size = new System.Drawing.Size(789, 596);
            this.LabortorytabControl.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Document;
            this.LabortorytabControl.TabIndex = 128;
            this.LabortorytabControl.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.LabortorytabControl.Tabs.Add(this.kachaSonatabItem);
            this.LabortorytabControl.Tabs.Add(this.tabItemKhalisSona);
            this.LabortorytabControl.Tabs.Add(this.CashtabItem);
            this.LabortorytabControl.Tabs.Add(this.refineGoldtabItem);
            this.LabortorytabControl.Text = "Laboratory";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.tableLayoutPanelKACHSONA);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 32);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(789, 564);
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(247)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.kachaSonatabItem;
            // 
            // tableLayoutPanelKACHSONA
            // 
            this.tableLayoutPanelKACHSONA.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanelKACHSONA.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelKACHSONA.ColumnCount = 4;
            this.tableLayoutPanelKACHSONA.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.044117F));
            this.tableLayoutPanelKACHSONA.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.37411F));
            this.tableLayoutPanelKACHSONA.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.53764F));
            this.tableLayoutPanelKACHSONA.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.044117F));
            this.tableLayoutPanelKACHSONA.Controls.Add(this.txtKachaSonaGrams, 2, 1);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX8, 1, 7);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX1, 1, 1);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX3, 1, 2);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX6, 1, 3);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.txtSumKachaSona, 2, 2);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.txtPurityKachaSona, 2, 3);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX9, 1, 9);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX10, 1, 10);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.txtDescriptionKachasSona, 2, 10);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.cmbClientsKachaSona, 2, 9);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.panel3, 2, 11);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX7, 1, 11);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX4, 1, 6);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.line1, 0, 8);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.panel4, 1, 12);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.line2, 0, 4);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.panel1, 2, 5);
            this.tableLayoutPanelKACHSONA.Controls.Add(this.labelX18, 0, 0);
            this.tableLayoutPanelKACHSONA.Location = new System.Drawing.Point(0, 6);
            this.tableLayoutPanelKACHSONA.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelKACHSONA.Name = "tableLayoutPanelKACHSONA";
            this.tableLayoutPanelKACHSONA.RowCount = 15;
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.541116F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.520997F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.520997F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.520997F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.785668F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.268309F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.326792F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.459418F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.580492F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.090909F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.818182F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.63636F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.727273F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.181818F));
            this.tableLayoutPanelKACHSONA.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.520997F));
            this.tableLayoutPanelKACHSONA.Size = new System.Drawing.Size(787, 550);
            this.tableLayoutPanelKACHSONA.TabIndex = 2;
            // 
            // txtKachaSonaGrams
            // 
            this.txtKachaSonaGrams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKachaSonaGrams.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtKachaSonaGrams.Border.Class = "TextBoxBorder";
            this.txtKachaSonaGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtKachaSonaGrams.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKachaSonaGrams.ForeColor = System.Drawing.Color.Black;
            this.txtKachaSonaGrams.Location = new System.Drawing.Point(217, 54);
            this.txtKachaSonaGrams.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtKachaSonaGrams.Name = "txtKachaSonaGrams";
            this.txtKachaSonaGrams.Size = new System.Drawing.Size(533, 33);
            this.txtKachaSonaGrams.TabIndex = 0;
            this.txtKachaSonaGrams.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKachaSonaGrams_KeyPress_1);
            // 
            // labelX8
            // 
            this.labelX8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX8.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX8.Location = new System.Drawing.Point(33, 246);
            this.labelX8.Margin = new System.Windows.Forms.Padding(2);
            this.labelX8.Name = "labelX8";
            this.labelX8.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX8.Size = new System.Drawing.Size(179, 37);
            this.labelX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX8.TabIndex = 143;
            this.labelX8.Text = "Khalis Sona";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX1.Location = new System.Drawing.Point(33, 54);
            this.labelX1.Margin = new System.Windows.Forms.Padding(2);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX1.Size = new System.Drawing.Size(179, 32);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX1.TabIndex = 69;
            this.labelX1.Text = "Kacha Sona in Gram";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX3.Location = new System.Drawing.Point(33, 90);
            this.labelX3.Margin = new System.Windows.Forms.Padding(2);
            this.labelX3.Name = "labelX3";
            this.labelX3.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX3.Size = new System.Drawing.Size(179, 32);
            this.labelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX3.TabIndex = 72;
            this.labelX3.Text = "Sum";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX6.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX6.Location = new System.Drawing.Point(33, 126);
            this.labelX6.Margin = new System.Windows.Forms.Padding(2);
            this.labelX6.Name = "labelX6";
            this.labelX6.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX6.Size = new System.Drawing.Size(179, 32);
            this.labelX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX6.TabIndex = 75;
            this.labelX6.Text = "Purity  ";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtSumKachaSona
            // 
            this.txtSumKachaSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSumKachaSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSumKachaSona.Border.Class = "TextBoxBorder";
            this.txtSumKachaSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSumKachaSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumKachaSona.ForeColor = System.Drawing.Color.Black;
            this.txtSumKachaSona.Location = new System.Drawing.Point(217, 90);
            this.txtSumKachaSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSumKachaSona.Name = "txtSumKachaSona";
            this.txtSumKachaSona.Size = new System.Drawing.Size(533, 32);
            this.txtSumKachaSona.TabIndex = 1;
            // 
            // txtPurityKachaSona
            // 
            this.txtPurityKachaSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPurityKachaSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPurityKachaSona.Border.Class = "TextBoxBorder";
            this.txtPurityKachaSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPurityKachaSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPurityKachaSona.ForeColor = System.Drawing.Color.Black;
            this.txtPurityKachaSona.Location = new System.Drawing.Point(217, 126);
            this.txtPurityKachaSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPurityKachaSona.Name = "txtPurityKachaSona";
            this.txtPurityKachaSona.Size = new System.Drawing.Size(533, 32);
            this.txtPurityKachaSona.TabIndex = 2;
            this.txtPurityKachaSona.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPurityKachaSona_KeyPress);
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX9.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX9.Location = new System.Drawing.Point(33, 295);
            this.labelX9.Margin = new System.Windows.Forms.Padding(2);
            this.labelX9.Name = "labelX9";
            this.labelX9.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX9.Size = new System.Drawing.Size(179, 35);
            this.labelX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX9.TabIndex = 130;
            this.labelX9.Text = "Customer";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX10.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX10.Location = new System.Drawing.Point(33, 334);
            this.labelX10.Margin = new System.Windows.Forms.Padding(2);
            this.labelX10.Name = "labelX10";
            this.labelX10.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX10.Size = new System.Drawing.Size(179, 28);
            this.labelX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX10.TabIndex = 131;
            this.labelX10.Text = "Description";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtDescriptionKachasSona
            // 
            this.txtDescriptionKachasSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescriptionKachasSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDescriptionKachasSona.Border.Class = "TextBoxBorder";
            this.txtDescriptionKachasSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescriptionKachasSona.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.txtDescriptionKachasSona.ForeColor = System.Drawing.Color.Black;
            this.txtDescriptionKachasSona.Location = new System.Drawing.Point(217, 334);
            this.txtDescriptionKachasSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDescriptionKachasSona.Name = "txtDescriptionKachasSona";
            this.txtDescriptionKachasSona.Size = new System.Drawing.Size(533, 29);
            this.txtDescriptionKachasSona.TabIndex = 5;
            this.txtDescriptionKachasSona.WatermarkText = "Enter Discription of Kacha Sona Transaction";
            // 
            // cmbClientsKachaSona
            // 
            this.cmbClientsKachaSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbClientsKachaSona.DisplayMember = "Name";
            this.cmbClientsKachaSona.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbClientsKachaSona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClientsKachaSona.FormattingEnabled = true;
            this.cmbClientsKachaSona.ItemHeight = 23;
            this.cmbClientsKachaSona.Location = new System.Drawing.Point(217, 296);
            this.cmbClientsKachaSona.Name = "cmbClientsKachaSona";
            this.cmbClientsKachaSona.Size = new System.Drawing.Size(533, 29);
            this.cmbClientsKachaSona.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbClientsKachaSona.TabIndex = 4;
            this.cmbClientsKachaSona.Text = "Select Customer";
            this.cmbClientsKachaSona.ValueMember = "C_Id";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.Rdb2GoldDebitKachaSona);
            this.panel3.Controls.Add(this.Rdb1GoldCreditKachaSona);
            this.panel3.Location = new System.Drawing.Point(217, 367);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(533, 69);
            this.panel3.TabIndex = 136;
            // 
            // Rdb2GoldDebitKachaSona
            // 
            this.Rdb2GoldDebitKachaSona.BackColor = System.Drawing.Color.Transparent;
            this.Rdb2GoldDebitKachaSona.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb2GoldDebitKachaSona.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(181)))), ((int)(((byte)(25)))));
            this.Rdb2GoldDebitKachaSona.Location = new System.Drawing.Point(143, 39);
            this.Rdb2GoldDebitKachaSona.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb2GoldDebitKachaSona.Name = "Rdb2GoldDebitKachaSona";
            this.Rdb2GoldDebitKachaSona.Size = new System.Drawing.Size(250, 25);
            this.Rdb2GoldDebitKachaSona.TabIndex = 1;
            this.Rdb2GoldDebitKachaSona.Text = "Gold Debit سونا ادائیگی  (-)";
            this.Rdb2GoldDebitKachaSona.UseVisualStyleBackColor = false;
            // 
            // Rdb1GoldCreditKachaSona
            // 
            this.Rdb1GoldCreditKachaSona.BackColor = System.Drawing.Color.Transparent;
            this.Rdb1GoldCreditKachaSona.Checked = true;
            this.Rdb1GoldCreditKachaSona.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rdb1GoldCreditKachaSona.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(145)))), ((int)(((byte)(42)))));
            this.Rdb1GoldCreditKachaSona.Location = new System.Drawing.Point(143, 6);
            this.Rdb1GoldCreditKachaSona.Margin = new System.Windows.Forms.Padding(4);
            this.Rdb1GoldCreditKachaSona.Name = "Rdb1GoldCreditKachaSona";
            this.Rdb1GoldCreditKachaSona.Size = new System.Drawing.Size(250, 25);
            this.Rdb1GoldCreditKachaSona.TabIndex = 0;
            this.Rdb1GoldCreditKachaSona.TabStop = true;
            this.Rdb1GoldCreditKachaSona.Text = "Gold Credit سونا وصولی  (+)";
            this.Rdb1GoldCreditKachaSona.UseVisualStyleBackColor = false;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX7.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX7.Location = new System.Drawing.Point(33, 366);
            this.labelX7.Margin = new System.Windows.Forms.Padding(2);
            this.labelX7.Name = "labelX7";
            this.labelX7.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX7.Size = new System.Drawing.Size(179, 71);
            this.labelX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX7.TabIndex = 137;
            this.labelX7.Text = "Transaction Type";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX4.Location = new System.Drawing.Point(33, 200);
            this.labelX4.Margin = new System.Windows.Forms.Padding(2);
            this.labelX4.Name = "labelX4";
            this.labelX4.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX4.Size = new System.Drawing.Size(179, 42);
            this.labelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX4.TabIndex = 142;
            this.labelX4.Text = "Sum";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // line1
            // 
            this.line1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelKACHSONA.SetColumnSpan(this.line1, 4);
            this.line1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.line1.Location = new System.Drawing.Point(3, 288);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(781, 2);
            this.line1.TabIndex = 128;
            this.line1.Text = "line1";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelKACHSONA.SetColumnSpan(this.panel4, 2);
            this.panel4.Controls.Add(this.btnAddTransactionKachaSona);
            this.panel4.Controls.Add(this.btnRefreshKachSona);
            this.panel4.Location = new System.Drawing.Point(33, 441);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.tableLayoutPanelKACHSONA.SetRowSpan(this.panel4, 2);
            this.panel4.Size = new System.Drawing.Size(718, 67);
            this.panel4.TabIndex = 87;
            // 
            // btnAddTransactionKachaSona
            // 
            this.btnAddTransactionKachaSona.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddTransactionKachaSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddTransactionKachaSona.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAddTransactionKachaSona.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTransactionKachaSona.Image = global::Presentation.Properties.Resources.add;
            this.btnAddTransactionKachaSona.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnAddTransactionKachaSona.Location = new System.Drawing.Point(444, 18);
            this.btnAddTransactionKachaSona.Name = "btnAddTransactionKachaSona";
            this.btnAddTransactionKachaSona.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnAddTransactionKachaSona.Size = new System.Drawing.Size(143, 46);
            this.btnAddTransactionKachaSona.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnAddTransactionKachaSona.TabIndex = 1;
            this.btnAddTransactionKachaSona.Text = "Add Transaction";
            this.btnAddTransactionKachaSona.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnAddTransactionKachaSona.Click += new System.EventHandler(this.BtnTransactionKachaSona_Click);
            // 
            // btnRefreshKachSona
            // 
            this.btnRefreshKachSona.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefreshKachSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshKachSona.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnRefreshKachSona.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshKachSona.Image = global::Presentation.Properties.Resources.cancel;
            this.btnRefreshKachSona.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnRefreshKachSona.Location = new System.Drawing.Point(593, 18);
            this.btnRefreshKachSona.Name = "btnRefreshKachSona";
            this.btnRefreshKachSona.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnRefreshKachSona.Size = new System.Drawing.Size(117, 46);
            this.btnRefreshKachSona.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnRefreshKachSona.TabIndex = 0;
            this.btnRefreshKachSona.Text = "Refresh";
            this.btnRefreshKachSona.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRefreshKachSona.Click += new System.EventHandler(this.btnRefreshKachSona_Click);
            // 
            // line2
            // 
            this.line2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelKACHSONA.SetColumnSpan(this.line2, 4);
            this.line2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.line2.Location = new System.Drawing.Point(3, 163);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(781, 3);
            this.line2.TabIndex = 142;
            this.line2.Text = "line2";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.labelX13);
            this.panel1.Controls.Add(this.labelX5);
            this.panel1.Controls.Add(this.txtPureGoldKachaSona);
            this.panel1.Controls.Add(this.txtSumCustomToKachaSona);
            this.panel1.Controls.Add(this.txtSumCustomFromKachaSona);
            this.panel1.Location = new System.Drawing.Point(217, 172);
            this.panel1.Name = "panel1";
            this.tableLayoutPanelKACHSONA.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(533, 110);
            this.panel1.TabIndex = 139;
            // 
            // labelX13
            // 
            this.labelX13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.labelX13.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX13.Location = new System.Drawing.Point(3, 5);
            this.labelX13.Margin = new System.Windows.Forms.Padding(2);
            this.labelX13.Name = "labelX13";
            this.labelX13.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX13.Size = new System.Drawing.Size(527, 26);
            this.labelX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX13.TabIndex = 147;
            this.labelX13.Text = "Custom Conversion";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX5.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(248, 39);
            this.labelX5.Margin = new System.Windows.Forms.Padding(2);
            this.labelX5.Name = "labelX5";
            this.labelX5.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX5.Size = new System.Drawing.Size(37, 28);
            this.labelX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX5.TabIndex = 95;
            this.labelX5.Text = "B\\W";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtPureGoldKachaSona
            // 
            this.txtPureGoldKachaSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPureGoldKachaSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPureGoldKachaSona.Border.Class = "TextBoxBorder";
            this.txtPureGoldKachaSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPureGoldKachaSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPureGoldKachaSona.ForeColor = System.Drawing.Color.Black;
            this.txtPureGoldKachaSona.Location = new System.Drawing.Point(3, 71);
            this.txtPureGoldKachaSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPureGoldKachaSona.Name = "txtPureGoldKachaSona";
            this.txtPureGoldKachaSona.Size = new System.Drawing.Size(527, 32);
            this.txtPureGoldKachaSona.TabIndex = 3;
            this.txtPureGoldKachaSona.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPureGoldKachaSona_KeyPress);
            // 
            // txtSumCustomToKachaSona
            // 
            this.txtSumCustomToKachaSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSumCustomToKachaSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSumCustomToKachaSona.Border.Class = "TextBoxBorder";
            this.txtSumCustomToKachaSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSumCustomToKachaSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumCustomToKachaSona.ForeColor = System.Drawing.Color.Black;
            this.txtSumCustomToKachaSona.Location = new System.Drawing.Point(299, 33);
            this.txtSumCustomToKachaSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSumCustomToKachaSona.Name = "txtSumCustomToKachaSona";
            this.txtSumCustomToKachaSona.Size = new System.Drawing.Size(231, 32);
            this.txtSumCustomToKachaSona.TabIndex = 1;
            // 
            // txtSumCustomFromKachaSona
            // 
            this.txtSumCustomFromKachaSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSumCustomFromKachaSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSumCustomFromKachaSona.Border.Class = "TextBoxBorder";
            this.txtSumCustomFromKachaSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSumCustomFromKachaSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumCustomFromKachaSona.ForeColor = System.Drawing.Color.Black;
            this.txtSumCustomFromKachaSona.Location = new System.Drawing.Point(3, 34);
            this.txtSumCustomFromKachaSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSumCustomFromKachaSona.Name = "txtSumCustomFromKachaSona";
            this.txtSumCustomFromKachaSona.Size = new System.Drawing.Size(231, 32);
            this.txtSumCustomFromKachaSona.TabIndex = 0;
            this.txtSumCustomFromKachaSona.Text = "  ";
            this.txtSumCustomFromKachaSona.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSumCustomFromKachaSona_KeyPress_1);
            // 
            // labelX18
            // 
            this.labelX18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX18.BackgroundStyle.BorderBottomWidth = 1;
            this.labelX18.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.labelX18.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX18.BackgroundStyle.BorderLeftWidth = 1;
            this.labelX18.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX18.BackgroundStyle.BorderRightWidth = 1;
            this.labelX18.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX18.BackgroundStyle.BorderTopWidth = 1;
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelKACHSONA.SetColumnSpan(this.labelX18, 4);
            this.labelX18.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX18.Location = new System.Drawing.Point(2, 2);
            this.labelX18.Margin = new System.Windows.Forms.Padding(2);
            this.labelX18.Name = "labelX18";
            this.labelX18.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX18.Size = new System.Drawing.Size(783, 48);
            this.labelX18.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX18.TabIndex = 148;
            this.labelX18.Text = "Impure Gold Transaction ( کچا سونا)";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // kachaSonatabItem
            // 
            this.kachaSonatabItem.AttachedControl = this.tabControlPanel1;
            this.kachaSonatabItem.Name = "kachaSonatabItem";
            this.kachaSonatabItem.Text = "Kacha Sona";
            this.kachaSonatabItem.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.tableLayoutPanelOTHERS);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 32);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(789, 564);
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(247)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 6;
            this.tabControlPanel2.TabItem = this.tabItemKhalisSona;
            // 
            // tableLayoutPanelOTHERS
            // 
            this.tableLayoutPanelOTHERS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanelOTHERS.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelOTHERS.ColumnCount = 4;
            this.tableLayoutPanelOTHERS.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.379562F));
            this.tableLayoutPanelOTHERS.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.2035F));
            this.tableLayoutPanelOTHERS.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.03738F));
            this.tableLayoutPanelOTHERS.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.379562F));
            this.tableLayoutPanelOTHERS.Controls.Add(this.txtPureGoldNoneKhalisSona, 2, 1);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX23, 1, 5);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX24, 1, 1);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX29, 1, 7);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX30, 1, 8);
            this.tableLayoutPanelOTHERS.Controls.Add(this.txtDescriptionKhalisSona, 2, 8);
            this.tableLayoutPanelOTHERS.Controls.Add(this.cmbClientsKhilsSona, 2, 7);
            this.tableLayoutPanelOTHERS.Controls.Add(this.panel8, 2, 9);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX31, 1, 9);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX34, 1, 4);
            this.tableLayoutPanelOTHERS.Controls.Add(this.line5, 0, 6);
            this.tableLayoutPanelOTHERS.Controls.Add(this.panel11, 1, 10);
            this.tableLayoutPanelOTHERS.Controls.Add(this.line6, 0, 2);
            this.tableLayoutPanelOTHERS.Controls.Add(this.panel9, 2, 3);
            this.tableLayoutPanelOTHERS.Controls.Add(this.labelX17, 0, 0);
            this.tableLayoutPanelOTHERS.Location = new System.Drawing.Point(1, 6);
            this.tableLayoutPanelOTHERS.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelOTHERS.Name = "tableLayoutPanelOTHERS";
            this.tableLayoutPanelOTHERS.RowCount = 13;
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.944301F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.507228F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.501806F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.965846F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.890312F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.044992F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.656486F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.312973F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.614938F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.43712F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.03395F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.539573F));
            this.tableLayoutPanelOTHERS.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.550473F));
            this.tableLayoutPanelOTHERS.Size = new System.Drawing.Size(787, 538);
            this.tableLayoutPanelOTHERS.TabIndex = 4;
            // 
            // txtPureGoldNoneKhalisSona
            // 
            this.txtPureGoldNoneKhalisSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPureGoldNoneKhalisSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPureGoldNoneKhalisSona.Border.Class = "TextBoxBorder";
            this.txtPureGoldNoneKhalisSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPureGoldNoneKhalisSona.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPureGoldNoneKhalisSona.ForeColor = System.Drawing.Color.Black;
            this.txtPureGoldNoneKhalisSona.Location = new System.Drawing.Point(219, 55);
            this.txtPureGoldNoneKhalisSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPureGoldNoneKhalisSona.Name = "txtPureGoldNoneKhalisSona";
            this.txtPureGoldNoneKhalisSona.Size = new System.Drawing.Size(529, 33);
            this.txtPureGoldNoneKhalisSona.TabIndex = 0;
            // 
            // labelX23
            // 
            this.labelX23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX23.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX23.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX23.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX23.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX23.Location = new System.Drawing.Point(36, 182);
            this.labelX23.Margin = new System.Windows.Forms.Padding(2);
            this.labelX23.Name = "labelX23";
            this.labelX23.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX23.Size = new System.Drawing.Size(178, 44);
            this.labelX23.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX23.TabIndex = 143;
            this.labelX23.Text = "Final Khalis Sona";
            this.labelX23.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX24
            // 
            this.labelX24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX24.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX24.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX24.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX24.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX24.Location = new System.Drawing.Point(36, 55);
            this.labelX24.Margin = new System.Windows.Forms.Padding(2);
            this.labelX24.Name = "labelX24";
            this.labelX24.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX24.Size = new System.Drawing.Size(178, 36);
            this.labelX24.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX24.TabIndex = 69;
            this.labelX24.Text = "Khalis Sona in Gram";
            this.labelX24.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX29
            // 
            this.labelX29.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX29.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX29.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX29.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX29.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX29.Location = new System.Drawing.Point(36, 249);
            this.labelX29.Margin = new System.Windows.Forms.Padding(2);
            this.labelX29.Name = "labelX29";
            this.labelX29.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX29.Size = new System.Drawing.Size(178, 35);
            this.labelX29.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX29.TabIndex = 130;
            this.labelX29.Text = "Customer";
            this.labelX29.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX30
            // 
            this.labelX30.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX30.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX30.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX30.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX30.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX30.Location = new System.Drawing.Point(36, 288);
            this.labelX30.Margin = new System.Windows.Forms.Padding(2);
            this.labelX30.Name = "labelX30";
            this.labelX30.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX30.Size = new System.Drawing.Size(178, 31);
            this.labelX30.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX30.TabIndex = 131;
            this.labelX30.Text = "Description";
            this.labelX30.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtDescriptionKhalisSona
            // 
            this.txtDescriptionKhalisSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescriptionKhalisSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDescriptionKhalisSona.Border.Class = "TextBoxBorder";
            this.txtDescriptionKhalisSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDescriptionKhalisSona.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.txtDescriptionKhalisSona.ForeColor = System.Drawing.Color.Black;
            this.txtDescriptionKhalisSona.Location = new System.Drawing.Point(219, 288);
            this.txtDescriptionKhalisSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDescriptionKhalisSona.Name = "txtDescriptionKhalisSona";
            this.txtDescriptionKhalisSona.Size = new System.Drawing.Size(529, 29);
            this.txtDescriptionKhalisSona.TabIndex = 133;
            this.txtDescriptionKhalisSona.WatermarkText = "Enter Discription of Khali Sona Transaction";
            // 
            // cmbClientsKhilsSona
            // 
            this.cmbClientsKhilsSona.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbClientsKhilsSona.DisplayMember = "Name";
            this.cmbClientsKhilsSona.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbClientsKhilsSona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClientsKhilsSona.FormattingEnabled = true;
            this.cmbClientsKhilsSona.ItemHeight = 23;
            this.cmbClientsKhilsSona.Location = new System.Drawing.Point(219, 250);
            this.cmbClientsKhilsSona.Name = "cmbClientsKhilsSona";
            this.cmbClientsKhilsSona.Size = new System.Drawing.Size(529, 29);
            this.cmbClientsKhilsSona.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbClientsKhilsSona.TabIndex = 135;
            this.cmbClientsKhilsSona.Text = "Select Customer";
            this.cmbClientsKhilsSona.ValueMember = "C_Id";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.rdbrdbGoldDebitKhalisSona);
            this.panel8.Controls.Add(this.rdbGoldCreditKhalisSona);
            this.panel8.Location = new System.Drawing.Point(219, 324);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(529, 82);
            this.panel8.TabIndex = 136;
            // 
            // rdbrdbGoldDebitKhalisSona
            // 
            this.rdbrdbGoldDebitKhalisSona.BackColor = System.Drawing.Color.Transparent;
            this.rdbrdbGoldDebitKhalisSona.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbrdbGoldDebitKhalisSona.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(181)))), ((int)(((byte)(25)))));
            this.rdbrdbGoldDebitKhalisSona.Location = new System.Drawing.Point(102, 37);
            this.rdbrdbGoldDebitKhalisSona.Margin = new System.Windows.Forms.Padding(4);
            this.rdbrdbGoldDebitKhalisSona.Name = "rdbrdbGoldDebitKhalisSona";
            this.rdbrdbGoldDebitKhalisSona.Size = new System.Drawing.Size(250, 25);
            this.rdbrdbGoldDebitKhalisSona.TabIndex = 43;
            this.rdbrdbGoldDebitKhalisSona.Text = "Gold Debit سونا ادائیگی  (-)";
            this.rdbrdbGoldDebitKhalisSona.UseVisualStyleBackColor = false;
            // 
            // rdbGoldCreditKhalisSona
            // 
            this.rdbGoldCreditKhalisSona.BackColor = System.Drawing.Color.Transparent;
            this.rdbGoldCreditKhalisSona.Checked = true;
            this.rdbGoldCreditKhalisSona.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGoldCreditKhalisSona.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(145)))), ((int)(((byte)(42)))));
            this.rdbGoldCreditKhalisSona.Location = new System.Drawing.Point(102, 4);
            this.rdbGoldCreditKhalisSona.Margin = new System.Windows.Forms.Padding(4);
            this.rdbGoldCreditKhalisSona.Name = "rdbGoldCreditKhalisSona";
            this.rdbGoldCreditKhalisSona.Size = new System.Drawing.Size(250, 25);
            this.rdbGoldCreditKhalisSona.TabIndex = 42;
            this.rdbGoldCreditKhalisSona.TabStop = true;
            this.rdbGoldCreditKhalisSona.Text = "Gold Credit سونا وصولی  (+)";
            this.rdbGoldCreditKhalisSona.UseVisualStyleBackColor = false;
            // 
            // labelX31
            // 
            this.labelX31.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX31.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX31.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX31.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX31.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX31.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX31.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX31.Location = new System.Drawing.Point(36, 323);
            this.labelX31.Margin = new System.Windows.Forms.Padding(2);
            this.labelX31.Name = "labelX31";
            this.labelX31.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX31.Size = new System.Drawing.Size(178, 84);
            this.labelX31.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX31.TabIndex = 137;
            this.labelX31.Text = "Transaction Type";
            this.labelX31.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX34
            // 
            this.labelX34.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX34.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX34.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX34.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX34.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX34.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX34.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX34.Location = new System.Drawing.Point(36, 140);
            this.labelX34.Margin = new System.Windows.Forms.Padding(2);
            this.labelX34.Name = "labelX34";
            this.labelX34.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX34.Size = new System.Drawing.Size(178, 38);
            this.labelX34.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX34.TabIndex = 142;
            this.labelX34.Text = "Sum";
            this.labelX34.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // line5
            // 
            this.line5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelOTHERS.SetColumnSpan(this.line5, 4);
            this.line5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.line5.Location = new System.Drawing.Point(3, 236);
            this.line5.Name = "line5";
            this.line5.Size = new System.Drawing.Size(781, 3);
            this.line5.TabIndex = 128;
            this.line5.Text = "line5";
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelOTHERS.SetColumnSpan(this.panel11, 2);
            this.panel11.Controls.Add(this.btnAddTransactionKhalisSona);
            this.panel11.Controls.Add(this.btnRefreshKhalisSona);
            this.panel11.Location = new System.Drawing.Point(36, 411);
            this.panel11.Margin = new System.Windows.Forms.Padding(2);
            this.panel11.Name = "panel11";
            this.tableLayoutPanelOTHERS.SetRowSpan(this.panel11, 2);
            this.panel11.Size = new System.Drawing.Size(713, 84);
            this.panel11.TabIndex = 87;
            // 
            // btnAddTransactionKhalisSona
            // 
            this.btnAddTransactionKhalisSona.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddTransactionKhalisSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddTransactionKhalisSona.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAddTransactionKhalisSona.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTransactionKhalisSona.Image = global::Presentation.Properties.Resources.add;
            this.btnAddTransactionKhalisSona.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnAddTransactionKhalisSona.Location = new System.Drawing.Point(440, 26);
            this.btnAddTransactionKhalisSona.Name = "btnAddTransactionKhalisSona";
            this.btnAddTransactionKhalisSona.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnAddTransactionKhalisSona.Size = new System.Drawing.Size(143, 46);
            this.btnAddTransactionKhalisSona.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnAddTransactionKhalisSona.TabIndex = 125;
            this.btnAddTransactionKhalisSona.Text = "Add Transaction";
            this.btnAddTransactionKhalisSona.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnAddTransactionKhalisSona.Click += new System.EventHandler(this.btnAddTransactionKhalisSona_Click);
            // 
            // btnRefreshKhalisSona
            // 
            this.btnRefreshKhalisSona.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefreshKhalisSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshKhalisSona.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnRefreshKhalisSona.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshKhalisSona.Image = global::Presentation.Properties.Resources.cancel;
            this.btnRefreshKhalisSona.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnRefreshKhalisSona.Location = new System.Drawing.Point(589, 26);
            this.btnRefreshKhalisSona.Name = "btnRefreshKhalisSona";
            this.btnRefreshKhalisSona.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnRefreshKhalisSona.Size = new System.Drawing.Size(117, 46);
            this.btnRefreshKhalisSona.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnRefreshKhalisSona.TabIndex = 124;
            this.btnRefreshKhalisSona.Text = "Refresh";
            this.btnRefreshKhalisSona.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRefreshKhalisSona.Click += new System.EventHandler(this.btnRefreshKhalisSona_Click);
            // 
            // line6
            // 
            this.line6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelOTHERS.SetColumnSpan(this.line6, 4);
            this.line6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.line6.Location = new System.Drawing.Point(3, 97);
            this.line6.Name = "line6";
            this.line6.Size = new System.Drawing.Size(781, 4);
            this.line6.TabIndex = 142;
            this.line6.Text = "line6";
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.labelX33);
            this.panel9.Controls.Add(this.txtSumTo990KhalisSona);
            this.panel9.Controls.Add(this.labelX32);
            this.panel9.Controls.Add(this.txtSumFrom997KhalisSona);
            this.panel9.Controls.Add(this.txtPureGoldKhalisSonaFinal);
            this.panel9.Location = new System.Drawing.Point(219, 109);
            this.panel9.Name = "panel9";
            this.tableLayoutPanelOTHERS.SetRowSpan(this.panel9, 3);
            this.panel9.Size = new System.Drawing.Size(529, 116);
            this.panel9.TabIndex = 139;
            // 
            // labelX33
            // 
            this.labelX33.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX33.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX33.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.labelX33.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX33.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX33.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX33.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX33.Location = new System.Drawing.Point(3, 4);
            this.labelX33.Margin = new System.Windows.Forms.Padding(2);
            this.labelX33.Name = "labelX33";
            this.labelX33.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX33.Size = new System.Drawing.Size(521, 25);
            this.labelX33.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX33.TabIndex = 146;
            this.labelX33.Text = "Custom Conversion";
            this.labelX33.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtSumTo990KhalisSona
            // 
            this.txtSumTo990KhalisSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSumTo990KhalisSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSumTo990KhalisSona.Border.Class = "TextBoxBorder";
            this.txtSumTo990KhalisSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSumTo990KhalisSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumTo990KhalisSona.ForeColor = System.Drawing.Color.Black;
            this.txtSumTo990KhalisSona.Location = new System.Drawing.Point(290, 35);
            this.txtSumTo990KhalisSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSumTo990KhalisSona.Name = "txtSumTo990KhalisSona";
            this.txtSumTo990KhalisSona.Size = new System.Drawing.Size(233, 32);
            this.txtSumTo990KhalisSona.TabIndex = 96;
            this.txtSumTo990KhalisSona.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSumTo990KhalisSona_KeyDown);
            // 
            // labelX32
            // 
            this.labelX32.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.labelX32.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX32.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX32.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX32.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX32.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX32.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX32.ForeColor = System.Drawing.Color.Black;
            this.labelX32.Location = new System.Drawing.Point(247, 37);
            this.labelX32.Margin = new System.Windows.Forms.Padding(2);
            this.labelX32.Name = "labelX32";
            this.labelX32.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX32.Size = new System.Drawing.Size(37, 28);
            this.labelX32.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX32.TabIndex = 95;
            this.labelX32.Text = "B\\W";
            this.labelX32.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtSumFrom997KhalisSona
            // 
            this.txtSumFrom997KhalisSona.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSumFrom997KhalisSona.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtSumFrom997KhalisSona.Border.Class = "TextBoxBorder";
            this.txtSumFrom997KhalisSona.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSumFrom997KhalisSona.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumFrom997KhalisSona.ForeColor = System.Drawing.Color.Black;
            this.txtSumFrom997KhalisSona.Location = new System.Drawing.Point(4, 35);
            this.txtSumFrom997KhalisSona.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSumFrom997KhalisSona.Name = "txtSumFrom997KhalisSona";
            this.txtSumFrom997KhalisSona.Size = new System.Drawing.Size(235, 32);
            this.txtSumFrom997KhalisSona.TabIndex = 92;
            // 
            // txtPureGoldKhalisSonaFinal
            // 
            this.txtPureGoldKhalisSonaFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPureGoldKhalisSonaFinal.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPureGoldKhalisSonaFinal.Border.Class = "TextBoxBorder";
            this.txtPureGoldKhalisSonaFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPureGoldKhalisSonaFinal.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPureGoldKhalisSonaFinal.ForeColor = System.Drawing.Color.Black;
            this.txtPureGoldKhalisSonaFinal.Location = new System.Drawing.Point(3, 77);
            this.txtPureGoldKhalisSonaFinal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPureGoldKhalisSonaFinal.Name = "txtPureGoldKhalisSonaFinal";
            this.txtPureGoldKhalisSonaFinal.Size = new System.Drawing.Size(521, 32);
            this.txtPureGoldKhalisSonaFinal.TabIndex = 138;
            this.txtPureGoldKhalisSonaFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPureGoldKhalisSonaFinal_KeyDown);
            // 
            // labelX17
            // 
            this.labelX17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX17.BackgroundStyle.BorderBottomWidth = 1;
            this.labelX17.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.labelX17.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX17.BackgroundStyle.BorderLeftWidth = 1;
            this.labelX17.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX17.BackgroundStyle.BorderRightWidth = 1;
            this.labelX17.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX17.BackgroundStyle.BorderTopWidth = 1;
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelOTHERS.SetColumnSpan(this.labelX17, 4);
            this.labelX17.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX17.Location = new System.Drawing.Point(2, 2);
            this.labelX17.Margin = new System.Windows.Forms.Padding(2);
            this.labelX17.Name = "labelX17";
            this.labelX17.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX17.Size = new System.Drawing.Size(783, 49);
            this.labelX17.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX17.TabIndex = 147;
            this.labelX17.Text = "Pure Gold Transaction ( خالص سونا)";
            this.labelX17.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tabItemKhalisSona
            // 
            this.tabItemKhalisSona.AttachedControl = this.tabControlPanel2;
            this.tabItemKhalisSona.Name = "tabItemKhalisSona";
            this.tabItemKhalisSona.Text = "KhalisSona";
            this.tabItemKhalisSona.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // tabControlPanel5
            // 
            this.tabControlPanel5.Controls.Add(this.tableLayoutPanelCASH);
            this.tabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel5.Location = new System.Drawing.Point(0, 32);
            this.tabControlPanel5.Name = "tabControlPanel5";
            this.tabControlPanel5.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel5.Size = new System.Drawing.Size(789, 564);
            this.tabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(247)))));
            this.tabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.tabControlPanel5.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel5.Style.GradientAngle = 90;
            this.tabControlPanel5.TabIndex = 5;
            this.tabControlPanel5.TabItem = this.CashtabItem;
            // 
            // tableLayoutPanelCASH
            // 
            this.tableLayoutPanelCASH.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanelCASH.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelCASH.ColumnCount = 4;
            this.tableLayoutPanelCASH.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.044117F));
            this.tableLayoutPanelCASH.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.37411F));
            this.tableLayoutPanelCASH.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.53765F));
            this.tableLayoutPanelCASH.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.044118F));
            this.tableLayoutPanelCASH.Controls.Add(this.labelX22, 1, 4);
            this.tableLayoutPanelCASH.Controls.Add(this.txtDiscriptionCash, 2, 4);
            this.tableLayoutPanelCASH.Controls.Add(this.panel12, 1, 10);
            this.tableLayoutPanelCASH.Controls.Add(this.labelX36, 1, 8);
            this.tableLayoutPanelCASH.Controls.Add(this.panel13, 2, 8);
            this.tableLayoutPanelCASH.Controls.Add(this.txtAmountCash, 2, 1);
            this.tableLayoutPanelCASH.Controls.Add(this.labelX15, 1, 1);
            this.tableLayoutPanelCASH.Controls.Add(this.line7, 0, 6);
            this.tableLayoutPanelCASH.Controls.Add(this.labelX35, 1, 7);
            this.tableLayoutPanelCASH.Controls.Add(this.cmbCash, 2, 7);
            this.tableLayoutPanelCASH.Controls.Add(this.txtPureGoldCash, 2, 3);
            this.tableLayoutPanelCASH.Controls.Add(this.txtGoldRateCash, 2, 2);
            this.tableLayoutPanelCASH.Controls.Add(this.labelX12, 1, 3);
            this.tableLayoutPanelCASH.Controls.Add(this.labelX16, 1, 2);
            this.tableLayoutPanelCASH.Controls.Add(this.labelX19, 0, 0);
            this.tableLayoutPanelCASH.Location = new System.Drawing.Point(1, 0);
            this.tableLayoutPanelCASH.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelCASH.Name = "tableLayoutPanelCASH";
            this.tableLayoutPanelCASH.RowCount = 12;
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.420956F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.961398F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.558558F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.783784F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.207207F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.378378F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.663603F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.04228F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.4761F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.801802F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.89189F));
            this.tableLayoutPanelCASH.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.101103F));
            this.tableLayoutPanelCASH.Size = new System.Drawing.Size(787, 444);
            this.tableLayoutPanelCASH.TabIndex = 5;
            // 
            // labelX22
            // 
            this.labelX22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX22.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX22.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX22.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX22.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX22.Location = new System.Drawing.Point(33, 161);
            this.labelX22.Margin = new System.Windows.Forms.Padding(2);
            this.labelX22.Name = "labelX22";
            this.labelX22.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX22.Size = new System.Drawing.Size(179, 28);
            this.labelX22.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX22.TabIndex = 144;
            this.labelX22.Text = "Discription";
            this.labelX22.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtDiscriptionCash
            // 
            this.txtDiscriptionCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscriptionCash.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDiscriptionCash.Border.Class = "TextBoxBorder";
            this.txtDiscriptionCash.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDiscriptionCash.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.txtDiscriptionCash.ForeColor = System.Drawing.Color.Black;
            this.txtDiscriptionCash.Location = new System.Drawing.Point(217, 161);
            this.txtDiscriptionCash.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDiscriptionCash.Name = "txtDiscriptionCash";
            this.txtDiscriptionCash.Size = new System.Drawing.Size(533, 29);
            this.txtDiscriptionCash.TabIndex = 1;
            this.txtDiscriptionCash.WatermarkText = "Enter Discription of Cash Transaction";
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelCASH.SetColumnSpan(this.panel12, 2);
            this.panel12.Controls.Add(this.btnAddTransactionCash);
            this.panel12.Controls.Add(this.btnRefreshCash);
            this.panel12.Location = new System.Drawing.Point(33, 344);
            this.panel12.Margin = new System.Windows.Forms.Padding(2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(718, 71);
            this.panel12.TabIndex = 87;
            // 
            // btnAddTransactionCash
            // 
            this.btnAddTransactionCash.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddTransactionCash.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddTransactionCash.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAddTransactionCash.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTransactionCash.Image = global::Presentation.Properties.Resources.add;
            this.btnAddTransactionCash.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnAddTransactionCash.Location = new System.Drawing.Point(443, 18);
            this.btnAddTransactionCash.Name = "btnAddTransactionCash";
            this.btnAddTransactionCash.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnAddTransactionCash.Size = new System.Drawing.Size(143, 46);
            this.btnAddTransactionCash.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnAddTransactionCash.TabIndex = 1;
            this.btnAddTransactionCash.Text = "Add Transaction";
            this.btnAddTransactionCash.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnAddTransactionCash.Click += new System.EventHandler(this.btnAddTransactionCash_Click);
            // 
            // btnRefreshCash
            // 
            this.btnRefreshCash.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefreshCash.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshCash.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnRefreshCash.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshCash.Image = global::Presentation.Properties.Resources.cancel;
            this.btnRefreshCash.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnRefreshCash.Location = new System.Drawing.Point(592, 18);
            this.btnRefreshCash.Name = "btnRefreshCash";
            this.btnRefreshCash.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnRefreshCash.Size = new System.Drawing.Size(117, 46);
            this.btnRefreshCash.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnRefreshCash.TabIndex = 0;
            this.btnRefreshCash.Text = "Refresh";
            this.btnRefreshCash.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRefreshCash.Click += new System.EventHandler(this.btnRefreshCash_Click);
            // 
            // labelX36
            // 
            this.labelX36.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX36.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX36.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX36.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX36.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX36.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX36.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX36.Location = new System.Drawing.Point(33, 272);
            this.labelX36.Margin = new System.Windows.Forms.Padding(2);
            this.labelX36.Name = "labelX36";
            this.tableLayoutPanelCASH.SetRowSpan(this.labelX36, 2);
            this.labelX36.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX36.Size = new System.Drawing.Size(179, 68);
            this.labelX36.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX36.TabIndex = 146;
            this.labelX36.Text = "Transaction Type";
            this.labelX36.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // panel13
            // 
            this.panel13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.rdbCashDebit);
            this.panel13.Controls.Add(this.rdbCashCredit);
            this.panel13.Location = new System.Drawing.Point(217, 273);
            this.panel13.Name = "panel13";
            this.tableLayoutPanelCASH.SetRowSpan(this.panel13, 2);
            this.panel13.Size = new System.Drawing.Size(533, 66);
            this.panel13.TabIndex = 149;
            // 
            // rdbCashDebit
            // 
            this.rdbCashDebit.BackColor = System.Drawing.Color.Transparent;
            this.rdbCashDebit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbCashDebit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(171)))));
            this.rdbCashDebit.Location = new System.Drawing.Point(149, 34);
            this.rdbCashDebit.Margin = new System.Windows.Forms.Padding(4);
            this.rdbCashDebit.Name = "rdbCashDebit";
            this.rdbCashDebit.Size = new System.Drawing.Size(218, 21);
            this.rdbCashDebit.TabIndex = 148;
            this.rdbCashDebit.Text = "Cash Debit  کیش ادائیگی (-)";
            this.rdbCashDebit.UseVisualStyleBackColor = false;
            // 
            // rdbCashCredit
            // 
            this.rdbCashCredit.BackColor = System.Drawing.Color.Transparent;
            this.rdbCashCredit.Checked = true;
            this.rdbCashCredit.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbCashCredit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(160)))), ((int)(((byte)(111)))));
            this.rdbCashCredit.Location = new System.Drawing.Point(149, 7);
            this.rdbCashCredit.Margin = new System.Windows.Forms.Padding(4);
            this.rdbCashCredit.Name = "rdbCashCredit";
            this.rdbCashCredit.Size = new System.Drawing.Size(250, 21);
            this.rdbCashCredit.TabIndex = 147;
            this.rdbCashCredit.TabStop = true;
            this.rdbCashCredit.Text = "Cash Credit  کیش وصولی (+)";
            this.rdbCashCredit.UseVisualStyleBackColor = false;
            // 
            // txtAmountCash
            // 
            this.txtAmountCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAmountCash.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAmountCash.Border.Class = "TextBoxBorder";
            this.txtAmountCash.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAmountCash.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountCash.ForeColor = System.Drawing.Color.Black;
            this.txtAmountCash.Location = new System.Drawing.Point(217, 44);
            this.txtAmountCash.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAmountCash.Name = "txtAmountCash";
            this.txtAmountCash.Size = new System.Drawing.Size(533, 33);
            this.txtAmountCash.TabIndex = 0;
            // 
            // labelX15
            // 
            this.labelX15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX15.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX15.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX15.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX15.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX15.Location = new System.Drawing.Point(33, 44);
            this.labelX15.Margin = new System.Windows.Forms.Padding(2);
            this.labelX15.Name = "labelX15";
            this.labelX15.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX15.Size = new System.Drawing.Size(179, 36);
            this.labelX15.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX15.TabIndex = 69;
            this.labelX15.Text = "Amount in PKR";
            this.labelX15.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // line7
            // 
            this.line7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelCASH.SetColumnSpan(this.line7, 4);
            this.line7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.line7.Location = new System.Drawing.Point(3, 219);
            this.line7.Name = "line7";
            this.line7.Size = new System.Drawing.Size(781, 3);
            this.line7.TabIndex = 128;
            this.line7.Text = "line7";
            // 
            // labelX35
            // 
            this.labelX35.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX35.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX35.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX35.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX35.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX35.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX35.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX35.Location = new System.Drawing.Point(33, 237);
            this.labelX35.Margin = new System.Windows.Forms.Padding(2);
            this.labelX35.Name = "labelX35";
            this.labelX35.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX35.Size = new System.Drawing.Size(179, 31);
            this.labelX35.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX35.TabIndex = 150;
            this.labelX35.Text = "Customer";
            this.labelX35.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // cmbCash
            // 
            this.cmbCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCash.DisplayMember = "Name";
            this.cmbCash.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbCash.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCash.FormattingEnabled = true;
            this.cmbCash.ItemHeight = 23;
            this.cmbCash.Location = new System.Drawing.Point(217, 238);
            this.cmbCash.Name = "cmbCash";
            this.cmbCash.Size = new System.Drawing.Size(533, 29);
            this.cmbCash.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbCash.TabIndex = 145;
            this.cmbCash.Text = "Select Customer";
            this.cmbCash.ValueMember = "C_Id";
            // 
            // txtPureGoldCash
            // 
            this.txtPureGoldCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPureGoldCash.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPureGoldCash.Border.Class = "TextBoxBorder";
            this.txtPureGoldCash.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPureGoldCash.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPureGoldCash.ForeColor = System.Drawing.Color.Black;
            this.txtPureGoldCash.Location = new System.Drawing.Point(217, 122);
            this.txtPureGoldCash.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPureGoldCash.Name = "txtPureGoldCash";
            this.txtPureGoldCash.Size = new System.Drawing.Size(533, 33);
            this.txtPureGoldCash.TabIndex = 151;
            // 
            // txtGoldRateCash
            // 
            this.txtGoldRateCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGoldRateCash.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGoldRateCash.Border.Class = "TextBoxBorder";
            this.txtGoldRateCash.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGoldRateCash.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGoldRateCash.ForeColor = System.Drawing.Color.Black;
            this.txtGoldRateCash.Location = new System.Drawing.Point(217, 84);
            this.txtGoldRateCash.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtGoldRateCash.Name = "txtGoldRateCash";
            this.txtGoldRateCash.Size = new System.Drawing.Size(533, 33);
            this.txtGoldRateCash.TabIndex = 152;
            // 
            // labelX12
            // 
            this.labelX12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX12.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX12.Location = new System.Drawing.Point(33, 122);
            this.labelX12.Margin = new System.Windows.Forms.Padding(2);
            this.labelX12.Name = "labelX12";
            this.labelX12.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX12.Size = new System.Drawing.Size(179, 35);
            this.labelX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX12.TabIndex = 153;
            this.labelX12.Text = "Khalis Sona";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX16
            // 
            this.labelX16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX16.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX16.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX16.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX16.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX16.Location = new System.Drawing.Point(33, 84);
            this.labelX16.Margin = new System.Windows.Forms.Padding(2);
            this.labelX16.Name = "labelX16";
            this.labelX16.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX16.Size = new System.Drawing.Size(179, 34);
            this.labelX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX16.TabIndex = 154;
            this.labelX16.Text = "Gold Rate ";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX19
            // 
            this.labelX19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX19.BackgroundStyle.BorderBottomWidth = 1;
            this.labelX19.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.labelX19.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX19.BackgroundStyle.BorderLeftWidth = 1;
            this.labelX19.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX19.BackgroundStyle.BorderRightWidth = 1;
            this.labelX19.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX19.BackgroundStyle.BorderTopWidth = 1;
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelCASH.SetColumnSpan(this.labelX19, 4);
            this.labelX19.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX19.Location = new System.Drawing.Point(2, 2);
            this.labelX19.Margin = new System.Windows.Forms.Padding(2);
            this.labelX19.Name = "labelX19";
            this.labelX19.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX19.Size = new System.Drawing.Size(783, 38);
            this.labelX19.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX19.TabIndex = 155;
            this.labelX19.Text = "Cash Transaction ( کیش)";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // CashtabItem
            // 
            this.CashtabItem.AttachedControl = this.tabControlPanel5;
            this.CashtabItem.Name = "CashtabItem";
            this.CashtabItem.Text = "Cash";
            this.CashtabItem.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.tableLayoutPanelREFINEGOLD);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 32);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(789, 564);
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(247)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.refineGoldtabItem;
            // 
            // tableLayoutPanelREFINEGOLD
            // 
            this.tableLayoutPanelREFINEGOLD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanelREFINEGOLD.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelREFINEGOLD.ColumnCount = 4;
            this.tableLayoutPanelREFINEGOLD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.044117F));
            this.tableLayoutPanelREFINEGOLD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.37411F));
            this.tableLayoutPanelREFINEGOLD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.53764F));
            this.tableLayoutPanelREFINEGOLD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.044117F));
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.txtImpureGoldRefineGold, 2, 1);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.labelX27, 1, 1);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.labelX25, 1, 2);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.txtPurityRefineGold, 2, 2);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.labelX14, 1, 3);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.txtPureRefineGold, 2, 3);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.txtDisriptionRefineGold, 2, 4);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.labelX40, 1, 4);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.line4, 0, 5);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.labelX20, 0, 0);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.panel10, 1, 7);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.panel5, 2, 6);
            this.tableLayoutPanelREFINEGOLD.Controls.Add(this.labelX21, 1, 6);
            this.tableLayoutPanelREFINEGOLD.Location = new System.Drawing.Point(1, 6);
            this.tableLayoutPanelREFINEGOLD.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelREFINEGOLD.Name = "tableLayoutPanelREFINEGOLD";
            this.tableLayoutPanelREFINEGOLD.RowCount = 8;
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.38614F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.90099F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.405941F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.90099F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.415842F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.14851F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.81188F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.28713F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelREFINEGOLD.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelREFINEGOLD.Size = new System.Drawing.Size(787, 404);
            this.tableLayoutPanelREFINEGOLD.TabIndex = 4;
            // 
            // txtImpureGoldRefineGold
            // 
            this.txtImpureGoldRefineGold.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImpureGoldRefineGold.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtImpureGoldRefineGold.Border.Class = "TextBoxBorder";
            this.txtImpureGoldRefineGold.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtImpureGoldRefineGold.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImpureGoldRefineGold.ForeColor = System.Drawing.Color.Black;
            this.txtImpureGoldRefineGold.Location = new System.Drawing.Point(217, 48);
            this.txtImpureGoldRefineGold.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtImpureGoldRefineGold.Name = "txtImpureGoldRefineGold";
            this.txtImpureGoldRefineGold.Size = new System.Drawing.Size(533, 33);
            this.txtImpureGoldRefineGold.TabIndex = 0;
            // 
            // labelX27
            // 
            this.labelX27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX27.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX27.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX27.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX27.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX27.Location = new System.Drawing.Point(33, 48);
            this.labelX27.Margin = new System.Windows.Forms.Padding(2);
            this.labelX27.Name = "labelX27";
            this.labelX27.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX27.Size = new System.Drawing.Size(179, 36);
            this.labelX27.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX27.TabIndex = 69;
            this.labelX27.Text = "Impure Gold in Gram";
            this.labelX27.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX25
            // 
            this.labelX25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX25.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX25.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX25.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX25.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX25.Location = new System.Drawing.Point(33, 88);
            this.labelX25.Margin = new System.Windows.Forms.Padding(2);
            this.labelX25.Name = "labelX25";
            this.labelX25.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX25.Size = new System.Drawing.Size(179, 34);
            this.labelX25.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX25.TabIndex = 144;
            this.labelX25.Text = "Purity  ";
            this.labelX25.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtPurityRefineGold
            // 
            this.txtPurityRefineGold.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPurityRefineGold.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPurityRefineGold.Border.Class = "TextBoxBorder";
            this.txtPurityRefineGold.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPurityRefineGold.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPurityRefineGold.ForeColor = System.Drawing.Color.Black;
            this.txtPurityRefineGold.Location = new System.Drawing.Point(217, 88);
            this.txtPurityRefineGold.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPurityRefineGold.Name = "txtPurityRefineGold";
            this.txtPurityRefineGold.Size = new System.Drawing.Size(533, 32);
            this.txtPurityRefineGold.TabIndex = 1;
            // 
            // labelX14
            // 
            this.labelX14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX14.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX14.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX14.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX14.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX14.Location = new System.Drawing.Point(33, 126);
            this.labelX14.Margin = new System.Windows.Forms.Padding(2);
            this.labelX14.Name = "labelX14";
            this.labelX14.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX14.Size = new System.Drawing.Size(179, 36);
            this.labelX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX14.TabIndex = 143;
            this.labelX14.Text = "Pure Refine Gold";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtPureRefineGold
            // 
            this.txtPureRefineGold.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPureRefineGold.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPureRefineGold.Border.Class = "TextBoxBorder";
            this.txtPureRefineGold.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPureRefineGold.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPureRefineGold.ForeColor = System.Drawing.Color.Black;
            this.txtPureRefineGold.Location = new System.Drawing.Point(217, 126);
            this.txtPureRefineGold.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPureRefineGold.Name = "txtPureRefineGold";
            this.txtPureRefineGold.Size = new System.Drawing.Size(533, 32);
            this.txtPureRefineGold.TabIndex = 2;
            // 
            // txtDisriptionRefineGold
            // 
            this.txtDisriptionRefineGold.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDisriptionRefineGold.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDisriptionRefineGold.Border.Class = "TextBoxBorder";
            this.txtDisriptionRefineGold.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDisriptionRefineGold.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisriptionRefineGold.ForeColor = System.Drawing.Color.Black;
            this.txtDisriptionRefineGold.Location = new System.Drawing.Point(217, 166);
            this.txtDisriptionRefineGold.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDisriptionRefineGold.Name = "txtDisriptionRefineGold";
            this.txtDisriptionRefineGold.Size = new System.Drawing.Size(533, 29);
            this.txtDisriptionRefineGold.TabIndex = 145;
            this.txtDisriptionRefineGold.WatermarkText = "Enter Discription of Refine Gold Transaction";
            // 
            // labelX40
            // 
            this.labelX40.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX40.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX40.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX40.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX40.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX40.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX40.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX40.Location = new System.Drawing.Point(33, 166);
            this.labelX40.Margin = new System.Windows.Forms.Padding(2);
            this.labelX40.Name = "labelX40";
            this.labelX40.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX40.Size = new System.Drawing.Size(179, 30);
            this.labelX40.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX40.TabIndex = 146;
            this.labelX40.Text = "Discription";
            this.labelX40.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // line4
            // 
            this.line4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelREFINEGOLD.SetColumnSpan(this.line4, 4);
            this.line4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.line4.Location = new System.Drawing.Point(3, 217);
            this.line4.Name = "line4";
            this.line4.Size = new System.Drawing.Size(781, 3);
            this.line4.TabIndex = 128;
            this.line4.Text = "line4";
            // 
            // labelX20
            // 
            this.labelX20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX20.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX20.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.labelX20.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX20.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX20.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX20.BackgroundStyle.BorderRightWidth = 2;
            this.labelX20.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX20.BackgroundStyle.BorderTopWidth = 2;
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelREFINEGOLD.SetColumnSpan(this.labelX20, 4);
            this.labelX20.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX20.Location = new System.Drawing.Point(2, 2);
            this.labelX20.Margin = new System.Windows.Forms.Padding(2);
            this.labelX20.Name = "labelX20";
            this.labelX20.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX20.Size = new System.Drawing.Size(783, 42);
            this.labelX20.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX20.TabIndex = 148;
            this.labelX20.Text = "Refine Gold Transaction ";
            this.labelX20.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelREFINEGOLD.SetColumnSpan(this.panel10, 2);
            this.panel10.Controls.Add(this.btnAddTransactionRefineGold);
            this.panel10.Controls.Add(this.btnRefreshRefineGold);
            this.panel10.Location = new System.Drawing.Point(33, 317);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(718, 85);
            this.panel10.TabIndex = 87;
            // 
            // btnAddTransactionRefineGold
            // 
            this.btnAddTransactionRefineGold.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddTransactionRefineGold.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddTransactionRefineGold.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnAddTransactionRefineGold.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTransactionRefineGold.Image = global::Presentation.Properties.Resources.add;
            this.btnAddTransactionRefineGold.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnAddTransactionRefineGold.Location = new System.Drawing.Point(434, 14);
            this.btnAddTransactionRefineGold.Name = "btnAddTransactionRefineGold";
            this.btnAddTransactionRefineGold.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnAddTransactionRefineGold.Size = new System.Drawing.Size(143, 46);
            this.btnAddTransactionRefineGold.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnAddTransactionRefineGold.TabIndex = 1;
            this.btnAddTransactionRefineGold.Text = "Add Transaction";
            this.btnAddTransactionRefineGold.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnAddTransactionRefineGold.Click += new System.EventHandler(this.BtnTransactionRefineGold_Click);
            // 
            // btnRefreshRefineGold
            // 
            this.btnRefreshRefineGold.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefreshRefineGold.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRefreshRefineGold.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnRefreshRefineGold.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshRefineGold.Image = global::Presentation.Properties.Resources.cancel;
            this.btnRefreshRefineGold.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnRefreshRefineGold.Location = new System.Drawing.Point(583, 14);
            this.btnRefreshRefineGold.Name = "btnRefreshRefineGold";
            this.btnRefreshRefineGold.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnRefreshRefineGold.Size = new System.Drawing.Size(117, 46);
            this.btnRefreshRefineGold.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnRefreshRefineGold.TabIndex = 0;
            this.btnRefreshRefineGold.Text = "Refresh";
            this.btnRefreshRefineGold.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRefreshRefineGold.Click += new System.EventHandler(this.btnRefreshRefineGold_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.rdbGoldDebitRefineGold);
            this.panel5.Controls.Add(this.rdbGoldCreditRefineGold);
            this.panel5.Location = new System.Drawing.Point(217, 242);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(533, 70);
            this.panel5.TabIndex = 149;
            // 
            // rdbGoldDebitRefineGold
            // 
            this.rdbGoldDebitRefineGold.BackColor = System.Drawing.Color.Transparent;
            this.rdbGoldDebitRefineGold.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGoldDebitRefineGold.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(181)))), ((int)(((byte)(25)))));
            this.rdbGoldDebitRefineGold.Location = new System.Drawing.Point(102, 37);
            this.rdbGoldDebitRefineGold.Margin = new System.Windows.Forms.Padding(4);
            this.rdbGoldDebitRefineGold.Name = "rdbGoldDebitRefineGold";
            this.rdbGoldDebitRefineGold.Size = new System.Drawing.Size(250, 25);
            this.rdbGoldDebitRefineGold.TabIndex = 43;
            this.rdbGoldDebitRefineGold.Text = "Gold Debit سونا ادائیگی  (-)";
            this.rdbGoldDebitRefineGold.UseVisualStyleBackColor = false;
            // 
            // rdbGoldCreditRefineGold
            // 
            this.rdbGoldCreditRefineGold.BackColor = System.Drawing.Color.Transparent;
            this.rdbGoldCreditRefineGold.Checked = true;
            this.rdbGoldCreditRefineGold.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGoldCreditRefineGold.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(145)))), ((int)(((byte)(42)))));
            this.rdbGoldCreditRefineGold.Location = new System.Drawing.Point(102, 4);
            this.rdbGoldCreditRefineGold.Margin = new System.Windows.Forms.Padding(4);
            this.rdbGoldCreditRefineGold.Name = "rdbGoldCreditRefineGold";
            this.rdbGoldCreditRefineGold.Size = new System.Drawing.Size(250, 25);
            this.rdbGoldCreditRefineGold.TabIndex = 42;
            this.rdbGoldCreditRefineGold.TabStop = true;
            this.rdbGoldCreditRefineGold.Text = "Gold Credit سونا وصولی  (+)";
            this.rdbGoldCreditRefineGold.UseVisualStyleBackColor = false;
            // 
            // labelX21
            // 
            this.labelX21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX21.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX21.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX21.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX21.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX21.Location = new System.Drawing.Point(33, 241);
            this.labelX21.Margin = new System.Windows.Forms.Padding(2);
            this.labelX21.Name = "labelX21";
            this.labelX21.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX21.Size = new System.Drawing.Size(179, 72);
            this.labelX21.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX21.TabIndex = 150;
            this.labelX21.Text = "Transaction Type";
            this.labelX21.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // refineGoldtabItem
            // 
            this.refineGoldtabItem.AttachedControl = this.tabControlPanel3;
            this.refineGoldtabItem.Name = "refineGoldtabItem";
            this.refineGoldtabItem.Text = "Refine Gold";
            this.refineGoldtabItem.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // labelX11
            // 
            this.labelX11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX11.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX11.Location = new System.Drawing.Point(37, 163);
            this.labelX11.Margin = new System.Windows.Forms.Padding(2);
            this.labelX11.Name = "labelX11";
            this.labelX11.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX11.Size = new System.Drawing.Size(177, 35);
            this.labelX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX11.TabIndex = 143;
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tabItem1
            // 
            this.tabItem1.AttachedControl = this.tabControlPanel6;
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "Khalis Sona";
            this.tabItem1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // tabControlPanel6
            // 
            this.tabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel6.Location = new System.Drawing.Point(0, 30);
            this.tabControlPanel6.Name = "tabControlPanel6";
            this.tabControlPanel6.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel6.Size = new System.Drawing.Size(789, 566);
            this.tabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(250)))), ((int)(((byte)(247)))));
            this.tabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel6.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.tabControlPanel6.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel6.Style.GradientAngle = 90;
            this.tabControlPanel6.TabIndex = 6;
            this.tabControlPanel6.TabItem = this.tabItem1;
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // LaboratoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(795, 661);
            this.Controls.Add(this.LabortorytabControl);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "LaboratoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Laboratory Form";
            this.Load += new System.EventHandler(this.LaboratoryForm_Load);
            this.panel2.ResumeLayout(false);
            this.HeadertableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabortorytabControl)).EndInit();
            this.LabortorytabControl.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.tableLayoutPanelKACHSONA.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.tableLayoutPanelOTHERS.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.tabControlPanel5.ResumeLayout(false);
            this.tableLayoutPanelCASH.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.tabControlPanel3.ResumeLayout(false);
            this.tableLayoutPanelREFINEGOLD.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel HeadertableLayoutPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.TabControl LabortorytabControl;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelKACHSONA;
        private DevComponents.DotNetBar.Controls.TextBoxX txtKachaSonaGrams;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSumKachaSona;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPurityKachaSona;
        private System.Windows.Forms.Panel panel4;
        private DevComponents.DotNetBar.ButtonX btnAddTransactionKachaSona;
        private DevComponents.DotNetBar.ButtonX btnRefreshKachSona;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescriptionKachasSona;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbClientsKachaSona;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton Rdb2GoldDebitKachaSona;
        private System.Windows.Forms.RadioButton Rdb1GoldCreditKachaSona;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.Line line1;
        private DevComponents.DotNetBar.Controls.Line line2;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.TabItem kachaSonatabItem;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem refineGoldtabItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelREFINEGOLD;
        private DevComponents.DotNetBar.Controls.Line line4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtImpureGoldRefineGold;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPurityRefineGold;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPureRefineGold;
        private System.Windows.Forms.Panel panel10;
        private DevComponents.DotNetBar.ButtonX btnAddTransactionRefineGold;
        private DevComponents.DotNetBar.ButtonX btnRefreshRefineGold;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel5;
        private DevComponents.DotNetBar.TabItem CashtabItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCASH;
        private DevComponents.DotNetBar.Controls.Line line7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAmountCash;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDiscriptionCash;
        private System.Windows.Forms.Panel panel12;
        private DevComponents.DotNetBar.ButtonX btnAddTransactionCash;
        private DevComponents.DotNetBar.ButtonX btnRefreshCash;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbCash;
        private DevComponents.DotNetBar.LabelX labelX36;
        private System.Windows.Forms.RadioButton rdbCashCredit;
        private System.Windows.Forms.RadioButton rdbCashDebit;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel6;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tabItemKhalisSona;
        private DevComponents.DotNetBar.LabelX labelX35;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelOTHERS;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPureGoldNoneKhalisSona;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPureGoldKhalisSonaFinal;
        private DevComponents.DotNetBar.LabelX labelX29;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDescriptionKhalisSona;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbClientsKhilsSona;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RadioButton rdbrdbGoldDebitKhalisSona;
        private System.Windows.Forms.RadioButton rdbGoldCreditKhalisSona;
        private DevComponents.DotNetBar.LabelX labelX31;
        private System.Windows.Forms.Panel panel9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSumTo990KhalisSona;
        private DevComponents.DotNetBar.LabelX labelX32;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSumFrom997KhalisSona;
        private DevComponents.DotNetBar.LabelX labelX34;
        private DevComponents.DotNetBar.Controls.Line line5;
        private DevComponents.DotNetBar.Controls.Line line6;
        private System.Windows.Forms.Panel panel11;
        private DevComponents.DotNetBar.ButtonX btnAddTransactionKhalisSona;
        private DevComponents.DotNetBar.ButtonX btnRefreshKhalisSona;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDisriptionRefineGold;
        private DevComponents.DotNetBar.LabelX labelX40;
        private DevComponents.DotNetBar.LabelX labelX33;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPureGoldCash;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGoldRateCash;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPureGoldKachaSona;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSumCustomToKachaSona;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSumCustomFromKachaSona;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.LabelX labelX18;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rdbGoldDebitRefineGold;
        private System.Windows.Forms.RadioButton rdbGoldCreditRefineGold;
        private DevComponents.DotNetBar.LabelX labelX21;
    }
}