﻿namespace Presentation
{
    partial class AddCurrentGoldPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.TxtGoldCurrentRate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BtnAddCurrentGoldRate = new DevComponents.DotNetBar.ButtonX();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.96674F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.03326F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.TxtGoldCurrentRate, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.BtnAddCurrentGoldRate, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.07692F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.38462F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.38462F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.38462F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.38462F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.38462F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(481, 256);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.labelX1, 3);
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(3, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(475, 53);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "Today Gold Rate (Per Tola)";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX2.Location = new System.Drawing.Point(3, 101);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(152, 33);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "Add Gold Rate ";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtGoldCurrentRate
            // 
            this.TxtGoldCurrentRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtGoldCurrentRate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtGoldCurrentRate.Border.Class = "TextBoxBorder";
            this.TxtGoldCurrentRate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtGoldCurrentRate.Font = new System.Drawing.Font("Segoe UI Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGoldCurrentRate.ForeColor = System.Drawing.Color.Blue;
            this.TxtGoldCurrentRate.Location = new System.Drawing.Point(162, 100);
            this.TxtGoldCurrentRate.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TxtGoldCurrentRate.Name = "TxtGoldCurrentRate";
            this.TxtGoldCurrentRate.Size = new System.Drawing.Size(273, 37);
            this.TxtGoldCurrentRate.TabIndex = 17;
            this.TxtGoldCurrentRate.WatermarkText = "0";
            this.TxtGoldCurrentRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtGoldCurrentRate_KeyPress);
            // 
            // BtnAddCurrentGoldRate
            // 
            this.BtnAddCurrentGoldRate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnAddCurrentGoldRate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnAddCurrentGoldRate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.tableLayoutPanel1.SetColumnSpan(this.BtnAddCurrentGoldRate, 2);
            this.BtnAddCurrentGoldRate.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddCurrentGoldRate.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.BtnAddCurrentGoldRate.Location = new System.Drawing.Point(158, 193);
            this.BtnAddCurrentGoldRate.Name = "BtnAddCurrentGoldRate";
            this.tableLayoutPanel1.SetRowSpan(this.BtnAddCurrentGoldRate, 2);
            this.BtnAddCurrentGoldRate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.BtnAddCurrentGoldRate.Size = new System.Drawing.Size(122, 45);
            this.BtnAddCurrentGoldRate.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.BtnAddCurrentGoldRate.TabIndex = 125;
            this.BtnAddCurrentGoldRate.Text = "Add Gold Rate";
            this.BtnAddCurrentGoldRate.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.BtnAddCurrentGoldRate.Click += new System.EventHandler(this.BtnAddCurrentGoldRate_Click);
            // 
            // AddCurrentGoldPriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(226)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(482, 258);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "AddCurrentGoldPriceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Current Gold Price";
            this.Load += new System.EventHandler(this.AddCurrentGoldPriceForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtGoldCurrentRate;
        private DevComponents.DotNetBar.ButtonX BtnAddCurrentGoldRate;
    }
}