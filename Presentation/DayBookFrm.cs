﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;


namespace Presentation
{
    public partial class DayBookFrm : Form
    {
        
        public DayBookFrm()
        {
            InitializeComponent();
           
        }
        
        void DGVPopulate()
        {
            DataTable dt = Core.DBUtils.GetDataTable("SELECT  t.T_Id, c.C_Id, c.Name, tt.T_TypeName, tt.T_Id as TrTypeId, t.Itme_Name, t.ImpureGoldinGram,t.Net_Amount FROM tblTransaction as t INNER JOIN tblClient as c ON t.ClientId = c.C_Id INNER JOIN tblTransactionType as tt ON t.Tran_Type = tt.T_Id  WHERE t.T_Date >= '" + DateTime.Now.Date + "' and IsDeleted =0  and Tran_Status ='Jewellers'");
            DataRow dr = dt.NewRow();
            double openingBlancce = 0;
            double openingBlanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient  WHERE AddedDate<'" + DateTime.Now.Date + "' "));
            double openingBlanceGoldDebit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebit FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));
            double openingBlanceGoldCredit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCredit FROM tblTransaction WHERE Tran_Type=1 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));
     
            openingBlancce = (openingBlanceGoldCredit -openingBlanceGoldDebit + openingBlanceGoldAllClients);
           
            if (openingBlancce == 0) { openingBlancce = 0; }
            dr["Name"] = "Opening Balance";
            dr["T_TypeName"] = "";
            dr["Itme_Name"] = "Date:" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            dr["ImpureGoldinGram"] = openingBlancce;
            
            dt.Rows.InsertAt(dr, 0);
            dataGridViewX1.DataSource = dt;
            int n = dataGridViewX1.Columns.Count;
            dataGridViewX1.Columns["Action"].DisplayIndex = 8;
            dataGridViewX1.Columns["TrTypeId"].DisplayIndex = 5;
            dataGridViewX1.Columns["T_Id"].Visible = false;
            dataGridViewX1.Columns["C_Id"].Visible = false;
            dataGridViewX1.Columns["TrTypeId"].Visible = false;
            //dataGridViewX1.Columns["Action"].DataPropertyName = "T_Id";
            dataGridViewX1.Columns["Name"].HeaderText = "Client Name";
            //dataGridViewX1.Columns["Name"].Width = 198;
            dataGridViewX1.Columns["Action"].Width = 100;
            dataGridViewX1.Columns["T_TypeName"].HeaderText = "Transaction Type";
            dataGridViewX1.Columns["Itme_Name"].HeaderText = "Description";
            dataGridViewX1.Columns["Itme_Name"].Width = 300;
            dataGridViewX1.Columns["ImpureGoldinGram"].HeaderText = "ImpureGold in Gram";
            dataGridViewX1.Columns["Net_Amount"].HeaderText = "Net Amount in Rs";
            for (int i = 1; i < dataGridViewX1.RowCount; i++)
            {
                if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "1")
                { dataGridViewX1.Rows[i].Cells["Action"].Value = "Add"; }
                else if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "2")
                {  dataGridViewX1.Rows[i].Cells["Action"].Value = "Edit";  }
                else { dataGridViewX1.Rows[i].Cells["Action"].Value = "History"; }
            }
            int columnIndex = 5;
            for (int i = 0; i < dataGridViewX1.RowCount; i++)
            {
                if (i == 0)
                {
                    if (openingBlancce > 0)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                    }
                    else { dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Red; }
                    dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.Moccasin;
                    
                    dataGridViewX1.Rows[i].DefaultCellStyle.Font = new Font("Segoe UI", 11, FontStyle.Bold);
                }
                else
                {
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 1)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(160, 181, 174);
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 2)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(69, 149, 164);
                    }

                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 3)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(217, 221, 216);
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 4)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(152, 198, 209);
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 5)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 148, 154);
                    }
                }
            }
            double currentBlance = 0;
            double openingbalanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient "));
            double GoldCreditCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCreditCurrentBalance FROM tblTransaction WHERE Tran_Type = 1 and IsDeleted = 0 "));
            double GoldDebitCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebitCurrentBalance FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 "));
            currentBlance = ( GoldCreditCurrentBalance - GoldDebitCurrentBalance + openingbalanceGoldAllClients);
            if (currentBlance < 0)
            {
                lblCurrentBalance.ForeColor = Color.Red;
                lblCurrentBalance.Text = currentBlance+" in Grams";
            }
            else
            {
                lblCurrentBalance.ForeColor = Color.Green;
                lblCurrentBalance.Text = currentBlance + " in Grams";
            }
        }

        public void RefreshDayBookForm()
        {
            DGVPopulate();

        }
        private void DayBookFrm_Load(object sender, EventArgs e)
        {
            DGVPopulate();
            
        }
        
        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridViewX1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (e.ColumnIndex ==0 && e.RowIndex > 0) 
            {
                //int row = Convert.ToInt32(dataGridViewX1.DataKeys[id].Value.ToString());
                if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "") { }
                else
                {
                    if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "Add")
                    {
                        int id = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                        Presentation.AddSelectCustomerForm jwfrm = new AddSelectCustomerForm(id);
                        jwfrm.ShowDialog();
                        DGVPopulate();
                    }
                    else if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "Edit")
                    {
                        int id = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                        int ClientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                        Presentation.HistoryForm jwfrm = new HistoryForm(ClientId);
                        jwfrm.ShowDialog();
                        DGVPopulate();

                    }
                    else
                    {
                        int ClientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                        Presentation.HistoryForm jwfrm = new HistoryForm(ClientId);
                        jwfrm.ShowDialog();
                        DGVPopulate();
                    }
                }
            }
            else { }
        }
        private void btnNewEntery_Click(object sender, EventArgs e)
        {
            
            tblTransaction objTrans = new tblTransaction();
            if (objTrans != null)
            {
                GoldTransactionForm frEmp = new GoldTransactionForm(objTrans);
                frEmp.ShowDialog();
                DGVPopulate();
            }
        }
    }
}
