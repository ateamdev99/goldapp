﻿namespace Presentation
{
    partial class HistoryLaboratoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.lblCurrentBalance = new DevComponents.DotNetBar.LabelX();
            this.Remove = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.Edit = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanelForCloring = new System.Windows.Forms.TableLayoutPanel();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lblGoldDebitclor = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.lblGoldCreditclor = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.lblCashCreditclor = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.lblCashDebitclor = new DevComponents.DotNetBar.LabelX();
            this.lblCustName = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanelSums = new System.Windows.Forms.TableLayoutPanel();
            this.lblPureGoldTotal = new DevComponents.DotNetBar.LabelX();
            this.lblNetAmountTotal = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.lblImpureGoldTotal = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanelForCloring.SuspendLayout();
            this.tableLayoutPanelSums.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelSums.SetColumnSpan(this.labelX10, 3);
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX10.Location = new System.Drawing.Point(175, 50);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(467, 22);
            this.labelX10.TabIndex = 5;
            this.labelX10.Text = "Current Balance :";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // lblCurrentBalance
            // 
            this.lblCurrentBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCurrentBalance.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanelSums.SetColumnSpan(this.lblCurrentBalance, 2);
            this.lblCurrentBalance.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentBalance.Location = new System.Drawing.Point(651, 50);
            this.lblCurrentBalance.Name = "lblCurrentBalance";
            this.lblCurrentBalance.Size = new System.Drawing.Size(327, 22);
            this.lblCurrentBalance.TabIndex = 4;
            this.lblCurrentBalance.Text = "000";
            // 
            // Remove
            // 
            this.Remove.HeaderText = "Remove";
            this.Remove.Name = "Remove";
            this.Remove.ReadOnly = true;
            this.Remove.Text = "Remove";
            this.Remove.UseColumnTextForButtonValue = true;
            // 
            // Edit
            // 
            this.Edit.HeaderText = "Edit";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Text = "Edit";
            this.Edit.UseColumnTextForButtonValue = true;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Edit,
            this.Remove});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 57);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.Size = new System.Drawing.Size(984, 554);
            this.dataGridViewX1.TabIndex = 4;
            this.dataGridViewX1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.panel1.Controls.Add(this.tableLayoutPanelForCloring);
            this.panel1.Controls.Add(this.lblCustName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 60);
            this.panel1.TabIndex = 7;
            // 
            // tableLayoutPanelForCloring
            // 
            this.tableLayoutPanelForCloring.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tableLayoutPanelForCloring.ColumnCount = 8;
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelForCloring.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX5, 3, 1);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX1, 3, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX3, 7, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.lblGoldDebitclor, 2, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX7, 7, 1);
            this.tableLayoutPanelForCloring.Controls.Add(this.lblGoldCreditclor, 0, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX2, 1, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX6, 1, 1);
            this.tableLayoutPanelForCloring.Controls.Add(this.lblCashCreditclor, 4, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX4, 5, 0);
            this.tableLayoutPanelForCloring.Controls.Add(this.labelX9, 5, 1);
            this.tableLayoutPanelForCloring.Controls.Add(this.lblCashDebitclor, 6, 0);
            this.tableLayoutPanelForCloring.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanelForCloring.Location = new System.Drawing.Point(559, 7);
            this.tableLayoutPanelForCloring.Name = "tableLayoutPanelForCloring";
            this.tableLayoutPanelForCloring.RowCount = 2;
            this.tableLayoutPanelForCloring.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelForCloring.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelForCloring.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelForCloring.Size = new System.Drawing.Size(422, 45);
            this.tableLayoutPanelForCloring.TabIndex = 40;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.White;
            this.labelX5.Location = new System.Drawing.Point(129, 25);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(78, 17);
            this.labelX5.TabIndex = 17;
            this.labelX5.Text = "سونا ادائیگی";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(129, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(78, 16);
            this.labelX1.TabIndex = 16;
            this.labelX1.Text = "Gold Debit";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.White;
            this.labelX3.Location = new System.Drawing.Point(339, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(80, 16);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "Cash Debit";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGoldDebitclor
            // 
            this.lblGoldDebitclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblGoldDebitclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(149)))), ((int)(((byte)(164)))));
            // 
            // 
            // 
            this.lblGoldDebitclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGoldDebitclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldDebitclor.ForeColor = System.Drawing.Color.White;
            this.lblGoldDebitclor.Location = new System.Drawing.Point(108, 3);
            this.lblGoldDebitclor.Name = "lblGoldDebitclor";
            this.tableLayoutPanelForCloring.SetRowSpan(this.lblGoldDebitclor, 2);
            this.lblGoldDebitclor.Size = new System.Drawing.Size(15, 39);
            this.lblGoldDebitclor.TabIndex = 8;
            this.lblGoldDebitclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.White;
            this.labelX7.Location = new System.Drawing.Point(339, 25);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(80, 17);
            this.labelX7.TabIndex = 14;
            this.labelX7.Text = " کیش ادائیگی";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGoldCreditclor
            // 
            this.lblGoldCreditclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblGoldCreditclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(181)))), ((int)(((byte)(174)))));
            // 
            // 
            // 
            this.lblGoldCreditclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGoldCreditclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldCreditclor.ForeColor = System.Drawing.Color.White;
            this.lblGoldCreditclor.Location = new System.Drawing.Point(3, 3);
            this.lblGoldCreditclor.Name = "lblGoldCreditclor";
            this.tableLayoutPanelForCloring.SetRowSpan(this.lblGoldCreditclor, 2);
            this.lblGoldCreditclor.Size = new System.Drawing.Size(15, 39);
            this.lblGoldCreditclor.TabIndex = 9;
            this.lblGoldCreditclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(24, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(78, 16);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "Gold Credit";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.White;
            this.labelX6.Location = new System.Drawing.Point(24, 25);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(78, 17);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "سونا  وصولی";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblCashCreditclor
            // 
            this.lblCashCreditclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCashCreditclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(221)))), ((int)(((byte)(216)))));
            // 
            // 
            // 
            this.lblCashCreditclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCashCreditclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashCreditclor.ForeColor = System.Drawing.Color.White;
            this.lblCashCreditclor.Location = new System.Drawing.Point(213, 3);
            this.lblCashCreditclor.Name = "lblCashCreditclor";
            this.tableLayoutPanelForCloring.SetRowSpan(this.lblCashCreditclor, 2);
            this.lblCashCreditclor.Size = new System.Drawing.Size(15, 39);
            this.lblCashCreditclor.TabIndex = 11;
            this.lblCashCreditclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.White;
            this.labelX4.Location = new System.Drawing.Point(234, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(78, 16);
            this.labelX4.TabIndex = 3;
            this.labelX4.Text = "Cash Credit";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.White;
            this.labelX9.Location = new System.Drawing.Point(234, 25);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(78, 17);
            this.labelX9.TabIndex = 18;
            this.labelX9.Text = " کیش وصولی";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblCashDebitclor
            // 
            this.lblCashDebitclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCashDebitclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(198)))), ((int)(((byte)(209)))));
            // 
            // 
            // 
            this.lblCashDebitclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCashDebitclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashDebitclor.ForeColor = System.Drawing.Color.White;
            this.lblCashDebitclor.Location = new System.Drawing.Point(318, 4);
            this.lblCashDebitclor.Name = "lblCashDebitclor";
            this.tableLayoutPanelForCloring.SetRowSpan(this.lblCashDebitclor, 2);
            this.lblCashDebitclor.Size = new System.Drawing.Size(15, 37);
            this.lblCashDebitclor.TabIndex = 10;
            this.lblCashDebitclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblCustName
            // 
            // 
            // 
            // 
            this.lblCustName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCustName.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustName.ForeColor = System.Drawing.Color.White;
            this.lblCustName.Location = new System.Drawing.Point(5, 7);
            this.lblCustName.Name = "lblCustName";
            this.lblCustName.Size = new System.Drawing.Size(550, 42);
            this.lblCustName.TabIndex = 1;
            this.lblCustName.Text = "Cutomer Name History";
            this.lblCustName.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tableLayoutPanelSums
            // 
            this.tableLayoutPanelSums.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelSums.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.tableLayoutPanelSums.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanelSums.ColumnCount = 6;
            this.tableLayoutPanelSums.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.28016F));
            this.tableLayoutPanelSums.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.74642F));
            this.tableLayoutPanelSums.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.74642F));
            this.tableLayoutPanelSums.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.17791F));
            this.tableLayoutPanelSums.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.15542F));
            this.tableLayoutPanelSums.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.89366F));
            this.tableLayoutPanelSums.Controls.Add(this.lblCurrentBalance, 4, 1);
            this.tableLayoutPanelSums.Controls.Add(this.labelX10, 1, 1);
            this.tableLayoutPanelSums.Controls.Add(this.lblPureGoldTotal, 3, 0);
            this.tableLayoutPanelSums.Controls.Add(this.lblNetAmountTotal, 5, 0);
            this.tableLayoutPanelSums.Controls.Add(this.labelX19, 2, 0);
            this.tableLayoutPanelSums.Controls.Add(this.labelX20, 4, 0);
            this.tableLayoutPanelSums.Controls.Add(this.lblImpureGoldTotal, 1, 0);
            this.tableLayoutPanelSums.Controls.Add(this.labelX16, 0, 0);
            this.tableLayoutPanelSums.Location = new System.Drawing.Point(0, 611);
            this.tableLayoutPanelSums.Name = "tableLayoutPanelSums";
            this.tableLayoutPanelSums.RowCount = 2;
            this.tableLayoutPanelSums.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanelSums.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelSums.Size = new System.Drawing.Size(984, 78);
            this.tableLayoutPanelSums.TabIndex = 8;
            // 
            // lblPureGoldTotal
            // 
            this.lblPureGoldTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblPureGoldTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPureGoldTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPureGoldTotal.ForeColor = System.Drawing.Color.Black;
            this.lblPureGoldTotal.Location = new System.Drawing.Point(483, 6);
            this.lblPureGoldTotal.Name = "lblPureGoldTotal";
            this.lblPureGoldTotal.Size = new System.Drawing.Size(159, 35);
            this.lblPureGoldTotal.TabIndex = 3;
            this.lblPureGoldTotal.Text = "PureGold";
            this.lblPureGoldTotal.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblNetAmountTotal
            // 
            this.lblNetAmountTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblNetAmountTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNetAmountTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetAmountTotal.ForeColor = System.Drawing.Color.Black;
            this.lblNetAmountTotal.Location = new System.Drawing.Point(809, 6);
            this.lblNetAmountTotal.Name = "lblNetAmountTotal";
            this.lblNetAmountTotal.Size = new System.Drawing.Size(169, 35);
            this.lblNetAmountTotal.TabIndex = 5;
            this.lblNetAmountTotal.Text = "NetAmmount";
            // 
            // labelX19
            // 
            this.labelX19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX19.Location = new System.Drawing.Point(329, 6);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(145, 35);
            this.labelX19.TabIndex = 9;
            this.labelX19.Text = "Current Pure Gold Total\r\n(خالص سونا)";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX20
            // 
            this.labelX20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX20.Location = new System.Drawing.Point(651, 6);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(149, 35);
            this.labelX20.TabIndex = 10;
            this.labelX20.Text = "CurrentNetAmountTotal\r\n(ادائیگی رقم)";
            this.labelX20.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // lblImpureGoldTotal
            // 
            this.lblImpureGoldTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblImpureGoldTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblImpureGoldTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImpureGoldTotal.ForeColor = System.Drawing.Color.Black;
            this.lblImpureGoldTotal.Location = new System.Drawing.Point(175, 6);
            this.lblImpureGoldTotal.Name = "lblImpureGoldTotal";
            this.lblImpureGoldTotal.Size = new System.Drawing.Size(145, 35);
            this.lblImpureGoldTotal.TabIndex = 0;
            this.lblImpureGoldTotal.Text = "impurGold";
            this.lblImpureGoldTotal.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX16
            // 
            this.labelX16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX16.Location = new System.Drawing.Point(6, 6);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(160, 35);
            this.labelX16.TabIndex = 6;
            this.labelX16.Text = "Current Impure Gold Total\r\n(کچا سونا)";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // HistoryLaboratoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(984, 691);
            this.Controls.Add(this.tableLayoutPanelSums);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewX1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "HistoryLaboratoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "HistoryLaboratoryForm";
            this.Load += new System.EventHandler(this.HistoryLaboratoryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanelForCloring.ResumeLayout(false);
            this.tableLayoutPanelSums.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX lblCurrentBalance;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Remove;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Edit;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelForCloring;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lblGoldDebitclor;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX lblGoldCreditclor;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX lblCashCreditclor;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX lblCashDebitclor;
        private DevComponents.DotNetBar.LabelX lblCustName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSums;
        private DevComponents.DotNetBar.LabelX lblPureGoldTotal;
        private DevComponents.DotNetBar.LabelX lblNetAmountTotal;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.LabelX lblImpureGoldTotal;
        private DevComponents.DotNetBar.LabelX labelX16;
    }
}