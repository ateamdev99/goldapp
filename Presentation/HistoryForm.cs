﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class HistoryForm : Form
    {
        
        public int CLIENT_ID;
        public HistoryForm(int clientId)
        {
            InitializeComponent();
            if (clientId != 0)
            {
                CLIENT_ID = clientId;
            }
        }
        private void HistoryForm_Load(object sender, EventArgs e)
        {
            DGVPopulate();
        }
        public void RefreshHistoryForm()
        {
            DGVPopulate();
        }
        void DGVPopulate()
        {
            DataTable dt = Core.DBUtils.GetDataTable("SELECT  t.T_Id,t.Itme_Name,t.ImpureGoldinGram,t.T_Date,tt.T_TypeName,tt.T_Id as TrTypeId,t.Net_Amount FROM tblTransaction as t INNER JOIN tblClient as c ON t.ClientId = c.C_Id INNER JOIN tblTransactionType as tt ON t.Tran_Type = tt.T_Id WHERE   ClientId  =' " + CLIENT_ID + "' and IsDeleted =0 and Tran_Status ='Jewellers'");
            string name = Core.DBUtils.executeSqlGetID("select Name from tblClient where C_Id =' " + CLIENT_ID + "'");
            if (name == "Admin") { tableLayoutPanelForCloring.Visible = false; }

            lblCustName.Text = name + " Transaction History";
            double openingBlancce = 0;
            double openingBlanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT GoldOpeningBalance as openingbalanceGoldOneClient FROM tblClient WHERE C_Id = '" + CLIENT_ID + "' and UpdatedDate<'" + DateTime.Now + "' "));
            double openingBlanceGoldDebit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebitOneClient  FROM tblTransaction WHERE Tran_Type = 2 and IsDeleted = 0 and ClientId = '" + CLIENT_ID + "' and T_Date< '" + DateTime.Now.Date + "'"));
            double openingBlanceGoldCredit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCreditOneClient  FROM tblTransaction WHERE Tran_Type = 1  and IsDeleted = 0 and ClientId = '" + CLIENT_ID + "' and T_Date<'" + DateTime.Now.Date + "'"));

            openingBlancce = (openingBlanceGoldCredit - openingBlanceGoldDebit + openingBlanceGoldAllClients);
            DataRow dr = dt.NewRow();
            if (openingBlancce == 0) { openingBlancce = 0; }
            dr["T_Date"] = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            dr["Itme_Name"] = "Opening Balance";
            dr["ImpureGoldinGram"] = Conversion.toDouble(openingBlancce);
            dr["T_TypeName"] = "";
            dt.Rows.InsertAt(dr, 0);
            dataGridViewX1.DataSource = dt;
            int n = dataGridViewX1.Columns.Count;
            dataGridViewX1.Columns["T_Id"].DisplayIndex = 0;
            dataGridViewX1.Columns["TrTypeId"].DisplayIndex = 1;
            dataGridViewX1.Columns["T_Date"].DisplayIndex = 2;
            dataGridViewX1.Columns["Itme_Name"].DisplayIndex = 3;
            dataGridViewX1.Columns["Itme_Name"].Width = 200;
            dataGridViewX1.Columns["ImpureGoldinGram"].DisplayIndex = 4;
            dataGridViewX1.Columns["T_TypeName"].DisplayIndex = 5;
            dataGridViewX1.Columns["Remove"].DisplayIndex = 8;
            dataGridViewX1.Columns["Remove"].Width = 50;
            dataGridViewX1.Columns["Edit"].DisplayIndex = 8;
            dataGridViewX1.Columns["Edit"].Width = 50;
            dataGridViewX1.Columns["T_Id"].Visible = false;
            dataGridViewX1.Columns["TrTypeId"].Visible = false;
            dataGridViewX1.Columns["T_Date"].HeaderText = "Tran Date";
            dataGridViewX1.Columns["T_TypeName"].HeaderText = "Transaction Type";
            dataGridViewX1.Columns["Itme_Name"].HeaderText = "Description";
            dataGridViewX1.Columns["ImpureGoldinGram"].HeaderText = "ImpureGold in Gram";
            dataGridViewX1.Columns["Net_Amount"].HeaderText = "Net Amount in Rs";
            int columnIndex = 7;
            for (int i = 0; i < dataGridViewX1.RowCount; i++)
            {
                if (i == 0)
                {
                    if (openingBlancce > 0)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                    }
                    else { dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Red; }
                    dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.Moccasin;
                    dataGridViewX1.Rows[i].DefaultCellStyle.Font = new Font("Segoe UI", 11, FontStyle.Bold);
                    var BtnCell = (DataGridViewButtonCell)dataGridViewX1.Rows[i].Cells["Edit"];
                    BtnCell.UseColumnTextForButtonValue = false;
                    var BtnCell1 = (DataGridViewButtonCell)dataGridViewX1.Rows[i].Cells["Remove"];
                    BtnCell1.UseColumnTextForButtonValue = false;
                }

                else
                {
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 1)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(160, 181, 174);
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 2)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(69, 149, 164);
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 3)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(217, 221, 216);
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 4)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(152, 198, 209); 
                    }
                    if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 5)
                    {
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 148, 154);
                    }
                }
            }
            double currentBlance = 0;
            double openingbalanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT GoldOpeningBalance as openingbalanceGoldOneClient FROM tblClient WHERE  C_Id ='"+CLIENT_ID+"'"));
            double GoldCreditCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCreditCurrentBalanceOneClient  FROM tblTransaction WHERE Tran_Type = 1 and IsDeleted = 0 and ClientId = '" + CLIENT_ID + "'"));
            double GoldDebitCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebitCurrentBalanceOneClient  FROM tblTransaction WHERE Tran_Type = 2 and IsDeleted = 0 and ClientId = '" + CLIENT_ID + "'"));
            currentBlance = (GoldCreditCurrentBalance - GoldDebitCurrentBalance + openingbalanceGoldAllClients);
            if (currentBlance < 0)
            {
                lblCurrentBalance.ForeColor = Color.Red;
                lblCurrentBalance.Text = currentBlance + " in Grams";
            }
            else
            {
                lblCurrentBalance.ForeColor = Color.Green;
                lblCurrentBalance.Text = currentBlance + " in Grams";
            }
        }
        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridViewX1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (e.ColumnIndex == 0 || e.ColumnIndex == 1 && e.RowIndex > 0)
            {
                if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Edit"].Value) == "") { }
                else if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Remove"].Value) == "") { }
                else
                {
                    if (dataGridViewX1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Edit")
                    {
                        int TrId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                        int trTypeId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["TrTypeId"].Value);
                        tblTransaction obj = new tblTransaction();
                        if (trTypeId == 5)
                        {
                            obj.T_Id = TrId;
                            Presentation.ExpenseForm jwfrm = new ExpenseForm(obj);
                            jwfrm.ShowDialog();
                            DGVPopulate();
                        }
                        else
                        {
                            obj.T_Id = TrId;
                            GoldTransactionForm frm = new GoldTransactionForm(obj);
                            frm.ShowDialog();
                            DGVPopulate();
                        }
                        //string itme_Name = Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Itme_Name"].Value);
                        //double weight_in_Gram = Convert.ToDouble(dataGridViewX1.Rows[e.RowIndex].Cells["Weight_in_Gram"].Value);
                        //obj.ClientId = CLIENT_ID;
                        //obj.Tran_Type = trTypeId;
                        //obj.Itme_Name = itme_Name;
                        //obj.Weight_in_Gram = weight_in_Gram;
                    }
                    else if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == "Remove")
                    {
                        try
                        {
                            int TrId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                            if (TrId > 0)
                            {
                                DialogResult DResult;
                                DResult = MessageBox.Show("Click Yes to Delete Transaction !", "Are You Sure to Process ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                if (DResult == DialogResult.Yes)
                                {
                                    DBUtils.ExecuteSQL("update tblTransaction set IsDeleted=1 where T_Id='" + TrId + "'");
                                    this.RefreshHistoryForm();
                                    //Close();
                                }
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }


                    }
                }
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}

