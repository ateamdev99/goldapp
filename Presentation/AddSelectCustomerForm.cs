﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;
using System.Data.SqlClient;

namespace Presentation
{
    public partial class AddSelectCustomerForm : Form
    {
        public int TrID;
        public AddSelectCustomerForm( int id)
        {
            InitializeComponent();
            if (id != 0)
            {
                TrID = id;
            }
        }

        private void AddSelectCustomerForm_Load(object sender, EventArgs e)
        {
            DGVPopulate();
        }
        void DGVPopulate()
        {
            DataTable dt = Core.DBUtils.GetDataTable("select Name, C_Id ,MobileNo from tblClient where C_Id != 1 and C_Id != 2 and ActivateAccount=0 ");

            dataGridViewX1.DataSource = dt;
            int n = dataGridViewX1.Columns.Count;
            dataGridViewX1.Columns["Action"].DisplayIndex = 3;
            dataGridViewX1.Columns["C_Id"].Visible = false;
            //dataGridViewX1.Columns["C_Id"].Visible = false;
            //dataGridViewX1.Columns["Action"].DataPropertyName = "T_Id";
            dataGridViewX1.Columns["Name"].HeaderText = "Client Name";
            dataGridViewX1.Columns["Name"].Width = 300;
            dataGridViewX1.Columns["Action"].Width = 100;
            dataGridViewX1.Columns["MobileNo"].HeaderText = "Mobile No";
            
        }
        private void btnDebit_Click(object sender, EventArgs e)
        {
            tblClient obj = new tblClient();
            if (obj != null)
            {
                Presentation.ClintInfoForm frEmp = new ClintInfoForm(obj, TrID);
                frEmp.ShowDialog();
                DGVPopulate();
            }
        }

        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridViewX1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (e.ColumnIndex == 0 && e.RowIndex > -1)
            {
                if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "Select")
                {
                    Boolean i;
                    int clientid = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);

                    i = DBUtils.ExecuteSQL("Update tblTransaction set ClientId='" + clientid + "' where T_Id='" + TrID + "'");
                    if (i == true)
                    {
                        DialogResult DResult;
                        DResult = MessageBox.Show("Succesfully Client Selected to Transaction no " + TrID, "Client Selected", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (DResult == DialogResult.Yes)
                        {
                            Close();
                        }
                    }

                }
            }
        }
        public void closethis()
        {
            this.Close();
        }
        private void AddSelectCustomerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
