﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class ExpenseForm : Form
    {
        public ExpenseForm( tblTransaction obj)
        {
            InitializeComponent();
            if (obj == null || obj.T_Id == 0)
            {
                tblTransactionBindingSource.DataSource = new tblTransaction();

            }
            else
            {
                tblTransaction objTr = TransactionServicesClass.GetByIdTransaction(obj.T_Id);
                
                txtExpenseDetail.Text = Conversion.toString(objTr.Itme_Name);
                txtExpenseAmount.Text = Conversion.toString(objTr.Net_Amount);
                tblTransactionBindingSource.DataSource = objTr;
                BtnAddExpense.Text = "Update";
            }
        }

        private void txtExpenseAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void ExpenseForm_Load(object sender, EventArgs e)
        {

        }

        private void BtnAddExpense_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtExpenseDetail.Text))
                {
                    MessageBox.Show("Enter Expense Detail", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtExpenseDetail.Focus();
                }

                else if (string.IsNullOrEmpty(txtExpenseAmount.Text) || txtExpenseAmount.Text == "0")
                {
                    MessageBox.Show("Enter Expense Amount", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtExpenseAmount.Focus();
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                    if (tblObj != null && tblObj.T_Id == 0 && BtnAddExpense.Text == "Add Expense")
                    {
                        try
                        {
                            tblObj.ClientId = 2;
                          tblObj.Itme_Name = Conversion.toString(txtExpenseDetail.Text );
                            tblObj.Net_Amount = Conversion.toDouble(txtExpenseAmount.Text );
                            tblObj.T_Date = DateTime.Now;
                            tblObj.Tran_Type = 5;
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Added_By = "Admin";
                            tblObj.Tran_Status = "Nill";
                            tblObj.IsTeleConfirmation = false;
                            tblObj.TeleDate = DateTime.Now;
                            tblObj.Purity = 1000;
                            tblObj.IsDeleted = false;
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Make Expense", "Sure to Make Expense ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Insert(tblObj);
                                Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        try
                        {
                            tblObj.Itme_Name = Conversion.toString(txtExpenseDetail.Text);
                            tblObj.Net_Amount = Conversion.toDouble(txtExpenseAmount.Text);
                            if (tblObj.IsTeleConfirmation == true) { tblObj.IsTeleConfirmation = true; } else { tblObj.IsTeleConfirmation = false; tblObj.TeleDate = DateTime.Now; }

                            tblObj.Tran_Type = 5;
                            tblObj.Added_Date = DateTime.Now;
                            tblObj.Purity = 1000;
                            tblObj.Added_By = "Admin";
                            tblObj.T_Date = DateTime.Now;
                            tblObj.UpatedDate = DateTime.Now;
                            DialogResult DResult;
                            DResult = MessageBox.Show("Click Yes To Updated Expense", "Sure to Updated Expense ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (DResult == DialogResult.Yes)
                            {
                                TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                                this.Close();
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
