﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class MainDashbordForm : Form
    {
        public MainDashbordForm()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
            RefreshDashBoard();
        }
        public void RefreshDashBoard()
        {
            LblTotalClients.Text = ClientServicesClass.GetClientCount().ToString();
            LblTotlTransac.Text = TransactionServicesClass.GetTransactiponsCount().ToString();
            lblGoldDebitSum.Text = TransactionServicesClass.GetGoldDebitTotalSum().ToString();
            lblGoldCreditSum.Text = TransactionServicesClass.GetGoldCreditTotalSum().ToString();
            LblCashDebitSum.Text = TransactionServicesClass.GetCashDebitTotalSum().ToString();
            LblCashCreditSum.Text = TransactionServicesClass.GetCashCreditTotalSum().ToString();
        }
        private void BtnAddClentInf_Click(object sender, EventArgs e)
        {
            tblClient obj = new tblClient();
            if (obj != null)
            {
                //Presentation.ClintInfoForm frEmp = new ClintInfoForm(obj);
                //frEmp.ShowDialog();
            }
        }

        private void BtnTrnasactJewelr_Click(object sender, EventArgs e)
        {

            tblTransaction obj = new tblTransaction();
            if (obj != null)
            {
                Presentation.JewellersForm jwfrm = new JewellersForm(obj);
                jwfrm.ShowDialog();
            }
        }

        private void BtnDateWisTran_Click(object sender, EventArgs e)
        {
            tblTransaction obj = new tblTransaction();
            if (obj != null)
            {
                Presentation.DateWiseTransactionsForm frEmp = new DateWiseTransactionsForm(obj);
                frEmp.ShowDialog();
            }
        }

        private void BtnTranEntrySearc_Click(object sender, EventArgs e)
        {
            tblTransaction objTrans = new tblTransaction();
            if (objTrans != null)
            {
                Presentation.TransactionEnteryAndSearchForm frEmp = new TransactionEnteryAndSearchForm(objTrans);
                frEmp.ShowDialog();
            }
        }


        private void BtnAddDistrict_Click_1(object sender, EventArgs e)
        {
            tblTransaction objTrans = new tblTransaction();
            if (objTrans != null)
            {
                Presentation.TransactionForm frEmp = new TransactionForm(objTrans);
                frEmp.ShowDialog();
            }
        }

        private void BtnCloseDshBrdFrm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrntRecipt_Click(object sender, EventArgs e)
        {
            Presentation.ReportViewerForm frEmp = new ReportViewerForm();
            frEmp.ShowDialog();
        }

        private void BtnDBClenup_Click(object sender, EventArgs e)
        {
            Presentation.DataBaseCleanupForm frEmp = new DataBaseCleanupForm();
            frEmp.ShowDialog();
            
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            LblTimer.Text = DateTime.Now.ToString("hh:mm:ss tt");

            this.RefreshDashBoard();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //Presentation.LaboratoryForm frEmp = new LaboratoryForm();
            //frEmp.ShowDialog();
        }

        private void BtnSetting_Click(object sender, EventArgs e)
        {
            Presentation.SettingForm frEmp = new SettingForm();
            frEmp.ShowDialog();
        }

        private void btnCrystalReport_Click(object sender, EventArgs e)
        {
            //Presentation.PracticForm frEmp = new PracticForm();
            //    frEmp.ShowDialog();
        }

        private void BtnClientAccDetils_Click(object sender, EventArgs e)
        {
            Presentation.AccountDetailsForm frEmp = new AccountDetailsForm();
            frEmp.ShowDialog();
        }

        private void BtnDebtorsList_Click(object sender, EventArgs e)
        {
            Presentation.DebtorsListForm frEmp = new DebtorsListForm();
            frEmp.ShowDialog();
        }

        

        


        

    }
}
