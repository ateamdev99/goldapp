﻿namespace Presentation
{
    partial class DashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCustName = new DevComponents.DotNetBar.LabelX();
            this.lblTodayGoldRate = new DevComponents.DotNetBar.LabelX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTodayOpBlnc = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.linkAddGoldRate = new System.Windows.Forms.LinkLabel();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panelBody = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRefineGold = new DevComponents.DotNetBar.ButtonX();
            this.btnGold = new DevComponents.DotNetBar.ButtonX();
            this.btnDayBook = new DevComponents.DotNetBar.ButtonX();
            this.btnCash = new DevComponents.DotNetBar.ButtonX();
            this.btnCredit = new DevComponents.DotNetBar.ButtonX();
            this.btnDebit = new DevComponents.DotNetBar.ButtonX();
            this.btnOthers = new DevComponents.DotNetBar.ButtonX();
            this.btnRecord = new DevComponents.DotNetBar.ButtonX();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClientList = new DevComponents.DotNetBar.ButtonX();
            this.btnJewellery = new DevComponents.DotNetBar.ButtonX();
            this.btnSetting = new DevComponents.DotNetBar.ButtonX();
            this.btnRustedGold = new DevComponents.DotNetBar.ButtonX();
            this.btnLaboratory = new DevComponents.DotNetBar.ButtonX();
            this.btnCasting = new DevComponents.DotNetBar.ButtonX();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbTr3 = new System.Windows.Forms.PictureBox();
            this.lblTr3 = new DevComponents.DotNetBar.LabelX();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pbTr2 = new System.Windows.Forms.PictureBox();
            this.lblTr2 = new DevComponents.DotNetBar.LabelX();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pbTr1 = new System.Windows.Forms.PictureBox();
            this.lblTr1 = new DevComponents.DotNetBar.LabelX();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.lblViewAll = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblPAK = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.lblUK = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.lblUSA = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.panelTop.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelBody.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTr3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.panelTop.Controls.Add(this.tableLayoutPanel1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1184, 90);
            this.panelTop.TabIndex = 0;
            this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.45384F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.59154F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0.7735947F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.01908F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.01066F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.15128F));
            this.tableLayoutPanel1.Controls.Add(this.lblCustName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTodayGoldRate, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTodayOpBlnc, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1181, 90);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblCustName
            // 
            this.lblCustName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCustName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCustName.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustName.ForeColor = System.Drawing.Color.White;
            this.lblCustName.Location = new System.Drawing.Point(3, 3);
            this.lblCustName.Name = "lblCustName";
            this.tableLayoutPanel1.SetRowSpan(this.lblCustName, 2);
            this.lblCustName.Size = new System.Drawing.Size(353, 84);
            this.lblCustName.TabIndex = 0;
            this.lblCustName.Text = "Cutomer Name";
            this.lblCustName.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblTodayGoldRate
            // 
            this.lblTodayGoldRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblTodayGoldRate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTodayGoldRate.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTodayGoldRate.ForeColor = System.Drawing.Color.White;
            this.lblTodayGoldRate.Location = new System.Drawing.Point(896, 57);
            this.lblTodayGoldRate.Name = "lblTodayGoldRate";
            this.lblTodayGoldRate.Size = new System.Drawing.Size(159, 30);
            this.lblTodayGoldRate.TabIndex = 6;
            this.lblTodayGoldRate.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBox1.Image = global::Presentation.Properties.Resources.notification_important_24px;
            this.pictureBox1.Location = new System.Drawing.Point(1107, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox1, 2);
            this.pictureBox1.Size = new System.Drawing.Size(25, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lblTodayOpBlnc
            // 
            this.lblTodayOpBlnc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblTodayOpBlnc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTodayOpBlnc.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTodayOpBlnc.ForeColor = System.Drawing.Color.White;
            this.lblTodayOpBlnc.Location = new System.Drawing.Point(896, 3);
            this.lblTodayOpBlnc.Name = "lblTodayOpBlnc";
            this.lblTodayOpBlnc.Size = new System.Drawing.Size(159, 48);
            this.lblTodayOpBlnc.TabIndex = 2;
            this.lblTodayOpBlnc.Text = "000000";
            this.lblTodayOpBlnc.TextLineAlignment = System.Drawing.StringAlignment.Far;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.labelX6);
            this.panel1.Controls.Add(this.linkAddGoldRate);
            this.panel1.Location = new System.Drawing.Point(687, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 30);
            this.panel1.TabIndex = 8;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.White;
            this.labelX6.Location = new System.Drawing.Point(80, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(123, 24);
            this.labelX6.TabIndex = 7;
            this.labelX6.Text = "Today Gold Rate ";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // linkAddGoldRate
            // 
            this.linkAddGoldRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkAddGoldRate.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkAddGoldRate.ForeColor = System.Drawing.Color.Maroon;
            this.linkAddGoldRate.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkAddGoldRate.LinkColor = System.Drawing.Color.White;
            this.linkAddGoldRate.Location = new System.Drawing.Point(0, 3);
            this.linkAddGoldRate.Name = "linkAddGoldRate";
            this.linkAddGoldRate.Size = new System.Drawing.Size(79, 24);
            this.linkAddGoldRate.TabIndex = 4;
            this.linkAddGoldRate.TabStop = true;
            this.linkAddGoldRate.Text = "Add Rate";
            this.linkAddGoldRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.linkAddGoldRate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddGoldRate_LinkClicked);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tableLayoutPanel1.SetColumnSpan(this.labelX1, 2);
            this.labelX1.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(687, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(203, 48);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "Today Opening Balance";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX1.TextLineAlignment = System.Drawing.StringAlignment.Far;
            // 
            // panelBody
            // 
            this.panelBody.AutoScroll = true;
            this.panelBody.Controls.Add(this.tableLayoutPanel3);
            this.panelBody.Controls.Add(this.panel4);
            this.panelBody.Controls.Add(this.panel2);
            this.panelBody.Controls.Add(this.pictureBox4);
            this.panelBody.Controls.Add(this.panel3);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 90);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1184, 671);
            this.panelBody.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.5F));
            this.tableLayoutPanel3.Controls.Add(this.btnRefineGold, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnGold, 6, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnDayBook, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnCash, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnCredit, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnDebit, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnOthers, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnRecord, 4, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(220, 268);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(749, 247);
            this.tableLayoutPanel3.TabIndex = 16;
            // 
            // btnRefineGold
            // 
            this.btnRefineGold.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRefineGold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefineGold.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRefineGold.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefineGold.Image = global::Presentation.Properties.Resources.Group_82;
            this.btnRefineGold.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRefineGold.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRefineGold.Location = new System.Drawing.Point(573, 3);
            this.btnRefineGold.Name = "btnRefineGold";
            this.btnRefineGold.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnRefineGold.Size = new System.Drawing.Size(173, 107);
            this.btnRefineGold.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRefineGold.TabIndex = 31;
            this.btnRefineGold.Text = "Rfine Gold\r\nخالص سونا";
            this.btnRefineGold.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRefineGold.Click += new System.EventHandler(this.btnRefineGold_Click);
            // 
            // btnGold
            // 
            this.btnGold.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGold.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnGold.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGold.Image = global::Presentation.Properties.Resources.Group_862;
            this.btnGold.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnGold.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGold.Location = new System.Drawing.Point(573, 128);
            this.btnGold.Name = "btnGold";
            this.btnGold.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnGold.Size = new System.Drawing.Size(173, 109);
            this.btnGold.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnGold.TabIndex = 35;
            this.btnGold.Text = "Gold\r\nسونا";
            this.btnGold.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnGold.Click += new System.EventHandler(this.btnGold_Click);
            // 
            // btnDayBook
            // 
            this.btnDayBook.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDayBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDayBook.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDayBook.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDayBook.Image = global::Presentation.Properties.Resources.Group_80;
            this.btnDayBook.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnDayBook.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDayBook.Location = new System.Drawing.Point(3, 3);
            this.btnDayBook.Name = "btnDayBook";
            this.btnDayBook.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnDayBook.Size = new System.Drawing.Size(170, 107);
            this.btnDayBook.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDayBook.TabIndex = 34;
            this.btnDayBook.Text = "Day Book\r\n";
            this.btnDayBook.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnDayBook.Click += new System.EventHandler(this.btnDayBook_Click);
            // 
            // btnCash
            // 
            this.btnCash.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCash.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCash.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCash.Image = global::Presentation.Properties.Resources.Group_85;
            this.btnCash.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCash.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCash.Location = new System.Drawing.Point(383, 128);
            this.btnCash.Name = "btnCash";
            this.btnCash.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnCash.Size = new System.Drawing.Size(170, 109);
            this.btnCash.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCash.TabIndex = 38;
            this.btnCash.Text = "Cash\nرقم";
            this.btnCash.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnCash.Click += new System.EventHandler(this.btnCash_Click);
            // 
            // btnCredit
            // 
            this.btnCredit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCredit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCredit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCredit.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCredit.Image = global::Presentation.Properties.Resources.Group_84;
            this.btnCredit.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnCredit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCredit.Location = new System.Drawing.Point(193, 128);
            this.btnCredit.Name = "btnCredit";
            this.btnCredit.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnCredit.Size = new System.Drawing.Size(170, 109);
            this.btnCredit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCredit.TabIndex = 37;
            this.btnCredit.Text = "Credit\r\nوصولی";
            this.btnCredit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnCredit.Click += new System.EventHandler(this.btnCredit_Click);
            // 
            // btnDebit
            // 
            this.btnDebit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDebit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDebit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDebit.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDebit.Image = global::Presentation.Properties.Resources.Group_83;
            this.btnDebit.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnDebit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDebit.Location = new System.Drawing.Point(3, 128);
            this.btnDebit.Name = "btnDebit";
            this.btnDebit.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnDebit.Size = new System.Drawing.Size(170, 109);
            this.btnDebit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDebit.TabIndex = 36;
            this.btnDebit.Text = "Debit\r\nادا ئیگیی";
            this.btnDebit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnDebit.Click += new System.EventHandler(this.btnDebit_Click);
            // 
            // btnOthers
            // 
            this.btnOthers.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOthers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOthers.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnOthers.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOthers.Image = global::Presentation.Properties.Resources.Group_81;
            this.btnOthers.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnOthers.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOthers.Location = new System.Drawing.Point(193, 3);
            this.btnOthers.Name = "btnOthers";
            this.btnOthers.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnOthers.Size = new System.Drawing.Size(170, 107);
            this.btnOthers.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOthers.TabIndex = 33;
            this.btnOthers.Text = "Other\r\nمتفرق\r\n";
            this.btnOthers.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnOthers.Click += new System.EventHandler(this.btnOthers_Click);
            // 
            // btnRecord
            // 
            this.btnRecord.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRecord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRecord.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnRecord.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecord.Image = global::Presentation.Properties.Resources.Group_79;
            this.btnRecord.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnRecord.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRecord.Location = new System.Drawing.Point(383, 3);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnRecord.Size = new System.Drawing.Size(170, 107);
            this.btnRecord.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRecord.TabIndex = 32;
            this.btnRecord.Text = "Record\r\nریکارڈ";
            this.btnRecord.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.tableLayoutPanel2);
            this.panel4.Location = new System.Drawing.Point(3, 542);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1181, 126);
            this.panel4.TabIndex = 15;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.ColumnCount = 11;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.41617F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.5006F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.41617F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.5006F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.41617F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.5006F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.41617F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.5006F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.41617F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.5006F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.41617F));
            this.tableLayoutPanel2.Controls.Add(this.btnClientList, 10, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnJewellery, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSetting, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnRustedGold, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnLaboratory, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCasting, 4, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(79, 7);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1028, 107);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // btnClientList
            // 
            this.btnClientList.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnClientList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClientList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnClientList.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnClientList.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClientList.Image = global::Presentation.Properties.Resources._110_1102927_create_your_profile_user_icon_white_color_hd;
            this.btnClientList.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnClientList.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnClientList.Location = new System.Drawing.Point(868, 3);
            this.btnClientList.Name = "btnClientList";
            this.btnClientList.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnClientList.Size = new System.Drawing.Size(157, 101);
            this.btnClientList.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnClientList.TabIndex = 33;
            this.btnClientList.Text = "Client List";
            this.btnClientList.TextColor = System.Drawing.Color.White;
            this.btnClientList.Click += new System.EventHandler(this.btnClientList_Click);
            // 
            // btnJewellery
            // 
            this.btnJewellery.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnJewellery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJewellery.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnJewellery.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnJewellery.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJewellery.Image = global::Presentation.Properties.Resources.goldRing;
            this.btnJewellery.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnJewellery.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnJewellery.Location = new System.Drawing.Point(3, 3);
            this.btnJewellery.Name = "btnJewellery";
            this.btnJewellery.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnJewellery.Size = new System.Drawing.Size(152, 101);
            this.btnJewellery.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnJewellery.TabIndex = 31;
            this.btnJewellery.Text = "Jewellery\r\nزیورات";
            this.btnJewellery.TextColor = System.Drawing.Color.White;
            this.btnJewellery.Click += new System.EventHandler(this.btnJewellery_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnSetting.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnSetting.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetting.Image = global::Presentation.Properties.Resources.setting1;
            this.btnSetting.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnSetting.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSetting.Location = new System.Drawing.Point(695, 3);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnSetting.Size = new System.Drawing.Size(152, 101);
            this.btnSetting.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSetting.TabIndex = 32;
            this.btnSetting.Text = "Setting\r\nترتیب";
            this.btnSetting.TextColor = System.Drawing.Color.White;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnRustedGold
            // 
            this.btnRustedGold.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnRustedGold.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRustedGold.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnRustedGold.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnRustedGold.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRustedGold.Image = global::Presentation.Properties.Resources.noun_gold_coin_3604981;
            this.btnRustedGold.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnRustedGold.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRustedGold.Location = new System.Drawing.Point(522, 3);
            this.btnRustedGold.Name = "btnRustedGold";
            this.btnRustedGold.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnRustedGold.Size = new System.Drawing.Size(152, 101);
            this.btnRustedGold.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnRustedGold.TabIndex = 28;
            this.btnRustedGold.Text = "Rusted Gold\r\n\rکچا/سونا";
            this.btnRustedGold.TextColor = System.Drawing.Color.White;
            this.btnRustedGold.Click += new System.EventHandler(this.btnRustedGold_Click);
            // 
            // btnLaboratory
            // 
            this.btnLaboratory.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnLaboratory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLaboratory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnLaboratory.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnLaboratory.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLaboratory.Image = global::Presentation.Properties.Resources._2699438;
            this.btnLaboratory.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnLaboratory.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLaboratory.Location = new System.Drawing.Point(176, 3);
            this.btnLaboratory.Name = "btnLaboratory";
            this.btnLaboratory.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnLaboratory.Size = new System.Drawing.Size(152, 101);
            this.btnLaboratory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnLaboratory.TabIndex = 29;
            this.btnLaboratory.Text = "Laboratory\r\nلیبارٹری";
            this.btnLaboratory.TextColor = System.Drawing.Color.White;
            this.btnLaboratory.Click += new System.EventHandler(this.btnLaboratory_Click);
            // 
            // btnCasting
            // 
            this.btnCasting.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCasting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCasting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnCasting.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnCasting.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCasting.Image = global::Presentation.Properties.Resources._380565_200;
            this.btnCasting.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.btnCasting.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCasting.Location = new System.Drawing.Point(349, 3);
            this.btnCasting.Name = "btnCasting";
            this.btnCasting.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnCasting.Size = new System.Drawing.Size(152, 101);
            this.btnCasting.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCasting.TabIndex = 30;
            this.btnCasting.Text = "Casting\r\nکاسٹ";
            this.btnCasting.TextColor = System.Drawing.Color.White;
            this.btnCasting.Click += new System.EventHandler(this.btnCasting_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.pbTr3);
            this.panel2.Controls.Add(this.lblTr3);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.pictureBox26);
            this.panel2.Controls.Add(this.pbTr2);
            this.panel2.Controls.Add(this.lblTr2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.pictureBox24);
            this.panel2.Controls.Add(this.pbTr1);
            this.panel2.Controls.Add(this.lblTr1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox21);
            this.panel2.Controls.Add(this.lblViewAll);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.pictureBox20);
            this.panel2.Location = new System.Drawing.Point(405, 70);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(713, 192);
            this.panel2.TabIndex = 14;
            // 
            // pbTr3
            // 
            this.pbTr3.Image = global::Presentation.Properties.Resources.arrow;
            this.pbTr3.Location = new System.Drawing.Point(585, 90);
            this.pbTr3.Name = "pbTr3";
            this.pbTr3.Size = new System.Drawing.Size(21, 19);
            this.pbTr3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTr3.TabIndex = 20;
            this.pbTr3.TabStop = false;
            // 
            // lblTr3
            // 
            this.lblTr3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTr3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTr3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTr3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTr3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblTr3.Location = new System.Drawing.Point(479, 94);
            this.lblTr3.Name = "lblTr3";
            this.lblTr3.Size = new System.Drawing.Size(100, 34);
            this.lblTr3.TabIndex = 19;
            this.lblTr3.Text = "9134";
            this.lblTr3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.label5.Location = new System.Drawing.Point(472, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 30);
            this.label5.TabIndex = 18;
            this.label5.Text = "Details Transaction 3";
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox26.Image = global::Presentation.Properties.Resources.Rectangle_23;
            this.pictureBox26.Location = new System.Drawing.Point(462, 59);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(199, 114);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 17;
            this.pictureBox26.TabStop = false;
            // 
            // pbTr2
            // 
            this.pbTr2.Image = global::Presentation.Properties.Resources.down_arrow;
            this.pbTr2.Location = new System.Drawing.Point(369, 90);
            this.pbTr2.Name = "pbTr2";
            this.pbTr2.Size = new System.Drawing.Size(21, 19);
            this.pbTr2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTr2.TabIndex = 16;
            this.pbTr2.TabStop = false;
            // 
            // lblTr2
            // 
            this.lblTr2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTr2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTr2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTr2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTr2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblTr2.Location = new System.Drawing.Point(263, 92);
            this.lblTr2.Name = "lblTr2";
            this.lblTr2.Size = new System.Drawing.Size(100, 34);
            this.lblTr2.TabIndex = 15;
            this.lblTr2.Text = "1139";
            this.lblTr2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.label4.Location = new System.Drawing.Point(256, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 30);
            this.label4.TabIndex = 14;
            this.label4.Text = "Details Transaction 2";
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox24.Image = global::Presentation.Properties.Resources.Rectangle_23;
            this.pictureBox24.Location = new System.Drawing.Point(246, 57);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(199, 114);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 13;
            this.pictureBox24.TabStop = false;
            // 
            // pbTr1
            // 
            this.pbTr1.Image = global::Presentation.Properties.Resources.arrow;
            this.pbTr1.Location = new System.Drawing.Point(152, 90);
            this.pbTr1.Name = "pbTr1";
            this.pbTr1.Size = new System.Drawing.Size(21, 19);
            this.pbTr1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTr1.TabIndex = 12;
            this.pbTr1.TabStop = false;
            // 
            // lblTr1
            // 
            this.lblTr1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTr1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTr1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTr1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTr1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblTr1.Location = new System.Drawing.Point(46, 92);
            this.lblTr1.Name = "lblTr1";
            this.lblTr1.Size = new System.Drawing.Size(100, 34);
            this.lblTr1.TabIndex = 11;
            this.lblTr1.Text = "2134";
            this.lblTr1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.label3.Location = new System.Drawing.Point(39, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 30);
            this.label3.TabIndex = 6;
            this.label3.Text = "Details Transaction 1";
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox21.Image = global::Presentation.Properties.Resources.Rectangle_23;
            this.pictureBox21.Location = new System.Drawing.Point(29, 57);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(199, 114);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 3;
            this.pictureBox21.TabStop = false;
            // 
            // lblViewAll
            // 
            this.lblViewAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.lblViewAll.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViewAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblViewAll.Location = new System.Drawing.Point(589, 15);
            this.lblViewAll.Name = "lblViewAll";
            this.lblViewAll.Size = new System.Drawing.Size(90, 30);
            this.lblViewAll.TabIndex = 2;
            this.lblViewAll.Text = "view all";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.label1.Location = new System.Drawing.Point(24, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Last Three Records";
            // 
            // pictureBox20
            // 
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox20.Image = global::Presentation.Properties.Resources.Rectangle_20;
            this.pictureBox20.Location = new System.Drawing.Point(0, 0);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(713, 56);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 0;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::Presentation.Properties.Resources.Rectangle_19;
            this.pictureBox4.Location = new System.Drawing.Point(73, 250);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1045, 291);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.lblPAK);
            this.panel3.Controls.Add(this.labelX11);
            this.panel3.Controls.Add(this.labelX3);
            this.panel3.Controls.Add(this.pictureBox14);
            this.panel3.Controls.Add(this.labelX12);
            this.panel3.Controls.Add(this.lblUK);
            this.panel3.Controls.Add(this.labelX8);
            this.panel3.Controls.Add(this.pictureBox13);
            this.panel3.Controls.Add(this.labelX9);
            this.panel3.Controls.Add(this.lblUSA);
            this.panel3.Controls.Add(this.labelX5);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.labelX4);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(405, 259);
            this.panel3.TabIndex = 10;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox2.Image = global::Presentation.Properties.Resources.rs;
            this.pictureBox2.Location = new System.Drawing.Point(65, 18);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(55, 44);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // lblPAK
            // 
            this.lblPAK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPAK.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblPAK.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPAK.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPAK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblPAK.Location = new System.Drawing.Point(131, 194);
            this.lblPAK.Name = "lblPAK";
            this.lblPAK.Size = new System.Drawing.Size(186, 34);
            this.lblPAK.TabIndex = 18;
            this.lblPAK.Text = "Rs 85586.956";
            this.lblPAK.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX11
            // 
            this.labelX11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX11.Location = new System.Drawing.Point(70, 198);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(55, 30);
            this.labelX11.TabIndex = 17;
            this.labelX11.Text = "PAK";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX3.Location = new System.Drawing.Point(124, 18);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(193, 44);
            this.labelX3.TabIndex = 20;
            this.labelX3.Text = "EXCHANGE RATE";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox14.Image = global::Presentation.Properties.Resources.Rectangle_3;
            this.pictureBox14.Location = new System.Drawing.Point(48, 188);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(285, 57);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 15;
            this.pictureBox14.TabStop = false;
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX12.Location = new System.Drawing.Point(102, 198);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(75, 24);
            this.labelX12.TabIndex = 16;
            this.labelX12.Text = "USA";
            // 
            // lblUK
            // 
            this.lblUK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUK.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblUK.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblUK.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblUK.Location = new System.Drawing.Point(131, 144);
            this.lblUK.Name = "lblUK";
            this.lblUK.Size = new System.Drawing.Size(186, 34);
            this.lblUK.TabIndex = 14;
            this.lblUK.Text = "$ 85586.956";
            this.lblUK.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX8.Location = new System.Drawing.Point(70, 147);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(55, 30);
            this.labelX8.TabIndex = 13;
            this.labelX8.Text = "UK";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox13.Image = global::Presentation.Properties.Resources.Rectangle_3;
            this.pictureBox13.Location = new System.Drawing.Point(48, 138);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(285, 57);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 11;
            this.pictureBox13.TabStop = false;
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX9.Location = new System.Drawing.Point(102, 148);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(75, 24);
            this.labelX9.TabIndex = 12;
            this.labelX9.Text = "USA";
            // 
            // lblUSA
            // 
            this.lblUSA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUSA.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblUSA.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblUSA.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUSA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.lblUSA.Location = new System.Drawing.Point(131, 90);
            this.lblUSA.Name = "lblUSA";
            this.lblUSA.Size = new System.Drawing.Size(186, 34);
            this.lblUSA.TabIndex = 10;
            this.lblUSA.Text = "$ 85586.956";
            this.lblUSA.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX5.Location = new System.Drawing.Point(70, 90);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(55, 34);
            this.labelX5.TabIndex = 9;
            this.labelX5.Text = "USA";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox3.Image = global::Presentation.Properties.Resources.Rectangle_3;
            this.pictureBox3.Location = new System.Drawing.Point(48, 84);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(285, 57);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX4.Location = new System.Drawing.Point(102, 94);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 24);
            this.labelX4.TabIndex = 8;
            this.labelX4.Text = "USA";
            // 
            // DashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 761);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(1200, 800);
            this.Name = "DashboardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.DashboardForm_Load);
            this.panelTop.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelBody.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbTr3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX lblTodayOpBlnc;
        private DevComponents.DotNetBar.LabelX lblCustName;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel3;
        private DevComponents.DotNetBar.LabelX lblPAK;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.PictureBox pictureBox14;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX lblUK;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.PictureBox pictureBox13;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX lblUSA;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pbTr1;
        private DevComponents.DotNetBar.LabelX lblTr1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Label lblViewAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbTr3;
        private DevComponents.DotNetBar.LabelX lblTr3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pbTr2;
        private DevComponents.DotNetBar.LabelX lblTr2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevComponents.DotNetBar.ButtonX btnRefineGold;
        private DevComponents.DotNetBar.ButtonX btnGold;
        private DevComponents.DotNetBar.ButtonX btnDayBook;
        private DevComponents.DotNetBar.ButtonX btnCash;
        private DevComponents.DotNetBar.ButtonX btnCredit;
        private DevComponents.DotNetBar.ButtonX btnDebit;
        private DevComponents.DotNetBar.ButtonX btnOthers;
        private DevComponents.DotNetBar.ButtonX btnRecord;
        private DevComponents.DotNetBar.ButtonX btnJewellery;
        private DevComponents.DotNetBar.ButtonX btnSetting;
        private DevComponents.DotNetBar.ButtonX btnRustedGold;
        private DevComponents.DotNetBar.ButtonX btnLaboratory;
        private DevComponents.DotNetBar.ButtonX btnCasting;
        private DevComponents.DotNetBar.LabelX lblTodayGoldRate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.LinkLabel linkAddGoldRate;
        private DevComponents.DotNetBar.ButtonX btnClientList;
    }
}