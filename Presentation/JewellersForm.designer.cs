﻿namespace Presentation
{
    partial class JewellersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelFirstTopParent = new System.Windows.Forms.Panel();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtCurrentTime = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.txtCurrentDate = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX14 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX15 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX16 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX17 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX18 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX19 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX20 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX21 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.Textboxpymnt = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX23 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TxtCutFrmTotlWghtTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCurRatePrTolaRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPriceofGoldRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtLabourPerTolaRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlLaburRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlAmuntRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPymtPercntCut = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtNetAmuntRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutPrGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCutFrmTotlWghtGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtPolishPrGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtTotlPolishofGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtFinlWghtGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCurRatePrMashaRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCurRatePrRattiRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtCurRatePrGramsRs = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.itemPanelForButtonsandRadiobtns = new DevComponents.DotNetBar.ItemPanel();
            this.BtnExit = new DevComponents.DotNetBar.ButtonX();
            this.BtnSetting = new DevComponents.DotNetBar.ButtonX();
            this.BtnImages = new DevComponents.DotNetBar.ButtonX();
            this.BtnPlayer = new DevComponents.DotNetBar.ButtonX();
            this.BtnPad = new DevComponents.DotNetBar.ButtonX();
            this.BtnGoldCalculator = new DevComponents.DotNetBar.ButtonX();
            this.BtnAccounts = new DevComponents.DotNetBar.ButtonX();
            this.BtnReceipt = new DevComponents.DotNetBar.ButtonX();
            this.BtnKarat = new DevComponents.DotNetBar.ButtonX();
            this.BtnTransaction = new DevComponents.DotNetBar.ButtonX();
            this.BtnClearData = new DevComponents.DotNetBar.ButtonX();
            this.BtnCalculator = new DevComponents.DotNetBar.ButtonX();
            this.groupPanelcutPriority = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.RdbTotlCutFirst = new System.Windows.Forms.RadioButton();
            this.RdbCutPrTolaFirst = new System.Windows.Forms.RadioButton();
            this.groupPanelLabourPolisCalcu = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.RdbFinalWeight = new System.Windows.Forms.RadioButton();
            this.RdbTopWeight = new System.Windows.Forms.RadioButton();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.RdbPakaTola = new System.Windows.Forms.RadioButton();
            this.RdbKachTola = new System.Windows.Forms.RadioButton();
            this.groupPanelWeightinMode = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.TxtWeightTola = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.textBoxX6 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightMasha = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightRatti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TxtWeightGrams = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.panelSecondTopParent = new System.Windows.Forms.Panel();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelFirstTopParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            this.itemPanelForButtonsandRadiobtns.SuspendLayout();
            this.groupPanelcutPriority.SuspendLayout();
            this.groupPanelLabourPolisCalcu.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.groupPanelWeightinMode.SuspendLayout();
            this.panelSecondTopParent.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFirstTopParent
            // 
            this.panelFirstTopParent.Controls.Add(this.pictureBox17);
            this.panelFirstTopParent.Controls.Add(this.labelX2);
            this.panelFirstTopParent.Controls.Add(this.txtCurrentTime);
            this.panelFirstTopParent.Controls.Add(this.groupBox2);
            this.panelFirstTopParent.Controls.Add(this.txtCurrentDate);
            this.panelFirstTopParent.Location = new System.Drawing.Point(14, 13);
            this.panelFirstTopParent.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.panelFirstTopParent.Name = "panelFirstTopParent";
            this.panelFirstTopParent.Size = new System.Drawing.Size(1184, 83);
            this.panelFirstTopParent.TabIndex = 1;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Presentation.Properties.Resources.Group_86;
            this.pictureBox17.Location = new System.Drawing.Point(1153, 17);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(21, 35);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox17.TabIndex = 61;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX2.Location = new System.Drawing.Point(566, 4);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(308, 51);
            this.labelX2.TabIndex = 7;
            this.labelX2.Text = "Calculate Transaction";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtCurrentTime
            // 
            this.txtCurrentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.txtCurrentTime.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCurrentTime.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentTime.ForeColor = System.Drawing.Color.White;
            this.txtCurrentTime.Location = new System.Drawing.Point(873, 14);
            this.txtCurrentTime.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtCurrentTime.Name = "txtCurrentTime";
            this.txtCurrentTime.ReadOnly = true;
            this.txtCurrentTime.Size = new System.Drawing.Size(253, 42);
            this.txtCurrentTime.TabIndex = 6;
            this.txtCurrentTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonX2);
            this.groupBox2.Controls.Add(this.buttonX3);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.groupBox2.Location = new System.Drawing.Point(15, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox2.Size = new System.Drawing.Size(281, 76);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX2.Image = global::Presentation.Properties.Resources.WeightSacle_png;
            this.buttonX2.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.buttonX2.Location = new System.Drawing.Point(49, 21);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Size = new System.Drawing.Size(105, 49);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX2.TabIndex = 5;
            this.buttonX2.Text = "Scale";
            this.buttonX2.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonX3.Image = global::Presentation.Properties.Resources.INFOBOOK_JPG;
            this.buttonX3.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.buttonX3.Location = new System.Drawing.Point(160, 21);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Size = new System.Drawing.Size(115, 49);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP;
            this.buttonX3.TabIndex = 4;
            this.buttonX3.Text = "Manual";
            this.buttonX3.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // txtCurrentDate
            // 
            this.txtCurrentDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.txtCurrentDate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtCurrentDate.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentDate.ForeColor = System.Drawing.Color.White;
            this.txtCurrentDate.Location = new System.Drawing.Point(309, 17);
            this.txtCurrentDate.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtCurrentDate.Name = "txtCurrentDate";
            this.txtCurrentDate.Size = new System.Drawing.Size(253, 42);
            this.txtCurrentDate.TabIndex = 3;
            this.txtCurrentDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxX1
            // 
            this.textBoxX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX1.ForeColor = System.Drawing.Color.White;
            this.textBoxX1.Location = new System.Drawing.Point(12, 244);
            this.textBoxX1.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.ReadOnly = true;
            this.textBoxX1.Size = new System.Drawing.Size(291, 39);
            this.textBoxX1.TabIndex = 0;
            this.textBoxX1.Text = "1. Cut Per Tola";
            // 
            // textBoxX3
            // 
            this.textBoxX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX3.Border.Class = "TextBoxBorder";
            this.textBoxX3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX3.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX3.ForeColor = System.Drawing.Color.White;
            this.textBoxX3.Location = new System.Drawing.Point(12, 181);
            this.textBoxX3.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX3.Name = "textBoxX3";
            this.textBoxX3.ReadOnly = true;
            this.textBoxX3.Size = new System.Drawing.Size(291, 39);
            this.textBoxX3.TabIndex = 1;
            this.textBoxX3.Text = "2. Cut Frm Total Weight";
            // 
            // textBoxX14
            // 
            this.textBoxX14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX14.Border.Class = "TextBoxBorder";
            this.textBoxX14.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX14.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX14.ForeColor = System.Drawing.Color.White;
            this.textBoxX14.Location = new System.Drawing.Point(12, 283);
            this.textBoxX14.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX14.Name = "textBoxX14";
            this.textBoxX14.ReadOnly = true;
            this.textBoxX14.Size = new System.Drawing.Size(291, 39);
            this.textBoxX14.TabIndex = 3;
            this.textBoxX14.Text = "3. Polish Per Tola";
            // 
            // textBoxX15
            // 
            this.textBoxX15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX15.Border.Class = "TextBoxBorder";
            this.textBoxX15.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX15.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX15.ForeColor = System.Drawing.Color.White;
            this.textBoxX15.Location = new System.Drawing.Point(12, 322);
            this.textBoxX15.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX15.Name = "textBoxX15";
            this.textBoxX15.ReadOnly = true;
            this.textBoxX15.Size = new System.Drawing.Size(291, 39);
            this.textBoxX15.TabIndex = 10;
            this.textBoxX15.Text = "4. Total Polish";
            // 
            // textBoxX16
            // 
            this.textBoxX16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX16.Border.Class = "TextBoxBorder";
            this.textBoxX16.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX16.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX16.ForeColor = System.Drawing.Color.Red;
            this.textBoxX16.Location = new System.Drawing.Point(12, 362);
            this.textBoxX16.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX16.Name = "textBoxX16";
            this.textBoxX16.ReadOnly = true;
            this.textBoxX16.Size = new System.Drawing.Size(291, 44);
            this.textBoxX16.TabIndex = 11;
            this.textBoxX16.Text = "Final Weight";
            // 
            // textBoxX17
            // 
            this.textBoxX17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX17.Border.Class = "TextBoxBorder";
            this.textBoxX17.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX17.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX17.ForeColor = System.Drawing.Color.White;
            this.textBoxX17.Location = new System.Drawing.Point(12, 404);
            this.textBoxX17.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX17.Name = "textBoxX17";
            this.textBoxX17.ReadOnly = true;
            this.textBoxX17.Size = new System.Drawing.Size(291, 39);
            this.textBoxX17.TabIndex = 12;
            this.textBoxX17.Text = "5. Current Rate Per Tola Rs.";
            // 
            // textBoxX18
            // 
            this.textBoxX18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX18.Border.Class = "TextBoxBorder";
            this.textBoxX18.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX18.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX18.ForeColor = System.Drawing.Color.Red;
            this.textBoxX18.Location = new System.Drawing.Point(12, 444);
            this.textBoxX18.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX18.Name = "textBoxX18";
            this.textBoxX18.ReadOnly = true;
            this.textBoxX18.Size = new System.Drawing.Size(291, 44);
            this.textBoxX18.TabIndex = 13;
            this.textBoxX18.Text = "Price of Gold               Rs.";
            // 
            // textBoxX19
            // 
            this.textBoxX19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX19.Border.Class = "TextBoxBorder";
            this.textBoxX19.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX19.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX19.ForeColor = System.Drawing.Color.White;
            this.textBoxX19.Location = new System.Drawing.Point(12, 487);
            this.textBoxX19.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX19.Name = "textBoxX19";
            this.textBoxX19.ReadOnly = true;
            this.textBoxX19.Size = new System.Drawing.Size(291, 39);
            this.textBoxX19.TabIndex = 14;
            this.textBoxX19.Text = "6. Labour Per Tola            Rs.";
            // 
            // textBoxX20
            // 
            this.textBoxX20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX20.Border.Class = "TextBoxBorder";
            this.textBoxX20.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX20.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX20.ForeColor = System.Drawing.Color.White;
            this.textBoxX20.Location = new System.Drawing.Point(12, 526);
            this.textBoxX20.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX20.Name = "textBoxX20";
            this.textBoxX20.ReadOnly = true;
            this.textBoxX20.Size = new System.Drawing.Size(291, 39);
            this.textBoxX20.TabIndex = 15;
            this.textBoxX20.Text = "7. Total Labour                  Rs.";
            // 
            // textBoxX21
            // 
            this.textBoxX21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX21.Border.Class = "TextBoxBorder";
            this.textBoxX21.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX21.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX21.ForeColor = System.Drawing.Color.Red;
            this.textBoxX21.Location = new System.Drawing.Point(12, 565);
            this.textBoxX21.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX21.Name = "textBoxX21";
            this.textBoxX21.ReadOnly = true;
            this.textBoxX21.Size = new System.Drawing.Size(291, 44);
            this.textBoxX21.TabIndex = 16;
            this.textBoxX21.Text = "Total Amount             Rs.";
            // 
            // Textboxpymnt
            // 
            this.Textboxpymnt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.Textboxpymnt.Border.Class = "TextBoxBorder";
            this.Textboxpymnt.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Textboxpymnt.Font = new System.Drawing.Font("Segoe UI Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Textboxpymnt.ForeColor = System.Drawing.Color.White;
            this.Textboxpymnt.Location = new System.Drawing.Point(12, 608);
            this.Textboxpymnt.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.Textboxpymnt.Name = "Textboxpymnt";
            this.Textboxpymnt.ReadOnly = true;
            this.Textboxpymnt.Size = new System.Drawing.Size(291, 39);
            this.Textboxpymnt.TabIndex = 17;
            this.Textboxpymnt.Text = "8. Payment/Percent Cut";
            // 
            // textBoxX23
            // 
            this.textBoxX23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX23.Border.Class = "TextBoxBorder";
            this.textBoxX23.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX23.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX23.ForeColor = System.Drawing.Color.Red;
            this.textBoxX23.Location = new System.Drawing.Point(12, 647);
            this.textBoxX23.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX23.Name = "textBoxX23";
            this.textBoxX23.ReadOnly = true;
            this.textBoxX23.Size = new System.Drawing.Size(291, 44);
            this.textBoxX23.TabIndex = 18;
            this.textBoxX23.Text = "Net Amount              Rs.";
            // 
            // TxtCutPrTola
            // 
            this.TxtCutPrTola.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutPrTola.Border.Class = "TextBoxBorder";
            this.TxtCutPrTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Cut_Per_Tola", true));
            this.TxtCutPrTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrTola.ForeColor = System.Drawing.Color.SlateGray;
            this.TxtCutPrTola.Location = new System.Drawing.Point(303, 244);
            this.TxtCutPrTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutPrTola.Name = "TxtCutPrTola";
            this.TxtCutPrTola.Size = new System.Drawing.Size(225, 39);
            this.TxtCutPrTola.TabIndex = 0;
            this.TxtCutPrTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // TxtCutFrmTotlWghtTola
            // 
            this.TxtCutFrmTotlWghtTola.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtTola.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Cut_from_total_Weight", true));
            this.TxtCutFrmTotlWghtTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtTola.ForeColor = System.Drawing.Color.SlateGray;
            this.TxtCutFrmTotlWghtTola.Location = new System.Drawing.Point(302, 182);
            this.TxtCutFrmTotlWghtTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutFrmTotlWghtTola.Name = "TxtCutFrmTotlWghtTola";
            this.TxtCutFrmTotlWghtTola.Size = new System.Drawing.Size(225, 39);
            this.TxtCutFrmTotlWghtTola.TabIndex = 1;
            this.TxtCutFrmTotlWghtTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPolishPrTola
            // 
            this.TxtPolishPrTola.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtPolishPrTola.Border.Class = "TextBoxBorder";
            this.TxtPolishPrTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Polish_Per_Tola", true));
            this.TxtPolishPrTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrTola.ForeColor = System.Drawing.Color.Blue;
            this.TxtPolishPrTola.Location = new System.Drawing.Point(303, 283);
            this.TxtPolishPrTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtPolishPrTola.Name = "TxtPolishPrTola";
            this.TxtPolishPrTola.Size = new System.Drawing.Size(225, 39);
            this.TxtPolishPrTola.TabIndex = 2;
            this.TxtPolishPrTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotlPolishofTola
            // 
            this.TxtTotlPolishofTola.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtTotlPolishofTola.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofTola.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Total_Polish", true));
            this.TxtTotlPolishofTola.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofTola.ForeColor = System.Drawing.Color.Blue;
            this.TxtTotlPolishofTola.Location = new System.Drawing.Point(303, 322);
            this.TxtTotlPolishofTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtTotlPolishofTola.Name = "TxtTotlPolishofTola";
            this.TxtTotlPolishofTola.Size = new System.Drawing.Size(225, 39);
            this.TxtTotlPolishofTola.TabIndex = 3;
            this.TxtTotlPolishofTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtFinlWghtTola
            // 
            this.TxtFinlWghtTola.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtFinlWghtTola.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtTola.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtTola.ForeColor = System.Drawing.Color.Red;
            this.TxtFinlWghtTola.Location = new System.Drawing.Point(303, 362);
            this.TxtFinlWghtTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtFinlWghtTola.Name = "TxtFinlWghtTola";
            this.TxtFinlWghtTola.Size = new System.Drawing.Size(225, 44);
            this.TxtFinlWghtTola.TabIndex = 4;
            this.TxtFinlWghtTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCurRatePrTolaRs
            // 
            this.TxtCurRatePrTolaRs.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCurRatePrTolaRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrTolaRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrTolaRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Current_Rate_Per_Tola", true));
            this.TxtCurRatePrTolaRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrTolaRs.ForeColor = System.Drawing.Color.Blue;
            this.TxtCurRatePrTolaRs.Location = new System.Drawing.Point(303, 404);
            this.TxtCurRatePrTolaRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCurRatePrTolaRs.Name = "TxtCurRatePrTolaRs";
            this.TxtCurRatePrTolaRs.Size = new System.Drawing.Size(225, 39);
            this.TxtCurRatePrTolaRs.TabIndex = 5;
            this.TxtCurRatePrTolaRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPriceofGoldRs
            // 
            this.TxtPriceofGoldRs.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtPriceofGoldRs.Border.Class = "TextBoxBorder";
            this.TxtPriceofGoldRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPriceofGoldRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Price_of_Gold", true));
            this.TxtPriceofGoldRs.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceofGoldRs.ForeColor = System.Drawing.Color.Red;
            this.TxtPriceofGoldRs.Location = new System.Drawing.Point(303, 444);
            this.TxtPriceofGoldRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtPriceofGoldRs.Name = "TxtPriceofGoldRs";
            this.TxtPriceofGoldRs.Size = new System.Drawing.Size(225, 44);
            this.TxtPriceofGoldRs.TabIndex = 6;
            this.TxtPriceofGoldRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtLabourPerTolaRs
            // 
            this.TxtLabourPerTolaRs.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtLabourPerTolaRs.Border.Class = "TextBoxBorder";
            this.TxtLabourPerTolaRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtLabourPerTolaRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Labour_Per_Tola", true));
            this.TxtLabourPerTolaRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabourPerTolaRs.ForeColor = System.Drawing.Color.Blue;
            this.TxtLabourPerTolaRs.Location = new System.Drawing.Point(303, 487);
            this.TxtLabourPerTolaRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtLabourPerTolaRs.Name = "TxtLabourPerTolaRs";
            this.TxtLabourPerTolaRs.Size = new System.Drawing.Size(225, 39);
            this.TxtLabourPerTolaRs.TabIndex = 7;
            this.TxtLabourPerTolaRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotlLaburRs
            // 
            this.TxtTotlLaburRs.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtTotlLaburRs.Border.Class = "TextBoxBorder";
            this.TxtTotlLaburRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlLaburRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Total_Labour", true));
            this.TxtTotlLaburRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlLaburRs.ForeColor = System.Drawing.Color.Blue;
            this.TxtTotlLaburRs.Location = new System.Drawing.Point(303, 526);
            this.TxtTotlLaburRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtTotlLaburRs.Name = "TxtTotlLaburRs";
            this.TxtTotlLaburRs.Size = new System.Drawing.Size(225, 39);
            this.TxtTotlLaburRs.TabIndex = 8;
            this.TxtTotlLaburRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotlAmuntRs
            // 
            this.TxtTotlAmuntRs.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtTotlAmuntRs.Border.Class = "TextBoxBorder";
            this.TxtTotlAmuntRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlAmuntRs.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlAmuntRs.ForeColor = System.Drawing.Color.Red;
            this.TxtTotlAmuntRs.Location = new System.Drawing.Point(303, 565);
            this.TxtTotlAmuntRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtTotlAmuntRs.Name = "TxtTotlAmuntRs";
            this.TxtTotlAmuntRs.Size = new System.Drawing.Size(225, 44);
            this.TxtTotlAmuntRs.TabIndex = 9;
            this.TxtTotlAmuntRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPymtPercntCut
            // 
            this.TxtPymtPercntCut.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtPymtPercntCut.Border.Class = "TextBoxBorder";
            this.TxtPymtPercntCut.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPymtPercntCut.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Payment_Percnt_Cut", true));
            this.TxtPymtPercntCut.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPymtPercntCut.ForeColor = System.Drawing.Color.Blue;
            this.TxtPymtPercntCut.Location = new System.Drawing.Point(303, 608);
            this.TxtPymtPercntCut.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtPymtPercntCut.Name = "TxtPymtPercntCut";
            this.TxtPymtPercntCut.Size = new System.Drawing.Size(225, 39);
            this.TxtPymtPercntCut.TabIndex = 10;
            this.TxtPymtPercntCut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtNetAmuntRs
            // 
            this.TxtNetAmuntRs.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtNetAmuntRs.Border.Class = "TextBoxBorder";
            this.TxtNetAmuntRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtNetAmuntRs.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Net_Amount", true));
            this.TxtNetAmuntRs.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetAmuntRs.ForeColor = System.Drawing.Color.Red;
            this.TxtNetAmuntRs.Location = new System.Drawing.Point(303, 647);
            this.TxtNetAmuntRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtNetAmuntRs.Name = "TxtNetAmuntRs";
            this.TxtNetAmuntRs.Size = new System.Drawing.Size(225, 44);
            this.TxtNetAmuntRs.TabIndex = 11;
            this.TxtNetAmuntRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCutPrMasha
            // 
            this.TxtCutPrMasha.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutPrMasha.Border.Class = "TextBoxBorder";
            this.TxtCutPrMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrMasha.ForeColor = System.Drawing.Color.SlateGray;
            this.TxtCutPrMasha.Location = new System.Drawing.Point(527, 244);
            this.TxtCutPrMasha.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutPrMasha.Name = "TxtCutPrMasha";
            this.TxtCutPrMasha.ReadOnly = true;
            this.TxtCutPrMasha.Size = new System.Drawing.Size(225, 39);
            this.TxtCutPrMasha.TabIndex = 31;
            this.TxtCutPrMasha.Text = " ";
            this.TxtCutPrMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCutPrMasha.TextChanged += new System.EventHandler(this.TxtCutPrMasha_TextChanged);
            // 
            // TxtCutFrmTotlWghtMasha
            // 
            this.TxtCutFrmTotlWghtMasha.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtMasha.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtMasha.ForeColor = System.Drawing.Color.SlateGray;
            this.TxtCutFrmTotlWghtMasha.Location = new System.Drawing.Point(527, 182);
            this.TxtCutFrmTotlWghtMasha.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutFrmTotlWghtMasha.Name = "TxtCutFrmTotlWghtMasha";
            this.TxtCutFrmTotlWghtMasha.ReadOnly = true;
            this.TxtCutFrmTotlWghtMasha.Size = new System.Drawing.Size(226, 39);
            this.TxtCutFrmTotlWghtMasha.TabIndex = 32;
            this.TxtCutFrmTotlWghtMasha.Text = "0";
            this.TxtCutFrmTotlWghtMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPolishPrMasha
            // 
            this.TxtPolishPrMasha.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtPolishPrMasha.Border.Class = "TextBoxBorder";
            this.TxtPolishPrMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrMasha.ForeColor = System.Drawing.Color.Blue;
            this.TxtPolishPrMasha.Location = new System.Drawing.Point(527, 283);
            this.TxtPolishPrMasha.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtPolishPrMasha.Name = "TxtPolishPrMasha";
            this.TxtPolishPrMasha.Size = new System.Drawing.Size(225, 39);
            this.TxtPolishPrMasha.TabIndex = 34;
            this.TxtPolishPrMasha.Text = "0";
            this.TxtPolishPrMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCutPrRatti
            // 
            this.TxtCutPrRatti.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutPrRatti.Border.Class = "TextBoxBorder";
            this.TxtCutPrRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrRatti.ForeColor = System.Drawing.Color.Blue;
            this.TxtCutPrRatti.Location = new System.Drawing.Point(752, 244);
            this.TxtCutPrRatti.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutPrRatti.Name = "TxtCutPrRatti";
            this.TxtCutPrRatti.Size = new System.Drawing.Size(225, 39);
            this.TxtCutPrRatti.TabIndex = 35;
            this.TxtCutPrRatti.Text = "0";
            this.TxtCutPrRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCutFrmTotlWghtRatti
            // 
            this.TxtCutFrmTotlWghtRatti.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtRatti.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtRatti.ForeColor = System.Drawing.Color.SlateGray;
            this.TxtCutFrmTotlWghtRatti.Location = new System.Drawing.Point(753, 182);
            this.TxtCutFrmTotlWghtRatti.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutFrmTotlWghtRatti.Name = "TxtCutFrmTotlWghtRatti";
            this.TxtCutFrmTotlWghtRatti.ReadOnly = true;
            this.TxtCutFrmTotlWghtRatti.Size = new System.Drawing.Size(225, 39);
            this.TxtCutFrmTotlWghtRatti.TabIndex = 36;
            this.TxtCutFrmTotlWghtRatti.Text = "0";
            this.TxtCutFrmTotlWghtRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCutPrGrams
            // 
            this.TxtCutPrGrams.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutPrGrams.Border.Class = "TextBoxBorder";
            this.TxtCutPrGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutPrGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutPrGrams.ForeColor = System.Drawing.Color.Red;
            this.TxtCutPrGrams.Location = new System.Drawing.Point(975, 244);
            this.TxtCutPrGrams.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutPrGrams.Name = "TxtCutPrGrams";
            this.TxtCutPrGrams.Size = new System.Drawing.Size(222, 39);
            this.TxtCutPrGrams.TabIndex = 38;
            this.TxtCutPrGrams.Text = "0";
            this.TxtCutPrGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCutFrmTotlWghtGrams
            // 
            this.TxtCutFrmTotlWghtGrams.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCutFrmTotlWghtGrams.Border.Class = "TextBoxBorder";
            this.TxtCutFrmTotlWghtGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCutFrmTotlWghtGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCutFrmTotlWghtGrams.ForeColor = System.Drawing.Color.Red;
            this.TxtCutFrmTotlWghtGrams.Location = new System.Drawing.Point(978, 182);
            this.TxtCutFrmTotlWghtGrams.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCutFrmTotlWghtGrams.Name = "TxtCutFrmTotlWghtGrams";
            this.TxtCutFrmTotlWghtGrams.Size = new System.Drawing.Size(221, 39);
            this.TxtCutFrmTotlWghtGrams.TabIndex = 39;
            this.TxtCutFrmTotlWghtGrams.Text = "0";
            this.TxtCutFrmTotlWghtGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotlPolishofMasha
            // 
            this.TxtTotlPolishofMasha.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtTotlPolishofMasha.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofMasha.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofMasha.ForeColor = System.Drawing.Color.Blue;
            this.TxtTotlPolishofMasha.Location = new System.Drawing.Point(527, 322);
            this.TxtTotlPolishofMasha.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtTotlPolishofMasha.Name = "TxtTotlPolishofMasha";
            this.TxtTotlPolishofMasha.Size = new System.Drawing.Size(225, 39);
            this.TxtTotlPolishofMasha.TabIndex = 40;
            this.TxtTotlPolishofMasha.Text = "0";
            this.TxtTotlPolishofMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPolishPrRatti
            // 
            this.TxtPolishPrRatti.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtPolishPrRatti.Border.Class = "TextBoxBorder";
            this.TxtPolishPrRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrRatti.ForeColor = System.Drawing.Color.Blue;
            this.TxtPolishPrRatti.Location = new System.Drawing.Point(752, 283);
            this.TxtPolishPrRatti.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtPolishPrRatti.Name = "TxtPolishPrRatti";
            this.TxtPolishPrRatti.Size = new System.Drawing.Size(225, 39);
            this.TxtPolishPrRatti.TabIndex = 42;
            this.TxtPolishPrRatti.Text = "0";
            this.TxtPolishPrRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPolishPrGrams
            // 
            this.TxtPolishPrGrams.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtPolishPrGrams.Border.Class = "TextBoxBorder";
            this.TxtPolishPrGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtPolishPrGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolishPrGrams.ForeColor = System.Drawing.Color.Blue;
            this.TxtPolishPrGrams.Location = new System.Drawing.Point(975, 283);
            this.TxtPolishPrGrams.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtPolishPrGrams.Name = "TxtPolishPrGrams";
            this.TxtPolishPrGrams.Size = new System.Drawing.Size(222, 39);
            this.TxtPolishPrGrams.TabIndex = 43;
            this.TxtPolishPrGrams.Text = "0";
            this.TxtPolishPrGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotlPolishofRatti
            // 
            this.TxtTotlPolishofRatti.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtTotlPolishofRatti.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofRatti.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofRatti.ForeColor = System.Drawing.Color.Blue;
            this.TxtTotlPolishofRatti.Location = new System.Drawing.Point(752, 322);
            this.TxtTotlPolishofRatti.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtTotlPolishofRatti.Name = "TxtTotlPolishofRatti";
            this.TxtTotlPolishofRatti.Size = new System.Drawing.Size(225, 39);
            this.TxtTotlPolishofRatti.TabIndex = 44;
            this.TxtTotlPolishofRatti.Text = "0";
            this.TxtTotlPolishofRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotlPolishofGrams
            // 
            this.TxtTotlPolishofGrams.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtTotlPolishofGrams.Border.Class = "TextBoxBorder";
            this.TxtTotlPolishofGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtTotlPolishofGrams.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotlPolishofGrams.ForeColor = System.Drawing.Color.Blue;
            this.TxtTotlPolishofGrams.Location = new System.Drawing.Point(975, 322);
            this.TxtTotlPolishofGrams.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtTotlPolishofGrams.Name = "TxtTotlPolishofGrams";
            this.TxtTotlPolishofGrams.Size = new System.Drawing.Size(222, 39);
            this.TxtTotlPolishofGrams.TabIndex = 45;
            this.TxtTotlPolishofGrams.Text = "0";
            this.TxtTotlPolishofGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtFinlWghtMasha
            // 
            this.TxtFinlWghtMasha.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtFinlWghtMasha.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtMasha.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtMasha.ForeColor = System.Drawing.Color.Red;
            this.TxtFinlWghtMasha.Location = new System.Drawing.Point(527, 362);
            this.TxtFinlWghtMasha.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtFinlWghtMasha.Name = "TxtFinlWghtMasha";
            this.TxtFinlWghtMasha.Size = new System.Drawing.Size(225, 44);
            this.TxtFinlWghtMasha.TabIndex = 46;
            this.TxtFinlWghtMasha.Text = "0";
            this.TxtFinlWghtMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtFinlWghtRatti
            // 
            this.TxtFinlWghtRatti.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtFinlWghtRatti.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtRatti.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtRatti.ForeColor = System.Drawing.Color.Red;
            this.TxtFinlWghtRatti.Location = new System.Drawing.Point(752, 362);
            this.TxtFinlWghtRatti.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtFinlWghtRatti.Name = "TxtFinlWghtRatti";
            this.TxtFinlWghtRatti.Size = new System.Drawing.Size(225, 44);
            this.TxtFinlWghtRatti.TabIndex = 47;
            this.TxtFinlWghtRatti.Text = "0";
            this.TxtFinlWghtRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtFinlWghtGrams
            // 
            this.TxtFinlWghtGrams.BackColor = System.Drawing.Color.LimeGreen;
            // 
            // 
            // 
            this.TxtFinlWghtGrams.Border.Class = "TextBoxBorder";
            this.TxtFinlWghtGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtFinlWghtGrams.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinlWghtGrams.ForeColor = System.Drawing.Color.Red;
            this.TxtFinlWghtGrams.Location = new System.Drawing.Point(975, 362);
            this.TxtFinlWghtGrams.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtFinlWghtGrams.Name = "TxtFinlWghtGrams";
            this.TxtFinlWghtGrams.Size = new System.Drawing.Size(222, 44);
            this.TxtFinlWghtGrams.TabIndex = 48;
            this.TxtFinlWghtGrams.Text = "0.000";
            this.TxtFinlWghtGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCurRatePrMashaRs
            // 
            this.TxtCurRatePrMashaRs.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCurRatePrMashaRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrMashaRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrMashaRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrMashaRs.ForeColor = System.Drawing.Color.Red;
            this.TxtCurRatePrMashaRs.Location = new System.Drawing.Point(527, 404);
            this.TxtCurRatePrMashaRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCurRatePrMashaRs.Name = "TxtCurRatePrMashaRs";
            this.TxtCurRatePrMashaRs.Size = new System.Drawing.Size(226, 39);
            this.TxtCurRatePrMashaRs.TabIndex = 49;
            this.TxtCurRatePrMashaRs.Text = "0";
            this.TxtCurRatePrMashaRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCurRatePrRattiRs
            // 
            this.TxtCurRatePrRattiRs.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCurRatePrRattiRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrRattiRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrRattiRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrRattiRs.ForeColor = System.Drawing.Color.Red;
            this.TxtCurRatePrRattiRs.Location = new System.Drawing.Point(752, 404);
            this.TxtCurRatePrRattiRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCurRatePrRattiRs.Name = "TxtCurRatePrRattiRs";
            this.TxtCurRatePrRattiRs.Size = new System.Drawing.Size(225, 39);
            this.TxtCurRatePrRattiRs.TabIndex = 50;
            this.TxtCurRatePrRattiRs.Text = "0";
            this.TxtCurRatePrRattiRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtCurRatePrGramsRs
            // 
            this.TxtCurRatePrGramsRs.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtCurRatePrGramsRs.Border.Class = "TextBoxBorder";
            this.TxtCurRatePrGramsRs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtCurRatePrGramsRs.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRatePrGramsRs.ForeColor = System.Drawing.Color.Blue;
            this.TxtCurRatePrGramsRs.Location = new System.Drawing.Point(975, 404);
            this.TxtCurRatePrGramsRs.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtCurRatePrGramsRs.Name = "TxtCurRatePrGramsRs";
            this.TxtCurRatePrGramsRs.Size = new System.Drawing.Size(222, 39);
            this.TxtCurRatePrGramsRs.TabIndex = 51;
            this.TxtCurRatePrGramsRs.Text = "0";
            this.TxtCurRatePrGramsRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // itemPanelForButtonsandRadiobtns
            // 
            // 
            // 
            // 
            this.itemPanelForButtonsandRadiobtns.BackgroundStyle.Class = "ItemPanel";
            this.itemPanelForButtonsandRadiobtns.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemPanelForButtonsandRadiobtns.ContainerControlProcessDialogKey = true;
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnExit);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnSetting);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnImages);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnPlayer);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnPad);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnGoldCalculator);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnAccounts);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnReceipt);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnKarat);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnTransaction);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnClearData);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.BtnCalculator);
            this.itemPanelForButtonsandRadiobtns.Controls.Add(this.groupPanelcutPriority);
            this.itemPanelForButtonsandRadiobtns.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemPanelForButtonsandRadiobtns.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemPanelForButtonsandRadiobtns.Location = new System.Drawing.Point(527, 444);
            this.itemPanelForButtonsandRadiobtns.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.itemPanelForButtonsandRadiobtns.Name = "itemPanelForButtonsandRadiobtns";
            this.itemPanelForButtonsandRadiobtns.Size = new System.Drawing.Size(671, 246);
            this.itemPanelForButtonsandRadiobtns.TabIndex = 55;
            this.itemPanelForButtonsandRadiobtns.Text = "itemPanel1";
            // 
            // BtnExit
            // 
            this.BtnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnExit.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnExit.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Image = global::Presentation.Properties.Resources.cancel;
            this.BtnExit.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnExit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnExit.Location = new System.Drawing.Point(563, 176);
            this.BtnExit.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(99, 65);
            this.BtnExit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnExit.TabIndex = 11;
            this.BtnExit.Text = "Exit";
            this.BtnExit.TextColor = System.Drawing.Color.White;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnSetting
            // 
            this.BtnSetting.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnSetting.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnSetting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSetting.Image = global::Presentation.Properties.Resources.SEtting_;
            this.BtnSetting.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnSetting.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnSetting.Location = new System.Drawing.Point(457, 175);
            this.BtnSetting.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnSetting.Name = "BtnSetting";
            this.BtnSetting.Size = new System.Drawing.Size(99, 65);
            this.BtnSetting.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnSetting.TabIndex = 10;
            this.BtnSetting.Text = "Setting";
            this.BtnSetting.TextColor = System.Drawing.Color.White;
            // 
            // BtnImages
            // 
            this.BtnImages.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnImages.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnImages.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnImages.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImages.Image = global::Presentation.Properties.Resources.PIC_JPG;
            this.BtnImages.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnImages.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnImages.Location = new System.Drawing.Point(563, 104);
            this.BtnImages.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnImages.Name = "BtnImages";
            this.BtnImages.Size = new System.Drawing.Size(99, 65);
            this.BtnImages.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnImages.TabIndex = 9;
            this.BtnImages.Text = "Images";
            this.BtnImages.TextColor = System.Drawing.Color.White;
            // 
            // BtnPlayer
            // 
            this.BtnPlayer.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPlayer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnPlayer.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnPlayer.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlayer.Image = global::Presentation.Properties.Resources.MusicIcon_jpg;
            this.BtnPlayer.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnPlayer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnPlayer.Location = new System.Drawing.Point(456, 104);
            this.BtnPlayer.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnPlayer.Name = "BtnPlayer";
            this.BtnPlayer.Size = new System.Drawing.Size(99, 65);
            this.BtnPlayer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPlayer.TabIndex = 8;
            this.BtnPlayer.Text = "Player";
            this.BtnPlayer.TextColor = System.Drawing.Color.White;
            // 
            // BtnPad
            // 
            this.BtnPad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnPad.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnPad.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPad.Image = global::Presentation.Properties.Resources.NoteBookPencil_JPG;
            this.BtnPad.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnPad.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnPad.Location = new System.Drawing.Point(341, 178);
            this.BtnPad.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnPad.Name = "BtnPad";
            this.BtnPad.Size = new System.Drawing.Size(99, 65);
            this.BtnPad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPad.TabIndex = 7;
            this.BtnPad.Text = "Pad";
            this.BtnPad.TextColor = System.Drawing.Color.White;
            // 
            // BtnGoldCalculator
            // 
            this.BtnGoldCalculator.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnGoldCalculator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnGoldCalculator.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnGoldCalculator.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGoldCalculator.Image = global::Presentation.Properties.Resources.GOLDCALCU_JPG;
            this.BtnGoldCalculator.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnGoldCalculator.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnGoldCalculator.Location = new System.Drawing.Point(236, 176);
            this.BtnGoldCalculator.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnGoldCalculator.Name = "BtnGoldCalculator";
            this.BtnGoldCalculator.Size = new System.Drawing.Size(99, 65);
            this.BtnGoldCalculator.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnGoldCalculator.TabIndex = 6;
            this.BtnGoldCalculator.Text = "Gold Calc";
            this.BtnGoldCalculator.TextColor = System.Drawing.Color.White;
            // 
            // BtnAccounts
            // 
            this.BtnAccounts.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnAccounts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnAccounts.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnAccounts.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAccounts.Image = global::Presentation.Properties.Resources.AccountDetilsJpg;
            this.BtnAccounts.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnAccounts.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnAccounts.Location = new System.Drawing.Point(340, 104);
            this.BtnAccounts.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnAccounts.Name = "BtnAccounts";
            this.BtnAccounts.Size = new System.Drawing.Size(99, 65);
            this.BtnAccounts.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnAccounts.TabIndex = 5;
            this.BtnAccounts.Text = "Accounts";
            this.BtnAccounts.TextColor = System.Drawing.Color.White;
            // 
            // BtnReceipt
            // 
            this.BtnReceipt.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnReceipt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnReceipt.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnReceipt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReceipt.Image = global::Presentation.Properties.Resources.AccountJPG;
            this.BtnReceipt.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnReceipt.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnReceipt.Location = new System.Drawing.Point(235, 104);
            this.BtnReceipt.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnReceipt.Name = "BtnReceipt";
            this.BtnReceipt.Size = new System.Drawing.Size(99, 65);
            this.BtnReceipt.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnReceipt.TabIndex = 4;
            this.BtnReceipt.Text = "Receipt";
            this.BtnReceipt.TextColor = System.Drawing.Color.White;
            // 
            // BtnKarat
            // 
            this.BtnKarat.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnKarat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnKarat.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnKarat.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKarat.Image = global::Presentation.Properties.Resources.Quran;
            this.BtnKarat.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnKarat.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnKarat.Location = new System.Drawing.Point(119, 176);
            this.BtnKarat.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnKarat.Name = "BtnKarat";
            this.BtnKarat.Size = new System.Drawing.Size(99, 65);
            this.BtnKarat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnKarat.TabIndex = 3;
            this.BtnKarat.Text = "Karat";
            this.BtnKarat.TextColor = System.Drawing.Color.White;
            // 
            // BtnTransaction
            // 
            this.BtnTransaction.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnTransaction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnTransaction.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnTransaction.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTransaction.Image = global::Presentation.Properties.Resources.HandMoneyjpg;
            this.BtnTransaction.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnTransaction.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnTransaction.Location = new System.Drawing.Point(119, 104);
            this.BtnTransaction.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnTransaction.Name = "BtnTransaction";
            this.BtnTransaction.Size = new System.Drawing.Size(99, 65);
            this.BtnTransaction.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnTransaction.TabIndex = 0;
            this.BtnTransaction.Text = "Transaction";
            this.BtnTransaction.TextColor = System.Drawing.Color.White;
            this.BtnTransaction.Click += new System.EventHandler(this.BtnTransaction_Click);
            // 
            // BtnClearData
            // 
            this.BtnClearData.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnClearData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnClearData.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnClearData.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClearData.Image = global::Presentation.Properties.Resources.Refresh_jpg;
            this.BtnClearData.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnClearData.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnClearData.Location = new System.Drawing.Point(11, 104);
            this.BtnClearData.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnClearData.Name = "BtnClearData";
            this.BtnClearData.Size = new System.Drawing.Size(99, 65);
            this.BtnClearData.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnClearData.TabIndex = 1;
            this.BtnClearData.Text = "Clear Data";
            this.BtnClearData.TextColor = System.Drawing.Color.White;
            this.BtnClearData.Click += new System.EventHandler(this.BtnClearData_Click);
            // 
            // BtnCalculator
            // 
            this.BtnCalculator.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnCalculator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnCalculator.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.BtnCalculator.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCalculator.Image = global::Presentation.Properties.Resources.Calculator_jpg;
            this.BtnCalculator.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnCalculator.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnCalculator.Location = new System.Drawing.Point(11, 174);
            this.BtnCalculator.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.BtnCalculator.Name = "BtnCalculator";
            this.BtnCalculator.Size = new System.Drawing.Size(99, 65);
            this.BtnCalculator.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnCalculator.TabIndex = 12;
            this.BtnCalculator.Text = "Calculator";
            this.BtnCalculator.TextColor = System.Drawing.Color.White;
            // 
            // groupPanelcutPriority
            // 
            this.groupPanelcutPriority.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanelcutPriority.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanelcutPriority.ColorTable = DevComponents.DotNetBar.Controls.ePanelColorTable.Yellow;
            this.groupPanelcutPriority.Controls.Add(this.RdbTotlCutFirst);
            this.groupPanelcutPriority.Controls.Add(this.RdbCutPrTolaFirst);
            this.groupPanelcutPriority.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelcutPriority.Location = new System.Drawing.Point(235, 3);
            this.groupPanelcutPriority.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupPanelcutPriority.Name = "groupPanelcutPriority";
            this.groupPanelcutPriority.Size = new System.Drawing.Size(205, 93);
            // 
            // 
            // 
            this.groupPanelcutPriority.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(178)))));
            this.groupPanelcutPriority.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(217)))), ((int)(((byte)(69)))));
            this.groupPanelcutPriority.Style.BackColorGradientAngle = 90;
            this.groupPanelcutPriority.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderBottomWidth = 1;
            this.groupPanelcutPriority.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(147)))), ((int)(((byte)(17)))));
            this.groupPanelcutPriority.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderLeftWidth = 1;
            this.groupPanelcutPriority.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderRightWidth = 1;
            this.groupPanelcutPriority.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelcutPriority.Style.BorderTopWidth = 1;
            this.groupPanelcutPriority.Style.CornerDiameter = 4;
            this.groupPanelcutPriority.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanelcutPriority.Style.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelcutPriority.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanelcutPriority.Style.TextColor = System.Drawing.Color.Blue;
            this.groupPanelcutPriority.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanelcutPriority.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanelcutPriority.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanelcutPriority.TabIndex = 59;
            this.groupPanelcutPriority.Text = "Cut Priority";
            // 
            // RdbTotlCutFirst
            // 
            this.RdbTotlCutFirst.AutoSize = true;
            this.RdbTotlCutFirst.BackColor = System.Drawing.Color.Transparent;
            this.RdbTotlCutFirst.Checked = true;
            this.RdbTotlCutFirst.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbTotlCutFirst.ForeColor = System.Drawing.Color.Red;
            this.RdbTotlCutFirst.Location = new System.Drawing.Point(31, 35);
            this.RdbTotlCutFirst.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.RdbTotlCutFirst.Name = "RdbTotlCutFirst";
            this.RdbTotlCutFirst.Size = new System.Drawing.Size(119, 24);
            this.RdbTotlCutFirst.TabIndex = 3;
            this.RdbTotlCutFirst.TabStop = true;
            this.RdbTotlCutFirst.Text = "Total Cut 1st";
            this.RdbTotlCutFirst.UseVisualStyleBackColor = false;
            // 
            // RdbCutPrTolaFirst
            // 
            this.RdbCutPrTolaFirst.AutoSize = true;
            this.RdbCutPrTolaFirst.BackColor = System.Drawing.Color.Transparent;
            this.RdbCutPrTolaFirst.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbCutPrTolaFirst.ForeColor = System.Drawing.Color.DarkGreen;
            this.RdbCutPrTolaFirst.Location = new System.Drawing.Point(33, 3);
            this.RdbCutPrTolaFirst.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.RdbCutPrTolaFirst.Name = "RdbCutPrTolaFirst";
            this.RdbCutPrTolaFirst.Size = new System.Drawing.Size(140, 24);
            this.RdbCutPrTolaFirst.TabIndex = 2;
            this.RdbCutPrTolaFirst.Text = "Cut Per Tola 1st";
            this.RdbCutPrTolaFirst.UseVisualStyleBackColor = false;
            // 
            // groupPanelLabourPolisCalcu
            // 
            this.groupPanelLabourPolisCalcu.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanelLabourPolisCalcu.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanelLabourPolisCalcu.ColorTable = DevComponents.DotNetBar.Controls.ePanelColorTable.Yellow;
            this.groupPanelLabourPolisCalcu.Controls.Add(this.RdbFinalWeight);
            this.groupPanelLabourPolisCalcu.Controls.Add(this.RdbTopWeight);
            this.groupPanelLabourPolisCalcu.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelLabourPolisCalcu.Location = new System.Drawing.Point(983, 446);
            this.groupPanelLabourPolisCalcu.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupPanelLabourPolisCalcu.Name = "groupPanelLabourPolisCalcu";
            this.groupPanelLabourPolisCalcu.Size = new System.Drawing.Size(208, 93);
            // 
            // 
            // 
            this.groupPanelLabourPolisCalcu.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(178)))));
            this.groupPanelLabourPolisCalcu.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(217)))), ((int)(((byte)(69)))));
            this.groupPanelLabourPolisCalcu.Style.BackColorGradientAngle = 90;
            this.groupPanelLabourPolisCalcu.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderBottomWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(147)))), ((int)(((byte)(17)))));
            this.groupPanelLabourPolisCalcu.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderLeftWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderRightWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelLabourPolisCalcu.Style.BorderTopWidth = 1;
            this.groupPanelLabourPolisCalcu.Style.CornerDiameter = 4;
            this.groupPanelLabourPolisCalcu.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanelLabourPolisCalcu.Style.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelLabourPolisCalcu.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanelLabourPolisCalcu.Style.TextColor = System.Drawing.Color.Blue;
            this.groupPanelLabourPolisCalcu.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanelLabourPolisCalcu.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanelLabourPolisCalcu.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanelLabourPolisCalcu.TabIndex = 58;
            this.groupPanelLabourPolisCalcu.Text = "Labour & Polish Calculation";
            // 
            // RdbFinalWeight
            // 
            this.RdbFinalWeight.AutoSize = true;
            this.RdbFinalWeight.BackColor = System.Drawing.Color.Transparent;
            this.RdbFinalWeight.Checked = true;
            this.RdbFinalWeight.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbFinalWeight.ForeColor = System.Drawing.Color.Red;
            this.RdbFinalWeight.Location = new System.Drawing.Point(17, 35);
            this.RdbFinalWeight.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.RdbFinalWeight.Name = "RdbFinalWeight";
            this.RdbFinalWeight.Size = new System.Drawing.Size(118, 24);
            this.RdbFinalWeight.TabIndex = 1;
            this.RdbFinalWeight.TabStop = true;
            this.RdbFinalWeight.Text = "Final Weight";
            this.RdbFinalWeight.UseVisualStyleBackColor = false;
            // 
            // RdbTopWeight
            // 
            this.RdbTopWeight.AutoSize = true;
            this.RdbTopWeight.BackColor = System.Drawing.Color.Transparent;
            this.RdbTopWeight.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbTopWeight.ForeColor = System.Drawing.Color.DarkGreen;
            this.RdbTopWeight.Location = new System.Drawing.Point(17, 3);
            this.RdbTopWeight.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.RdbTopWeight.Name = "RdbTopWeight";
            this.RdbTopWeight.Size = new System.Drawing.Size(111, 24);
            this.RdbTopWeight.TabIndex = 0;
            this.RdbTopWeight.Text = "Top Weight";
            this.RdbTopWeight.UseVisualStyleBackColor = false;
            // 
            // groupPanel2
            // 
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.radioButton3);
            this.groupPanel2.Controls.Add(this.radioButton4);
            this.groupPanel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupPanel2.Location = new System.Drawing.Point(625, 193);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(162, 62);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 57;
            this.groupPanel2.Text = "Cut Priority";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radioButton3.Checked = true;
            this.radioButton3.ForeColor = System.Drawing.Color.Red;
            this.radioButton3.Location = new System.Drawing.Point(12, 22);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(109, 21);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = " Tola Cut 1st";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radioButton4.ForeColor = System.Drawing.Color.DarkGreen;
            this.radioButton4.Location = new System.Drawing.Point(12, 1);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(131, 21);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.Text = "Cut Per Tola 1st";
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // RdbPakaTola
            // 
            this.RdbPakaTola.AutoSize = true;
            this.RdbPakaTola.BackColor = System.Drawing.Color.Transparent;
            this.RdbPakaTola.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbPakaTola.Location = new System.Drawing.Point(17, 3);
            this.RdbPakaTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.RdbPakaTola.Name = "RdbPakaTola";
            this.RdbPakaTola.Size = new System.Drawing.Size(165, 24);
            this.RdbPakaTola.TabIndex = 0;
            this.RdbPakaTola.Text = "Paka Tola (12.15 g)";
            this.RdbPakaTola.UseVisualStyleBackColor = false;
            // 
            // RdbKachTola
            // 
            this.RdbKachTola.AutoSize = true;
            this.RdbKachTola.BackColor = System.Drawing.Color.Transparent;
            this.RdbKachTola.Checked = true;
            this.RdbKachTola.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RdbKachTola.ForeColor = System.Drawing.Color.Red;
            this.RdbKachTola.Location = new System.Drawing.Point(17, 35);
            this.RdbKachTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.RdbKachTola.Name = "RdbKachTola";
            this.RdbKachTola.Size = new System.Drawing.Size(175, 24);
            this.RdbKachTola.TabIndex = 1;
            this.RdbKachTola.TabStop = true;
            this.RdbKachTola.Text = "Kach Tola (11.664 g)";
            this.RdbKachTola.UseVisualStyleBackColor = false;
            // 
            // groupPanelWeightinMode
            // 
            this.groupPanelWeightinMode.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanelWeightinMode.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanelWeightinMode.ColorTable = DevComponents.DotNetBar.Controls.ePanelColorTable.Yellow;
            this.groupPanelWeightinMode.Controls.Add(this.RdbKachTola);
            this.groupPanelWeightinMode.Controls.Add(this.RdbPakaTola);
            this.groupPanelWeightinMode.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelWeightinMode.Location = new System.Drawing.Point(534, 446);
            this.groupPanelWeightinMode.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupPanelWeightinMode.Name = "groupPanelWeightinMode";
            this.groupPanelWeightinMode.Size = new System.Drawing.Size(215, 93);
            // 
            // 
            // 
            this.groupPanelWeightinMode.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(243)))), ((int)(((byte)(178)))));
            this.groupPanelWeightinMode.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(217)))), ((int)(((byte)(69)))));
            this.groupPanelWeightinMode.Style.BackColorGradientAngle = 90;
            this.groupPanelWeightinMode.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderBottomWidth = 1;
            this.groupPanelWeightinMode.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(147)))), ((int)(((byte)(17)))));
            this.groupPanelWeightinMode.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderLeftWidth = 1;
            this.groupPanelWeightinMode.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderRightWidth = 1;
            this.groupPanelWeightinMode.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanelWeightinMode.Style.BorderTopWidth = 1;
            this.groupPanelWeightinMode.Style.CornerDiameter = 4;
            this.groupPanelWeightinMode.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanelWeightinMode.Style.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanelWeightinMode.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanelWeightinMode.Style.TextColor = System.Drawing.Color.Blue;
            this.groupPanelWeightinMode.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanelWeightinMode.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanelWeightinMode.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanelWeightinMode.TabIndex = 56;
            this.groupPanelWeightinMode.Text = "Weighting Mode";
            // 
            // TxtWeightTola
            // 
            this.TxtWeightTola.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtWeightTola.Border.Class = "TextBoxBorder";
            this.TxtWeightTola.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightTola.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightTola.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightTola.Location = new System.Drawing.Point(295, 38);
            this.TxtWeightTola.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtWeightTola.Name = "TxtWeightTola";
            this.TxtWeightTola.Size = new System.Drawing.Size(225, 48);
            this.TxtWeightTola.TabIndex = 3;
            this.TxtWeightTola.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxX6
            // 
            this.textBoxX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            // 
            // 
            // 
            this.textBoxX6.Border.Class = "TextBoxBorder";
            this.textBoxX6.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 41F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxX6.ForeColor = System.Drawing.Color.White;
            this.textBoxX6.Location = new System.Drawing.Point(5, 0);
            this.textBoxX6.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.textBoxX6.Name = "textBoxX6";
            this.textBoxX6.ReadOnly = true;
            this.textBoxX6.Size = new System.Drawing.Size(291, 85);
            this.textBoxX6.TabIndex = 7;
            this.textBoxX6.Text = "Weight";
            // 
            // TxtWeightMasha
            // 
            this.TxtWeightMasha.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtWeightMasha.Border.Class = "TextBoxBorder";
            this.TxtWeightMasha.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightMasha.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightMasha.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightMasha.Location = new System.Drawing.Point(520, 38);
            this.TxtWeightMasha.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtWeightMasha.Name = "TxtWeightMasha";
            this.TxtWeightMasha.Size = new System.Drawing.Size(226, 48);
            this.TxtWeightMasha.TabIndex = 2;
            this.TxtWeightMasha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtWeightRatti
            // 
            this.TxtWeightRatti.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtWeightRatti.Border.Class = "TextBoxBorder";
            this.TxtWeightRatti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightRatti.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightRatti.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightRatti.Location = new System.Drawing.Point(746, 38);
            this.TxtWeightRatti.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtWeightRatti.Name = "TxtWeightRatti";
            this.TxtWeightRatti.Size = new System.Drawing.Size(225, 48);
            this.TxtWeightRatti.TabIndex = 1;
            this.TxtWeightRatti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtWeightGrams
            // 
            this.TxtWeightGrams.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // 
            // 
            this.TxtWeightGrams.Border.Class = "TextBoxBorder";
            this.TxtWeightGrams.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtWeightGrams.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblTransactionBindingSource, "Weight_in_Gram", true));
            this.TxtWeightGrams.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeightGrams.ForeColor = System.Drawing.Color.Blue;
            this.TxtWeightGrams.Location = new System.Drawing.Point(971, 38);
            this.TxtWeightGrams.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.TxtWeightGrams.Name = "TxtWeightGrams";
            this.TxtWeightGrams.Size = new System.Drawing.Size(220, 48);
            this.TxtWeightGrams.TabIndex = 0;
            this.TxtWeightGrams.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panelSecondTopParent
            // 
            this.panelSecondTopParent.Controls.Add(this.labelX9);
            this.panelSecondTopParent.Controls.Add(this.labelX8);
            this.panelSecondTopParent.Controls.Add(this.labelX7);
            this.panelSecondTopParent.Controls.Add(this.labelX6);
            this.panelSecondTopParent.Controls.Add(this.TxtWeightGrams);
            this.panelSecondTopParent.Controls.Add(this.TxtWeightRatti);
            this.panelSecondTopParent.Controls.Add(this.TxtWeightMasha);
            this.panelSecondTopParent.Controls.Add(this.textBoxX6);
            this.panelSecondTopParent.Controls.Add(this.TxtWeightTola);
            this.panelSecondTopParent.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelSecondTopParent.Location = new System.Drawing.Point(7, 96);
            this.panelSecondTopParent.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.panelSecondTopParent.Name = "panelSecondTopParent";
            this.panelSecondTopParent.Size = new System.Drawing.Size(1190, 85);
            this.panelSecondTopParent.TabIndex = 4;
            this.panelSecondTopParent.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSecondTopParent_Paint);
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.White;
            this.labelX9.Location = new System.Drawing.Point(967, 0);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(223, 38);
            this.labelX9.TabIndex = 11;
            this.labelX9.Text = "Grams";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.White;
            this.labelX8.Location = new System.Drawing.Point(743, 0);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(224, 38);
            this.labelX8.TabIndex = 10;
            this.labelX8.Text = "Ratti";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.White;
            this.labelX7.Location = new System.Drawing.Point(519, 0);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(224, 38);
            this.labelX7.TabIndex = 9;
            this.labelX7.Text = "Masha";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.White;
            this.labelX6.Location = new System.Drawing.Point(296, 0);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(224, 38);
            this.labelX6.TabIndex = 8;
            this.labelX6.Text = "Tola";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(83, 217);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(219, 32);
            this.dateTimePicker1.TabIndex = 60;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(0, 0);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(1209, 12);
            this.labelX5.TabIndex = 63;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(5, 690);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(1205, 12);
            this.labelX1.TabIndex = 64;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(1199, -4);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(11, 706);
            this.labelX3.TabIndex = 65;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(0, -4);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(13, 706);
            this.labelX4.TabIndex = 66;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // JewellersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1212, 704);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.groupPanelLabourPolisCalcu);
            this.Controls.Add(this.groupPanelWeightinMode);
            this.Controls.Add(this.itemPanelForButtonsandRadiobtns);
            this.Controls.Add(this.TxtCurRatePrGramsRs);
            this.Controls.Add(this.TxtCurRatePrRattiRs);
            this.Controls.Add(this.TxtCurRatePrMashaRs);
            this.Controls.Add(this.TxtFinlWghtGrams);
            this.Controls.Add(this.TxtFinlWghtRatti);
            this.Controls.Add(this.TxtFinlWghtMasha);
            this.Controls.Add(this.TxtTotlPolishofGrams);
            this.Controls.Add(this.TxtTotlPolishofRatti);
            this.Controls.Add(this.TxtPolishPrGrams);
            this.Controls.Add(this.TxtPolishPrRatti);
            this.Controls.Add(this.TxtTotlPolishofMasha);
            this.Controls.Add(this.TxtCutFrmTotlWghtGrams);
            this.Controls.Add(this.TxtCutPrGrams);
            this.Controls.Add(this.TxtCutFrmTotlWghtRatti);
            this.Controls.Add(this.TxtCutPrRatti);
            this.Controls.Add(this.TxtPolishPrMasha);
            this.Controls.Add(this.TxtCutFrmTotlWghtMasha);
            this.Controls.Add(this.TxtCutPrMasha);
            this.Controls.Add(this.TxtNetAmuntRs);
            this.Controls.Add(this.TxtPymtPercntCut);
            this.Controls.Add(this.TxtTotlAmuntRs);
            this.Controls.Add(this.TxtTotlLaburRs);
            this.Controls.Add(this.TxtLabourPerTolaRs);
            this.Controls.Add(this.TxtPriceofGoldRs);
            this.Controls.Add(this.TxtCurRatePrTolaRs);
            this.Controls.Add(this.TxtFinlWghtTola);
            this.Controls.Add(this.TxtTotlPolishofTola);
            this.Controls.Add(this.TxtPolishPrTola);
            this.Controls.Add(this.TxtCutFrmTotlWghtTola);
            this.Controls.Add(this.TxtCutPrTola);
            this.Controls.Add(this.textBoxX23);
            this.Controls.Add(this.Textboxpymnt);
            this.Controls.Add(this.textBoxX21);
            this.Controls.Add(this.textBoxX20);
            this.Controls.Add(this.textBoxX19);
            this.Controls.Add(this.textBoxX18);
            this.Controls.Add(this.textBoxX17);
            this.Controls.Add(this.textBoxX16);
            this.Controls.Add(this.textBoxX15);
            this.Controls.Add(this.textBoxX14);
            this.Controls.Add(this.textBoxX3);
            this.Controls.Add(this.textBoxX1);
            this.Controls.Add(this.panelSecondTopParent);
            this.Controls.Add(this.panelFirstTopParent);
            this.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.Name = "JewellersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transaction Calculation Form";
            this.Load += new System.EventHandler(this.JewellersForm_Load);
            this.panelFirstTopParent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            this.itemPanelForButtonsandRadiobtns.ResumeLayout(false);
            this.groupPanelcutPriority.ResumeLayout(false);
            this.groupPanelcutPriority.PerformLayout();
            this.groupPanelLabourPolisCalcu.ResumeLayout(false);
            this.groupPanelLabourPolisCalcu.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.groupPanelWeightinMode.ResumeLayout(false);
            this.groupPanelWeightinMode.PerformLayout();
            this.panelSecondTopParent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFirstTopParent;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCurrentDate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtCurrentTime;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX3;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX14;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX15;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX16;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX17;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX18;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX19;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX20;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX21;
        private DevComponents.DotNetBar.Controls.TextBoxX Textboxpymnt;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX23;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtTola;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrTolaRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPriceofGoldRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtLabourPerTolaRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlLaburRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlAmuntRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPymtPercntCut;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtNetAmuntRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutPrGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCutFrmTotlWghtGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtPolishPrGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtTotlPolishofGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtFinlWghtGrams;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrMashaRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrRattiRs;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtCurRatePrGramsRs;
        private DevComponents.DotNetBar.ItemPanel itemPanelForButtonsandRadiobtns;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanelLabourPolisCalcu;
        private System.Windows.Forms.RadioButton RdbFinalWeight;
        private System.Windows.Forms.RadioButton RdbTopWeight;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private DevComponents.DotNetBar.ButtonX BtnExit;
        private DevComponents.DotNetBar.ButtonX BtnSetting;
        private DevComponents.DotNetBar.ButtonX BtnImages;
        private DevComponents.DotNetBar.ButtonX BtnPlayer;
        private DevComponents.DotNetBar.ButtonX BtnPad;
        private DevComponents.DotNetBar.ButtonX BtnGoldCalculator;
        private DevComponents.DotNetBar.ButtonX BtnAccounts;
        private DevComponents.DotNetBar.ButtonX BtnReceipt;
        private DevComponents.DotNetBar.ButtonX BtnKarat;
        private DevComponents.DotNetBar.ButtonX BtnTransaction;
        private DevComponents.DotNetBar.ButtonX BtnClearData;
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private System.Windows.Forms.RadioButton RdbCutPrTolaFirst;
        private System.Windows.Forms.RadioButton RdbTotlCutFirst;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanelcutPriority;
        private System.Windows.Forms.RadioButton RdbPakaTola;
        private System.Windows.Forms.RadioButton RdbKachTola;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanelWeightinMode;
        private DevComponents.DotNetBar.ButtonX BtnCalculator;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightTola;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX6;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightMasha;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightRatti;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtWeightGrams;
        private System.Windows.Forms.Panel panelSecondTopParent;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX7;


    }
}