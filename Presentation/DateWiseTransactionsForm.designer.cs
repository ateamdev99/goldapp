﻿namespace Presentation
{
    partial class DateWiseTransactionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBarX1 = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpFrom = new System.Windows.Forms.DateTimePicker();
            this.DtpTo = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.tblTransactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonX();
            this.BtnGo = new DevComponents.DotNetBar.ButtonX();
            this.BtnPreview = new DevComponents.DotNetBar.ButtonX();
            this.BtnClose = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.ChkGrupTranByClintNo = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ChkDisplayTemporaryAccounts = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.tIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceofGoldDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentRatePerTolaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightinGramDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labourPerTolaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalLabourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentPercntCutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.netAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.polishPerTolaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalPolishDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutPerTolaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutfromtotalWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightinModeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutPriorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labourAndPolishCalculationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addedByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addedDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itmeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tranTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tranStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isTeleConfirmationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teleDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.TeleConfirmation = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "To";
            // 
            // progressBarX1
            // 
            // 
            // 
            // 
            this.progressBarX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.progressBarX1.Location = new System.Drawing.Point(551, 43);
            this.progressBarX1.Name = "progressBarX1";
            this.progressBarX1.Size = new System.Drawing.Size(302, 22);
            this.progressBarX1.TabIndex = 6;
            this.progressBarX1.Text = "progressBarX1";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(551, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(302, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Record(s) Found";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DtpFrom
            // 
            this.DtpFrom.CalendarForeColor = System.Drawing.SystemColors.GrayText;
            this.DtpFrom.CalendarMonthBackground = System.Drawing.Color.PaleGreen;
            this.DtpFrom.CustomFormat = "dd/MM/yyyy";
            this.DtpFrom.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrom.Location = new System.Drawing.Point(66, 3);
            this.DtpFrom.Name = "DtpFrom";
            this.DtpFrom.Size = new System.Drawing.Size(243, 29);
            this.DtpFrom.TabIndex = 2;
            // 
            // DtpTo
            // 
            this.DtpTo.CalendarForeColor = System.Drawing.SystemColors.GrayText;
            this.DtpTo.CalendarMonthBackground = System.Drawing.Color.PaleGreen;
            this.DtpTo.CustomFormat = "dd/MM/yyyy";
            this.DtpTo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpTo.Location = new System.Drawing.Point(66, 30);
            this.DtpTo.Name = "DtpTo";
            this.DtpTo.Size = new System.Drawing.Size(243, 29);
            this.DtpTo.TabIndex = 3;
            this.DtpTo.ValueChanged += new System.EventHandler(this.DtpTo_ValueChanged);
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.dataGridViewX1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tIdDataGridViewTextBoxColumn,
            this.clientIdDataGridViewTextBoxColumn,
            this.tDateDataGridViewTextBoxColumn,
            this.priceofGoldDataGridViewTextBoxColumn,
            this.currentRatePerTolaDataGridViewTextBoxColumn,
            this.weightinGramDataGridViewTextBoxColumn,
            this.labourPerTolaDataGridViewTextBoxColumn,
            this.totalLabourDataGridViewTextBoxColumn,
            this.paymentPercntCutDataGridViewTextBoxColumn,
            this.netAmountDataGridViewTextBoxColumn,
            this.polishPerTolaDataGridViewTextBoxColumn,
            this.totalPolishDataGridViewTextBoxColumn,
            this.cutPerTolaDataGridViewTextBoxColumn,
            this.cutfromtotalWeightDataGridViewTextBoxColumn,
            this.weightinModeDataGridViewTextBoxColumn,
            this.cutPriorityDataGridViewTextBoxColumn,
            this.labourAndPolishCalculationDataGridViewTextBoxColumn,
            this.addedByDataGridViewTextBoxColumn,
            this.addedDateDataGridViewTextBoxColumn,
            this.itmeNameDataGridViewTextBoxColumn,
            this.tranTypeDataGridViewTextBoxColumn,
            this.tranStatusDataGridViewTextBoxColumn,
            this.isTeleConfirmationDataGridViewTextBoxColumn,
            this.teleDateDataGridViewTextBoxColumn,
            this.purityDataGridViewTextBoxColumn,
            this.Edit,
            this.TeleConfirmation});
            this.dataGridViewX1.DataSource = this.tblTransactionBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.Size = new System.Drawing.Size(1222, 560);
            this.dataGridViewX1.TabIndex = 16;
            this.dataGridViewX1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellClick_1);
            // 
            // tblTransactionBindingSource
            // 
            this.tblTransactionBindingSource.DataSource = typeof(Data.tblTransaction);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.89704F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.15445F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.17473F));
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnPrint, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.BtnGo, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnPreview, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.BtnClose, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.progressBarX1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.ChkGrupTranByClintNo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.ChkDisplayTemporaryAccounts, 1, 4);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 554);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1222, 128);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.DtpFrom);
            this.panel4.Controls.Add(this.DtpTo);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(222, 15);
            this.panel4.Name = "panel4";
            this.tableLayoutPanel1.SetRowSpan(this.panel4, 2);
            this.panel4.Size = new System.Drawing.Size(323, 50);
            this.panel4.TabIndex = 87;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX6.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX6.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX6.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderRightWidth = 2;
            this.labelX6.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX6.BackgroundStyle.BorderTopWidth = 2;
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.White;
            this.labelX6.Location = new System.Drawing.Point(3, 15);
            this.labelX6.Name = "labelX6";
            this.tableLayoutPanel1.SetRowSpan(this.labelX6, 2);
            this.labelX6.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX6.Size = new System.Drawing.Size(213, 50);
            this.labelX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX6.TabIndex = 75;
            this.labelX6.Text = "Search by Date";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // BtnPrint
            // 
            this.BtnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnPrint.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Image = global::Presentation.Properties.Resources.Printer_JPG;
            this.BtnPrint.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnPrint.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.BtnPrint.Location = new System.Drawing.Point(866, 71);
            this.BtnPrint.Name = "BtnPrint";
            this.tableLayoutPanel1.SetRowSpan(this.BtnPrint, 2);
            this.BtnPrint.Size = new System.Drawing.Size(127, 54);
            this.BtnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPrint.TabIndex = 5;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.TextColor = System.Drawing.Color.White;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnGo
            // 
            this.BtnGo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnGo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnGo.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnGo.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //this.BtnGo.Image = global::Presentation.Properties.Resources.Go1;
            this.BtnGo.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnGo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.BtnGo.Location = new System.Drawing.Point(866, 15);
            this.BtnGo.Name = "BtnGo";
            this.tableLayoutPanel1.SetRowSpan(this.BtnGo, 2);
            this.BtnGo.Size = new System.Drawing.Size(127, 50);
            this.BtnGo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnGo.TabIndex = 4;
            this.BtnGo.Text = "Go";
            this.BtnGo.TextColor = System.Drawing.Color.White;
            this.BtnGo.Click += new System.EventHandler(this.BtnGo_Click);
            // 
            // BtnPreview
            // 
            this.BtnPreview.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnPreview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnPreview.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnPreview.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPreview.Image = global::Presentation.Properties.Resources.AccountJPG;
            this.BtnPreview.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnPreview.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.BtnPreview.Location = new System.Drawing.Point(1001, 71);
            this.BtnPreview.Name = "BtnPreview";
            this.tableLayoutPanel1.SetRowSpan(this.BtnPreview, 2);
            this.BtnPreview.Size = new System.Drawing.Size(127, 54);
            this.BtnPreview.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnPreview.TabIndex = 6;
            this.BtnPreview.Text = "Preview";
            this.BtnPreview.TextColor = System.Drawing.Color.White;
            this.BtnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(160)))), ((int)(((byte)(6)))));
            this.BtnClose.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BtnClose.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClose.Image = global::Presentation.Properties.Resources.close;
            this.BtnClose.ImageFixedSize = new System.Drawing.Size(30, 30);
            this.BtnClose.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.BtnClose.Location = new System.Drawing.Point(1001, 15);
            this.BtnClose.Name = "BtnClose";
            this.tableLayoutPanel1.SetRowSpan(this.BtnClose, 2);
            this.BtnClose.Size = new System.Drawing.Size(127, 50);
            this.BtnClose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnClose.TabIndex = 7;
            this.BtnClose.Text = "Close";
            this.BtnClose.TextColor = System.Drawing.Color.White;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderBottomWidth = 2;
            this.labelX1.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.labelX1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderLeftWidth = 2;
            this.labelX1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderRightWidth = 2;
            this.labelX1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.labelX1.BackgroundStyle.BorderTopWidth = 2;
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(3, 71);
            this.labelX1.Name = "labelX1";
            this.tableLayoutPanel1.SetRowSpan(this.labelX1, 2);
            this.labelX1.SingleLineColor = System.Drawing.Color.Transparent;
            this.labelX1.Size = new System.Drawing.Size(213, 54);
            this.labelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.labelX1.TabIndex = 88;
            this.labelX1.Text = "Search by Order or Tem Acc";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // ChkGrupTranByClintNo
            // 
            // 
            // 
            // 
            this.ChkGrupTranByClintNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkGrupTranByClintNo.CheckSignSize = new System.Drawing.Size(20, 20);
            this.ChkGrupTranByClintNo.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkGrupTranByClintNo.Location = new System.Drawing.Point(222, 71);
            this.ChkGrupTranByClintNo.Name = "ChkGrupTranByClintNo";
            this.ChkGrupTranByClintNo.Size = new System.Drawing.Size(317, 22);
            this.ChkGrupTranByClintNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkGrupTranByClintNo.TabIndex = 89;
            this.ChkGrupTranByClintNo.Text = "Group Transactions by Clint No";
            // 
            // ChkDisplayTemporaryAccounts
            // 
            // 
            // 
            // 
            this.ChkDisplayTemporaryAccounts.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChkDisplayTemporaryAccounts.CheckSignSize = new System.Drawing.Size(20, 20);
            this.ChkDisplayTemporaryAccounts.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDisplayTemporaryAccounts.Location = new System.Drawing.Point(222, 99);
            this.ChkDisplayTemporaryAccounts.Name = "ChkDisplayTemporaryAccounts";
            this.ChkDisplayTemporaryAccounts.Size = new System.Drawing.Size(317, 26);
            this.ChkDisplayTemporaryAccounts.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChkDisplayTemporaryAccounts.TabIndex = 90;
            this.ChkDisplayTemporaryAccounts.Text = "Display Temporary Accounts";
            this.ChkDisplayTemporaryAccounts.TextColor = System.Drawing.Color.Red;
            // 
            // tIdDataGridViewTextBoxColumn
            // 
            this.tIdDataGridViewTextBoxColumn.DataPropertyName = "T_Id";
            this.tIdDataGridViewTextBoxColumn.HeaderText = "T_Id";
            this.tIdDataGridViewTextBoxColumn.Name = "tIdDataGridViewTextBoxColumn";
            this.tIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // clientIdDataGridViewTextBoxColumn
            // 
            this.clientIdDataGridViewTextBoxColumn.DataPropertyName = "ClientId";
            this.clientIdDataGridViewTextBoxColumn.HeaderText = "ClientId";
            this.clientIdDataGridViewTextBoxColumn.Name = "clientIdDataGridViewTextBoxColumn";
            this.clientIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tDateDataGridViewTextBoxColumn
            // 
            this.tDateDataGridViewTextBoxColumn.DataPropertyName = "T_Date";
            this.tDateDataGridViewTextBoxColumn.HeaderText = "T_Date";
            this.tDateDataGridViewTextBoxColumn.Name = "tDateDataGridViewTextBoxColumn";
            this.tDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // priceofGoldDataGridViewTextBoxColumn
            // 
            this.priceofGoldDataGridViewTextBoxColumn.DataPropertyName = "Price_of_Gold";
            this.priceofGoldDataGridViewTextBoxColumn.HeaderText = "Price_of_Gold";
            this.priceofGoldDataGridViewTextBoxColumn.Name = "priceofGoldDataGridViewTextBoxColumn";
            this.priceofGoldDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // currentRatePerTolaDataGridViewTextBoxColumn
            // 
            this.currentRatePerTolaDataGridViewTextBoxColumn.DataPropertyName = "Current_Rate_Per_Tola";
            this.currentRatePerTolaDataGridViewTextBoxColumn.HeaderText = "Current_Rate_Per_Tola";
            this.currentRatePerTolaDataGridViewTextBoxColumn.Name = "currentRatePerTolaDataGridViewTextBoxColumn";
            this.currentRatePerTolaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // weightinGramDataGridViewTextBoxColumn
            // 
            this.weightinGramDataGridViewTextBoxColumn.DataPropertyName = "Weight_in_Gram";
            this.weightinGramDataGridViewTextBoxColumn.HeaderText = "Weight_in_Gram";
            this.weightinGramDataGridViewTextBoxColumn.Name = "weightinGramDataGridViewTextBoxColumn";
            this.weightinGramDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // labourPerTolaDataGridViewTextBoxColumn
            // 
            this.labourPerTolaDataGridViewTextBoxColumn.DataPropertyName = "Labour_Per_Tola";
            this.labourPerTolaDataGridViewTextBoxColumn.HeaderText = "Labour_Per_Tola";
            this.labourPerTolaDataGridViewTextBoxColumn.Name = "labourPerTolaDataGridViewTextBoxColumn";
            this.labourPerTolaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalLabourDataGridViewTextBoxColumn
            // 
            this.totalLabourDataGridViewTextBoxColumn.DataPropertyName = "Total_Labour";
            this.totalLabourDataGridViewTextBoxColumn.HeaderText = "Total_Labour";
            this.totalLabourDataGridViewTextBoxColumn.Name = "totalLabourDataGridViewTextBoxColumn";
            this.totalLabourDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // paymentPercntCutDataGridViewTextBoxColumn
            // 
            this.paymentPercntCutDataGridViewTextBoxColumn.DataPropertyName = "Payment_Percnt_Cut";
            this.paymentPercntCutDataGridViewTextBoxColumn.HeaderText = "Payment_Percnt_Cut";
            this.paymentPercntCutDataGridViewTextBoxColumn.Name = "paymentPercntCutDataGridViewTextBoxColumn";
            this.paymentPercntCutDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // netAmountDataGridViewTextBoxColumn
            // 
            this.netAmountDataGridViewTextBoxColumn.DataPropertyName = "Net_Amount";
            this.netAmountDataGridViewTextBoxColumn.HeaderText = "Net_Amount";
            this.netAmountDataGridViewTextBoxColumn.Name = "netAmountDataGridViewTextBoxColumn";
            this.netAmountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // polishPerTolaDataGridViewTextBoxColumn
            // 
            this.polishPerTolaDataGridViewTextBoxColumn.DataPropertyName = "Polish_Per_Tola";
            this.polishPerTolaDataGridViewTextBoxColumn.HeaderText = "Polish_Per_Tola";
            this.polishPerTolaDataGridViewTextBoxColumn.Name = "polishPerTolaDataGridViewTextBoxColumn";
            this.polishPerTolaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalPolishDataGridViewTextBoxColumn
            // 
            this.totalPolishDataGridViewTextBoxColumn.DataPropertyName = "Total_Polish";
            this.totalPolishDataGridViewTextBoxColumn.HeaderText = "Total_Polish";
            this.totalPolishDataGridViewTextBoxColumn.Name = "totalPolishDataGridViewTextBoxColumn";
            this.totalPolishDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cutPerTolaDataGridViewTextBoxColumn
            // 
            this.cutPerTolaDataGridViewTextBoxColumn.DataPropertyName = "Cut_Per_Tola";
            this.cutPerTolaDataGridViewTextBoxColumn.HeaderText = "Cut_Per_Tola";
            this.cutPerTolaDataGridViewTextBoxColumn.Name = "cutPerTolaDataGridViewTextBoxColumn";
            this.cutPerTolaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cutfromtotalWeightDataGridViewTextBoxColumn
            // 
            this.cutfromtotalWeightDataGridViewTextBoxColumn.DataPropertyName = "Cut_from_total_Weight";
            this.cutfromtotalWeightDataGridViewTextBoxColumn.HeaderText = "Cut_from_total_Weight";
            this.cutfromtotalWeightDataGridViewTextBoxColumn.Name = "cutfromtotalWeightDataGridViewTextBoxColumn";
            this.cutfromtotalWeightDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // weightinModeDataGridViewTextBoxColumn
            // 
            this.weightinModeDataGridViewTextBoxColumn.DataPropertyName = "Weightin_Mode";
            this.weightinModeDataGridViewTextBoxColumn.HeaderText = "Weightin_Mode";
            this.weightinModeDataGridViewTextBoxColumn.Name = "weightinModeDataGridViewTextBoxColumn";
            this.weightinModeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cutPriorityDataGridViewTextBoxColumn
            // 
            this.cutPriorityDataGridViewTextBoxColumn.DataPropertyName = "Cut_Priority";
            this.cutPriorityDataGridViewTextBoxColumn.HeaderText = "Cut_Priority";
            this.cutPriorityDataGridViewTextBoxColumn.Name = "cutPriorityDataGridViewTextBoxColumn";
            this.cutPriorityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // labourAndPolishCalculationDataGridViewTextBoxColumn
            // 
            this.labourAndPolishCalculationDataGridViewTextBoxColumn.DataPropertyName = "Labour_And_Polish_Calculation";
            this.labourAndPolishCalculationDataGridViewTextBoxColumn.HeaderText = "Labour_And_Polish_Calculation";
            this.labourAndPolishCalculationDataGridViewTextBoxColumn.Name = "labourAndPolishCalculationDataGridViewTextBoxColumn";
            this.labourAndPolishCalculationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // addedByDataGridViewTextBoxColumn
            // 
            this.addedByDataGridViewTextBoxColumn.DataPropertyName = "Added_By";
            this.addedByDataGridViewTextBoxColumn.HeaderText = "Added_By";
            this.addedByDataGridViewTextBoxColumn.Name = "addedByDataGridViewTextBoxColumn";
            this.addedByDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // addedDateDataGridViewTextBoxColumn
            // 
            this.addedDateDataGridViewTextBoxColumn.DataPropertyName = "Added_Date";
            this.addedDateDataGridViewTextBoxColumn.HeaderText = "Added_Date";
            this.addedDateDataGridViewTextBoxColumn.Name = "addedDateDataGridViewTextBoxColumn";
            this.addedDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // itmeNameDataGridViewTextBoxColumn
            // 
            this.itmeNameDataGridViewTextBoxColumn.DataPropertyName = "Itme_Name";
            this.itmeNameDataGridViewTextBoxColumn.HeaderText = "Itme_Name";
            this.itmeNameDataGridViewTextBoxColumn.Name = "itmeNameDataGridViewTextBoxColumn";
            this.itmeNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tranTypeDataGridViewTextBoxColumn
            // 
            this.tranTypeDataGridViewTextBoxColumn.DataPropertyName = "Tran_Type";
            this.tranTypeDataGridViewTextBoxColumn.HeaderText = "Tran_Type";
            this.tranTypeDataGridViewTextBoxColumn.Name = "tranTypeDataGridViewTextBoxColumn";
            this.tranTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tranStatusDataGridViewTextBoxColumn
            // 
            this.tranStatusDataGridViewTextBoxColumn.DataPropertyName = "Tran_Status";
            this.tranStatusDataGridViewTextBoxColumn.HeaderText = "Tran_Status";
            this.tranStatusDataGridViewTextBoxColumn.Name = "tranStatusDataGridViewTextBoxColumn";
            this.tranStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isTeleConfirmationDataGridViewTextBoxColumn
            // 
            this.isTeleConfirmationDataGridViewTextBoxColumn.DataPropertyName = "IsTeleConfirmation";
            this.isTeleConfirmationDataGridViewTextBoxColumn.HeaderText = "IsTeleConfirmation";
            this.isTeleConfirmationDataGridViewTextBoxColumn.Name = "isTeleConfirmationDataGridViewTextBoxColumn";
            this.isTeleConfirmationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // teleDateDataGridViewTextBoxColumn
            // 
            this.teleDateDataGridViewTextBoxColumn.DataPropertyName = "TeleDate";
            this.teleDateDataGridViewTextBoxColumn.HeaderText = "TeleDate";
            this.teleDateDataGridViewTextBoxColumn.Name = "teleDateDataGridViewTextBoxColumn";
            this.teleDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // purityDataGridViewTextBoxColumn
            // 
            this.purityDataGridViewTextBoxColumn.DataPropertyName = "Purity";
            this.purityDataGridViewTextBoxColumn.HeaderText = "Purity";
            this.purityDataGridViewTextBoxColumn.Name = "purityDataGridViewTextBoxColumn";
            this.purityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Edit
            // 
            this.Edit.DataPropertyName = "T_Id";
            this.Edit.HeaderText = "Edit";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.Edit.Text = "Edit";
            this.Edit.UseColumnTextForButtonValue = true;
            this.Edit.Width = 50;
            // 
            // TeleConfirmation
            // 
            this.TeleConfirmation.DataPropertyName = "T_Id";
            this.TeleConfirmation.HeaderText = "TeleConfirmation";
            this.TeleConfirmation.Name = "TeleConfirmation";
            this.TeleConfirmation.ReadOnly = true;
            this.TeleConfirmation.Text = "IsTele";
            this.TeleConfirmation.UseColumnTextForButtonValue = true;
            // 
            // DateWiseTransactionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(216)))), ((int)(((byte)(227)))));
            this.ClientSize = new System.Drawing.Size(1222, 693);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.dataGridViewX1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DateWiseTransactionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DateWiseTransactionsForm";
            this.Load += new System.EventHandler(this.DateWiseTransactionsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTransactionBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.ProgressBarX progressBarX1;
        private DevComponents.DotNetBar.ButtonX BtnGo;
        private DevComponents.DotNetBar.ButtonX BtnPrint;
        private DevComponents.DotNetBar.ButtonX BtnPreview;
        private DevComponents.DotNetBar.ButtonX BtnClose;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpFrom;
        private System.Windows.Forms.DateTimePicker DtpTo;
        private System.Windows.Forms.BindingSource tblTransactionBindingSource;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel4;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkGrupTranByClintNo;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChkDisplayTemporaryAccounts;
        private System.Windows.Forms.DataGridViewTextBoxColumn tIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceofGoldDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentRatePerTolaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightinGramDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn labourPerTolaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalLabourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentPercntCutDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn netAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn polishPerTolaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalPolishDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutPerTolaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutfromtotalWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightinModeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutPriorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn labourAndPolishCalculationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addedByDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addedDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn itmeNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tranTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tranStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isTeleConfirmationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teleDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn purityDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Edit;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn TeleConfirmation;

    }
}