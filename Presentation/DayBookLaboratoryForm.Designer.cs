﻿namespace Presentation
{
    partial class DayBookLaboratoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPrintReport = new DevComponents.DotNetBar.ButtonX();
            this.lblCurrentBalance = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.BtnExportToPdf = new DevComponents.DotNetBar.ButtonX();
            this.lblDaybook = new DevComponents.DotNetBar.LabelX();
            this.btnNewEntery = new DevComponents.DotNetBar.ButtonX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lblGoldDebitclor = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.lblGoldCreditclor = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.lblCashCreditclor = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.lblCashDebitclor = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Action = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblImpureGoldTotal = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.lblPurityTotal = new DevComponents.DotNetBar.LabelX();
            this.lblPureGoldTotal = new DevComponents.DotNetBar.LabelX();
            this.lblNetAmountTotal = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 384F));
            this.tableLayoutPanel2.Controls.Add(this.btnPrintReport, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblCurrentBalance, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX10, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.BtnExportToPdf, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 668);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1034, 42);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // btnPrintReport
            // 
            this.btnPrintReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrintReport.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnPrintReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnPrintReport.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintReport.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnPrintReport.Location = new System.Drawing.Point(274, 6);
            this.btnPrintReport.Name = "btnPrintReport";
            this.btnPrintReport.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnPrintReport.Size = new System.Drawing.Size(151, 30);
            this.btnPrintReport.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnPrintReport.TabIndex = 39;
            this.btnPrintReport.Text = "Print Report";
            this.btnPrintReport.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnPrintReport.Click += new System.EventHandler(this.btnPrintReport_Click);
            // 
            // lblCurrentBalance
            // 
            this.lblCurrentBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCurrentBalance.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCurrentBalance.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentBalance.Location = new System.Drawing.Point(648, 6);
            this.lblCurrentBalance.Name = "lblCurrentBalance";
            this.lblCurrentBalance.Size = new System.Drawing.Size(380, 30);
            this.lblCurrentBalance.TabIndex = 4;
            this.lblCurrentBalance.Text = "000";
            // 
            // labelX10
            // 
            this.labelX10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX10.Location = new System.Drawing.Point(434, 6);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(205, 30);
            this.labelX10.TabIndex = 5;
            this.labelX10.Text = "Current Balance :";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // BtnExportToPdf
            // 
            this.BtnExportToPdf.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnExportToPdf.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BtnExportToPdf.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.BtnExportToPdf.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExportToPdf.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.BtnExportToPdf.Location = new System.Drawing.Point(60, 6);
            this.BtnExportToPdf.Name = "BtnExportToPdf";
            this.BtnExportToPdf.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.BtnExportToPdf.Size = new System.Drawing.Size(151, 30);
            this.BtnExportToPdf.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.BtnExportToPdf.TabIndex = 38;
            this.BtnExportToPdf.Text = "Export to Pdf";
            this.BtnExportToPdf.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.BtnExportToPdf.Click += new System.EventHandler(this.BtnExportToPdf_Click);
            // 
            // lblDaybook
            // 
            // 
            // 
            // 
            this.lblDaybook.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDaybook.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDaybook.ForeColor = System.Drawing.Color.White;
            this.lblDaybook.Location = new System.Drawing.Point(12, 12);
            this.lblDaybook.Name = "lblDaybook";
            this.lblDaybook.Size = new System.Drawing.Size(436, 42);
            this.lblDaybook.TabIndex = 38;
            this.lblDaybook.Text = "Day Book Laboratory روز نامچہ";
            this.lblDaybook.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnNewEntery
            // 
            this.btnNewEntery.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNewEntery.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnNewEntery.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnNewEntery.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewEntery.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.btnNewEntery.Location = new System.Drawing.Point(842, 12);
            this.btnNewEntery.Name = "btnNewEntery";
            this.btnNewEntery.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor();
            this.btnNewEntery.Size = new System.Drawing.Size(151, 45);
            this.btnNewEntery.Style = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.btnNewEntery.TabIndex = 37;
            this.btnNewEntery.Text = "New Entery";
            this.btnNewEntery.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.btnNewEntery.Click += new System.EventHandler(this.btnNewEntery_Click);
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.ForeColor = System.Drawing.Color.White;
            this.labelX5.Location = new System.Drawing.Point(118, 25);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(71, 17);
            this.labelX5.TabIndex = 17;
            this.labelX5.Text = "سونا ادائیگی";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.ForeColor = System.Drawing.Color.White;
            this.labelX1.Location = new System.Drawing.Point(118, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(71, 16);
            this.labelX1.TabIndex = 16;
            this.labelX1.Text = "Gold Debit";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.ForeColor = System.Drawing.Color.White;
            this.labelX3.Location = new System.Drawing.Point(310, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(72, 16);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "Cash Debit";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGoldDebitclor
            // 
            this.lblGoldDebitclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblGoldDebitclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(149)))), ((int)(((byte)(164)))));
            // 
            // 
            // 
            this.lblGoldDebitclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGoldDebitclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldDebitclor.ForeColor = System.Drawing.Color.White;
            this.lblGoldDebitclor.Location = new System.Drawing.Point(99, 3);
            this.lblGoldDebitclor.Name = "lblGoldDebitclor";
            this.tableLayoutPanel1.SetRowSpan(this.lblGoldDebitclor, 2);
            this.lblGoldDebitclor.Size = new System.Drawing.Size(13, 39);
            this.lblGoldDebitclor.TabIndex = 8;
            this.lblGoldDebitclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.ForeColor = System.Drawing.Color.White;
            this.labelX7.Location = new System.Drawing.Point(310, 25);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(72, 17);
            this.labelX7.TabIndex = 14;
            this.labelX7.Text = " کیش ادائیگی";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGoldCreditclor
            // 
            this.lblGoldCreditclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblGoldCreditclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(181)))), ((int)(((byte)(174)))));
            // 
            // 
            // 
            this.lblGoldCreditclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGoldCreditclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoldCreditclor.ForeColor = System.Drawing.Color.White;
            this.lblGoldCreditclor.Location = new System.Drawing.Point(3, 3);
            this.lblGoldCreditclor.Name = "lblGoldCreditclor";
            this.tableLayoutPanel1.SetRowSpan(this.lblGoldCreditclor, 2);
            this.lblGoldCreditclor.Size = new System.Drawing.Size(13, 39);
            this.lblGoldCreditclor.TabIndex = 9;
            this.lblGoldCreditclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.White;
            this.labelX2.Location = new System.Drawing.Point(22, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(71, 16);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "Gold Credit";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.ForeColor = System.Drawing.Color.White;
            this.labelX6.Location = new System.Drawing.Point(22, 25);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(71, 17);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "سونا  وصولی";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblCashCreditclor
            // 
            this.lblCashCreditclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCashCreditclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(221)))), ((int)(((byte)(216)))));
            // 
            // 
            // 
            this.lblCashCreditclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCashCreditclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashCreditclor.ForeColor = System.Drawing.Color.White;
            this.lblCashCreditclor.Location = new System.Drawing.Point(195, 3);
            this.lblCashCreditclor.Name = "lblCashCreditclor";
            this.tableLayoutPanel1.SetRowSpan(this.lblCashCreditclor, 2);
            this.lblCashCreditclor.Size = new System.Drawing.Size(13, 39);
            this.lblCashCreditclor.TabIndex = 11;
            this.lblCashCreditclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.ForeColor = System.Drawing.Color.White;
            this.labelX4.Location = new System.Drawing.Point(214, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(71, 16);
            this.labelX4.TabIndex = 3;
            this.labelX4.Text = "Cash Credit";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.White;
            this.labelX9.Location = new System.Drawing.Point(214, 25);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(71, 17);
            this.labelX9.TabIndex = 18;
            this.labelX9.Text = " کیش وصولی";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblCashDebitclor
            // 
            this.lblCashDebitclor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCashDebitclor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(198)))), ((int)(((byte)(209)))));
            // 
            // 
            // 
            this.lblCashDebitclor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCashDebitclor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashDebitclor.ForeColor = System.Drawing.Color.White;
            this.lblCashDebitclor.Location = new System.Drawing.Point(291, 4);
            this.lblCashDebitclor.Name = "lblCashDebitclor";
            this.tableLayoutPanel1.SetRowSpan(this.lblCashDebitclor, 2);
            this.lblCashDebitclor.Size = new System.Drawing.Size(13, 37);
            this.lblCashDebitclor.TabIndex = 10;
            this.lblCashDebitclor.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.labelX5, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelX1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX3, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblGoldDebitclor, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX7, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblGoldCreditclor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCashCreditclor, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX4, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelX9, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCashDebitclor, 6, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(451, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(385, 45);
            this.tableLayoutPanel1.TabIndex = 41;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.lblDaybook);
            this.panel1.Controls.Add(this.btnNewEntery);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1034, 71);
            this.panel1.TabIndex = 4;
            // 
            // Action
            // 
            this.Action.HeaderText = "Action";
            this.Action.Name = "Action";
            this.Action.ReadOnly = true;
            this.Action.Text = "Button";
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Action});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 72);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.Size = new System.Drawing.Size(1034, 545);
            this.dataGridViewX1.TabIndex = 3;
            this.dataGridViewX1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellClick);
            this.dataGridViewX1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellContentClick);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanel3.ColumnCount = 8;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.92211F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.8031F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.953443F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.31814F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.602328F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.08729F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.63116F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.64032F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.lblImpureGoldTotal, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX16, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblPurityTotal, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblPureGoldTotal, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblNetAmountTotal, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX18, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX19, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX20, 6, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 617);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1034, 50);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // lblImpureGoldTotal
            // 
            this.lblImpureGoldTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblImpureGoldTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblImpureGoldTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImpureGoldTotal.ForeColor = System.Drawing.Color.Black;
            this.lblImpureGoldTotal.Location = new System.Drawing.Point(169, 6);
            this.lblImpureGoldTotal.Name = "lblImpureGoldTotal";
            this.lblImpureGoldTotal.Size = new System.Drawing.Size(122, 38);
            this.lblImpureGoldTotal.TabIndex = 0;
            this.lblImpureGoldTotal.Text = "impurGold";
            this.lblImpureGoldTotal.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX16
            // 
            this.labelX16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX16.Location = new System.Drawing.Point(6, 6);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(154, 38);
            this.labelX16.TabIndex = 6;
            this.labelX16.Text = "ImpureGoldTotal\r\n(کچا سونا)";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // lblPurityTotal
            // 
            this.lblPurityTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblPurityTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPurityTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPurityTotal.ForeColor = System.Drawing.Color.Black;
            this.lblPurityTotal.Location = new System.Drawing.Point(383, 6);
            this.lblPurityTotal.Name = "lblPurityTotal";
            this.lblPurityTotal.Size = new System.Drawing.Size(118, 38);
            this.lblPurityTotal.TabIndex = 2;
            this.lblPurityTotal.Text = "Purity";
            this.lblPurityTotal.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblPureGoldTotal
            // 
            this.lblPureGoldTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblPureGoldTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPureGoldTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPureGoldTotal.ForeColor = System.Drawing.Color.Black;
            this.lblPureGoldTotal.Location = new System.Drawing.Point(609, 6);
            this.lblPureGoldTotal.Name = "lblPureGoldTotal";
            this.lblPureGoldTotal.Size = new System.Drawing.Size(95, 38);
            this.lblPureGoldTotal.TabIndex = 3;
            this.lblPureGoldTotal.Text = "PureGold";
            this.lblPureGoldTotal.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblNetAmountTotal
            // 
            this.lblNetAmountTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblNetAmountTotal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNetAmountTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetAmountTotal.ForeColor = System.Drawing.Color.Black;
            this.lblNetAmountTotal.Location = new System.Drawing.Point(853, 6);
            this.lblNetAmountTotal.Name = "lblNetAmountTotal";
            this.lblNetAmountTotal.Size = new System.Drawing.Size(175, 38);
            this.lblNetAmountTotal.TabIndex = 5;
            this.lblNetAmountTotal.Text = "NetAmmount";
            // 
            // labelX18
            // 
            this.labelX18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX18.Location = new System.Drawing.Point(300, 6);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(74, 38);
            this.labelX18.TabIndex = 8;
            this.labelX18.Text = "PurityTotal\r\n()";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX19
            // 
            this.labelX19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX19.Location = new System.Drawing.Point(510, 6);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(90, 38);
            this.labelX19.TabIndex = 9;
            this.labelX19.Text = "PureGoldTotal\r\n(خالص سونا)";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX20
            // 
            this.labelX20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(93)))), ((int)(((byte)(107)))));
            this.labelX20.Location = new System.Drawing.Point(713, 6);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(131, 38);
            this.labelX20.TabIndex = 10;
            this.labelX20.Text = "NetAmountTotal\r\n(ادائیگی رقم)";
            this.labelX20.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // DayBookLaboratoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(225)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(1034, 711);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewX1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "DayBookLaboratoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Day Book Laboratory";
            this.Load += new System.EventHandler(this.DayBookLaboratoryForm_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevComponents.DotNetBar.LabelX lblCurrentBalance;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX lblDaybook;
        private DevComponents.DotNetBar.ButtonX btnNewEntery;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lblGoldDebitclor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX lblGoldCreditclor;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX lblCashCreditclor;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX lblCashDebitclor;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Action;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevComponents.DotNetBar.LabelX lblImpureGoldTotal;
        private DevComponents.DotNetBar.LabelX lblPurityTotal;
        private DevComponents.DotNetBar.LabelX lblPureGoldTotal;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.LabelX lblNetAmountTotal;
        private DevComponents.DotNetBar.ButtonX BtnExportToPdf;
        private DevComponents.DotNetBar.ButtonX btnPrintReport;
    }
}