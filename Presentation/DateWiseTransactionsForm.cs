﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class DateWiseTransactionsForm : Form
    {
        public DateWiseTransactionsForm(Data.tblTransaction obj)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            if (obj == null || obj.T_Id == 0)
            {
                tblTransactionBindingSource.DataSource = new tblTransaction();
            }
            else
            {
                tblTransactionBindingSource.DataSource = obj;
            }
        }
        public DateWiseTransactionsForm(List<tblTransaction> tblObj)
        {
            InitializeComponent();
            tblTransactionBindingSource.DataSource = tblObj;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void LoadGVTransaction()
        {
            try
            {
                if (DtpFrom.Text != "" && DtpTo.Text != "")
                {
                    if (ChkGrupTranByClintNo.Checked == true && ChkDisplayTemporaryAccounts.Checked == false)
                    {
                        List<tblTransaction> tblObj = TransactionServicesClass.GetTranByDateAndOrderByCID(DtpFrom.Value.Date, DtpTo.Value);
                        if (tblObj != null) { tblTransactionBindingSource.DataSource = tblObj; }
                        else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else if (ChkDisplayTemporaryAccounts.Checked == true && ChkGrupTranByClintNo.Checked == false)
                    {
                        List<tblTransaction> tblObj = TransactionServicesClass.GetTranByDateAndTempAccount(DtpFrom.Value.Date, DtpTo.Value);
                        if (tblObj != null) { tblTransactionBindingSource.DataSource = tblObj; }
                        else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else if (ChkDisplayTemporaryAccounts.Checked == true && ChkGrupTranByClintNo.Checked == true)
                    {
                        List<tblTransaction> tblObj = TransactionServicesClass.GetTranByDateAndTmpAccOrByCId(DtpFrom.Value.Date, DtpTo.Value);
                        if (tblObj != null) { tblTransactionBindingSource.DataSource = tblObj; }
                        else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        List<tblTransaction> tblObj = TransactionServicesClass.GetTranByDate(DtpFrom.Value.Date, DtpTo.Value);
                        if (tblObj != null) { tblTransactionBindingSource.DataSource = tblObj; }
                        else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception exp) { MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        private void BtnGo_Click(object sender, EventArgs e)
        {
            LoadGVTransaction();
        }
        private void DateWiseTransactionsForm_Load(object sender, EventArgs e)
        {
            LoadGVTransaction();
            
        }
        private void dataGridViewX1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (tblTransactionBindingSource == null)
                    return;

                tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                int id = Convert.ToInt32(tblObj.T_Id);

                if (id != 0 && e.ColumnIndex == 25)
                {
                    tblTransaction tbltrObj = TransactionServicesClass.GetByIdTransaction(id);
                    if (tbltrObj != null)
                    {
                        Presentation.TransactionForm jwfrm = new TransactionForm(tbltrObj);
                        jwfrm.ShowDialog();
                    }
                    else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
                if (id != 0 && e.ColumnIndex == 26)
                {
                    tblTransaction tbltrObj = TransactionServicesClass.GetByIdTransaction(id);
                    //tblTransaction tbltrObj = TransactionServicesClass.UpdateTeleConfimationTruebyIdTransaction(tbltrObj);
                    if (tbltrObj != null)
                    {
                        DialogResult res = MessageBox.Show("Are you sure you want to Update Tele Confimation", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (res == DialogResult.OK)
                        {
                            if (tbltrObj.IsTeleConfirmation == false)
                            {
                                TransactionServicesClass.UpdateTeleConfimationTruebyIdTransaction(tblTransactionBindingSource.Current as tblTransaction);
                                MessageBox.Show("Transaction Data Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                LoadGVTransaction();
                            }
                            else
                            {
                                TransactionServicesClass.UpdateTeleConfimationFalsebyIdTransaction(tblTransactionBindingSource.Current as tblTransaction);
                                MessageBox.Show("Transaction Data Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                LoadGVTransaction();
                            } 
                        }
                        if (res == DialogResult.Cancel)
                        {
                        }   
                    }
                    else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void DtpTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {

        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                //tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;
                //int id = Convert.ToInt32(tblObj.T_Id);
                //tblTransaction tbltrObj = TransactionServicesClass.GetById(id);
                //if (tbltrObj == null)
                //{
                //    Presentation.DateWiseSummeryForm jwfrm = new DateWiseSummeryForm();
                //    jwfrm.ShowDialog();

                //}
            }
            catch(Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
