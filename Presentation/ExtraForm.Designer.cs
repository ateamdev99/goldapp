﻿namespace Presentation
{
    partial class ExtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rdbTMR = new System.Windows.Forms.RadioButton();
            this.rdbGrams = new System.Windows.Forms.RadioButton();
            this.rdbBoth = new System.Windows.Forms.RadioButton();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.chkActivateAcc = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkTemporaryAcc = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkBalncSMS = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkTransactionSMS = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkCustomSMS = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.tabStrip1 = new DevComponents.DotNetBar.TabStrip();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControl1 = new DevComponents.DotNetBar.TabControl();
            this.superTabStrip1 = new DevComponents.DotNetBar.SuperTabStrip();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabItem3 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.tabItem4 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.tabItem5 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabItem6 = new DevComponents.DotNetBar.TabItem(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Presentation.Properties.Resources.actorinfo;
            this.pictureBox1.Location = new System.Drawing.Point(225, 94);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 119;
            this.pictureBox1.TabStop = false;
            // 
            // groupPanel2
            // 
            this.groupPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.rdbTMR);
            this.groupPanel2.Controls.Add(this.rdbGrams);
            this.groupPanel2.Controls.Add(this.rdbBoth);
            this.groupPanel2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel2.Location = new System.Drawing.Point(216, 22);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(294, 65);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 121;
            this.groupPanel2.Text = "Balance SMS Gold Weight ";
            // 
            // rdbTMR
            // 
            this.rdbTMR.AutoSize = true;
            this.rdbTMR.BackColor = System.Drawing.Color.Transparent;
            this.rdbTMR.Checked = true;
            this.rdbTMR.Font = new System.Drawing.Font("Segoe UI Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbTMR.ForeColor = System.Drawing.Color.Black;
            this.rdbTMR.Location = new System.Drawing.Point(203, 4);
            this.rdbTMR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbTMR.Name = "rdbTMR";
            this.rdbTMR.Size = new System.Drawing.Size(56, 21);
            this.rdbTMR.TabIndex = 2;
            this.rdbTMR.TabStop = true;
            this.rdbTMR.Text = "TMR";
            this.rdbTMR.UseVisualStyleBackColor = false;
            // 
            // rdbGrams
            // 
            this.rdbGrams.AutoSize = true;
            this.rdbGrams.BackColor = System.Drawing.Color.Transparent;
            this.rdbGrams.Font = new System.Drawing.Font("Segoe UI Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGrams.ForeColor = System.Drawing.Color.Black;
            this.rdbGrams.Location = new System.Drawing.Point(113, 4);
            this.rdbGrams.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbGrams.Name = "rdbGrams";
            this.rdbGrams.Size = new System.Drawing.Size(66, 21);
            this.rdbGrams.TabIndex = 1;
            this.rdbGrams.Text = "Grams";
            this.rdbGrams.UseVisualStyleBackColor = false;
            // 
            // rdbBoth
            // 
            this.rdbBoth.AutoSize = true;
            this.rdbBoth.BackColor = System.Drawing.Color.Transparent;
            this.rdbBoth.Font = new System.Drawing.Font("Segoe UI Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbBoth.ForeColor = System.Drawing.Color.Black;
            this.rdbBoth.Location = new System.Drawing.Point(36, 4);
            this.rdbBoth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbBoth.Name = "rdbBoth";
            this.rdbBoth.Size = new System.Drawing.Size(56, 21);
            this.rdbBoth.TabIndex = 0;
            this.rdbBoth.Text = "Both";
            this.rdbBoth.UseVisualStyleBackColor = false;
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.chkActivateAcc);
            this.groupPanel1.Controls.Add(this.chkTemporaryAcc);
            this.groupPanel1.Controls.Add(this.chkBalncSMS);
            this.groupPanel1.Controls.Add(this.chkTransactionSMS);
            this.groupPanel1.Controls.Add(this.chkCustomSMS);
            this.groupPanel1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPanel1.Location = new System.Drawing.Point(48, 12);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(155, 201);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 120;
            this.groupPanel1.Text = "Account Type";
            this.groupPanel1.Click += new System.EventHandler(this.groupPanel1_Click);
            // 
            // chkActivateAcc
            // 
            this.chkActivateAcc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkActivateAcc.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkActivateAcc.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkActivateAcc.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.chkActivateAcc.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkActivateAcc.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkActivateAcc.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkActivateAcc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkActivateAcc.CheckBoxPosition = DevComponents.DotNetBar.eCheckBoxPosition.Right;
            this.chkActivateAcc.CheckSignSize = new System.Drawing.Size(20, 20);
            this.chkActivateAcc.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActivateAcc.Location = new System.Drawing.Point(2, 15);
            this.chkActivateAcc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkActivateAcc.Name = "chkActivateAcc";
            this.chkActivateAcc.Size = new System.Drawing.Size(145, 25);
            this.chkActivateAcc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkActivateAcc.TabIndex = 127;
            this.chkActivateAcc.Text = "Activate Acount :   ";
            this.chkActivateAcc.TextColor = System.Drawing.Color.Black;
            // 
            // chkTemporaryAcc
            // 
            this.chkTemporaryAcc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkTemporaryAcc.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkTemporaryAcc.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTemporaryAcc.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.chkTemporaryAcc.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTemporaryAcc.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTemporaryAcc.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTemporaryAcc.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkTemporaryAcc.CheckBoxPosition = DevComponents.DotNetBar.eCheckBoxPosition.Right;
            this.chkTemporaryAcc.CheckSignSize = new System.Drawing.Size(20, 20);
            this.chkTemporaryAcc.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTemporaryAcc.Location = new System.Drawing.Point(8, 44);
            this.chkTemporaryAcc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkTemporaryAcc.Name = "chkTemporaryAcc";
            this.chkTemporaryAcc.Size = new System.Drawing.Size(139, 29);
            this.chkTemporaryAcc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkTemporaryAcc.TabIndex = 128;
            this.chkTemporaryAcc.Text = "Temporary Acount :";
            this.chkTemporaryAcc.TextColor = System.Drawing.Color.Black;
            // 
            // chkBalncSMS
            // 
            this.chkBalncSMS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkBalncSMS.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkBalncSMS.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkBalncSMS.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.chkBalncSMS.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkBalncSMS.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkBalncSMS.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkBalncSMS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkBalncSMS.CheckBoxPosition = DevComponents.DotNetBar.eCheckBoxPosition.Right;
            this.chkBalncSMS.CheckSignSize = new System.Drawing.Size(20, 20);
            this.chkBalncSMS.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBalncSMS.Location = new System.Drawing.Point(6, 102);
            this.chkBalncSMS.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkBalncSMS.Name = "chkBalncSMS";
            this.chkBalncSMS.Size = new System.Drawing.Size(141, 27);
            this.chkBalncSMS.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkBalncSMS.TabIndex = 131;
            this.chkBalncSMS.Text = "Balance SMS :";
            this.chkBalncSMS.TextColor = System.Drawing.Color.Black;
            // 
            // chkTransactionSMS
            // 
            this.chkTransactionSMS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkTransactionSMS.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkTransactionSMS.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTransactionSMS.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.chkTransactionSMS.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTransactionSMS.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTransactionSMS.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkTransactionSMS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkTransactionSMS.CheckBoxPosition = DevComponents.DotNetBar.eCheckBoxPosition.Right;
            this.chkTransactionSMS.CheckSignSize = new System.Drawing.Size(20, 20);
            this.chkTransactionSMS.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTransactionSMS.Location = new System.Drawing.Point(6, 73);
            this.chkTransactionSMS.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkTransactionSMS.Name = "chkTransactionSMS";
            this.chkTransactionSMS.Size = new System.Drawing.Size(139, 28);
            this.chkTransactionSMS.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkTransactionSMS.TabIndex = 129;
            this.chkTransactionSMS.Text = "Transaction SMS :";
            this.chkTransactionSMS.TextColor = System.Drawing.Color.Black;
            // 
            // chkCustomSMS
            // 
            this.chkCustomSMS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkCustomSMS.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkCustomSMS.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkCustomSMS.BackgroundStyle.BorderColor = System.Drawing.Color.White;
            this.chkCustomSMS.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkCustomSMS.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkCustomSMS.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.chkCustomSMS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkCustomSMS.CheckBoxPosition = DevComponents.DotNetBar.eCheckBoxPosition.Right;
            this.chkCustomSMS.CheckSignSize = new System.Drawing.Size(20, 20);
            this.chkCustomSMS.Font = new System.Drawing.Font("Segoe UI Black", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCustomSMS.Location = new System.Drawing.Point(6, 129);
            this.chkCustomSMS.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkCustomSMS.Name = "chkCustomSMS";
            this.chkCustomSMS.Size = new System.Drawing.Size(141, 30);
            this.chkCustomSMS.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkCustomSMS.TabIndex = 130;
            this.chkCustomSMS.Text = "Custom SMS :";
            this.chkCustomSMS.TextColor = System.Drawing.Color.Black;
            // 
            // tabStrip1
            // 
            this.tabStrip1.AutoSelectAttachedControl = true;
            this.tabStrip1.CanReorderTabs = true;
            this.tabStrip1.CloseButtonVisible = true;
            this.tabStrip1.Location = new System.Drawing.Point(255, 320);
            this.tabStrip1.Name = "tabStrip1";
            this.tabStrip1.SelectedTab = this.tabItem6;
            this.tabStrip1.Size = new System.Drawing.Size(378, 264);
            this.tabStrip1.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005;
            this.tabStrip1.TabIndex = 122;
            this.tabStrip1.Tabs.Add(this.tabItem1);
            this.tabStrip1.Tabs.Add(this.tabItem5);
            this.tabStrip1.Tabs.Add(this.tabItem6);
            this.tabStrip1.Text = "tabStrip1";
            // 
            // tabItem1
            // 
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "tabItem1";
            // 
            // tabControl1
            // 
            this.tabControl1.CanReorderTabs = true;
            this.tabControl1.Controls.Add(this.tabControlPanel2);
            this.tabControl1.Controls.Add(this.tabControlPanel3);
            this.tabControl1.Location = new System.Drawing.Point(748, 60);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabControl1.SelectedTabIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(306, 212);
            this.tabControl1.TabIndex = 123;
            this.tabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl1.Tabs.Add(this.tabItem3);
            this.tabControl1.Tabs.Add(this.tabItem4);
            this.tabControl1.Text = "tabControl1";
            // 
            // superTabStrip1
            // 
            this.superTabStrip1.AutoSelectAttachedControl = false;
            // 
            // 
            // 
            this.superTabStrip1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.superTabStrip1.ContainerControlProcessDialogKey = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabStrip1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabStrip1.ControlBox.MenuBox.Name = "";
            this.superTabStrip1.ControlBox.Name = "";
            this.superTabStrip1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabStrip1.ControlBox.MenuBox,
            this.superTabStrip1.ControlBox.CloseBox});
            this.superTabStrip1.Location = new System.Drawing.Point(59, 247);
            this.superTabStrip1.Name = "superTabStrip1";
            this.superTabStrip1.ReorderTabsEnabled = true;
            this.superTabStrip1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabStrip1.SelectedTabIndex = 0;
            this.superTabStrip1.Size = new System.Drawing.Size(236, 25);
            this.superTabStrip1.TabCloseButtonHot = null;
            this.superTabStrip1.TabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabStrip1.TabIndex = 124;
            this.superTabStrip1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1});
            this.superTabStrip1.Text = "superTabStrip1";
            // 
            // superTabItem1
            // 
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "superTabItem1";
            // 
            // superTabControl1
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.MenuBox,
            this.superTabControl1.ControlBox.CloseBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Location = new System.Drawing.Point(748, 335);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(297, 190);
            this.superTabControl1.TabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl1.TabIndex = 125;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem2});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel1;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "superTabItem2";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 25);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(297, 165);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem2;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(32, 335);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(201, 214);
            this.tabControl2.TabIndex = 126;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(193, 188);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(192, 74);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabItem3
            // 
            this.tabItem3.AttachedControl = this.tabControlPanel2;
            this.tabItem3.Name = "tabItem3";
            this.tabItem3.Text = "tabItem3";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(306, 186);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tabItem3;
            // 
            // tabItem4
            // 
            this.tabItem4.AttachedControl = this.tabControlPanel3;
            this.tabItem4.Name = "tabItem4";
            this.tabItem4.Text = "tabItem4";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(306, 186);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.tabItem4;
            // 
            // tabItem5
            // 
            this.tabItem5.Name = "tabItem5";
            this.tabItem5.Text = "tabItem5";
            // 
            // tabItem6
            // 
            this.tabItem6.Name = "tabItem6";
            this.tabItem6.Text = "tabItem6";
            // 
            // ExtraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 661);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.superTabStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tabStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.groupPanel2);
            this.Name = "ExtraForm";
            this.Text = "ExtraForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.RadioButton rdbTMR;
        private System.Windows.Forms.RadioButton rdbGrams;
        private System.Windows.Forms.RadioButton rdbBoth;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkActivateAcc;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkTemporaryAcc;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkBalncSMS;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkTransactionSMS;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkCustomSMS;
        private DevComponents.DotNetBar.TabStrip tabStrip1;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private DevComponents.DotNetBar.TabControl tabControl1;
        private DevComponents.DotNetBar.SuperTabStrip superTabStrip1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.TabItem tabItem6;
        private DevComponents.DotNetBar.TabItem tabItem5;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tabItem3;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem tabItem4;
    }
}