﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class LaboratoryAllRecordsForm : Form
    {
        public LaboratoryAllRecordsForm()
        {
            InitializeComponent();
        }

        private void LaboratoryAllRecordsForm_Load(object sender, EventArgs e)
        {
            //dtpTo.MaxDate = DateTime.Today.AddDays(1);
            //dtpFrom.MaxDate = DateTime.Today.AddDays(1);
            DGVPopulate();
            CmbTransTypePopulate();
        }
        
        void DGVPopulate()
        {
            try
            {
                DataTable dt = Core.DBUtils.GetDataTable("SELECT  t.T_Id, c.C_Id, c.Name,t.Itme_Name, tt.T_TypeName,t.Tran_Status, tt.T_Id as TrTypeId, t.ImpureGoldinGram,t.Sum,t.Purity,t.PureGoldinGram,t.Net_Amount,t.Added_Date FROM tblTransaction as t INNER JOIN tblClient as c ON t.ClientId = c.C_Id INNER JOIN tblTransactionType as tt ON t.Tran_Type = tt.T_Id  WHERE  IsDeleted =0 and Tran_Status !='Jewellers'");
                DataRow dr = dt.NewRow();
                double openingBlancce = 0;
                double openingBlanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient  WHERE AddedDate<'" + DateTime.Now.Date + "' "));
                double openingBlanceGoldDebit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebit FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));
                double openingBlanceGoldCredit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCredit FROM tblTransaction WHERE Tran_Type=1 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));

                openingBlancce = (openingBlanceGoldCredit - openingBlanceGoldDebit + openingBlanceGoldAllClients);

                if (openingBlancce == 0) { openingBlancce = 0; }
                dr["Name"] = "Opening Balance";
                dr["T_TypeName"] = "";
                dr["Itme_Name"] = "Date:" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                dr["ImpureGoldinGram"] = openingBlancce;

                dt.Rows.InsertAt(dr, 0);
                dataGridViewX1.DataSource = dt;
                int n = dataGridViewX1.Columns.Count;
                dataGridViewX1.Columns["Action"].DisplayIndex = 13;
                dataGridViewX1.Columns["TrTypeId"].DisplayIndex = 5;
                dataGridViewX1.Columns["T_Id"].Visible = false;
                dataGridViewX1.Columns["C_Id"].Visible = false;
                dataGridViewX1.Columns["TrTypeId"].Visible = false;
                //dataGridViewX1.Columns["Action"].DataPropertyName = "T_Id";
                dataGridViewX1.Columns["Name"].HeaderText = "Client Name";
                dataGridViewX1.Columns["Itme_Name"].HeaderText = "Discription";
                //dataGridViewX1.Columns["Name"].Width = 198;
                dataGridViewX1.Columns["Action"].Width = 100;
                dataGridViewX1.Columns["T_TypeName"].HeaderText = "Transaction Type";
                dataGridViewX1.Columns["Tran_Status"].HeaderText = "Tran_Status";

                dataGridViewX1.Columns["ImpureGoldinGram"].HeaderText = "ImpureGold in Gram";
                dataGridViewX1.Columns["Sum"].HeaderText = "Sum";
                //dataGridViewX1.Columns["Sum"].Width = 100;
                dataGridViewX1.Columns["Purity"].HeaderText = "Purity";
                dataGridViewX1.Columns["PureGoldinGram"].HeaderText = "Pure Gold in Gram";
                dataGridViewX1.Columns["Net_Amount"].HeaderText = "Net Amount PKR";
                dataGridViewX1.Columns["Added_Date"].HeaderText = "Added Date";
                for (int i = 1; i < dataGridViewX1.RowCount; i++)
                {
                    if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "1")
                    {
                        if (dataGridViewX1.Rows[i].Cells["Tran_Status"].Value.ToString() == "RefineGold")
                        { dataGridViewX1.Rows[i].Cells["Action"].Value = "List"; }
                        else { dataGridViewX1.Rows[i].Cells["Action"].Value = "Add"; }

                    }
                    else if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "2")
                    { dataGridViewX1.Rows[i].Cells["Action"].Value = "Edit"; }
                    else { dataGridViewX1.Rows[i].Cells["Action"].Value = "History"; }
                }
                int columnIndex = 7;
                for (int i = 0; i < dataGridViewX1.RowCount; i++)
                {
                    if (i == 0)
                    {
                        if (openingBlancce > 0)
                        {
                            dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                        }
                        else { dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Red; }
                        dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.Moccasin;

                        dataGridViewX1.Rows[i].DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 11, FontStyle.Bold);
                    }
                    else
                    {
                        if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 1)
                        {
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(160, 181, 174);
                        }
                        if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 2)
                        {
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(69, 149, 164);
                        }

                        if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 3)
                        {
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(217, 221, 216);
                        }
                        if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 4)
                        {
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(152, 198, 209);
                        }
                        if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 5)
                        {
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 148, 154);
                        }
                    }
                }
                double currentBlance = 0;
                double openingbalanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient "));
                double GoldCreditCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCreditCurrentBalance FROM tblTransaction WHERE Tran_Type = 1 and IsDeleted = 0 "));
                double GoldDebitCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebitCurrentBalance FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 "));
                currentBlance = (GoldCreditCurrentBalance - GoldDebitCurrentBalance + openingbalanceGoldAllClients);
                if (currentBlance < 0)
                {
                    lblCurrentBalance.ForeColor = Color.Red;
                    lblCurrentBalance.Text = currentBlance + " in Grams";
                }
                else
                {
                    lblCurrentBalance.ForeColor = Color.Green;
                    lblCurrentBalance.Text = currentBlance + " in Grams";
                }
                double TotalImpureGold = 0;
                double TotalPurity = 0;
                double TotalPureGoldinGram = 0;
                double TotalNet_Amount = 0;

                for (int i = 1; i < dataGridViewX1.Rows.Count; i++)
                {
                    TotalImpureGold += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["ImpureGoldinGram"].Value);

                    TotalPurity += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["Purity"].Value);
                    TotalPureGoldinGram += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["PureGoldinGram"].Value);
                    TotalNet_Amount += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["Net_Amount"].Value);

                }

                lblImpureGoldTotal.Text = TotalImpureGold.ToString();
                lblPurityTotal.Text = TotalPurity.ToString();
                lblPureGoldTotal.Text = TotalPureGoldinGram.ToString();
                lblNetAmountTotal.Text = TotalNet_Amount.ToString();
            }
            catch (Exception exp) { MessageBox.Show("Error In Populating DGV Data.", exp.Message); }
        }

        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridViewX1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (e.ColumnIndex == 0 && e.RowIndex > 0)
            {
                try
                {
                    //int row = Convert.ToInt32(dataGridViewX1.DataKeys[id].Value.ToString());
                    if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "") { }
                    else
                    {
                        if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "Add")
                        {
                            int id = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                            Presentation.AddSelectCustomerForm jwfrm = new AddSelectCustomerForm(id);
                            jwfrm.ShowDialog();
                            DGVPopulate();
                        }
                        else if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "Edit")
                        {
                            int id = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                            int ClientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                            Presentation.HistoryForm jwfrm = new HistoryForm(ClientId);
                            //tblTransaction obj = new tblTransaction();
                            //obj.T_Id = id;
                            //Presentation.LaboratoryForm jwfrm = new LaboratoryForm(obj);
                            jwfrm.ShowDialog();
                            DGVPopulate();
                        }
                        else if (Convert.ToString(dataGridViewX1.Rows[e.RowIndex].Cells["Action"].Value) == "List")
                        {
                            int id = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["T_Id"].Value);
                            int ClientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                            Presentation.RefineGoldHistoryForm jwfrm = new RefineGoldHistoryForm();
                            //tblTransaction obj = new tblTransaction();
                            //obj.T_Id = id;
                            //Presentation.LaboratoryForm jwfrm = new LaboratoryForm(obj);
                            jwfrm.ShowDialog();
                            DGVPopulate();

                        }
                        else
                        {
                            int ClientId = Convert.ToInt32(dataGridViewX1.Rows[e.RowIndex].Cells["C_Id"].Value);
                            Presentation.HistoryLaboratoryForm jwfrm = new HistoryLaboratoryForm(ClientId);
                            jwfrm.ShowDialog();
                            DGVPopulate();
                        }
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Error Catched", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            }
        }
        void CmbTransTypePopulate()
        {
            try
            {
                using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
                {
                    var QueryReturn = (from t in db.tblTransactionTypes where t.T_Id != 5 select new { t.T_Id, t.T_TypeName }).ToList();
                    cmbTransType.DataSource = QueryReturn;
                    cmbTransType.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbTransType.AutoCompleteSource = AutoCompleteSource.ListItems;
                    if (QueryReturn.Count > 0)
                    {

                        cmbTransType.SelectedIndex = -1;
                        
                    }
                }
            }
            catch (Exception exp) { MessageBox.Show("Error In Populating Clients.", exp.Message); }
        }
        void LoadDGVLaboratoryRecordsBetwenDate()
        {
            try
            {
                if (dtpFrom.Text != "" && dtpTo.Text != "")
                {


                    DataTable dt = Core.DBUtils.GetDataTable("SELECT  t.T_Id, c.C_Id, c.Name,t.Itme_Name, tt.T_TypeName,t.Tran_Status, tt.T_Id as TrTypeId, t.ImpureGoldinGram,t.Sum,t.Purity,t.PureGoldinGram,t.Net_Amount,t.Added_Date FROM tblTransaction as t INNER JOIN tblClient as c ON t.ClientId = c.C_Id INNER JOIN tblTransactionType as tt ON t.Tran_Type = tt.T_Id  WHERE  IsDeleted =0 and Tran_Status !='Jewellers' and T_Date BETWEEN '" + dtpFrom.Value + "' AND '" + dtpTo.Value + "'");
                    DataRow dr = dt.NewRow();
                    double openingBlancce = 0;
                    double openingBlanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient  WHERE AddedDate<'" + DateTime.Now.Date + "' "));
                    double openingBlanceGoldDebit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebit FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));
                    double openingBlanceGoldCredit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCredit FROM tblTransaction WHERE Tran_Type=1 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));

                    openingBlancce = (openingBlanceGoldCredit - openingBlanceGoldDebit + openingBlanceGoldAllClients);
                    if (openingBlancce == 0) { openingBlancce = 0; }
                    dr["Name"] = "Opening Balance";
                    dr["T_TypeName"] = "";
                    dr["Itme_Name"] = "Date:" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                    dr["ImpureGoldinGram"] = openingBlancce;
                    dt.Rows.InsertAt(dr, 0);
                    dataGridViewX1.DataSource = dt;
                    int n = dataGridViewX1.Columns.Count;
                    dataGridViewX1.Columns["Action"].DisplayIndex = 13;
                    dataGridViewX1.Columns["TrTypeId"].DisplayIndex = 5;
                    dataGridViewX1.Columns["T_Id"].Visible = false;
                    dataGridViewX1.Columns["C_Id"].Visible = false;
                    dataGridViewX1.Columns["TrTypeId"].Visible = false;
                    dataGridViewX1.Columns["Name"].HeaderText = "Client Name";
                    dataGridViewX1.Columns["Itme_Name"].HeaderText = "Discription";
                    dataGridViewX1.Columns["Action"].Width = 100;
                    dataGridViewX1.Columns["T_TypeName"].HeaderText = "Transaction Type";
                    dataGridViewX1.Columns["Tran_Status"].HeaderText = "Tran_Status";
                    dataGridViewX1.Columns["ImpureGoldinGram"].HeaderText = "ImpureGold in Gram";
                    dataGridViewX1.Columns["Sum"].HeaderText = "Sum";
                    dataGridViewX1.Columns["Purity"].HeaderText = "Purity";
                    dataGridViewX1.Columns["PureGoldinGram"].HeaderText = "Pure Gold in Gram";
                    dataGridViewX1.Columns["Net_Amount"].HeaderText = "Net Amount PKR";
                    dataGridViewX1.Columns["Added_Date"].HeaderText = "Added Date";
                    for (int i = 1; i < dataGridViewX1.RowCount; i++)
                    {
                        if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "1")
                        {
                            if (dataGridViewX1.Rows[i].Cells["Tran_Status"].Value.ToString() == "RefineGold")
                            { dataGridViewX1.Rows[i].Cells["Action"].Value = "List"; }
                            else { dataGridViewX1.Rows[i].Cells["Action"].Value = "Add"; }

                        }
                        else if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "2")
                        { dataGridViewX1.Rows[i].Cells["Action"].Value = "Edit"; }
                        else { dataGridViewX1.Rows[i].Cells["Action"].Value = "History"; }
                    }
                    int columnIndex = 7;
                    for (int i = 0; i < dataGridViewX1.RowCount; i++)
                    {
                        if (i == 0)
                        {
                            if (openingBlancce > 0)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                            }
                            else { dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Red; }
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.Moccasin;

                            dataGridViewX1.Rows[i].DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 11, FontStyle.Bold);
                        }
                        else
                        {
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 1)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(160, 181, 174);
                            }
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 2)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(69, 149, 164);
                            }

                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 3)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(217, 221, 216);
                            }
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 4)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(152, 198, 209);
                            }
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 5)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 148, 154);
                            }
                        }
                    }
                    double currentBlance = 0;
                    double openingbalanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient "));
                    double GoldCreditCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCreditCurrentBalance FROM tblTransaction WHERE Tran_Type = 1 and IsDeleted = 0 "));
                    double GoldDebitCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebitCurrentBalance FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 "));
                    currentBlance = (GoldCreditCurrentBalance - GoldDebitCurrentBalance + openingbalanceGoldAllClients);
                    if (currentBlance < 0)
                    {
                        lblCurrentBalance.ForeColor = Color.Red;
                        lblCurrentBalance.Text = currentBlance + " in Grams";
                    }
                    else
                    {
                        lblCurrentBalance.ForeColor = Color.Green;
                        lblCurrentBalance.Text = currentBlance + " in Grams";
                    }
                    double TotalImpureGold = 0;
                    double TotalPurity = 0;
                    double TotalPureGoldinGram = 0;
                    double TotalNet_Amount = 0;

                    for (int i = 1; i < dataGridViewX1.Rows.Count; i++)
                    {
                        TotalImpureGold += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["ImpureGoldinGram"].Value);

                        TotalPurity += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["Purity"].Value);
                        TotalPureGoldinGram += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["PureGoldinGram"].Value);
                        TotalNet_Amount += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["Net_Amount"].Value);

                    }

                    lblImpureGoldTotal.Text = TotalImpureGold.ToString();
                    lblPurityTotal.Text = TotalPurity.ToString();
                    lblPureGoldTotal.Text = TotalPureGoldinGram.ToString();
                    lblNetAmountTotal.Text = TotalNet_Amount.ToString();

                }
                else { MessageBox.Show("Date Time is Invalid."); }
            }
            catch (Exception exp) { MessageBox.Show("Error In Populating DGV Data.", exp.Message); }
        }
        void LoadDGVLaboratoryRecordsBetwenDateAndAginstTransType()
        {
            try
            {
                if (cmbTransType.SelectedIndex > -1 && dtpFrom.Text != "" && dtpTo.Text != "")
                {


                    DataTable dt = Core.DBUtils.GetDataTable("SELECT  t.T_Id, c.C_Id, c.Name,t.Itme_Name, tt.T_TypeName,t.Tran_Status, tt.T_Id as TrTypeId, t.ImpureGoldinGram,t.Sum,t.Purity,t.PureGoldinGram,t.Net_Amount,t.Added_Date FROM tblTransaction as t INNER JOIN tblClient as c ON t.ClientId = c.C_Id INNER JOIN tblTransactionType as tt ON t.Tran_Type = tt.T_Id  WHERE  IsDeleted =0 and Tran_Status !='Jewellers' and T_Date BETWEEN '" + dtpFrom.Value + "' AND '" + dtpTo.Value + "' AND tt.T_Id='" + cmbTransType.SelectedValue+"' ");
                    DataRow dr = dt.NewRow();
                    double openingBlancce = 0;
                    double openingBlanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient  WHERE AddedDate<'" + DateTime.Now.Date + "' "));
                    double openingBlanceGoldDebit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebit FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));
                    double openingBlanceGoldCredit = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCredit FROM tblTransaction WHERE Tran_Type=1 and IsDeleted=0 and T_Date<'" + DateTime.Now.Date + "'"));

                    openingBlancce = (openingBlanceGoldCredit - openingBlanceGoldDebit + openingBlanceGoldAllClients);
                    if (openingBlancce == 0) { openingBlancce = 0; }
                    dr["Name"] = "Opening Balance";
                    dr["T_TypeName"] = "";
                    dr["Itme_Name"] = "Date:" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                    dr["ImpureGoldinGram"] = openingBlancce;
                    dt.Rows.InsertAt(dr, 0);
                    dataGridViewX1.DataSource = dt;
                    int n = dataGridViewX1.Columns.Count;
                    dataGridViewX1.Columns["Action"].DisplayIndex = 13;
                    dataGridViewX1.Columns["TrTypeId"].DisplayIndex = 5;
                    dataGridViewX1.Columns["T_Id"].Visible = false;
                    dataGridViewX1.Columns["C_Id"].Visible = false;
                    dataGridViewX1.Columns["TrTypeId"].Visible = false;
                    dataGridViewX1.Columns["Name"].HeaderText = "Client Name";
                    dataGridViewX1.Columns["Itme_Name"].HeaderText = "Discription";
                    dataGridViewX1.Columns["Action"].Width = 100;
                    dataGridViewX1.Columns["T_TypeName"].HeaderText = "Transaction Type";
                    dataGridViewX1.Columns["Tran_Status"].HeaderText = "Tran_Status";
                    dataGridViewX1.Columns["ImpureGoldinGram"].HeaderText = "ImpureGold in Gram";
                    dataGridViewX1.Columns["Sum"].HeaderText = "Sum";
                    dataGridViewX1.Columns["Purity"].HeaderText = "Purity";
                    dataGridViewX1.Columns["PureGoldinGram"].HeaderText = "Pure Gold in Gram";
                    dataGridViewX1.Columns["Net_Amount"].HeaderText = "Net Amount PKR";
                    dataGridViewX1.Columns["Added_Date"].HeaderText = "Added Date";
                    for (int i = 1; i < dataGridViewX1.RowCount; i++)
                    {
                        if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "1")
                        {
                            if (dataGridViewX1.Rows[i].Cells["Tran_Status"].Value.ToString() == "RefineGold")
                            { dataGridViewX1.Rows[i].Cells["Action"].Value = "List"; }
                            else { dataGridViewX1.Rows[i].Cells["Action"].Value = "Add"; }

                        }
                        else if (dataGridViewX1.Rows[i].Cells["C_Id"].Value.ToString() == "2")
                        { dataGridViewX1.Rows[i].Cells["Action"].Value = "Edit"; }
                        else { dataGridViewX1.Rows[i].Cells["Action"].Value = "History"; }
                    }
                    int columnIndex = 7;
                    for (int i = 0; i < dataGridViewX1.RowCount; i++)
                    {
                        if (i == 0)
                        {
                            if (openingBlancce > 0)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                            }
                            else { dataGridViewX1.Rows[i].DefaultCellStyle.ForeColor = Color.Red; }
                            dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.Moccasin;

                            dataGridViewX1.Rows[i].DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 11, FontStyle.Bold);
                        }
                        else
                        {
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 1)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(160, 181, 174);
                            }
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 2)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(69, 149, 164);
                            }

                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 3)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(217, 221, 216);
                            }
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 4)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(152, 198, 209);
                            }
                            if (Convert.ToInt32(dataGridViewX1.Rows[i].Cells[columnIndex].Value) == 5)
                            {
                                dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(255, 148, 154);
                            }
                        }
                    }
                    double currentBlance = 0;
                    double openingbalanceGoldAllClients = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient "));
                    double GoldCreditCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldCreditCurrentBalance FROM tblTransaction WHERE Tran_Type = 1 and IsDeleted = 0 "));
                    double GoldDebitCurrentBalance = Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(ImpureGoldinGram) as GoldDebitCurrentBalance FROM tblTransaction WHERE Tran_Type=2 and IsDeleted=0 "));
                    currentBlance = (GoldCreditCurrentBalance - GoldDebitCurrentBalance + openingbalanceGoldAllClients);
                    if (currentBlance < 0)
                    {
                        lblCurrentBalance.ForeColor = Color.Red;
                        lblCurrentBalance.Text = currentBlance + " in Grams";
                    }
                    else
                    {
                        lblCurrentBalance.ForeColor = Color.Green;
                        lblCurrentBalance.Text = currentBlance + " in Grams";
                    }
                    double TotalImpureGold = 0;
                    double TotalPurity = 0;
                    double TotalPureGoldinGram = 0;
                    double TotalNet_Amount = 0;

                    for (int i = 1; i < dataGridViewX1.Rows.Count; i++)
                    {
                        TotalImpureGold += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["ImpureGoldinGram"].Value);

                        TotalPurity += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["Purity"].Value);
                        TotalPureGoldinGram += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["PureGoldinGram"].Value);
                        TotalNet_Amount += Conversion.toDouble(dataGridViewX1.Rows[i].Cells["Net_Amount"].Value);

                    }

                    lblImpureGoldTotal.Text = TotalImpureGold.ToString();
                    lblPurityTotal.Text = TotalPurity.ToString();
                    lblPureGoldTotal.Text = TotalPureGoldinGram.ToString();
                    lblNetAmountTotal.Text = TotalNet_Amount.ToString();

                }
                else { MessageBox.Show("Date Time is Invalid."); }
            }
            catch (Exception exp) { MessageBox.Show("Error In Populating DGV Data.", exp.Message); }
        }
        private void cmbTransType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 1 GoldCredit
            if (Conversion.toInt( cmbTransType.SelectedValue) ==1 )
            {

            }
            // 2 GoldDebit
            else if (Conversion.toInt(cmbTransType.SelectedValue) == 2)
            {

            }
            // 3 CashCredit
            else if (Conversion.toInt(cmbTransType.SelectedValue) == 3)
            {

            }
            // 4 CashDebit
            else if (Conversion.toInt(cmbTransType.SelectedValue) == 4)
            {

            }
        }

        private void BtnSearchList_Click(object sender, EventArgs e)
        {
            if (dtpFrom.Text != "" && dtpTo.Text != "")
            {
                LoadDGVLaboratoryRecordsBetwenDate();
            }
            if (cmbTransType.SelectedIndex > -1 && dtpFrom.Text != "" && dtpTo.Text != "")
            {
                LoadDGVLaboratoryRecordsBetwenDateAndAginstTransType();
            }
            
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            cmbTransType.SelectedIndex = -1;
            cmbTransStatus.SelectedIndex = -1;
        }
    }
}
