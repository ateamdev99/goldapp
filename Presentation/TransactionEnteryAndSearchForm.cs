﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class TransactionEnteryAndSearchForm : Form
    {
        public TransactionEnteryAndSearchForm( tblTransaction objTrans)
        {
            InitializeComponent();
            tblTransactionBindingSource.DataSource = objTrans;
            if (objTrans.T_Id == 0)
            {
                //tblClient tblClientObj = tblClientBindingSource.Current as tblClient;
               tblClientBindingSource.DataSource = new tblClient();
                
            }  
            else
            {
                int CD = objTrans.Tran_Type;
                if (CD == 0){ Rdb1GoldCredit.Checked = true;}
                else if (CD == 1) { Rdb2GoldDebit.Checked = true; }
                else if (CD == 2) { Rdb3CashCredit.Checked = true; }
                else if (CD == 3) { Rdb4CashDebit.Checked = true; }
                else if (CD == 4) { RdbNone.Checked = true;}
                tblClientBindingSource.DataSource = ClientServicesClass.GetAllClients();

                CmbClints.SelectedValue = objTrans.ClientId;
                TxtClintNo.Text = objTrans.ClientId.ToString();
            }
        }

        private void TransactionEnteryAndSearchForm_Load(object sender, EventArgs e)
        {

            tblClientBindingSource.DataSource = ClientServicesClass.GetAllClients();
            //CmbClints.DataSource = tblClientBindingSource;
            //CmbClints.ValueMember = "C_Id";
            //CmbClints.DisplayMember = "C_Name";
        }

        private void BtnSaveExit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(TxtItemName.Text))
                {
                    MessageBox.Show("Enter Item Name", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtItemName.Focus();
                }
                else if (string.IsNullOrEmpty(TxtClintNo.Text))
                {
                    MessageBox.Show("Select Client", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    tblTransaction tblObj = tblTransactionBindingSource.Current as tblTransaction;

                    if (tblObj !=null && tblObj.T_Id == 0)
                    {
                        tblObj.ClientId = Convert.ToInt32(CmbClints.SelectedValue);
                        tblObj.Tran_Type = GetCreditDebit();
                        tblObj.Added_Date = DateTime.Now.Date;
                        tblObj.Added_By = "Admin";
                        tblObj.Tran_Status = "Nill";
                        TransactionServicesClass.Insert(tblObj);
                        MessageBox.Show("Data Inserted", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RestForm();
                        //MainDashbordForm form = new MainDashbordForm();
                        //form.Refresh();
                        this.Close();
                    }
                    else
                    {
                        tblObj.ClientId = Convert.ToInt32(CmbClints.SelectedValue);
                        tblObj.Tran_Type = GetCreditDebit();
                        tblObj.Added_Date = DateTime.Now.Date;
                        tblObj.Added_By = "Admin";
                        tblObj.Tran_Status = "Nill";
                        TransactionServicesClass.Update(tblTransactionBindingSource.Current as tblTransaction);
                        MessageBox.Show("Data Updated", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RestForm();
                        //MainDashbordForm form = new MainDashbordForm();
                        //form.Refresh();
                        this.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        void RestForm()
        {
            //CmbClints.SelectedIndex = -1;
            TxtSerialNo.Clear();
            TxtClintNo.Clear();
            TxtItemName.Clear();

        }
        public enum CreditDebit
        {
            GoldCredit = 0,
            GoldDebit = 1,
            CashCredit = 2,
            CashDebit = 3,
            None = 4
        }
        private int GetCreditDebit()
        {
            if (Rdb1GoldCredit.Checked)
            {
                return (int)CreditDebit.GoldCredit;
            }
            if (Rdb2GoldDebit.Checked)
            {
                return (int)CreditDebit.GoldDebit;
            }
            if (Rdb3CashCredit.Checked)
            {
                return (int)CreditDebit.CashCredit;
            } if (Rdb4CashDebit.Checked)
            {
                return (int)CreditDebit.CashDebit;
            }
            return (int)CreditDebit.None;
        }

        private void CmbClints_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (CmbClints.SelectedIndex > -1) { TxtClintNo.Text = CmbClints.SelectedValue.ToString();}
        }

        public void BtnSearchTransction_Click(object sender, EventArgs e)
        {
            try
            {
                if (DtpFromSearchTr.Text != "" && DtpToSearchTr.Text != "")
                {
                    if (ChkSerchGroupTr.Checked == true)
                    {
                        List<tblTransaction> tblObj = TransactionServicesClass.GetTranByDateAndOrderByCID(DtpFromSearchTr.Value, DtpToSearchTr.Value);
                        if (tblObj != null)
                        {
                            Presentation.DateWiseTransactionsForm frEmp = new DateWiseTransactionsForm(tblObj);
                            frEmp.ShowDialog();
                        }
                        else { MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else
                    {
                        List<tblTransaction> tblObj = TransactionServicesClass.GetTranByDate(DtpFromSearchTr.Value, DtpToSearchTr.Value);
                        if (tblObj != null)
                        {
                            Presentation.DateWiseTransactionsForm frEmp = new DateWiseTransactionsForm(tblObj);
                            frEmp.ShowDialog();
                        }
                        else{MessageBox.Show("Data not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message,"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
            
        }

        private void groupPanel2_Click(object sender, EventArgs e)
        {

        }
        
        

        
    }
}
