﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data;
using Core;

namespace Presentation
{
    public partial class DashboardForm : Form
    {
        public DashboardForm()
        {
            InitializeComponent();
            GetLastThreeTransactions();
        }

        void GetLastThreeTransactions()
        {
            JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess();

            var LastThreeTrans = db.tblTransactions.OrderByDescending(t => t.T_Id).Where(t => t.IsDeleted!=true).Take(3);
            int i = 1;
            foreach (var item in LastThreeTrans)
            {
                if (i == 1)
                {
                    lblTr1.Text = item.ImpureGoldinGram.ToString();
                    label3.Text = item.Itme_Name;
                    if (item.Tran_Type == 1)
                    {
                        //pbTr1.ImageLocation = Resources.imgfromresource
                        pbTr1.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\green-arrow-down.png");

                        pbTr1.Width = 50;
                        pbTr1.Height = 50;
                    }
                    else if (item.Tran_Type == 2)
                    {
                        pbTr1.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\red-arrow-up.png");

                        pbTr1.Width = 50;
                        pbTr1.Height = 50;
                        //pbTr1.ImageLocation = @"\Resources\red-arrow-up.png";
                    }
                    else if (item.Tran_Type == 3)
                    {
                        pbTr1.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\green-arrow-down.png");

                        pbTr1.Width = 50;
                        pbTr1.Height = 50;
                    }
                    else if (item.Tran_Type == 4)
                    {
                        pbTr1.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\red-arrow-up.png");

                        pbTr1.Width = 50;
                        pbTr1.Height = 50;
                    }
                }
                else if (i == 2)
                {
                    lblTr2.Text = item.ImpureGoldinGram.ToString();
                    label4.Text = item.Itme_Name;
                    if (item.Tran_Type == 1)
                    {
                        pbTr2.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\green-arrow-down.png");

                        pbTr2.Width = 50;
                        pbTr2.Height = 50;
                    }
                    else if (item.Tran_Type == 2)
                    {
                        pbTr2.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\red-arrow-up.png");

                        pbTr2.Width = 50;
                        pbTr2.Height = 50;
                    }
                    else if (item.Tran_Type == 3)
                    {
                        pbTr2.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\green-arrow-down.png");

                        pbTr2.Width = 50;
                        pbTr2.Height = 50;
                    }
                    else if (item.Tran_Type == 4)
                    {
                        pbTr2.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\red-arrow-up.png");

                        pbTr2.Width = 50;
                        pbTr2.Height = 50;
                    }

                }
                else if (i == 3)
                {
                    lblTr3.Text = item.ImpureGoldinGram.ToString();
                    label5.Text = item.Itme_Name;
                    if (item.Tran_Type == 1)
                    {
                        pbTr3.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\green-arrow-down.png");

                        pbTr3.Width = 50;
                        pbTr3.Height = 50;
                    }
                    else if (item.Tran_Type == 2)
                    {
                        pbTr3.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\red-arrow-up.png");

                        pbTr3.Width = 50;
                        pbTr3.Height = 50;
                    }
                    else if (item.Tran_Type == 3)
                    {
                        pbTr3.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\green-arrow-down.png");

                        pbTr3.Width = 50;
                        pbTr3.Height = 50;
                    }
                    else if (item.Tran_Type == 4)
                    {
                        pbTr3.Image = Image.FromFile("c:\\inetpub\\Presentation\\Resources\\red-arrow-up.png");

                        pbTr3.Width = 50;
                        pbTr3.Height = 50;
                    }

                }

                i++;
            }
        }
        private void btnDayBook_Click(object sender, EventArgs e)
        {
            Presentation.DayBookFrm frEmp = new DayBookFrm();
            frEmp.ShowDialog();
            GetLastThreeTransactions();
        }

        private void btnOthers_Click(object sender, EventArgs e)
        {

            //int ID = 0;
            tblTransaction obj = new tblTransaction();
            //DayBookFrm daybookForm = new DayBookFrm();
            //AddSelectCustomerForm selectClientForm = new AddSelectCustomerForm(ID, daybookForm);

            tblTransaction objTrans = new tblTransaction();
            if (objTrans != null)
            {
                ExpenseForm frEmp = new ExpenseForm(obj);
                frEmp.ShowDialog();
            }
        }

        private void btnRecord_Click(object sender, EventArgs e)
        {

            Presentation.LaboratoryAllRecordsForm frEmp = new LaboratoryAllRecordsForm();
            frEmp.ShowDialog();

        }

        private void btnRefineGold_Click(object sender, EventArgs e)
        {
            Presentation.ExtraForm frEmp = new ExtraForm();
            frEmp.ShowDialog();
        
    }

        private void btnDebit_Click(object sender, EventArgs e)
        {
            
                
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {

        }

        private void btnCash_Click(object sender, EventArgs e)
        {

        }

        private void btnGold_Click(object sender, EventArgs e)
        {

        }

        private void btnJewellery_Click(object sender, EventArgs e)
        {
            
        }

        private void btnLaboratory_Click(object sender, EventArgs e)
        {
            Presentation.DayBookLaboratoryForm frEmp = new DayBookLaboratoryForm();
            frEmp.ShowDialog();
            GetLastThreeTransactions();
        }

        private void btnCasting_Click(object sender, EventArgs e)
        {

        }

        private void btnRustedGold_Click(object sender, EventArgs e)
        {
            DialogResult DResult;
            DResult = MessageBox.Show("Click Yes to Delete Transactions and Clients And Set identity increment 0", "Sure to Delete ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (DResult == DialogResult.Yes)
            {
                DeleteQuerry();
                Close();
            }
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            Presentation.SettingForm frEmp = new SettingForm();
            frEmp.ShowDialog();
        }

        private void panelTop_Paint(object sender, PaintEventArgs e)
        {

        }
        void DeleteQuerry()
        {
            DBUtils.ExecuteSQL("delete from tblClient");
            DBUtils.ExecuteSQL("DBCC CHECKIDENT ('tblClient', RESEED, 0)");
            DBUtils.ExecuteSQL("delete from tblTransaction");
            DBUtils.ExecuteSQL("DBCC CHECKIDENT('tblTransaction', RESEED, 0)");
            DBUtils.ExecuteSQL("delete from tblGoldRate");
            DBUtils.ExecuteSQL("DBCC CHECKIDENT ('tblGoldRate', RESEED, 0)");
        }
        void DefaultSetupQuerry()
        {
            int clientcount = Convert.ToInt32(DBUtils.executeSqlGetID("select count (C_Id) from tblClient"));
            if (clientcount == 0)
            {
                DBUtils.ExecuteSQL("insert into tblClient (Name,ActivateAccount,TemporaryAccount,TranactionSMS,BalanceSMS,CustomSMS,BalncSMSGoldWight,CashOpeningBalance,GoldOpeningBalance) values('Anonymous',0,0,0,0,0,0,0,0)");
                DBUtils.ExecuteSQL("insert into tblClient (Name,ActivateAccount,TemporaryAccount,TranactionSMS,BalanceSMS,CustomSMS,BalncSMSGoldWight,CashOpeningBalance,GoldOpeningBalance) values('Admin',0,0,0,0,0,0,0,0)");

            }
        }
        private void DashboardForm_Load(object sender, EventArgs e)
        {
            DefaultSetupQuerry();
            GetGoldCurrentRate();
            if(lblTodayGoldRate.Text == "0.000 PKR")
            {
                tblGoldRate obj = new tblGoldRate();
                Presentation.AddCurrentGoldPriceForm frEmp = new AddCurrentGoldPriceForm(obj);
                frEmp.ShowDialog();
                GetGoldCurrentRate();
            }
        }
        void GetGoldCurrentRate()
        {
           string rate= DBUtils.executeSqlGetID(" SELECT TOP 1 TodayRate FROM tblGoldRate where UpdatedDate BETWEEN '" + DateTime.Now.Date + "' AND '" + DateTime.Now + "' ORDER BY Id desc   ");
            if (rate!=null && rate !="")
            {
                lblTodayGoldRate.Text = rate + " PKR";
                linkAddGoldRate.Text = "Edit Rate";
            }
            else
            {
                lblTodayGoldRate.Text = "0.000 PKR";
            }
            
        }
        private void linkAddGoldRate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tblGoldRate obj = new tblGoldRate(); 
            Presentation.AddCurrentGoldPriceForm frEmp = new AddCurrentGoldPriceForm(obj);
            frEmp.ShowDialog();
            GetGoldCurrentRate();

        }

        private void btnClientList_Click(object sender, EventArgs e)
        {
            
            Presentation.ClientListForm frEmp = new ClientListForm();
            frEmp.ShowDialog();
        }
    }
}
