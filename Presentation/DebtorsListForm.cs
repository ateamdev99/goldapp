﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;

namespace Presentation
{
    public partial class DebtorsListForm : Form
    {
        public DebtorsListForm()
        {
            InitializeComponent();
        }

        private void checkBoxX2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private DataTable GetAllClientLst()
        {
           return DBUtils.GetDataTable("SELECT [t0].[C_Name] AS [ClientName], [t0].[C_Address] AS [Address], [t1].[City_Name] AS [CityName], [t0].[C_District] AS [District], [t0].[C_MobileNo] AS [MobileNo], [t0].[C_OfficeNo] AS [OfficeNo], [t0].[C_OpBlncGold] AS [OpBalanceGold], [t0].[C_OpBlncCash] AS [OpBalanceCash], [t0].[C_OpeningDate] AS [OpeningDate] FROM [tblClient] AS [t0] INNER JOIN [tblCity] AS [t1] ON [t0].[C_CityId] = ([t1].[City_Id])");
        }
        public DataTable GetCreditorsLst()
        {
            return DBUtils.GetDataTable("");
        }
        public DataTable GetDebtorsLst()
        {
            return DBUtils.GetDataTable("");
        }
        void DGVPopulate()
        {
            if (RdbLstAllClients.Checked == true)
            {
                dataGridViewX1.DataSource = GetAllClientLst();
                LblHeading.Text = "All Clients Account Summery!";
            }
            else if (RdbLstCreditors.Checked == true)
            {
                LblHeading.Text = "All Creditors Account Summery!";
            }
            else if (RdbLstDebtors.Checked == true)
            {
                LblHeading.Text = "All Debtors Account Summery!";
            }
        }
        private void DebtorsListForm_Load(object sender, EventArgs e)
        {
            DGVPopulate();
        }

        private void RdbLstAllClients_CheckedChanged(object sender, EventArgs e)
        {
            DGVPopulate();
        }

        private void RdbLstCreditors_CheckedChanged(object sender, EventArgs e)
        {
            DGVPopulate();
        }

        private void RdbLstDebtors_CheckedChanged(object sender, EventArgs e)
        {
            DGVPopulate();
        }
    }
}
