USE [JewellersDataBase]
GO
/****** Object:  StoredProcedure [dbo].[procedure_ShowAllClients]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP PROCEDURE [dbo].[procedure_ShowAllClients]
GO
ALTER TABLE [dbo].[tblClient] DROP CONSTRAINT [FK_tblClient_tblCity]
GO
/****** Object:  View [dbo].[vw_Testing]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP VIEW [dbo].[vw_Testing]
GO
/****** Object:  Table [dbo].[tblTransactionType]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP TABLE [dbo].[tblTransactionType]
GO
/****** Object:  Table [dbo].[tblTransaction]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP TABLE [dbo].[tblTransaction]
GO
/****** Object:  Table [dbo].[tblGoldRate]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP TABLE [dbo].[tblGoldRate]
GO
/****** Object:  Table [dbo].[tblClient]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP TABLE [dbo].[tblClient]
GO
/****** Object:  Table [dbo].[tblCity]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP TABLE [dbo].[tblCity]
GO
USE [master]
GO
/****** Object:  Database [JewellersDataBase]    Script Date: 2/17/2021 7:02:35 PM ******/
DROP DATABASE [JewellersDataBase]
GO
/****** Object:  Database [JewellersDataBase]    Script Date: 2/17/2021 7:02:35 PM ******/
CREATE DATABASE [JewellersDataBase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JewellersDataBase', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\JewellersDataBase.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'JewellersDataBase_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\JewellersDataBase_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [JewellersDataBase] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JewellersDataBase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JewellersDataBase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JewellersDataBase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JewellersDataBase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JewellersDataBase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JewellersDataBase] SET ARITHABORT OFF 
GO
ALTER DATABASE [JewellersDataBase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JewellersDataBase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JewellersDataBase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JewellersDataBase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JewellersDataBase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JewellersDataBase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JewellersDataBase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JewellersDataBase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JewellersDataBase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JewellersDataBase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [JewellersDataBase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JewellersDataBase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JewellersDataBase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JewellersDataBase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [JewellersDataBase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JewellersDataBase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [JewellersDataBase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JewellersDataBase] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [JewellersDataBase] SET  MULTI_USER 
GO
ALTER DATABASE [JewellersDataBase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JewellersDataBase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JewellersDataBase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JewellersDataBase] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [JewellersDataBase] SET DELAYED_DURABILITY = DISABLED 
GO
USE [JewellersDataBase]
GO
/****** Object:  Table [dbo].[tblCity]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCity](
	[City_Id] [int] IDENTITY(1,1) NOT NULL,
	[City_Name] [nvarchar](250) NULL,
 CONSTRAINT [PK_tblCity] PRIMARY KEY CLUSTERED 
(
	[City_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblClient]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClient](
	[C_Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[CityId] [int] NULL,
	[District] [nvarchar](250) NULL,
	[MobileNo] [nvarchar](250) NULL,
	[OfficeNo] [nvarchar](250) NULL,
	[GoldOpeningBalance] [float] NOT NULL,
	[CashOpeningBalance] [float] NOT NULL,
	[AddedDate] [smalldatetime] NULL,
	[TMR] [nvarchar](250) NULL,
	[ActivateAccount] [bit] NOT NULL,
	[TemporaryAccount] [bit] NOT NULL,
	[TranactionSMS] [bit] NOT NULL,
	[BalanceSMS] [bit] NOT NULL,
	[CustomSMS] [bit] NOT NULL,
	[BalncSMSGoldWight] [int] NOT NULL,
	[UpdatedDate] [smalldatetime] NULL,
 CONSTRAINT [PK_tblClient] PRIMARY KEY CLUSTERED 
(
	[C_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblGoldRate]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGoldRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TodayRate] [float] NOT NULL,
	[AddedDate] [smalldatetime] NULL,
	[UpdatedDate] [smalldatetime] NULL,
 CONSTRAINT [PK_tblGoldRate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTransaction]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTransaction](
	[T_Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[T_Date] [smalldatetime] NULL,
	[Price_of_Gold] [float] NOT NULL,
	[Current_Rate_Per_Tola] [float] NOT NULL,
	[ImpureGoldinGram] [float] NOT NULL,
	[Labour_Per_Tola] [float] NOT NULL,
	[Total_Labour] [float] NOT NULL,
	[Payment_Percnt_Cut] [float] NOT NULL,
	[Net_Amount] [float] NOT NULL,
	[Polish_Per_Tola] [float] NOT NULL,
	[Total_Polish] [float] NOT NULL,
	[Cut_Per_Tola] [float] NOT NULL,
	[Cut_from_total_Weight] [float] NOT NULL,
	[Weightin_Mode] [int] NOT NULL,
	[Cut_Priority] [int] NOT NULL,
	[Labour_And_Polish_Calculation] [int] NOT NULL,
	[Added_By] [nvarchar](250) NULL,
	[Added_Date] [smalldatetime] NULL,
	[Itme_Name] [nvarchar](250) NULL,
	[Tran_Type] [int] NOT NULL,
	[Tran_Status] [nvarchar](250) NULL,
	[IsTeleConfirmation] [bit] NOT NULL,
	[TeleDate] [smalldatetime] NULL,
	[Purity] [float] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpatedDate] [smalldatetime] NULL,
	[PureGoldinGram] [float] NOT NULL,
	[Sum] [float] NOT NULL,
	[CustomSumFrom] [float] NOT NULL,
	[CustomSumTo] [float] NOT NULL,
 CONSTRAINT [PK_tblTransaction] PRIMARY KEY CLUSTERED 
(
	[T_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblTransactionType]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTransactionType](
	[T_Id] [int] IDENTITY(1,1) NOT NULL,
	[T_TypeName] [nvarchar](250) NULL,
 CONSTRAINT [PK_TransactionTypeTbl] PRIMARY KEY CLUSTERED 
(
	[T_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vw_Testing]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Testing]
AS 
SELECT  c.Name as Name,t.Itme_Name as Discription, tt.T_TypeName as TrType,t.Tran_Status as TrStatus, t.ImpureGoldinGram,t.Sum,t.Purity,t.PureGoldinGram,t.Net_Amount  
FROM tblTransaction as t INNER JOIN tblClient as c ON t.ClientId = c.C_Id INNER JOIN tblTransactionType as tt ON t.Tran_Type = tt.T_Id  
WHERE t.T_Date >= '2021-02-16 00:00:00' and IsDeleted =0 and Tran_Status !='Jewellers'
GO
SET IDENTITY_INSERT [dbo].[tblCity] ON 

INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (1, N'Sargodha')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (2, N'Islamabad')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (3, N'Lahore')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (4, N'Khushab')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (5, N'Jehlum')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (6, N'Gujraat')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (7, N'Karachi')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (8, N'Queta')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (9, N'Peshawer')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (10, N'Multan')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (11, N'Johrabad')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (12, N'Mianwali')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (13, N'Shekhupura')
INSERT [dbo].[tblCity] ([City_Id], [City_Name]) VALUES (14, N'Peshwar')
SET IDENTITY_INSERT [dbo].[tblCity] OFF
SET IDENTITY_INSERT [dbo].[tblClient] ON 

INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (1, N'Anonymous', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL)
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (2, N'Admin', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL)
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (3, N'Muhammad Israr', N'igjfgh', 2, NULL, N'5657-5678476', NULL, 6, 50000, CAST(N'2021-02-13 13:21:00' AS SmallDateTime), NULL, 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 12:08:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (4, N'Bilal Ahmad', N'Kachari Road Khushab', 4, NULL, N'0302-3441226', NULL, 50, 70000, CAST(N'2021-02-15 16:25:00' AS SmallDateTime), NULL, 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 09:33:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (5, N'Ahmad Ali', N'Gojra', 11, N'Faisalabad', N'0344-8907655', N'777-8899999', 6, 7, CAST(N'2021-02-15 16:25:00' AS SmallDateTime), N'7-8', 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 10:46:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (6, N'Rashid Bukhari', N'Mianwali City Area', 12, N'Mianwali', N'0300-0066666', N'048-6755788', 6, 50000, CAST(N'2021-02-15 16:25:00' AS SmallDateTime), N'78-88', 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 09:36:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (7, N'Ashir', N'', 1, NULL, NULL, NULL, 0, 0, CAST(N'2021-02-15 16:26:00' AS SmallDateTime), NULL, 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 10:48:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (8, N'Jameel', NULL, 2, NULL, NULL, NULL, 0, 0, CAST(N'2021-02-15 16:26:00' AS SmallDateTime), NULL, 0, 0, 0, 0, 0, 0, CAST(N'2021-02-15 16:26:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (11, N'Ibraheem', NULL, 1, NULL, NULL, NULL, 0, 0, CAST(N'2021-02-15 16:28:00' AS SmallDateTime), NULL, 1, 0, 0, 0, 0, 0, CAST(N'2021-02-15 16:28:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (13, N'Kamran Ghulam', N'SGD', 7, NULL, N'0303-3434342', NULL, 0, 0, CAST(N'2021-02-17 13:29:00' AS SmallDateTime), NULL, 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 13:29:00' AS SmallDateTime))
INSERT [dbo].[tblClient] ([C_Id], [Name], [Address], [CityId], [District], [MobileNo], [OfficeNo], [GoldOpeningBalance], [CashOpeningBalance], [AddedDate], [TMR], [ActivateAccount], [TemporaryAccount], [TranactionSMS], [BalanceSMS], [CustomSMS], [BalncSMSGoldWight], [UpdatedDate]) VALUES (14, N'Bashir', N'Sgd', 5, NULL, N'0488-8888888', NULL, 0, 0, CAST(N'2021-02-17 14:44:00' AS SmallDateTime), NULL, 0, 0, 0, 0, 0, 0, CAST(N'2021-02-17 14:44:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[tblClient] OFF
SET IDENTITY_INSERT [dbo].[tblGoldRate] ON 

INSERT [dbo].[tblGoldRate] ([Id], [TodayRate], [AddedDate], [UpdatedDate]) VALUES (1, 133000, CAST(N'2021-02-13 13:19:00' AS SmallDateTime), CAST(N'2021-02-13 13:19:00' AS SmallDateTime))
INSERT [dbo].[tblGoldRate] ([Id], [TodayRate], [AddedDate], [UpdatedDate]) VALUES (2, 122000, CAST(N'2021-02-15 11:42:00' AS SmallDateTime), CAST(N'2021-02-15 11:42:00' AS SmallDateTime))
INSERT [dbo].[tblGoldRate] ([Id], [TodayRate], [AddedDate], [UpdatedDate]) VALUES (3, 19000, CAST(N'2021-02-16 10:48:00' AS SmallDateTime), CAST(N'2021-02-16 10:48:00' AS SmallDateTime))
INSERT [dbo].[tblGoldRate] ([Id], [TodayRate], [AddedDate], [UpdatedDate]) VALUES (4, 112000, CAST(N'2021-02-17 09:30:00' AS SmallDateTime), CAST(N'2021-02-17 09:30:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[tblGoldRate] OFF
SET IDENTITY_INSERT [dbo].[tblTransaction] ON 

INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (1, 3, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 123, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'123KachSonaDebit', 1, N'KachaSona', 0, CAST(N'2021-02-13 13:20:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-13 15:57:00' AS SmallDateTime), 0.16, 22, 22, 333)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (2, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 122, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'CreditKachaSona', 1, N'KachaSona', 0, CAST(N'2021-02-13 13:21:00' AS SmallDateTime), 0.2441, 0, CAST(N'2021-02-13 13:21:00' AS SmallDateTime), 0.24, 22, 2, 22)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (3, 3, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'CrtoDBChngeKhalisSona', 1, N'KhalisSona', 0, CAST(N'2021-02-13 13:22:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-13 15:55:00' AS SmallDateTime), 396, 0, 45, 55)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (4, 3, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'5555', 1, N'KhalisSona', 0, CAST(N'2021-02-13 13:23:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-13 13:23:00' AS SmallDateTime), 4865, 0, 54, 55)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (5, 3, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 133000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'3', 4, N'Cash', 0, CAST(N'2021-02-13 13:23:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-13 15:56:00' AS SmallDateTime), 2, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (6, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'Enter Discription Here66', 2, N'RefineGold', 0, CAST(N'2021-02-13 13:24:00' AS SmallDateTime), 6, 1, CAST(N'2021-02-13 13:24:00' AS SmallDateTime), 6, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (7, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'disRefinedbit', 2, N'RefineGold', 0, CAST(N'2021-02-13 14:30:00' AS SmallDateTime), 888, 1, CAST(N'2021-02-13 15:59:00' AS SmallDateTime), 60, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (8, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'55kh;lis', 1, N'KhalisSona', 0, CAST(N'2021-02-13 14:37:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-13 14:37:00' AS SmallDateTime), 55, 0, 55, 55)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (9, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'77', 2, N'KhalisSona', 0, CAST(N'2021-02-13 14:37:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-13 14:37:00' AS SmallDateTime), 7, 0, 7, 7)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (10, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'88kACHA', 1, N'KachaSona', 0, CAST(N'2021-02-13 14:38:00' AS SmallDateTime), 6, 0, CAST(N'2021-02-13 14:38:00' AS SmallDateTime), 66, 66, 66, 88)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (11, 1, CAST(N'2021-02-13 00:00:00' AS SmallDateTime), 0, 0, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'66', 2, N'KachaSona', 0, CAST(N'2021-02-13 14:39:00' AS SmallDateTime), 66, 0, CAST(N'2021-02-13 14:39:00' AS SmallDateTime), 66, 656, 66, 6)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (12, 1, CAST(N'2021-02-13 16:24:00' AS SmallDateTime), 133000, 0, 656, 0, 0, 0, 0, 0, 0, 56, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'6565', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (13, 1, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 133000, 0, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'656', 3, N'Jewellers', 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (14, 1, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 133000, 0, 656, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'66', 2, N'Jewellers', 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (15, 1, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 133000, 0, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'66', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:25:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (16, 1, CAST(N'2021-02-13 16:40:00' AS SmallDateTime), 133000, 0, 556, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'565', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:40:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:40:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (17, 1, CAST(N'2021-02-13 16:40:00' AS SmallDateTime), 133000, 0, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'65', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:40:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:40:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (18, 1, CAST(N'2021-02-13 16:41:00' AS SmallDateTime), 133000, 0, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'33', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:41:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:41:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (19, 3, CAST(N'2021-02-13 16:41:00' AS SmallDateTime), 133000, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'4', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:44:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:44:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (20, 3, CAST(N'2021-02-13 16:44:00' AS SmallDateTime), 133000, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'7dis', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:45:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:45:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (21, 1, CAST(N'2021-02-13 16:45:00' AS SmallDateTime), 133000, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-13 00:00:00' AS SmallDateTime), N'6', 1, N'Jewellers', 0, CAST(N'2021-02-13 16:45:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-13 16:45:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (22, 3, CAST(N'2021-02-15 00:00:00' AS SmallDateTime), 0, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'33crkchasona', 1, N'KachaSona', 0, CAST(N'2021-02-15 11:44:00' AS SmallDateTime), 22, 0, CAST(N'2021-02-15 11:44:00' AS SmallDateTime), 33, 22, 3, 3)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (23, 3, CAST(N'2021-02-15 00:00:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'6dbitkhlis', 2, N'KhalisSona', 0, CAST(N'2021-02-15 11:44:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-15 11:44:00' AS SmallDateTime), 6, 0, 6, 6)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (24, 9, CAST(N'2021-02-15 00:00:00' AS SmallDateTime), 122000, 0, 0, 0, 0, 0, 444, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'444crcash', 3, N'Cash', 0, CAST(N'2021-02-15 11:44:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-15 11:44:00' AS SmallDateTime), 444, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (25, 3, CAST(N'2021-02-15 00:00:00' AS SmallDateTime), 122000, 0, 0, 0, 0, 0, 77, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'77debitcash', 3, N'Cash', 0, CAST(N'2021-02-15 11:45:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-15 11:45:00' AS SmallDateTime), 77, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (26, 3, CAST(N'2021-02-15 00:00:00' AS SmallDateTime), 122000, 0, 0, 0, 0, 0, 55, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'55', 4, N'Cash', 0, CAST(N'2021-02-15 11:45:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-15 11:45:00' AS SmallDateTime), 55, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (27, 1, CAST(N'2021-02-15 00:00:00' AS SmallDateTime), 0, 0, 88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'77', 1, N'RefineGold', 0, CAST(N'2021-02-15 11:55:00' AS SmallDateTime), 77, 0, CAST(N'2021-02-15 11:55:00' AS SmallDateTime), 66, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (28, 1, CAST(N'2021-02-15 11:59:00' AS SmallDateTime), 122000, 0, 77, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'99', 1, N'Jewellers', 0, CAST(N'2021-02-15 12:00:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-15 12:00:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (29, 3, CAST(N'2021-02-15 12:00:00' AS SmallDateTime), 122000, 0, 88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 00:00:00' AS SmallDateTime), N'88', 2, N'Jewellers', 0, CAST(N'2021-02-15 12:00:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-15 12:00:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (30, 1, CAST(N'2021-02-15 13:10:00' AS SmallDateTime), 122000, 0, 106.02576, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 13:10:00' AS SmallDateTime), N'899', 1, N'Jewellers', 0, CAST(N'2021-02-15 13:10:00' AS SmallDateTime), 1000, 0, CAST(N'2021-02-15 13:10:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (31, 1, CAST(N'2021-02-15 13:10:00' AS SmallDateTime), 0, 0, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 13:10:00' AS SmallDateTime), N'5', 1, N'RefineGold', 0, CAST(N'2021-02-15 13:10:00' AS SmallDateTime), 55, 0, CAST(N'2021-02-15 13:10:00' AS SmallDateTime), 5, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (32, 11, CAST(N'2021-02-15 16:27:00' AS SmallDateTime), 122000, 0, 0, 0, 0, 0, 56677, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 16:27:00' AS SmallDateTime), N'55', 3, N'Cash', 0, CAST(N'2021-02-15 16:27:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-15 16:27:00' AS SmallDateTime), 55, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (33, 1, CAST(N'2021-02-15 18:33:00' AS SmallDateTime), 0, 0, 34434444, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-15 18:33:00' AS SmallDateTime), N'33', 1, N'KachaSona', 0, CAST(N'2021-02-15 18:33:00' AS SmallDateTime), 103303.332, 0, CAST(N'2021-02-15 18:33:00' AS SmallDateTime), 1136336.65, 33, 33, 33)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (34, 3, CAST(N'2021-02-16 10:50:00' AS SmallDateTime), 0, 0, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-16 10:50:00' AS SmallDateTime), N'55', 1, N'KachaSona', 0, CAST(N'2021-02-16 10:50:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-16 10:50:00' AS SmallDateTime), 55, 55, 55, 5)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (35, 1, CAST(N'2021-02-16 10:54:00' AS SmallDateTime), 0, 0, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-16 10:54:00' AS SmallDateTime), N'5', 2, N'RefineGold', 0, CAST(N'2021-02-16 10:54:00' AS SmallDateTime), 55, 0, CAST(N'2021-02-16 10:54:00' AS SmallDateTime), 55, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (36, 7, CAST(N'2021-02-16 10:55:00' AS SmallDateTime), 19000, 0, 0, 0, 0, 0, 565, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-16 10:55:00' AS SmallDateTime), N'6', 3, N'Cash', 0, CAST(N'2021-02-16 10:55:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-16 10:55:00' AS SmallDateTime), 6566, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (37, 4, CAST(N'2021-02-17 11:44:00' AS SmallDateTime), 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 11:44:00' AS SmallDateTime), N'fg', 1, N'KachaSona', 0, CAST(N'2021-02-17 11:44:00' AS SmallDateTime), 5, 0, CAST(N'2021-02-17 11:44:00' AS SmallDateTime), 0.5, 55, 555, 555)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (38, 12, CAST(N'2021-02-17 12:28:00' AS SmallDateTime), 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 12:28:00' AS SmallDateTime), N'66', 1, N'KachaSona', 0, CAST(N'2021-02-17 12:28:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-17 12:28:00' AS SmallDateTime), 0, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (39, 12, CAST(N'2021-02-17 12:29:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 12:29:00' AS SmallDateTime), N'45', 1, N'KhalisSona', 0, CAST(N'2021-02-17 12:29:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-17 12:29:00' AS SmallDateTime), 545, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (40, 10, CAST(N'2021-02-17 13:08:00' AS SmallDateTime), 112000, 0, 0, 0, 0, 0, 455, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 13:08:00' AS SmallDateTime), N'454', 3, N'Cash', 0, CAST(N'2021-02-17 13:08:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-17 13:08:00' AS SmallDateTime), 454, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (41, 1, CAST(N'2021-02-17 15:10:00' AS SmallDateTime), 0, 0, 4545, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 15:10:00' AS SmallDateTime), N'4545', 1, N'RefineGold', 0, CAST(N'2021-02-17 15:10:00' AS SmallDateTime), 545, 0, CAST(N'2021-02-17 15:10:00' AS SmallDateTime), 4545, 0, 0, 0)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (42, 8, CAST(N'2021-02-17 15:10:00' AS SmallDateTime), 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 15:10:00' AS SmallDateTime), N'45', 2, N'KachaSona', 0, CAST(N'2021-02-17 15:10:00' AS SmallDateTime), 45, 0, CAST(N'2021-02-17 15:10:00' AS SmallDateTime), 0, 54, 544, 5)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (43, 4, CAST(N'2021-02-17 15:12:00' AS SmallDateTime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 15:12:00' AS SmallDateTime), N'55', 2, N'KhalisSona', 0, CAST(N'2021-02-17 15:12:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-17 15:12:00' AS SmallDateTime), 61055, 0, 55, 55)
INSERT [dbo].[tblTransaction] ([T_Id], [ClientId], [T_Date], [Price_of_Gold], [Current_Rate_Per_Tola], [ImpureGoldinGram], [Labour_Per_Tola], [Total_Labour], [Payment_Percnt_Cut], [Net_Amount], [Polish_Per_Tola], [Total_Polish], [Cut_Per_Tola], [Cut_from_total_Weight], [Weightin_Mode], [Cut_Priority], [Labour_And_Polish_Calculation], [Added_By], [Added_Date], [Itme_Name], [Tran_Type], [Tran_Status], [IsTeleConfirmation], [TeleDate], [Purity], [IsDeleted], [UpatedDate], [PureGoldinGram], [Sum], [CustomSumFrom], [CustomSumTo]) VALUES (44, 13, CAST(N'2021-02-17 15:12:00' AS SmallDateTime), 112000, 0, 0, 0, 0, 0, 55, 0, 0, 0, 0, 0, 0, 0, N'Admin', CAST(N'2021-02-17 15:12:00' AS SmallDateTime), N'55', 4, N'Cash', 0, CAST(N'2021-02-17 15:12:00' AS SmallDateTime), 0, 0, CAST(N'2021-02-17 15:12:00' AS SmallDateTime), 55, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[tblTransaction] OFF
SET IDENTITY_INSERT [dbo].[tblTransactionType] ON 

INSERT [dbo].[tblTransactionType] ([T_Id], [T_TypeName]) VALUES (1, N'Gold Credit')
INSERT [dbo].[tblTransactionType] ([T_Id], [T_TypeName]) VALUES (2, N'Gold Debit')
INSERT [dbo].[tblTransactionType] ([T_Id], [T_TypeName]) VALUES (3, N'Cash Credit')
INSERT [dbo].[tblTransactionType] ([T_Id], [T_TypeName]) VALUES (4, N'Cash Debit')
INSERT [dbo].[tblTransactionType] ([T_Id], [T_TypeName]) VALUES (5, N'Expense')
SET IDENTITY_INSERT [dbo].[tblTransactionType] OFF
ALTER TABLE [dbo].[tblClient]  WITH CHECK ADD  CONSTRAINT [FK_tblClient_tblCity] FOREIGN KEY([CityId])
REFERENCES [dbo].[tblCity] ([City_Id])
GO
ALTER TABLE [dbo].[tblClient] CHECK CONSTRAINT [FK_tblClient_tblCity]
GO
/****** Object:  StoredProcedure [dbo].[procedure_ShowAllClients]    Script Date: 2/17/2021 7:02:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[procedure_ShowAllClients]  
AS  
BEGIN   
SELECT  c.C_Id,c.Name,c.MobileNo , c.GoldOpeningBalance ,c.CashOpeningBalance ,c.Address, ct.City_Name, c.AddedDate 
FROM tblCity as ct INNER JOIN tblClient as c ON ct.City_Id = c.CityId
 where ActivateAccount=0
  
END  

GO
USE [master]
GO
ALTER DATABASE [JewellersDataBase] SET  READ_WRITE 
GO
