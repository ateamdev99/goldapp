﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Data
{
    public class TransactionClass : ITransaction
    {

        public tblTransaction InsertTransaction(tblTransaction obj)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                db.tblTransactions.Add(obj);
                db.SaveChanges();
                return obj;
            }
        }

        public void UpdateTransaction(tblTransaction obj)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                db.tblTransactions.Attach(obj);
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public tblTransaction GetByIdTransaction(int id)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblTransactions.Find(id);
            }
        }


        public List<tblTransaction> GetTranByDate(DateTime dtFrom, DateTime dtTo)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                //List<tblTransaction> list = db.tblTransactions.ToList();
                return db.tblTransactions.Where(entry => entry.T_Date >= dtFrom).Where(entry => entry.T_Date <= dtTo).ToList();
            }
        }


        public List<tblTransaction> GetTranByDateAndOrderByClintId(DateTime dtFrom, DateTime dtTo)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblTransactions.Where(entry => entry.T_Date >= dtFrom).Where(entry => entry.T_Date <= dtTo).OrderBy(t => t.ClientId).ToList();
            }
        }


        public List<tblTransaction> GetTranByDateAndTempAccount(DateTime dtFrom, DateTime dtTo)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblClients.Join(db.tblTransactions, c => (Int32?)(c.C_Id), t => t.ClientId, (c, t) => new { c = c, t = t }).Where(temp0 => (temp0.c.TemporaryAccount == true)).Where(entry => entry.t.T_Date >= dtFrom).Where(entry => entry.t.T_Date <= dtTo).Select(temp0 => temp0.t).ToList();
            }
        }



        public List<tblTransaction> GetTranByDateAndTempAccOrderByCId(DateTime dtFrom, DateTime dtTo)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblClients.Join(db.tblTransactions, c => (Int32?)(c.C_Id), t => t.ClientId, (c, t) => new { c = c, t = t }).Where(temp0 => (temp0.c.TemporaryAccount == true)).Where(entry => entry.t.T_Date >= dtFrom).Where(entry => entry.t.T_Date <= dtTo).Select(temp0 => temp0.t).OrderBy(t => t.ClientId).ToList();
            }
        }
        public int GetTotalTransaction()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblTransactions.Count();
            }
        }
        public double GetTotalDebitGold()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return Convert.ToDouble((from t in db.tblTransactions where t.Tran_Type == 0 select t.Net_Amount).Sum());
            }
        }
        public double GetTotalDebitCash()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return Convert.ToDouble((from t in db.tblTransactions where t.Tran_Type == 2 select t.Net_Amount).Sum());
            }
        }

        public double GetTotalCreditCash()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return Convert.ToDouble((from t in db.tblTransactions where t.Tran_Type == 3 select t.Net_Amount).Sum());
            }
        }

        public double GetAvailableBalanceInCash()
        {
            throw new NotImplementedException();
        }

        public double GetTotalCreditGold()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return Convert.ToDouble((from t in db.tblTransactions where t.Tran_Type == 1 select t.Net_Amount).Sum());
            }
        }

        public double GetAvailableBalanceInGold()
        {
            throw new NotImplementedException();
        }

        public List<tblTransaction> GetTranAll()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblTransactions.SqlQuery("Select *  from tblTransaction").ToList();
            }
        }

        public void UpdateTeleConfimationTruebyIdTransaction(tblTransaction obj)
        {
             using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
             {
                 obj.TeleDate = DateTime.Now;
                 obj.IsTeleConfirmation = true;
                 db.tblTransactions.Attach(obj);
                 db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                 db.SaveChanges();
             }
        }

        public void UpdateTeleConfimationFalsebyIdTransaction(tblTransaction obj)
        {
             using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
             {
                 //"20000-01-01 00:00:00"
                 obj.TeleDate =DateTime.Now;
                 obj.IsTeleConfirmation = false;
                 db.tblTransactions.Attach(obj);
                 db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                 db.SaveChanges();
             }
        }
        public void UpdateGoldRate(tblGoldRate obj)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                db.tblGoldRates.Attach(obj);
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            }
        }

        tblGoldRate ITransaction.InsertGoldRate(tblGoldRate obj)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                db.tblGoldRates.Add(obj);
                db.SaveChanges();
                return obj;
            }
        }

        public double GetOpeningbalanceGoldAllClients()
        {
            throw new NotImplementedException();
            //return Conversion.toDouble(DBUtils.executeSqlGetSingle("SELECT SUM(GoldOpeningBalance) as openingbalanceGoldAllClients FROM tblClient  WHERE AddedDate<'" + DateTime.Now.Date + "' "));
        }

        public double GetGoldCreditCurrentBalanceGoldAllClients()
        {
            throw new NotImplementedException();
        }

        public double GetGoldDebitCurrentBalanceGoldAllClients()
        {
            throw new NotImplementedException();
        }
    }
}
