﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Core
{
     public static class ClientServicesClass
    {
         static IClient repository;
         static ClientServicesClass()
          {
              repository=new ClientClass();
          }
          public static List<tblCity> GetAllCity()
          {
              return repository.GetAllCities();
          }
          public static List<tblClient> GetAllClients()
          {
              return repository.GetAllClients();
          }
          public static tblClient GetByIdClient(int id)
          {
              return  repository.GetByIdClient(id);
          }
          public static tblClient Insert(tblClient obj)
          {
              return repository.InsertClient(obj);
          }
          public static void Update(tblClient obj)
          {
              repository.UpdateClient(obj);
          }
          public static int GetClientCount()
          {
              return repository.GetTotalClients();
          }
          //public static List<tblClient> GetAllClientsWithCityname()
          //{
          //    return repository.GetAllClientsWithCityname();
          //}
    }
}
