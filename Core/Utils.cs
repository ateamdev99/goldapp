﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
using System.ComponentModel;


namespace Core
{
    public class Conversion
    {

        #region Common converstaion
        public static DateTime toDateTime(string pDate)
        {
            DateTime retDate = Convert.ToDateTime("1/1/1990 00:00:00");
            try
            {
                if (pDate.ToString() != string.Empty)
                {
                    retDate = DateTime.Parse(pDate.ToString());
                }
            }
            catch
            {
            }
            return retDate;
        }

        public static DateTime toDateTime(object pDate)
        {
            DateTime retDate = Convert.ToDateTime("1/1/1990 00:00:00");
            try
            {
                if (pDate != null)
                {
                    if (pDate.ToString() != string.Empty)
                    {
                        retDate = DateTime.Parse(pDate.ToString());
                    }
                }
            }
            catch
            {
            }
            return retDate;
        }
        public static string toString(object obj)
        {
            try
            {
                if (obj != null)
                    return obj.ToString();
                return "";
            }
            catch
            {
                return "";
            }
        }
        public static bool toBool(string str)
        {
            try
            {
                if (!isNull(str))
                {
                    string s = str.ToString();
                    s = s.Trim();
                    s = s.ToLower();

                    if (s != String.Empty)
                    {
                        if (s == "-1" || s == "1" ||
                        s == "yes" ||
                        s == "true")
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }

        }
        public static bool toBool(object obj)
        {
            try
            {
                if (!isNull(obj))
                {
                    string s = obj.ToString();
                    s = s.Trim();
                    s = s.ToLower();

                    if (s != String.Empty)
                    {
                        if (s == "-1" || s == "1" ||
                        s == "yes" ||
                        s == "true")
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }

        }
        public static bool isDate(object date)
        {
            try
            {
                if (date != null)
                    if (date.ToString() != string.Empty)
                    {
                        DateTime dt = DateTime.Parse(date.ToString());
                        return true;
                    }
                return false;
            }
            catch
            {
                return false;
            }

        }
        public static bool isNullDate(DateTime dt)
        {
            try
            {
                if (dt.Year == 1900 && dt.Month == 1 && dt.Day == 1)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public static bool isDbNull(object obj)
        {
            try
            {
                if (obj == null)
                    return true;
                if (obj.ToString() != string.Empty && obj.ToString().Length > 0)
                    return false;
                return true;
            }
            catch
            {
                return true;
            }

        }
        public static int toInt(string s)
        {
            try
            {
                if (s != string.Empty)
                {
                    int nm = int.Parse(s);
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static int toInt(object obj)
        {
            try
            {
                if (!isNull(obj))
                {
                    int nm = int.Parse(obj.ToString());
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static decimal toDecimal(string s)
        {
            try
            {
                if (s != string.Empty)
                {
                    decimal nm = decimal.Parse(s);
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static decimal toDecimal(object pObj)
        {
            try
            {
                if (!isNull(pObj))
                {
                    decimal nm = decimal.Parse(pObj.ToString());
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }
        public static double toDouble(string s)
        {
            try
            {
                if (s != string.Empty)
                {
                    double nm = double.Parse(s);
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static double toDouble(object obj)
        {
            try
            {
                if (!isNull(obj))
                {
                    double nm = double.Parse(obj.ToString());
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static bool isNull(object obj)
        {
            try
            {
                if (obj == null)
                    return true;
                if (obj.ToString() != string.Empty && obj.ToString().Length > 0)
                    return false;
                return true;
            }
            catch
            {
                return true;
            }

        }


        #endregion

        //Start Region of Gold Formulas
        #region Common GoldFormulas
        public static double ratti = 0.1215;
        public static double mash = 0.972;
        public static double Tola = 11.664;
        public static double ConvertToRatti(double grm) 
        {
            double returnValue = 0;
            returnValue = grm / ratti;
            return returnValue;   
        }
        public static double ConvertToMash(double grm)
        {
            double returnMashaValue = 0;
            returnMashaValue = grm / mash;
            return returnMashaValue;
        }

        public static double ConvertToTola(double grm)
        {
            double returnTolaValue = 0;
            returnTolaValue = grm / Tola;
            return returnTolaValue;
        }



        public static Dictionary<string,double> ConvertToTolaMshaRatti(double grm)
        {

            double tolaValue = 0.0; 
            double mashaValue = 0.0;
            double rattiValue = 0.0;

            double result = grm / ratti;

            if (result < 8)
            {
                rattiValue = Math.Round(result, 2);
            }
            else if (result >= 8.0 && result <= 96.0)
            {

                mashaValue = Math.Floor(result / 8);
                rattiValue = Math.Round((result  - mashaValue * 8), 2);
                if (mashaValue == 12)
                {
                    tolaValue += 1;
                    mashaValue = 0;
                }
            }
            else if (result > 96.0)
            {
                double toly = Math.Floor(result / 96);
                tolaValue = toly;

                double mashyInToly = (result - tolaValue * 96.0);
                

                double rattiVal = mashyInToly / 8;

                double mashy = Math.Floor(rattiVal);

                double remainingMashy = rattiVal - mashy;

                double remainRatti = Math.Round((remainingMashy / 0.1215),2,MidpointRounding.AwayFromZero);

                mashaValue = mashy;
                rattiValue = remainRatti;

                if (mashaValue == 12)
                {
                    tolaValue += 1;
                    mashaValue = 0;
                }

                //rattiValue = Math.Round((rattiInMashy - rattiVal * 8), 2);
            }


            Dictionary<string, double> resultreturn = new Dictionary<string, double>();
            resultreturn.Add("ratti", rattiValue);
            resultreturn.Add("masha", mashaValue);
            resultreturn.Add("tola", tolaValue);
            return resultreturn;
        }

        #endregion

        #region Common Methods

        public class CommonMethods {

            public void RemoveText(object sender, EventArgs e, TextBox myTxtbx)
            {
                if (myTxtbx.Text == "Enter text here...")
                {
                    myTxtbx.Text = "";
                }
            }

            public static void AddText(object sender, EventArgs e, TextBox myTxtbx)
            {
                if (string.IsNullOrWhiteSpace(myTxtbx.Text))
                    myTxtbx.Text = "Enter text here...";
            }
        }

        #endregion

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                //if (prop.Name != "tblClient")
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    //if (i == 26) { i += 1; }
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name != "tblClient")
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
                
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.Name != "tblClient")
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                    
                table.Rows.Add(row);
            }
            return table;

        }
    }
}
