﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface ITransaction
    {
        List<tblTransaction> GetTranAll();
        tblTransaction InsertTransaction(tblTransaction obj);
        void UpdateTransaction(tblTransaction obj);
        tblGoldRate InsertGoldRate(tblGoldRate obj);
        void UpdateGoldRate(tblGoldRate obj);
        tblTransaction GetByIdTransaction(int id);
        void UpdateTeleConfimationTruebyIdTransaction(tblTransaction obj);
        void UpdateTeleConfimationFalsebyIdTransaction(tblTransaction obj);
        List<tblTransaction> GetTranByDate(DateTime dtFrom, DateTime dtTo);
        List<tblTransaction> GetTranByDateAndOrderByClintId(DateTime dtFrom, DateTime dtTo);
        List<tblTransaction> GetTranByDateAndTempAccount(DateTime dtFrom, DateTime dtTo);
        List<tblTransaction> GetTranByDateAndTempAccOrderByCId(DateTime dtFrom, DateTime dtTo);
        int GetTotalTransaction();
        double GetTotalDebitCash();
        double GetTotalCreditCash();
        double GetAvailableBalanceInCash();
        double GetTotalDebitGold();
        double GetTotalCreditGold();
        double GetAvailableBalanceInGold();
        double GetOpeningbalanceGoldAllClients();
        double GetGoldCreditCurrentBalanceGoldAllClients();
        double GetGoldDebitCurrentBalanceGoldAllClients();
    }
}
