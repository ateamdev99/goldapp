﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Data
{
    public interface IClient
    {
        List<tblCity> GetAllCities();
        List<tblClient> GetAllClients();
        tblClient GetByIdClient(int id);
        tblClient InsertClient(tblClient obj);
        void UpdateClient(tblClient obj );
        int GetTotalClients();
        //datable GetAllClientsWithCityname();
         
        
    }
}
