﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Data
{
    public class ClientClass : IClient
    {
        public int GetTotalClients()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                return db.tblClients.Count();
            }
        }
        public List<tblCity> GetAllCities()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {

                return db.tblCities.SqlQuery("Select City_Id,City_Name from tblCity").ToList();
            }
        }
        public List<tblClient> GetAllClients()
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
            
                return db.tblClients.SqlQuery("select  C_Id ,C_Name from tblClient where C_Id != 1").ToList();
                //return db.tblClients.SqlQuery("Select C_Id,C_Name from tblClient").ToList<tblClient>();
            }
        }
        public tblClient GetByIdClient(int id)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                //db.Configuration.ProxyCreationEnabled = false;
                return db.tblClients.Find(id);
            } 
        }

        public tblClient InsertClient(tblClient obj)
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                db.tblClients.Add(obj);
                db.SaveChanges();
                return obj;
            } 
        }

        public void UpdateClient(tblClient obj )
        {
            using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
            {
                db.tblClients.Attach(obj);
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            } 
        }

        //List<string> IClient.GetAllClientsWithCityname()
        //{
        //    using (JewellersDataBaseEntitiess db = new JewellersDataBaseEntitiess())
        //    {
        //        //return db.tblClients.ToList();
//                    SELECT  tblCity.City_Id,tblCity.City_Name,tblClient.C_Id,tblClient.C_Name,tblClient.C_Address,tblClient.C_District,tblClient.C_MobileNo,tblClient.C_OfficeNo,tblClient.C_OpBlncGold,tblClient.C_OpBlncCash,tblClient.C_OpeningDate,tblClient.TMR,tblClient.ActivateAccount,tblClient.TemporaryAccount,tblClient.TranactionSMS,tblClient.BalanceSMS,tblClient.BalncSMSGoldWight,tblClient.CustomSMS
//FROM tblClient
//INNER JOIN tblCity ON tblClient.C_CityId = tblCity.City_Id;
        //        //return db.from e in TblClients  join d in TblCities on e.C_CityId equals d.City_Id select new  {  C_Id = e.C_Id,  C_Name = e.C_Name,  C_Address = e.C_Address,  C_District = e.C_District,  C_MobileNo = e.C_MobileNo,  C_OfficeNo = e.C_OfficeNo,  C_OpBlncGold = e.C_OpBlncGold,  C_OpBlncCash = e.C_OpBlncCash,  C_OpeningDate= e.C_OpeningDate,TMR= e.TMR,ActivateAccount= e.ActivateAccount,TemporaryAccount= e.TemporaryAccount,TranactionSMS= e.TranactionSMS,BalanceSMS= e.BalanceSMS,BalncSMSGoldWight= e.BalncSMSGoldWight,CustomSMS= e.CustomSMS ,City_Name = d.City_Name  ,City_Id = d.City_Id   }
        //       // return db.tblClients.SqlQuery("SELECT [t0].[C_Id], [t0].[C_Name], [t0].[C_Address], [t0].[C_District], [t0].[C_MobileNo], [t0].[C_OfficeNo], [t0].[C_OpBlncGold], [t0].[C_OpBlncCash], [t0].[C_OpeningDate], [t0].[TMR], [t0].[ActivateAccount], [t0].[TemporaryAccount], [t0].[TranactionSMS], [t0].[BalanceSMS], [t0].[BalncSMSGoldWight], [t0].[CustomSMS], [t1].[City_Name], [t1].[City_Id] FROM [tblClient] AS [t0] INNER JOIN [tblCity] AS [t1] ON [t0].[C_CityId] = ([t1].[City_Id])").ToList();
        //    }
        //}


    }
}
