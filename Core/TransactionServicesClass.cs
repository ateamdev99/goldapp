﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Core
{
    public static class TransactionServicesClass
    {
        static ITransaction repository;
        static TransactionServicesClass()
          {
              repository=new TransactionClass();
          }
          public static tblTransaction GetByIdTransaction(int id)
          {
              return  repository.GetByIdTransaction(id);
          }
          public static List<tblTransaction> GetTranByDate( DateTime dtFrom, DateTime dtTo)
          {
              return repository.GetTranByDate(dtFrom, dtTo);
          }
          public static List<tblTransaction> GetTranByDateAndOrderByCID(DateTime dtFrom, DateTime dtTo)
          {
              return repository.GetTranByDateAndOrderByClintId(dtFrom, dtTo);
          }
          public static List<tblTransaction> GetTranByDateAndTempAccount(DateTime dtFrom, DateTime dtTo)
          {
              return repository.GetTranByDateAndTempAccount(dtFrom, dtTo);
          }
          public static List<tblTransaction> GetTranByDateAndTmpAccOrByCId(DateTime dtFrom, DateTime dtTo)
          {
              return repository.GetTranByDateAndTempAccOrderByCId(dtFrom, dtTo);
          }
          public static tblTransaction Insert(tblTransaction obj)
          {
              return repository.InsertTransaction(obj);
          }
          public static void Update(tblTransaction obj)
          {
              repository.UpdateTransaction(obj);
          }
         
          public static int GetTransactiponsCount()
          {
              return repository.GetTotalTransaction();
          }
          public static double GetGoldDebitTotalSum()
          {
              return repository.GetTotalDebitGold();
          }
          public static double GetGoldCreditTotalSum()
          {
              return repository.GetTotalCreditGold();
          }
          public static double GetCashCreditTotalSum()
          {
              return repository.GetTotalCreditCash();
          }
          public static double GetCashDebitTotalSum()
          {
              return repository.GetTotalDebitCash();
          }
          public static List<tblTransaction> GetTranAll()
          {
              return repository.GetTranAll();
          }
          public static void UpdateTeleConfimationTruebyIdTransaction(tblTransaction obj)
          {
              repository.UpdateTeleConfimationTruebyIdTransaction(obj);
          }
          public static void UpdateTeleConfimationFalsebyIdTransaction(tblTransaction obj)
          {
              repository.UpdateTeleConfimationFalsebyIdTransaction(obj);
          }
        public static tblGoldRate InsertGoldRate(tblGoldRate obj)
        {
            return repository.InsertGoldRate(obj);
        }
        public static void UpdateGoldRate(tblGoldRate obj)
        {
            repository.UpdateGoldRate(obj);
        }

    }
}
